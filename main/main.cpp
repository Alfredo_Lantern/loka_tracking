
/**

 * @file main.cpp
 * @author Andres Arias
 * @date 17/9/2019
 * @brief Program entry point.
 */

#include "Tracker.h"

#define MAIN_TAG "Main"

extern "C" {
    void app_main(void);
}

extern "C" void app_main(void)
{
    ESP_LOGI(MAIN_TAG, "Main program started");
    ESP_ERROR_CHECK(nvs_flash_init());
    drivers::interruptions::initIsrHandler();

    programs::Tracker * pt = new programs::Tracker();

    if(esp_sleep_get_wakeup_cause() == ESP_SLEEP_WAKEUP_UNDEFINED){
        ESP_LOGI(MAIN_TAG, "ENTRE IF MAIN");
        drivers::ESPULP * ptrULP = new drivers::ESPULP();				// Initializes control GPIOs
        RTC_init(0);													// Initializes the RTC
        drivers::GPIO_STUB::pinMode(ACC_INT, INPUT, 1);

    }
    pt->run();  
}

