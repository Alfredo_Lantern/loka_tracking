/**
 * @file BoardIO.h
 * @author Rafael Pires
 * @author Jairo Ortega Calderón
 * @date 10/19/2019
 * @brief IO definition for boards
 */

#ifndef BOARD_IO_H
#define BOARD_IO_H

#include "sdkconfig.h"


#include "driver/gpio.h"
	


#ifdef CONFIG_ENABLE_NEURON
#define DCDC_ON	 						GPIO_NUM_14
#define SPI_MOSI						GPIO_NUM_15
#define SPI_MISO			 			GPIO_NUM_13
#define SPI_CLK							GPIO_NUM_2
#define ACC_CS							GPIO_NUM_27

#define UART_C_TX						GPIO_NUM_1
#define UART_C_RX						GPIO_NUM_3

#define UART_B_TX						GPIO_NUM_1
#define UART_B_RX						GPIO_NUM_3

#define CALIBRATION						GPIO_NUM_39
#define BLINK_GPIO  					GPIO_NUM_25

#else
#define DCDC_ON 						GPIO_NUM_27
#define SPI_MOSI						GPIO_NUM_19
#define SPI_MISO			 			GPIO_NUM_22
#define SPI_CLK							GPIO_NUM_26
#define ACC_CS							GPIO_NUM_5

#define UART_C_TX						WISOL_TX
#define UART_C_RX						WISOL_RX

#define UART_B_TX						(12)         //attention pullup this pin on the startup to force the flash to run a 1.8V
#define UART_B_RX						(39)

#define CALIBRATION						GPIO_NUM_13

#define BLINK_GPIO  					GPIO_NUM_13

#endif

// COMUNES
#define ACC_INT  						GPIO_NUM_34
#define UART_A_TX  		 				GPIO_NUM_1
#define UART_A_RX		     			GPIO_NUM_3


// NEURON
#define S2LP_SDN						GPIO_NUM_0
#define S2LP_CS							GPIO_NUM_12
#define S2LP_GPIO3						GPIO_NUM_35
#define FE_SDN							GPIO_NUM_4


#define VBAT 							GPIO_NUM_39

// PRIMIS

#define WISOL_RESET  					GPIO_NUM_21  // reset the state machine
#define WISOL_WAKEUP 					GPIO_NUM_17  //toggle to wakeup
#define WISOL_TX 						(18)
#define WISOL_RX 						(23)

#define FLASH6 							GPIO_NUM_6
#define FLASH7							GPIO_NUM_7
#define FLASH8 							GPIO_NUM_8
#define FLASH9 							GPIO_NUM_9
#define FLASH10 						GPIO_NUM_10
#define FLASH11 						GPIO_NUM_11

#define DCDC_LVL 						GPIO_NUM_16

#define BUTTON  						GPIO_NUM_35
#define LED 							GPIO_NUM_0

#define GPIO2 							GPIO_NUM_2
#define GPIO4 							GPIO_NUM_4
#define GPIO14							GPIO_NUM_14
#define GPIO15 							GPIO_NUM_15

enum pin_mode_t {
	INPUT,
	OUTPUT,
	INPUT_PULLDOWN,
	INPUT_PULLUP
};

enum {
	LOW,
	HIGH
};

#endif


