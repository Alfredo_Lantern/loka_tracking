// Variable definitions for ESP32ULP
// This file is generated automatically by esp32ulp_mapgen.py utility

#pragma once

extern uint32_t ulp_battery_locked;
extern uint32_t ulp_debug0;
extern uint32_t ulp_debug1;
extern uint32_t ulp_debug2;
extern uint32_t ulp_debug3;
extern uint32_t ulp_entry;
extern uint32_t ulp_target_time0;
extern uint32_t ulp_target_time1;
extern uint32_t ulp_target_time2;
extern uint32_t ulp_target_time3;
extern uint32_t ulp_wakeup_enable;
