#!/bin/bash

echo "Updating devie info and secrets partition"

$IDF_PATH/mkspiffs/mkspiffs -c info/storage -b 4096 -p 256 -s 0x20000 build/storage.bin

python $IDF_PATH/components/esptool_py/esptool/esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 921600 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 40m --flash_size detect 0x230000 build/storage.bin

