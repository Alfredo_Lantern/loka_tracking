//**********************************************************************************************************************************
// Filename: BOARD_LokaV2.cpp
// Date: 18.10.2017
// Author: Rafael Pires
// Company: LOKA SYSTEMS
// Version: 1.0
// Description: Code to operate the LOKA V2 board firmware
//**********************************************************************************************************************************
#ifndef BOARD_LOKAV2_H_
#define BOARD_LOKAV2_H_


//**********************************************************************************************************************************
//                                                      Includes Section
//**********************************************************************************************************************************
#include "BoardIO.h"

#include "BOARD_LokaV2_ULP.h"

#include "esp32/ulp.h"
#include "soc/sens_reg.h"
#include "string.h"
#include "esp_task_wdt.h"

#include "soc/rtc.h"
#include "soc/rtc_cntl_reg.h"
#include "soc/timer_group_reg.h"

#include "soc/apb_ctrl_reg.h"

extern "C" {
	#include "rom/uart.h"
	#include "driver/gpio.h"
	#include "driver/rtc_io.h"
	#include "nvs_flash.h"
	#include "nvs.h"
	#include "stdlib.h"
	#include "esp_system.h"
	#include "rom/rtc.h"
	#include "esp_sleep.h"
}

/*
#define stubInit()		REG_SET_FIELD(RTC_CNTL_REG, RTC_CNTL_DBIAS_WAK, 4); \
						REG_SET_FIELD(RTC_CNTL_REG, RTC_CNTL_DBIAS_SLP, 4); \
						REG_WRITE(TIMG_WDTFEED_REG(0), 1);
*/
						// Flushes UART pending data, sleeps, dummy operations until sleeping
#define stubSleep()		while(REG_GET_FIELD(UART_STATUS_REG(0), UART_ST_UTX_OUT)); \
						CLEAR_PERI_REG_MASK(RTC_CNTL_STATE0_REG, RTC_CNTL_SLEEP_EN); \
						SET_PERI_REG_MASK(RTC_CNTL_STATE0_REG, RTC_CNTL_SLEEP_EN); \
						while(true);


//**********************************************************************************************************************************
//                                                      Defined Section
//**********************************************************************************************************************************
#define FLASH_MAX			8
#define FLASH_SIZE_MAX 		32
#define FLASH_CONFIGS_PAGE	"CONFIGS"
#define FLASH_CALIBS_PAGE	"CALIBS"
#define FLASH_HARDWARE_PAGE	"HARDWARE"
#define FLASH_FLAGS_PAGE	"FLAGS"

#define	LED_ON				2
#define LED_HALF_BRIGHT		1
#define	LED_OFF				0


//**********************************************************************************************************************************
//                                                      Templates Section
//**********************************************************************************************************************************
class Board{
public:
	static void init();
	static void initULP();
	static void initDcDc();

	static void sleepPrepare();

	static void getMAC();
	static void getID();
	static void getPAC();

	static void reset();
	static void resetCause(char* message);

	static void setSystemFrequency(unsigned long clk);
	static unsigned long getCurrentFrequency();

	static void setFlash(const char* page, const char* name, const char* value);
	static void getFlash(const char* page, const char* name, char* value);
	static void clearFlash(const char* page, const char* name);
	static void eraseFlash(const char* page);

	static void getConfig(const char* name, char* value);
	static void getConfig(const char* name, char* value, int index);
	static void setConfig(const char* name, const char* value);
	static void initConfig();
	static void clearConfig(const char* name);
	static void eraseConfig();

	static void pinMode(gpio_num_t gpio, pin_mode_t mode);
	static void digitalWrite(gpio_num_t gpio, uint8_t value);
	static unsigned char digitalRead(gpio_num_t gpio);
	static unsigned int analogRead(gpio_num_t gpio);

	static char* uptime(char *c);
	static char* power(char *c);
	static char* temperature(char *c);
	static char* updateFirmware(char *c);

	static int getTemperature();
	static double getInputVoltageLoad();
	static double getInputVoltageOpen();

	static void gotoLowPowerMode(unsigned int seconds, void (*wakeStub)());
	static void setDcDcLevelOn();
	static void setDcDcLevelOff();
	static void endLED();
	static void startLED(unsigned char mode);
	static void setLedMode(unsigned char mode);
	static void runLED(void * pvParameter);
	static void blinkLED(unsigned int blinks, unsigned int time_on1, unsigned int time_off1, unsigned int time_on2, unsigned int time_off2);

	static void batteryLock();
private:
	static char * parseResetCause(RESET_REASON resetCode);
    static nvs_handle nvsHandle;
    static nvs_handle nvsHandleCalib;
	static unsigned long currentFrequency;
};


#endif
