//**********************************************************************************************************************************
// Filename: BOARD_WakeUpStubUtils.c
// Date: 18.10.2017
// Author: Rafael Pires
// Company: LOKA SYSTEMS
// Version: 1.0
// Description: Code to operate the ESP32 on stub mode
// Notes: "rtc_wake_stub" must be kept on the filename
//**********************************************************************************************************************************

//**********************************************************************************************************************************
//                                                      Important INFO
//**********************************************************************************************************************************
//	All the stub code should run in less than to 2sec
//	On the stub you can't use floating points (like doubles or floats)

//**********************************************************************************************************************************
//                                                      Includes Section
//**********************************************************************************************************************************
#include "soc/rtc_cntl_reg.h"
#include "rom/rtc.h"
#include "rom/gpio.h"
#include "soc/uart_reg.h"
#include "rom/ets_sys.h"
#include "esp_attr.h"
#include "esp_sleep.h"
#include "soc/rtc_cntl_reg.h"
#include "soc/timer_group_reg.h"
#include "driver/rtc_io.h"

#include "LOKA_LIS3DX_Constans.h"
#include "BoardIO.h"

//**********************************************************************************************************************************
//                                                       Define Section
//**********************************************************************************************************************************
#define SPI_READ  				0
#define SPI_WRITE 				1


//**********************************************************************************************************************************
//                                                      Global Variables and statics
//**********************************************************************************************************************************
int temperatureOffset;
int temperatureValue;
bool movementValue;

uint64_t keepAliveTime;
bool keepAliveEnable = false;
uint64_t currentTime;
static const char RTC_RODATA_ATTR movementString[] = "STUB: Movement value: %d\r\n";
static const char RTC_RODATA_ATTR temperatureString[] = "STUB: Temperature value: %d\r\n";
extern bool debug;

//**********************************************************************************************************************************
//                                                       Code Section
//**********************************************************************************************************************************


//**********************************************************************************************************************************
// Header: spiPinsDisable
// Function: SPI driver init function
//**********************************************************************************************************************************
void RTC_IRAM_ATTR spiPinsEnable(){
	REG_CLR_BIT(RTC_IO_PAD_DAC2_REG, RTC_IO_PDAC2_HOLD_M);
	REG_CLR_BIT(RTC_IO_PAD_DAC2_REG, RTC_IO_PDAC2_MUX_SEL);
}


//**********************************************************************************************************************************
// Header: spiPinsDisable
// Function: SPI driver pins disable function
//**********************************************************************************************************************************
void RTC_IRAM_ATTR spiPinsDisable(){
	REG_CLR_BIT(RTC_IO_PAD_DAC2_REG, RTC_IO_PDAC2_HOLD_M);
	REG_SET_BIT(RTC_IO_PAD_DAC2_REG, RTC_IO_PDAC2_MUX_SEL);
	REG_SET_BIT(RTC_IO_PAD_DAC2_REG, RTC_IO_PDAC2_SLP_OE);
	REG_SET_BIT(RTC_GPIO_ENABLE_W1TS_REG, BIT(30));
	REG_SET_BIT(RTC_GPIO_OUT_W1TS_REG, BIT(30));
	REG_SET_BIT(RTC_IO_PAD_DAC2_REG, RTC_IO_PDAC2_HOLD_M);
}


//**********************************************************************************************************************************
// Header: spiRequest
// Function: SPI driver addressing function on stub mode, readWrite = 0 to read, readWrite = 1 to write,
//**********************************************************************************************************************************
void RTC_IRAM_ATTR spiRequest(uint8_t address, uint8_t readWrite){
	uint8_t i, bitMask;

	address &= 0x3F;
	address |= (readWrite == 0) ? 0xC0 : 0x00; 							// Include the read/write bit on address

	GPIO_OUTPUT_SET(SPI_CLK, 1);  											// Sets initial bus state
	GPIO_OUTPUT_SET(ACC_CS, 1);
	GPIO_OUTPUT_SET(SPI_MOSI, 0);
	GPIO_OUTPUT_SET(ACC_CS, 0);

	for(i = 0; i < 8; i++){												// Shift address out
		GPIO_OUTPUT_SET(SPI_CLK, 0);
		bitMask = (address & 0x80) == 0x80;								// MSbit first
		GPIO_OUTPUT_SET(SPI_MOSI, bitMask);
		GPIO_OUTPUT_SET(SPI_CLK, 1);
		address = address << 1;											// Shift data
	}
}


//**********************************************************************************************************************************
// Header: spiRead8
// Function: SPI driver 8 bit read function
//**********************************************************************************************************************************
uint8_t RTC_IRAM_ATTR spiRead8(uint8_t addr){
	uint8_t result = 0;
	uint8_t i;

	spiRequest(addr, SPI_READ);
	GPIO_OUTPUT_SET(SPI_MOSI, 0);											// Notice that the CS was left asserted

	for(i = 0; i < 8; i++){												// Shift data in
		GPIO_OUTPUT_SET(SPI_CLK, 0);
		result |= (GPIO_INPUT_GET(SPI_MISO)) << (7 - i);					// MSbit first
		GPIO_OUTPUT_SET(SPI_CLK, 1);
	}
	GPIO_OUTPUT_SET(ACC_CS, 1);												// Finally de-assert the chip select
	return result;
}


//**********************************************************************************************************************************
// Header: spiRead16
// Function: SPI driver 16 bit read function (not needed for the LIS3DE)
//**********************************************************************************************************************************
uint16_t RTC_IRAM_ATTR spiRead16(uint8_t addr){
	uint16_t result = 0;
	uint8_t i;

	spiRequest(addr, SPI_READ);
	GPIO_OUTPUT_SET(SPI_MOSI, 0);											// Notice that the CS was left asserted

	for(i = 0; i < 8; i++){												// Shift MSB data in
		GPIO_OUTPUT_SET(SPI_CLK, 0);
		result |= (GPIO_INPUT_GET(SPI_MISO) << (15 - i));					// MSbit first
		GPIO_OUTPUT_SET(SPI_CLK, 1);
	}

	for(i = 0; i < 8; i++){												// Shift LSB data in
		GPIO_OUTPUT_SET(SPI_CLK, 0);
		result |= (GPIO_INPUT_GET(SPI_MISO)) << (7 - i);					// MSbit first
		GPIO_OUTPUT_SET(SPI_CLK, 1);
	}
	GPIO_OUTPUT_SET(ACC_CS, 1);												// Finally de-assert the chip select
	return result;
}


//**********************************************************************************************************************************
// Header: spiWrite8
// Function: SPI driver 8 bit write function (not needed for the LIS3DE)
//**********************************************************************************************************************************
void RTC_IRAM_ATTR spiWrite8(uint8_t addr, uint8_t data){
	uint8_t bitMask;

	spiRequest(addr, SPI_WRITE);

	for(uint8_t i=0; i<8; i++){											// Notice that the CS was left asserted
		GPIO_OUTPUT_SET(SPI_CLK, 0);
		bitMask = ((data & 0x80) == 0x80);								// MSBit first
		GPIO_OUTPUT_SET(SPI_MOSI, bitMask);  								// Shift MOSI accordingly
		GPIO_OUTPUT_SET(SPI_CLK, 1);
		data = data << 1;												// Move to the next bit
	}
	GPIO_OUTPUT_SET(ACC_CS, 1);												// Finally de-assert the chip select
}


//**********************************************************************************************************************************
// Header: readMovement
// Function: LIS3DE driver read movement function
//**********************************************************************************************************************************
void RTC_IRAM_ATTR readMovement(void){
																		// Check for movement
	spiPinsEnable();
	movementValue = (0x40 & spiRead8(LIS3DX_INT1_SRC)) == 0x40;
	spiPinsDisable();

	// ets_printf(movementString, movementValue);
}


//**********************************************************************************************************************************
// Header: readTemperature
// Function: LIS3DE driver read temperature function
//**********************************************************************************************************************************
void RTC_IRAM_ATTR readTemperature(void){
	temperatureValue = 0;
	uint8_t r, ctrl4;

	spiPinsEnable();
	ctrl4 = spiRead8(LIS3DX_CTRL_REG4);
	spiWrite8(LIS3DX_CTRL_REG4, (ctrl4 | 0x80)); 						// BDU enabled

	if((spiRead8(LIS3DX_STATUS_REG_AUX) & 0x04) != 0x04){					// Checks if a sample is available
		ets_delay_us(110000);											// Wait 10 Hz sampling + overhead
	}
	if((spiRead8(LIS3DX_STATUS_REG_AUX) & 0x04) == 0x04){
		spiRead8(LIS3DX_OUT_ADC3_L);
		r = (char) spiRead8(LIS3DX_OUT_ADC3_H);
		spiWrite8(LIS3DX_CTRL_REG4, (ctrl4 & 0x7F));					// BDU disabled
		temperatureValue = ((int8_t) r) + temperatureOffset;
	}else{
		temperatureValue = -100;
	}
	spiPinsDisable();

	// ets_printf(temperatureString, temperatureValue);
}
