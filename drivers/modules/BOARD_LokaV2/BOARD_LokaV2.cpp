//**********************************************************************************************************************************
// Filename: BOARD_LokaV2.cpp
// Date: 18.10.2017
// Author: Rafael Pires
// Company: LOKA SYSTEMS
// Version: 1.0
// Description: Code to operate the LOKA V2 board firmware
//**********************************************************************************************************************************


//**********************************************************************************************************************************
//                                                      Includes Section
//**********************************************************************************************************************************
#include "BOARD_LokaV2.h"


//**********************************************************************************************************************************
//                                                      External Functions
//**********************************************************************************************************************************
TaskHandle_t threadLEDHandler;
unsigned char ledBrightness;
unsigned char ledActive;
nvs_handle Board::nvsHandle;
int configsNumber;
char configsList[FLASH_MAX*FLASH_SIZE_MAX + FLASH_MAX + 1];
char configsNames[FLASH_MAX][FLASH_SIZE_MAX + 1];
char configsValues[FLASH_MAX][FLASH_SIZE_MAX + 1];
char configValue[FLASH_SIZE_MAX + 1];

unsigned long int boardID;
char boardIDString[9];
unsigned long int boardPAC;
char boardPACString[17];
unsigned long int boardMAC;
char boardMACString[18];


//**********************************************************************************************************************************
//                                                        Code Section
//**********************************************************************************************************************************


//**********************************************************************************************************************************
// Header: Board::init
// Function: Inits the LOKA board
//**********************************************************************************************************************************
void Board::init(){
	initDcDc();															// Initializes the DCDC pins, (re-)enables the DCDC

#ifdef DCDC_LVL_ON
	digitalWrite(DCDC_LVL, 1);											// Initializes the DCDC level pins
	pinMode(DCDC_LVL, OUTPUT);
#else
	digitalWrite(DCDC_LVL, 0);											// Initializes the DCDC level pins
	pinMode(DCDC_LVL, OUTPUT);
#endif

	digitalWrite(LED, 1);												// Turn the LED off
	pinMode(LED, OUTPUT);

	pinMode(BUTTON, INPUT);												// Initialiazes the button INPUT


	pinMode(GPIO2, INPUT);												// Unused pins
	pinMode(GPIO4, INPUT);
//	pinMode(GPIO_NUM_12, INPUT);
//	pinMode(CALIBRATION, INPUT); 										// Used by calibration mode, need to investigate un-commented
#ifndef GPIO14_OUTPUT
	pinMode(GPIO14, INPUT);
#endif
	pinMode(GPIO_NUM_15, INPUT);
	pinMode(GPIO_NUM_25, INPUT);
	pinMode(GPIO_NUM_32, INPUT);
	pinMode(GPIO_NUM_33, INPUT);
	pinMode(GPIO_NUM_36, INPUT);
	pinMode(GPIO_NUM_37, INPUT);
	pinMode(GPIO_NUM_38, INPUT);
	pinMode(GPIO_NUM_39, INPUT);
}


//**********************************************************************************************************************************
// Header: initDcDc
// Function: Inits the DCDC enable control pin
//**********************************************************************************************************************************
void Board::initDcDc(){
	REG_CLR_BIT(RTC_IO_TOUCH_PAD7_REG, RTC_IO_TOUCH_PAD7_HOLD);
	REG_SET_BIT(RTC_IO_TOUCH_PAD7_REG, RTC_IO_TOUCH_PAD7_MUX_SEL);
	REG_SET_BIT(RTC_IO_TOUCH_PAD7_REG, RTC_IO_TOUCH_PAD7_SLP_OE);
	REG_SET_BIT(RTC_IO_TOUCH_PAD7_REG, RTC_IO_TOUCH_PAD7_TO_GPIO);
	REG_SET_BIT(RTC_GPIO_ENABLE_W1TS_REG, BIT(31));
	REG_SET_BIT(RTC_GPIO_OUT_W1TS_REG, BIT(31));
	REG_SET_BIT(RTC_IO_TOUCH_PAD7_REG, RTC_IO_TOUCH_PAD7_HOLD);
}


//**********************************************************************************************************************************
// Header: Board::sleepPrepare
// Function: Prepares the board for sleep mode
//**********************************************************************************************************************************
void Board::sleepPrepare(){

	digitalWrite(SPI_CLK, 1);
	pinMode(SPI_CLK, OUTPUT);
}


//**********************************************************************************************************************************
// Header: Board::reset
// Function: Performs a software reset
//**********************************************************************************************************************************
void Board::reset(){
    esp_restart();
}

//**********************************************************************************************************************************
// Header: Board::getMAC
// Function: Retrieves the board MAC address
//**********************************************************************************************************************************
void Board::getMAC(){
	char* mac;

	esp_efuse_mac_get_default((unsigned char*) boardMACString);
	mac = boardMACString;
	sprintf(boardMACString, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
	boardMAC = 0;
}

//**********************************************************************************************************************************
// Header: Board::getIDPAC
// Function: Retrieves the sigfox id and pac
//**********************************************************************************************************************************
void Board::getID(){
	char* id;
}


//**********************************************************************************************************************************
// Header: Board::getIDPAC
// Function: Retrieves the sigfox id and pac
//**********************************************************************************************************************************
void Board::getPAC(){
	char* pac;
}


//**********************************************************************************************************************************
// Header: Board::parseResetCause
// Function: Retrieves the last reset cause
//**********************************************************************************************************************************
char * Board::parseResetCause(RESET_REASON resetCode){
	switch(resetCode){
		case NO_MEAN:
			return (char*) "(NoMean)";
		case POWERON_RESET:
			return (char*) "(PowerOn)";
		case SW_RESET:
			return (char*) "(SwRst)";
		case OWDT_RESET:
			return (char*) "(WdtRst)";
		case DEEPSLEEP_RESET:
			return (char*) "(DeepSleepRst)";
		case SDIO_RESET:
			return (char*) "(SdioRst)";
		case TG0WDT_SYS_RESET:
			return (char*) "(Tg0WdtRst)";
		case TG1WDT_SYS_RESET:
			return (char*) "(Tg1WdtRst)";
		case RTCWDT_SYS_RESET:
			return (char*) "(RtcWdtRst)";
		case INTRUSION_RESET:
			return (char*) "(IntrusionRst)";
		case TGWDT_CPU_RESET:
			return (char*) "(TgWdtRst)";
		case SW_CPU_RESET:
			return (char*) "(SwRst)";
		case EXT_CPU_RESET:
			return (char*) "(ExtCpuRst)";
		case RTCWDT_BROWN_OUT_RESET:
			return (char*) "(BrownOut)";
		case RTCWDT_CPU_RESET:
			return (char*) "(RtcWdtRst)";
		case  RTCWDT_RTC_RESET:
			return (char*) "(RtcWdtRst)";
	}
	return (char*) "(unknown)";
}


//**********************************************************************************************************************************
// Header: Board::resetCause
// Function: Prints the reset cause
//**********************************************************************************************************************************
void Board::resetCause(char* message){
	RESET_REASON coreApp = rtc_get_reset_reason(0);
	RESET_REASON corePro = rtc_get_reset_reason(1);

	sprintf(message,"coreApp: %s; corePro: %s\r\n", parseResetCause(coreApp), parseResetCause(corePro));
}


//**********************************************************************************************************************************
// Header: Board::pinMode
// Function: Configures a GPIO pin
//**********************************************************************************************************************************
void Board::pinMode(gpio_num_t gpio, pin_mode_t mode){
	if(rtc_gpio_is_valid_gpio(gpio) == false){							// Check if the GPIO is on the RTC domain or not

		gpio_config_t io_conf;
		io_conf.intr_type = GPIO_INTR_DISABLE;							// Disable any associated interrupt

		io_conf.pin_bit_mask = ((uint64_t) 1) << (gpio);				// Bit mask for pins to set

																		// Pin Mode selection
		io_conf.mode =(mode == INPUT || mode == INPUT_PULLUP || mode == INPUT_PULLDOWN) ? GPIO_MODE_INPUT : GPIO_MODE_OUTPUT ;

																		// Pull-down mode selection
		io_conf.pull_down_en = (mode==INPUT_PULLDOWN) ? GPIO_PULLDOWN_ENABLE : GPIO_PULLDOWN_DISABLE;

																		// Pull-up mode selection
		io_conf.pull_up_en = (mode==INPUT_PULLUP) ? GPIO_PULLUP_ENABLE : GPIO_PULLUP_DISABLE;

		ESP_ERROR_CHECK(gpio_config(&io_conf));							// Push the configuration

	}else{																// For pins on the RTC domain


		ESP_ERROR_CHECK(rtc_gpio_init(gpio));							// Sets the pin configurations
		rtc_gpio_mode_t gpioMode = (mode == INPUT || mode == INPUT_PULLUP || mode == INPUT_PULLDOWN) ? \
				RTC_GPIO_MODE_INPUT_ONLY : RTC_GPIO_MODE_OUTPUT_ONLY;
		ESP_ERROR_CHECK(rtc_gpio_set_direction(gpio, gpioMode));

		if(mode == INPUT_PULLUP){										// Pull up and pull down configuration
			rtc_gpio_pullup_en(gpio);
			rtc_gpio_pulldown_dis(gpio);
		}else if(mode == INPUT_PULLUP){
			rtc_gpio_pulldown_en(gpio);
			rtc_gpio_pullup_dis(gpio);
		}else{
			rtc_gpio_pullup_dis(gpio);
			rtc_gpio_pulldown_dis(gpio);
		}
	}
}


//**********************************************************************************************************************************
// Header: Board::digitalWrite
// Function: Writes a GPIO pin
//**********************************************************************************************************************************
void Board::digitalWrite(gpio_num_t gpio, uint8_t value){

	if(rtc_gpio_is_valid_gpio(gpio) == false){ 							// Regular pin
		ESP_ERROR_CHECK(gpio_set_level(gpio, value));
	}else{																// RTC domain pin
		rtc_gpio_hold_dis(gpio);
		ESP_ERROR_CHECK(rtc_gpio_set_level(gpio, value));
		rtc_gpio_hold_en(gpio);
	}
}


//**********************************************************************************************************************************
// Header: Board::digitalRead
// Function: Reads a GPIO pin
//**********************************************************************************************************************************
unsigned char Board::digitalRead(gpio_num_t gpio){
	if(rtc_gpio_is_valid_gpio(gpio) == false){ 							// Regular pin
		return gpio_get_level(gpio);
	}else{																// RTC domain pin
		return rtc_gpio_get_level(gpio);
	}
}


//**********************************************************************************************************************************
// Header: Board::getTemperature
// Function: Gets the temperature from the internal sensor
//**********************************************************************************************************************************
int Board::getTemperature(){

	SET_PERI_REG_BITS(SENS_SAR_MEAS_WAIT2_REG, SENS_FORCE_XPD_SAR, 3, SENS_FORCE_XPD_SAR_S);
	SET_PERI_REG_BITS(SENS_SAR_TSENS_CTRL_REG, SENS_TSENS_CLK_DIV, 10, SENS_TSENS_CLK_DIV_S);
	CLEAR_PERI_REG_MASK(SENS_SAR_TSENS_CTRL_REG, SENS_TSENS_POWER_UP);
	CLEAR_PERI_REG_MASK(SENS_SAR_TSENS_CTRL_REG, SENS_TSENS_DUMP_OUT);
	SET_PERI_REG_MASK(SENS_SAR_TSENS_CTRL_REG, SENS_TSENS_POWER_UP_FORCE);
	SET_PERI_REG_MASK(SENS_SAR_TSENS_CTRL_REG, SENS_TSENS_POWER_UP);

	ets_delay_us(100);

	SET_PERI_REG_MASK(SENS_SAR_TSENS_CTRL_REG, SENS_TSENS_DUMP_OUT);

    ets_delay_us(5);

	return GET_PERI_REG_BITS2(SENS_SAR_SLAVE_ADDR3_REG, SENS_TSENS_OUT, SENS_TSENS_OUT_S);
}


//**********************************************************************************************************************************
// Header: Board::setFlash
// Function: Sets a flash word
//**********************************************************************************************************************************
void Board::setFlash(const char* page, const char* name, const char* value){
	char list[FLASH_MAX*FLASH_SIZE_MAX + FLASH_MAX + 1] = {};			// Maximum size for the concatenated configurations names
	char* ptr;
	bool found = false;

	getFlash(page, "list", list);										// Retrieves the list of the existing configurations

	ptr = strtok(list,"/");												// Loop all the configurations
	while(ptr != NULL){
		if(strcmp(ptr, name) == 0){										// Look for the desired configuration
			found = true;
			break;
		}
		ptr = strtok(NULL, "/");
	}

	if(found){															// If the new configuration already exists
																		// Delete the previous definition
		nvs_open(page, NVS_READWRITE, &nvsHandle);
		nvs_erase_key(nvsHandle, name);
		nvs_commit(nvsHandle);
		nvs_close(nvsHandle);
	}else{																// If the new configuration doens't exists
		getFlash(page, "list", list);									// Retrieves the list of the existing configurations
		sprintf(list, "%s/%s", list, name);								// Add it to the configurations list
																		// Save the updated configurations list
		nvs_open(page, NVS_READWRITE, &nvsHandle);
		nvs_set_str (nvsHandle, "list", list);
		nvs_commit(nvsHandle);
		nvs_close(nvsHandle);
	}
																		// Finally store the new configuration value
	nvs_open(page, NVS_READWRITE, &nvsHandle);
	nvs_set_str (nvsHandle, name, value);
	nvs_commit(nvsHandle);
	nvs_close(nvsHandle);

	//DEBUG((char*)"BOARD_LokaV2:\t\t stored flash word");
}


//**********************************************************************************************************************************
// Header: Board::getFlash
// Function: Retrieves a flash word
//**********************************************************************************************************************************
void Board::getFlash(const char* page, const char* name, char* value){
	size_t requiredSize;

	if(strcmp(name, "list") == 0){
		memset(value, '\0', FLASH_MAX*FLASH_SIZE_MAX + FLASH_MAX + 1);
	}else{
		memset(value, '\0', FLASH_SIZE_MAX + 1);
	}
																		// Check if the configuration already exists
	nvs_open(page, NVS_READONLY, &nvsHandle);
	nvs_get_str(nvsHandle, name, NULL, &requiredSize);

	if (requiredSize == 0){												// Configuration not found
		//DEBUG((char*)"BOARD_LokaV2:\t\t flash word not found: %s", name);
	}else{
		nvs_get_str(nvsHandle, name, value, &requiredSize);				// Read the configuration
	}
	nvs_close(nvsHandle);
}


//**********************************************************************************************************************************
// Header: Board::eraseFlash
// Function: Deletes all words saved on a given flash page
//**********************************************************************************************************************************
void Board::eraseFlash(const char* page){
	nvs_open(page, NVS_READWRITE, &nvsHandle);
	nvs_erase_all(nvsHandle);
	nvs_commit(nvsHandle);
	nvs_close(nvsHandle);
}


//**********************************************************************************************************************************
// Header: Board::clearFlash
// Function: Deletes a certain word on the flash
//**********************************************************************************************************************************
void Board::clearFlash(const char* page, const char* name){
	char list[FLASH_SIZE_MAX*FLASH_MAX + 1] = {};
	char listAux[FLASH_SIZE_MAX*FLASH_MAX + 1] = {};
	char* ptr = NULL;

	nvs_open(page, NVS_READWRITE, &nvsHandle);
	nvs_erase_key(nvsHandle, name);
	nvs_commit(nvsHandle);
	nvs_close(nvsHandle);

	getFlash(page, "list", list);										// Retrieves the list of the existing configurations

	ptr = strtok(list, "/");											// Loop all the configurations
	while(ptr != NULL){
		if(strcmp(ptr, name) != 0){										// Build a new list without the deprecated configuration
			sprintf(listAux,"%s/%s", listAux, ptr);
		}
		ptr = strtok(NULL, "/");
	}
																		// Save the updated configurations list
	nvs_open(page, NVS_READWRITE, &nvsHandle);
	nvs_set_str (nvsHandle, "list", listAux);
	nvs_commit(nvsHandle);
	nvs_close(nvsHandle);
}


//**********************************************************************************************************************************
// Header: Board::initConfigs
// Function: Loads the configuration words from the non-volatile memory
//**********************************************************************************************************************************
void Board::initConfig(){
	char paramList[FLASH_MAX*FLASH_SIZE_MAX + FLASH_MAX + 1] = {};
	char* paramName;

	getFlash(FLASH_CONFIGS_PAGE, "list", paramList);					// Retrieves the list of the existing configurations
	strcpy(configsList, paramList);

	configsNumber = 0;													// Parse each parameter
	paramName = strtok(paramList, "/");
	while(paramName != NULL){
		strcpy(configsNames[configsNumber], paramName);					// Copy the name and values to RAM
		getFlash(FLASH_CONFIGS_PAGE, paramName, configsValues[configsNumber]);
		paramName = strtok(NULL, "/");
		configsNumber++;
	}
}


//**********************************************************************************************************************************
// Header: Board::getConfig
// Function: Gets a configuration word from previously loaded
//**********************************************************************************************************************************
void Board::getConfig(const char *param_s, char *value){
	int i = 0;

	if(strcmp(param_s, "list") == 0){
		memset(value, '\0', FLASH_MAX*FLASH_SIZE_MAX + FLASH_MAX + 1);
	}else{
		memset(value, '\0', FLASH_SIZE_MAX + 1);
	}

	if(strcmp(param_s, "list") == 0){
		strcpy(value, configsList);
	}else{
		while(i != configsNumber){
			if(strcmp(configsNames[i], param_s) == 0){
				strcpy(value, configsValues[i]);
				break;
			}
			i++;
		}
	}
}


//**********************************************************************************************************************************
// Header: Board::getConfig
// Function: Gets a configuration word from previously loaded, specify sub-configurations
//**********************************************************************************************************************************
void Board::getConfig(const char* param_s, char* value, int index){
	char aux[FLASH_SIZE_MAX + 1] = {};
	char* ptr;
	int i = 0;

	memset(value, '\0', FLASH_SIZE_MAX + 1);							// Clear the return memory

	while(i != configsNumber){
		if(strcmp(configsNames[i], param_s) == 0){						// Look for the proper parameter
			strcpy(aux, configsValues[i]);
			break;
		}
		i++;
	}
	if(strlen(aux) > 0){												// Look for specific sub-configuration
		ptr = strtok(aux, ",");
		for(i = 0; i < index && ptr != NULL; i++){
			ptr = strtok(NULL, ",");
		}
		if(ptr != NULL){
			strcpy(value, ptr);											// Update the return value
		}
	}
}


//**********************************************************************************************************************************
// Header: Board::setConfig
// Function: Sets a configuration word in the non-volatile memory
//**********************************************************************************************************************************
void Board::setConfig(const char* name, const char* value){
	setFlash(FLASH_CONFIGS_PAGE, name, value);
	initConfig();														// Reloads the configurations to main memory
}


//**********************************************************************************************************************************
// Header: Board::clearConfig
// Function: Deletes a configuration word from the non-volatile memory
//**********************************************************************************************************************************
void Board::clearConfig(const char* name){
	clearFlash(FLASH_CONFIGS_PAGE, name);
	initConfig();														// Reloads the configurations to main memory
}


//**********************************************************************************************************************************
// Header: Board::eraseConfigs
// Function: Deletes all configuration words from the non-volatile memory
//**********************************************************************************************************************************
void Board::eraseConfig(){
	eraseFlash(FLASH_CONFIGS_PAGE);
	initConfig();														// Reloads the configurations to main memory
}


//**********************************************************************************************************************************
// Header: Board::wakeStubDefault
// Function: Default wake stub used by gotoLowPowerMode()
//**********************************************************************************************************************************
void RTC_IRAM_ATTR wakeStubDefault(void){
	esp_default_wake_deep_sleep();
	return;
}

//**********************************************************************************************************************************
// Header: Board::gotoLowPowerMode
// Function: Enters the low power mode, this method is not safe for less than 3 seconds
//**********************************************************************************************************************************
void Board::gotoLowPowerMode(unsigned int seconds, void (*wakeStub)()){

	BoardULP::setWakeUpTime(seconds);

	if(wakeStub == NULL){
		esp_set_deep_sleep_wake_stub(&wakeStubDefault);						// Sets the sleep stub pointer
	}else{
		esp_set_deep_sleep_wake_stub(wakeStub);								// Sets the sleep stub pointer
	}
	esp_sleep_enable_ulp_wakeup();
	sleepPrepare();
	esp_deep_sleep_start();
}


//**********************************************************************************************************************************
// Header: Board::setDcDcLevelOn
// Function: Enables the DCDC 3.3 V supply level
//**********************************************************************************************************************************
void Board::setDcDcLevelOn(){
	digitalWrite(DCDC_LVL,1);
}


//**********************************************************************************************************************************
// Header: Board::setDcDcLevelOff
// Function: Disables the DCDC 3.3 V supply level
//**********************************************************************************************************************************
void Board::setDcDcLevelOff(){
	digitalWrite(DCDC_LVL,0);
}


//**********************************************************************************************************************************
// Header: Board::getInputVoltageLoad
// Function: Returns the latest battery readout while loading the batteries
//**********************************************************************************************************************************
double Board::getInputVoltageLoad(){
	return NULL;
}


//**********************************************************************************************************************************
// Header: Board::getInputVoltageOpen
// Function: Returns the latest battery readout while in light loading the batteries
//**********************************************************************************************************************************
double Board::getInputVoltageOpen(){
	double inputVoltage = 0;
	return inputVoltage;
}


//**********************************************************************************************************************************
// Header: Board::blinkLED
// Function: Blinks the LED in a given pattern
//**********************************************************************************************************************************
void Board::blinkLED(unsigned int blinks, unsigned int time_on1, unsigned int time_off1, unsigned int time_on2, unsigned int time_off2){
	unsigned int i, ledBrightnessOld;

	ledBrightnessOld = ledBrightness;
	for(i = 0; i < blinks; i++){
		ledBrightness = LED_ON;
		vTaskDelay(time_on1 / portTICK_PERIOD_MS);
		ledBrightness = ledBrightnessOld;
		vTaskDelay(time_off1 / portTICK_PERIOD_MS);
		ledBrightness = LED_ON;
		vTaskDelay(time_on2 / portTICK_PERIOD_MS);
		ledBrightness = ledBrightnessOld;
		vTaskDelay(time_off2 / portTICK_PERIOD_MS);
	}
}


//**********************************************************************************************************************************
// Header: Board::endLED
// Function: Joins the LED control thread
//**********************************************************************************************************************************
void Board::endLED(){
	//DEBUG((char*)"BOARD_LokaV2:\t ending LED task");
	ledActive = 1;
	while(ledActive == 1);
	if( threadLEDHandler != NULL){
		vTaskDelete(threadLEDHandler);
		threadLEDHandler = NULL;
	}
	ledActive = 0;
	Board::digitalWrite(LED, HIGH);
}


//**********************************************************************************************************************************
// Header: Board::startLED
// Function: Launches the LED control thread
//**********************************************************************************************************************************
void Board::startLED(unsigned char mode){
	ledBrightness = mode;
	ledActive = 0;
	if(threadLEDHandler == NULL){
		//DEBUG((char*)"BOARD_LokaV2:\t starting LED task");
		xTaskCreate(Board::runLED, "LokaStartLED", 4096, NULL, 15, &threadLEDHandler);
	}
}


//**********************************************************************************************************************************
// Header: Loka::startLED
// Function: Launches the LED control thread
//**********************************************************************************************************************************
void Board::setLedMode(unsigned char mode){
	ledBrightness = mode;
}


//**********************************************************************************************************************************
// Header: Board::runLED
// Function: Code for the LED control thread
//**********************************************************************************************************************************
void Board::runLED(void* pvParameter){

	while(ledActive == 0){
		if(ledBrightness == LED_OFF){
			Board::digitalWrite(LED, HIGH);
			vTaskDelay(10 / portTICK_PERIOD_MS);
		}
		if(ledBrightness == LED_HALF_BRIGHT){
			Board::digitalWrite(LED, LOW);
			ets_delay_us(50);
			Board::digitalWrite(LED, HIGH);
			vTaskDelay(10 / portTICK_PERIOD_MS);
		}
		if(ledBrightness == LED_ON){
			Board::digitalWrite(LED, LOW);
			vTaskDelay(10 / portTICK_PERIOD_MS);
		}
	}
	Board::digitalWrite(LED, HIGH);
	ledActive = 2;
}


//**********************************************************************************************************************************
// Header: Board::batteryLock
// Function: Releases the ULP program to squeeze the remaining of the battery
//**********************************************************************************************************************************
void Board::batteryLock(void){

	BoardULP::lockBattery();
	gotoLowPowerMode(0, NULL);
}
