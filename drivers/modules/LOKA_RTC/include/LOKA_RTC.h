/**
 * @file LOKA_RTC.h
 * @author Gabriel Sanchez
 * @author Andres Arias
 * @author Joao Pombas
 * @date 17/10/2019
 * @brief Driver for the ESP32 Real Time Clock.
 */

#ifndef LOKA_RTC_H_
#define LOKA_RTC_H_

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "sys/time.h"
#include "esp_attr.h"
#include "soc/rtc.h"

#include "esp32/ulp.h"
#include "soc/sens_reg.h"
#include "string.h"
#include "esp_task_wdt.h"

#include "soc/rtc.h"
#include "soc/rtc_cntl_reg.h"
#include "soc/timer_group_reg.h"

#include "soc/apb_ctrl_reg.h"

#include "rom/uart.h"
#include "driver/gpio.h"
#include "driver/rtc_io.h"
#include "stdlib.h"
#include "esp_system.h"
#include "rom/rtc.h"
#include "esp_sleep.h"



#ifdef __cplusplus

extern "C" {
#endif
	/**
	 * @brief Sets up the boot time for the board
	 * @param time Initial timestamp
	 * 
	 */
	void RTC_init(uint64_t time);

	/**
	 * @brief Returns the time since the last boot up in seconds
	 * @param[out] time seconds
	 */
	uint64_t RTC_IRAM_ATTR RTC_getUpTime();

	/**
	 * @brief Returns the time since the last boot up in milliseconds
	 * @param[out] time milliseconds
	 */
	uint64_t RTC_IRAM_ATTR RTC_getUpTimeMs();

	/**
	 * @brief Set a current timestamp of the board
	 * @param[in] timestamp seconds
	 */
	void RTC_IRAM_ATTR RTC_setTime(uint64_t time);

	/**
	 * @brief Returns the current time of the board
	 * @param[out] timestamp seconds
	 */
	uint64_t RTC_IRAM_ATTR RTC_getTime();

	/**
	 * @brief Read RTC timer register
	 * @param[out] timestamp seconds
	 */
	uint64_t RTC_IRAM_ATTR RTC_getTimer();

	/**
	 * @brief Calculate timestamp based on amount of seconds after current time
	 * @param[in] amount seconds
	 * @param[out] timestamp shift defined seconds after current time
	 */
	uint64_t RTC_IRAM_ATTR RTC_getTimerTarget(uint64_t seconds);

#ifdef __cplusplus
}
#endif


#endif
