/**
 * @file LOKA_RTC.c
 * @author Gabriel Sanchez
 * @author Andres Arias
 * @author Joao Pombas
 * @date 17/10/2019
 * @brief Driver for the ESP32 Real Time Clock.
 */

#include "include/LOKA_RTC.h"


RTC_DATA_ATTR uint64_t RTC_multiplier;
RTC_DATA_ATTR uint64_t RTC_start = 0;

void RTC_init(uint64_t time){
	RTC_multiplier = (((uint64_t)(1*1000000)) << RTC_CLK_CAL_FRACT) / (uint32_t) REG_READ(RTC_SLOW_CLK_CAL_REG);
	RTC_start = (time - RTC_getUpTime());
}

uint64_t RTC_IRAM_ATTR RTC_getUpTime(){
	RTC_multiplier = (((uint64_t)(1*1000000)) << RTC_CLK_CAL_FRACT) / (uint32_t) REG_READ(RTC_SLOW_CLK_CAL_REG);
	uint64_t timer;
	uint64_t time;

	timer = RTC_getTimer();
	time = timer/RTC_multiplier;
	return time;
}

uint64_t RTC_IRAM_ATTR RTC_getUpTimeMs(){
	RTC_multiplier = (((uint64_t)(1*1000000)) << RTC_CLK_CAL_FRACT) / (uint32_t) REG_READ(RTC_SLOW_CLK_CAL_REG);
	uint64_t timer;
	uint64_t time;

	timer = RTC_getTimer();
	time = timer*1000/RTC_multiplier;
	return time;
}
void RTC_IRAM_ATTR RTC_setTime(uint64_t time){
	RTC_start = (time - RTC_getUpTime());
}

uint64_t RTC_IRAM_ATTR RTC_getTime(){
	uint64_t time = 0;
	time = RTC_getUpTime() + RTC_start;
	return time;
}

uint64_t RTC_IRAM_ATTR RTC_getTimer(){
	uint64_t timer;

	SET_PERI_REG_MASK(RTC_CNTL_TIME_UPDATE_REG, RTC_CNTL_TIME_UPDATE);
	while (GET_PERI_REG_MASK(RTC_CNTL_TIME_UPDATE_REG, RTC_CNTL_TIME_VALID) == 0){
		ets_delay_us(1); 															// Might take 1 RTC slowclk period, avoids flooding the bus
	}
	SET_PERI_REG_MASK(RTC_CNTL_INT_CLR_REG, RTC_CNTL_TIME_VALID_INT_CLR);
	timer = READ_PERI_REG(RTC_CNTL_TIME0_REG);
	timer |= ((uint64_t) READ_PERI_REG(RTC_CNTL_TIME1_REG)) << 32;
	return timer;
}

uint64_t RTC_IRAM_ATTR RTC_getTimerTarget(uint64_t seconds){
	uint64_t timer;
	uint64_t timerTarget;

	SET_PERI_REG_MASK(RTC_CNTL_TIME_UPDATE_REG, RTC_CNTL_TIME_UPDATE);
	while (GET_PERI_REG_MASK(RTC_CNTL_TIME_UPDATE_REG, RTC_CNTL_TIME_VALID) == 0){
		ets_delay_us(1); 															// Might take 1 RTC slowclk period, avoids flooding the bus
	}
	SET_PERI_REG_MASK(RTC_CNTL_INT_CLR_REG, RTC_CNTL_TIME_VALID_INT_CLR);
	timer = READ_PERI_REG(RTC_CNTL_TIME0_REG);
	timer |= ((uint64_t) READ_PERI_REG(RTC_CNTL_TIME1_REG)) << 32;
	timerTarget = timer + seconds*RTC_multiplier;
	return timerTarget;
}