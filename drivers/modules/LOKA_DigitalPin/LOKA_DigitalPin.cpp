/**
 * @file DigitalPin.cpp
 * @author Andres Arias
 * @date 19/9/2019
 * @brief 
 */

#include "LOKA_DigitalPin.h"

namespace drivers
{
    namespace DigitalPin
    {
        static void configPin(gpio_config_t &config, Mode pin_mode_t)
        {
            switch (pin_mode_t) {
                case Mode::INPUT:
                    config.mode = GPIO_MODE_INPUT;
                    config.pull_up_en = GPIO_PULLUP_DISABLE;
                    config.pull_down_en = GPIO_PULLDOWN_DISABLE;
                    break;
                case Mode::INPUT_PULLUP:
                    config.mode = GPIO_MODE_INPUT;
                    config.pull_up_en = GPIO_PULLUP_ENABLE;
                    config.pull_down_en = GPIO_PULLDOWN_DISABLE;
                    break;
                case Mode::INPUT_PULLDOWN:
                    config.mode = GPIO_MODE_INPUT;
                    config.pull_up_en = GPIO_PULLUP_DISABLE;
                    config.pull_down_en = GPIO_PULLDOWN_ENABLE;
                    break;
                case Mode::OUTPUT:
                    config.mode = GPIO_MODE_OUTPUT;
                    break;
                case Mode::OUTPUT_PULLUP:
                    config.mode = GPIO_MODE_OUTPUT;
                    config.pull_up_en = GPIO_PULLUP_ENABLE;
                    config.pull_down_en = GPIO_PULLDOWN_DISABLE;
                    break;
                case Mode::OUTPUT_PULLDOWN:
                    config.mode = GPIO_MODE_OUTPUT;
                    config.pull_up_en = GPIO_PULLUP_DISABLE;
                    config.pull_down_en = GPIO_PULLDOWN_ENABLE;
                    break;
            }
        }

        void init(uint8_t gpio_num, Mode pin_mode_t)
        {
            gpio_config_t io_conf;
            io_conf.pin_bit_mask = (1ULL << gpio_num);
            io_conf.intr_type = GPIO_INTR_DISABLE;
            configPin(io_conf, pin_mode_t);
            gpio_config(&io_conf);
        }

        void initMask(uint64_t pin_mask, Mode pin_mode_t)
        {
            gpio_config_t io_conf;
            io_conf.pin_bit_mask = pin_mask;
            io_conf.intr_type = GPIO_INTR_DISABLE;
            configPin(io_conf, pin_mode_t);
            gpio_config(&io_conf);
        }

        void disable(uint8_t gpio_num)
        {
            gpio_config_t io_conf;
            io_conf.pin_bit_mask = (1ULL << gpio_num);
            io_conf.mode = GPIO_MODE_DISABLE;
            gpio_config(&io_conf);
        }

        void disableMask(uint64_t pin_mask)
        {
            gpio_config_t io_conf;
            io_conf.pin_bit_mask = pin_mask;
            io_conf.mode = GPIO_MODE_DISABLE;
            gpio_config(&io_conf);
        }

        void write(uint8_t gpio_num, uint8_t level)
        {
            gpio_set_level(static_cast<gpio_num_t>(gpio_num), level);
        }

        uint8_t read(uint8_t gpio_num)
        {
            return gpio_get_level(static_cast<gpio_num_t>(gpio_num));
        }

        void pullupSet(uint8_t gpio_num, bool enabled)
        {
            if (enabled)
                gpio_pullup_en(static_cast<gpio_num_t>(gpio_num));
            else
                gpio_pullup_dis(static_cast<gpio_num_t>(gpio_num));
        }

        void pulldownSet(uint8_t gpio_num, bool enabled)
        {
            if (enabled)
                gpio_pulldown_en(static_cast<gpio_num_t>(gpio_num));
            else
                gpio_pulldown_dis(static_cast<gpio_num_t>(gpio_num));
        }

        void isrServiceInit(void)
        {
            gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
        }

        void isrServiceStop(void)
        {
            gpio_uninstall_isr_service();
        }

        void attachInterrupt(uint8_t gpio_num,
                             gpio_int_type_t intr_type,
                             gpio_isr_t isr_handler,
                             void *args)
        {
            gpio_set_intr_type(static_cast<gpio_num_t>(gpio_num), intr_type);
            gpio_isr_handler_add(static_cast<gpio_num_t>(gpio_num),
                                 isr_handler,
                                 args);
        }

        void detachInterrupt(uint8_t gpio_num)
        {
            gpio_set_intr_type(static_cast<gpio_num_t>(gpio_num), GPIO_INTR_DISABLE);
            gpio_isr_handler_remove(static_cast<gpio_num_t>(gpio_num));
        }
    } /* DigitalPin */  
} /* drivers */ 
