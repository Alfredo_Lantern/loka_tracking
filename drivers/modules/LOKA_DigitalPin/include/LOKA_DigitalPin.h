/**
 * @file DigitalPin.h
 * @author Andres Arias
 * @date 19/9/2019
 * @brief Simple GPIO driver.
 */

#ifndef DIGITALPIN_H
#define DIGITALPIN_H

#include <cstdint>
#include "driver/gpio.h"
#include "esp_log.h"

/**
 * @brief Tag used by the logging library.
 */
#define DIGITALPIN_TAG  "DigitalPin"

/**
 * @brief GPIO number where the Loka's built-in LED is soldered to.
 */
#define LED_BUILTIN     GPIO_NUM_0

/**
 * @brief GPIO number where the Loka's built-in button is soldered to.
 */
#define BUTTON_BUILTIN  GPIO_NUM_35

#define ESP_INTR_FLAG_DEFAULT 0

/**
 * @namespace drivers
 * @brief Low-level routines related to the Hardware found on the current
 * Loka board.
 */
namespace drivers
{
    /**
     * @namespace drivers::DigitalPin
     * @brief Functions for managing and interfacing with GPIOs
     */
    namespace DigitalPin
    {
        /**
         * @enum Mode
         * @brief Indicates the mode of operation of the selected GPIOs
         */
        enum class Mode {
            /** Simple input */
            INPUT,
            /** Input with internal pullup resistors */
            INPUT_PULLUP,
            /** Input with internal pulldown resistors */
            INPUT_PULLDOWN,
            /** Simple output */
            OUTPUT,
            /** Output with internal pullup resistors */
            OUTPUT_PULLUP,
            /** Output with internal pulldown resistors */
            OUTPUT_PULLDOWN
        };

        /**
         * @brief Initializes a given pin with the given mode.
         * @param gpio_num The pin number to initialize.
         * @param pin_mode_t If input/output with or without internal
         * pullup/down resistors.
         */
        void init(uint8_t gpio_num, Mode pin_mode_t);

        /**
         * @brief Initializes several pins at once with the given mode.
         * @param pin_mask Binary mask where every bit represents a especific pin,
         * where 1 indicates "taking control of" and 0 ignores it.
         * @param pin_mode_t If input/output with or without internal
         * pullup/down resistors.
         */
        void initMask(uint64_t pin_mask, Mode pin_mode_t);

        /**
         * @brief Disables the pin with the given number.
         * @param gpio_num The number of the pin to let go control of.
         */
        void disable(uint8_t gpio_num);

        /**
         * @brief Disables several pins at once.
         * @param gpio_num The number of the pin to let go control of.
         */
        void disableMask(uint64_t pin_mask);

        /**
         * @brief Writes a logical 0 or 1 to the selected pin.
         * @param gpio_num The number of the GPIO to write to.
         * @param level 0 or 1, depending if logical level LOW or HIGH.
         */
        void write(uint8_t gpio_num, uint8_t level);

        /**
         * @brief Reads the given GPIO.
         * @param gpio_num The number of the GPIO to read from.
         * @retval 0 read logical level LOW.
         * @retval 1 read logical level HIGH.
         */
        uint8_t read(uint8_t gpio_num);

        /**
         * @brief Enables or disables the internal pullup resistors of 
         * a given pin number.
         * @param gpio_num The number of the pin to change its pullup resistors.
         * @param enabled If set to true, enables the internal pullup resistor, disables
         * it otherwise.
         */
        void pullupSet(uint8_t gpio_num, bool enabled);

        /**
         * @brief Enables or disables the internal pulldown resistors of 
         * a given pin number.
         * @param gpio_num The number of the pin to change its pulldown resistors.
         * @param enabled If set to true, enables the internal pulldown resistor, disables
         * it otherwise.
         */
        void pulldownSet(uint8_t gpio_num, bool enabled);

        /**
         * @brief Enables the interrupt service.
         * @note Must calll this function before attaching any interrupt, must
         * be called only once.
         */
        void isrServiceInit(void);

        /**
         * @brief Disables the interrupt service, releasing every resource.
         */
        void isrServiceStop(void);

        /**
         * @brief Attaches an interrupt to a given GPIO number to trigger an
         * interrupt handler when a given event happens on said GPIO.
         * @param gpio_num The number of the pin to attach the interrupt to.
         * @param intr_type The type of event that trigger the interrupt. Can be
         * GPIO_INTR_POSEDGE for positive edge trigger, GPIO_INTR_NEGEDGE for 
         * negative edge trigger and GPIO_INTR_ANYEDGE for any edge trigger.
         * @param isr_handler The function to call when the interrupt is triggered.
         * @param args Pointer to data to be passed to the interrupt handling 
         * function.
         */
        void attachInterrupt(uint8_t gpio_num,
                gpio_int_type_t intr_type,
                gpio_isr_t isr_handler,
                void *args);

        /**
         * @brief Removes the interrupt from a given pin number.
         * @param gpio_num The number of the pin to remove the interrupt attached.
         */
        void detachInterrupt(uint8_t gpio_num);
    } /* DigitalPin */ 
} /* drivers */ 

#endif
