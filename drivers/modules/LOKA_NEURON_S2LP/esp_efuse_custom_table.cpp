// Copyright 2017-2018 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at",
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License

#include "sdkconfig.h"
#include "esp_efuse.h"
#include <assert.h>
#include "esp_efuse_custom_table.h"

// md5_digest_table 3f8522918fa7bf1f9d82aad6fd8c2f1a
// This file was generated from the file esp_efuse_custom_table.csv. DO NOT CHANGE THIS FILE MANUALLY.
// If you want to change some fields, you need to change esp_efuse_custom_table.csv file
// then run `efuse_common_table` or `efuse_custom_table` command it will generate this file.
// To show efuse_table run the command 'show_efuse_table'.

#define MAX_BLK_LEN CONFIG_EFUSE_MAX_BLK_LEN

// The last free bit in the block is counted over the entire file.
#define LAST_FREE_BIT_BLK3 256

_Static_assert(LAST_FREE_BIT_BLK3 <= MAX_BLK_LEN, "The eFuse table does not match the coding scheme. Edit the table and restart the efuse_common_table or efuse_custom_table command to regenerate the new files.");

static const esp_efuse_desc_t SIGFOX_SECRETS[] = {
    {EFUSE_BLK3, 32, 8}, 	 // Sigfox ID [0],
    {EFUSE_BLK3, 40, 8}, 	 // Sigfox ID [1],
    {EFUSE_BLK3, 48, 8}, 	 // Sigfox ID [2],
    {EFUSE_BLK3, 56, 8}, 	 // Sigfox ID [3],
    {EFUSE_BLK3, 64, 8}, 	 // Sigfox PAC [0],
    {EFUSE_BLK3, 72, 8}, 	 // Sigfox PAC [1],
    {EFUSE_BLK3, 80, 8}, 	 // Sigfox PAC [2],
    {EFUSE_BLK3, 88, 8}, 	 // Sigfox PAC [3],
    {EFUSE_BLK3, 96, 8}, 	 // Sigfox PAC [4],
    {EFUSE_BLK3, 104, 8}, 	 // Sigfox PAC [5],
    {EFUSE_BLK3, 112, 8}, 	 // Sigfox PAC [6],
    {EFUSE_BLK3, 120, 8}, 	 // Sigfox PAC [7],
    {EFUSE_BLK3, 128, 8}, 	 // Sigfox KEY [0],
    {EFUSE_BLK3, 136, 8}, 	 // Sigfox KEY [1],
    {EFUSE_BLK3, 144, 8}, 	 // Sigfox KEY [2],
    {EFUSE_BLK3, 152, 8}, 	 // Sigfox KEY [3],
    {EFUSE_BLK3, 160, 8}, 	 // Sigfox KEY [4],
    {EFUSE_BLK3, 168, 8}, 	 // Sigfox KEY [5],
    {EFUSE_BLK3, 176, 8}, 	 // Sigfox KEY [6],
    {EFUSE_BLK3, 184, 8}, 	 // Sigfox KEY [7],
    {EFUSE_BLK3, 192, 8}, 	 // Sigfox KEY [8],
    {EFUSE_BLK3, 200, 8}, 	 // Sigfox KEY [9],
    {EFUSE_BLK3, 208, 8}, 	 // Sigfox KEY [10],
    {EFUSE_BLK3, 216, 8}, 	 // Sigfox KEY [11],
    {EFUSE_BLK3, 224, 8}, 	 // Sigfox KEY [12],
    {EFUSE_BLK3, 232, 8}, 	 // Sigfox KEY [13],
    {EFUSE_BLK3, 240, 8}, 	 // Sigfox KEY [14],
    {EFUSE_BLK3, 248, 8}, 	 // Sigfox KEY [15],
};
const int prueba = 123;





const esp_efuse_desc_t* ESP_EFUSE_SIGFOX_SECRETS[] = {
    &SIGFOX_SECRETS[0],    		// Sigfox ID [0]
    &SIGFOX_SECRETS[1],    		// Sigfox ID [1]
    &SIGFOX_SECRETS[2],    		// Sigfox ID [2]
    &SIGFOX_SECRETS[3],    		// Sigfox ID [3]
    &SIGFOX_SECRETS[4],    		// Sigfox PAC [0]
    &SIGFOX_SECRETS[5],    		// Sigfox PAC [1]
    &SIGFOX_SECRETS[6],    		// Sigfox PAC [2]
    &SIGFOX_SECRETS[7],    		// Sigfox PAC [3]
    &SIGFOX_SECRETS[8],    		// Sigfox PAC [4]
    &SIGFOX_SECRETS[9],    		// Sigfox PAC [5]
    &SIGFOX_SECRETS[10],    		// Sigfox PAC [6]
    &SIGFOX_SECRETS[11],    		// Sigfox PAC [7]
    &SIGFOX_SECRETS[12],    		// Sigfox KEY [0]
    &SIGFOX_SECRETS[13],    		// Sigfox KEY [1]
    &SIGFOX_SECRETS[14],    		// Sigfox KEY [2]
    &SIGFOX_SECRETS[15],    		// Sigfox KEY [3]
    &SIGFOX_SECRETS[16],    		// Sigfox KEY [4]
    &SIGFOX_SECRETS[17],    		// Sigfox KEY [5]
    &SIGFOX_SECRETS[18],    		// Sigfox KEY [6]
    &SIGFOX_SECRETS[19],    		// Sigfox KEY [7]
    &SIGFOX_SECRETS[20],    		// Sigfox KEY [8]
    &SIGFOX_SECRETS[21],    		// Sigfox KEY [9]
    &SIGFOX_SECRETS[22],    		// Sigfox KEY [10]
    &SIGFOX_SECRETS[23],    		// Sigfox KEY [11]
    &SIGFOX_SECRETS[24],    		// Sigfox KEY [12]
    &SIGFOX_SECRETS[25],    		// Sigfox KEY [13]
    &SIGFOX_SECRETS[26],    		// Sigfox KEY [14]
    &SIGFOX_SECRETS[27],    		// Sigfox KEY [15]
    NULL
};

