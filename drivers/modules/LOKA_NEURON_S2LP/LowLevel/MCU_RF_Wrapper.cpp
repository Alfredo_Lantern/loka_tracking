// Filename: MCU_RF_Wrapper.h
// Date: 18.12.2017
// Author: Hugo Silva & Luis Rosado
// Company: LOKA SYSTEMS
// Version: 1.0
// Description: Wrapper to access C++ code on the rf_api and mcu_api
//**********************************************************************************************************************************


//**********************************************************************************************************************************
//                                                      Includes Section
//**********************************************************************************************************************************
#include "include/MCU_RF_Drivers.h"


extern "C" void MCURFDrivers_InitIRQ(){
	MCURFDrivers::initIRQ();
}

extern "C" void MCURFDrivers_Shutdown(unsigned char value){
	MCURFDrivers::shutdownIC(value);
}

extern "C" void MCURFDrivers_Delay(unsigned int delay_ms){
	MCURFDrivers::delay(delay_ms);
}

extern "C" void MCURFDrivers_SpiInit(){
	MCURFDrivers::spiInit();
}

extern "C" void MCURFDrivers_SpiClose(){
	MCURFDrivers::spiClose();
}

extern "C" void MCURFDrivers_SpiRaw(unsigned char n_bytes, unsigned char* tx_buffer, unsigned char* rx_buffer){
	MCURFDrivers::spiRaw(n_bytes, tx_buffer, rx_buffer);
}

extern "C" void MCURFDrivers_Encrypt(unsigned char* encrypted, unsigned char* to_encrypt, unsigned char data_len, unsigned char * key, int use_key){
	MCURFDrivers::encrypt(encrypted, to_encrypt,  data_len, key, use_key);
}

extern "C" void MCURFDrivers_FFSReadBlock(unsigned char* read_mem, unsigned int* size){
	MCURFDrivers::FFSReadBlock(read_mem, (unsigned long int*) size);
}

extern "C" void MCURFDrivers_FFSWriteBlock(unsigned char* read_mem, unsigned int* size){
	MCURFDrivers::FFSWriteBlock(read_mem, (unsigned long int*) size);
}

extern "C" void MCURFDrivers_FFSReadInfo(unsigned char* read_mem, unsigned int* size){
	MCURFDrivers::FFSReadInfo(read_mem, (unsigned long int*) size);
}

extern "C" void MCURFDrivers_GetConfig(const char * param, char* value){
	drivers::NVS nvs_manager;
	nvs_manager.getFlash(SFX_CONFIGS_PAGE, param, value);
}

extern "C" void MCURFDrivers_GpioIRQ(unsigned char pin, unsigned char new_state, unsigned char trigger){
	MCURFDrivers::gpioIRQ(pin, new_state, trigger);
}

extern "C" int MCURFDrivers_WaitForInterrupt(void){
	return MCURFDrivers::waitForInterrupt();
}

extern "C" int MCURFDrivers_GetTemperature(void){
	return MCURFDrivers::getTemperature();
}

extern "C" int MCURFDrivers_GetVoltageLoad(void){
	return MCURFDrivers::getBatteryVoltageLoad();
}

extern "C" int MCURFDrivers_GetVoltageOpen(void){
	return MCURFDrivers::getBatteryVoltageOpen();
}

extern "C" void MCURFDrivers_Debug(void* arg){
	printf((char*) arg); // change for esp_log
}

extern "C" void MCURFDrivers_GpioDebug(unsigned char value){
	drivers::GPIO_STUB::pinMode(GPIO_NUM_25, OUTPUT);
	drivers::GPIO_STUB::digitalWrite(GPIO_NUM_25, value);
}
