//**********************************************************************************************************************************
// Filename: MCURFDrivers.cpp
// Date: 08.12.2017
// Author: Luis Rosado
// Company: LOKA SYSTEMS
// Version: 1.0
// Description: Implements low functions needed to operate the S2LP and the Sigfox library
//**********************************************************************************************************************************
#ifndef MCU_RF_DRIVERS_H_
#define MCU_RF_DRIVERS_H_


//**********************************************************************************************************************************
//                                                      Include Section
//**********************************************************************************************************************************
//#include "../../main/settings.h"
//#include "../../main/board.h"
//#include "../../main/board_io.h"
#include "BoardIO.h"
#include "../../drivers/modules/LOKA_NVS/include/LOKA_NVS.h"
#include "../../drivers/modules/LOKA_RTC/include/LOKA_RTC.h"
#include "../../drivers/modules/LOKA_Interruptions/include/LOKA_Interruptions.h"
#include "../../drivers/modules/LOKA_SPIFFS/include/LOKA_SPIFFS.h"

#include "driver/spi_master.h"
#include "spi_master_nodma.h"
#include "soc/gpio_struct.h"
#include "driver/gpio.h"
#include "rom/aes.h"
#include "soc/rtc.h"
#include "string.h"
#include "esp_task_wdt.h"
#include "esp_efuse.h"
#include "esp_efuse_custom_table.h"


#include "soc/sens_reg.h"
#include "string.h"
#include "soc/rtc.h"

extern "C" {
	#include "rom/uart.h"
	#include "driver/gpio.h"
	#include "driver/rtc_io.h"
	#include "nvs_flash.h"
	#include "nvs.h"
	#include "stdlib.h"
	#include "esp_system.h"
	#include "rom/rtc.h"
	#include "esp_sleep.h"
}

#include "mbedtls/ctr_drbg.h"																					// AES encrypt LIB
#include "mbedtls/arc4.h"
#include "string.h"

//**********************************************************************************************************************************
//                                                      Define Section
//**********************************************************************************************************************************
#define SFX_CONFIGS_PAGE			"sigfox"
#define SFX_STATE_PARTITION_LABEL	"storage"
#define SFX_STATE_FILE_NAME			"sigfox_state.num"
#define SFX_INFO_PARTITION_LABEL	"secrets"
#define SFX_INFO_FILE_NAME			"sigfox_secrets.bin"
#define SFX_STATE_PAGE_NAME			"sigfox_library" // Page used by S2LP to save data in nvs
#define SFX_STATE_KEY_NAME			"sigfox_state" // Key used by S2LP to save data in nvs
#define SFX_SECRETS_BUFF_LEN        28
#define SFX_SECRETS_FIELD_LEN       224



typedef unsigned char       sfx_u8;


#define ID_LENGTH      (sfx_u8)(4)            /* Size of device identifier */
#define PAC_LENGTH     (sfx_u8)(8)            /* Size of device initial PAC */
#define KEY_LENGTH     (sfx_u8)(16)           /* Size of device private key */

#ifndef S2LP_SDN
	#define	S2LP_SDN	(gpio_num_t)-1
#endif
#ifndef S2LP_CS
	#define S2LP_CS		(gpio_num_t)-1
#endif
#ifndef S2LP_GPIO3
	#define S2LP_GPIO3	(gpio_num_t)-1
#endif
#ifndef FE_SDN
	#define FE_SDN		(gpio_num_t)-1
#endif

//**********************************************************************************************************************************
//                                                     Templates Section
//**********************************************************************************************************************************
class MCURFDrivers{
	public:
		static void initIRQ();
		static void shutdownIC(unsigned char value);
		static void delay(unsigned int delay_millis);
		static void spiInit();
		static void spiClose();
		static void spiRaw(unsigned char n_bytes, unsigned char* tx_buffer, unsigned char* rx_buffer);
		static void encrypt(unsigned char* encrypted, unsigned char* to_encrypt, unsigned char data_len, unsigned char * key, int use_private_key);
		static void FFSWriteBlock(unsigned char* write_mem, unsigned long int* size);
		static void FFSReadBlock(unsigned char* read_mem, unsigned long int* size);
		static void FFSReadInfo(unsigned char* read_mem, unsigned long int* size);
		static void gpioIRQ(unsigned char pin, unsigned char new_state, unsigned char trigger);
		static int waitForInterrupt(void);
		static int getTemperature(void);
		static int getBatteryVoltageLoad(void);
		static int getBatteryVoltageOpen(void);
		static void initIQSampling();
	private:

};

#endif
