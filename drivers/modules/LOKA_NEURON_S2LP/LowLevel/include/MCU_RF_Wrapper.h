#ifndef MCU_RF_WRAPPER_H_
#define MCU_RF_WRAPPER_H_

void MCURFDrivers_InitIRQ();
void MCURFDrivers_Shutdown(unsigned char value);
void MCURFDrivers_Delay(unsigned int delay_ms);
void MCURFDrivers_SpiInit();
void MCURFDrivers_SpiClose();
void MCURFDrivers_SpiRaw(unsigned char n_bytes, unsigned char* tx_buffer, unsigned char* rx_buffer);
void MCURFDrivers_Encrypt(unsigned char* encrypted, unsigned char* to_encrypt, unsigned char data_len, unsigned char * key, int use_key);
void MCURFDrivers_FFSReadBlock(unsigned char* read_mem, unsigned int* size);
void MCURFDrivers_FFSWriteBlock(unsigned char* read_mem, unsigned int* size);
void MCURFDrivers_FFSReadInfo(unsigned char* read_mem, unsigned int* size);
void MCURFDrivers_GpioIRQ(unsigned char pin, unsigned char new_state, unsigned char trigger);
int MCURFDrivers_WaitForInterrupt(void);
int MCURFDrivers_GetTemperature(void);
int MCURFDrivers_GetVoltageLoad(void);
int MCURFDrivers_GetVoltageOpen(void);
void MCURFDrivers_InitIQSampling();
void MCURFDrivers_Debug(void* arg);
void MCURFDrivers_GetConfig(const char * param, char* value);
void MCURFDrivers_GpioDebug(unsigned char value);

#endif
