//**********************************************************************************************************************************
// Filename: mcu_api.c
// Date: 18.12.2017
// Author: Hugo Silva & Luis Rosado
// Company: LOKA SYSTEMS
// Version: 1.0
// Description: Provides mcu functionality access to the SIGFOX library. Built from scratch taking into account the mcu_api.h
//				from SIGFOX library
//**********************************************************************************************************************************


//**********************************************************************************************************************************
//                                                      Include Section
//**********************************************************************************************************************************
#include "include/mcu_api.h"
#include "LOKA_RTC.h"
#include "include/MCU_RF_Wrapper.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"


//**********************************************************************************************************************************
//                                                     Global Variables
//**********************************************************************************************************************************
#define ST_MANUF_API_VER_SIZE 3
sfx_u8 ST_MANUF_API_VER[ST_MANUF_API_VER_SIZE] = {1, 0, 0};
static const uint8_t ST_MANUF_API_VER_STR[] =  "V1.0.0";

unsigned long long timeout_mcu = 0;
unsigned char timeout_mcu_flag = 0;


//**********************************************************************************************************************************
//                                                        Code Section
//**********************************************************************************************************************************

/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_malloc(sfx_u16 size, sfx_u8 **returned_pointer)
 * \brief Allocate memory for library usage (Memory usage = size (Bytes))
 * This function is only called once at the opening of the Sigfox Library ( SIGF
 *
 * IMPORTANT NOTE:
 * --------------
 * The address reported need to be aligned with architecture of the microprocessor used.
 * For a Microprocessor of:
 *   - 8 bits  => any address is allowed
 *   - 16 bits => only address multiple of 2 are allowed
 *   - 32 bits => only address multiple of 4 are allowed
 *
 * \param[in] sfx_u16 size                  size of buffer to allocate in bytes
 * \param[out] sfx_u8 **returned_pointer    pointer to buffer (can be static)
 *
 * \retval SFX_ERR_NONE:              No error
 * \retval MCU_ERR_API_MALLOC         Malloc error
 **********************************************************************************************************************************/
sfx_u8 MCU_API_malloc(sfx_u16 size, sfx_u8 **returned_pointer){
	*returned_pointer = (sfx_u8*) malloc(size*sizeof(sfx_u8));
	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_free(sfx_u8 *ptr)
 * \brief Free memory allocated to library
 *
 * \param[in] sfx_u8 *ptr                        pointer to buffer
 * \param[out] none
 *
 * \retval SFX_ERR_NONE:                         No error
 * \retval MCU_ERR_API_FREE:                     Free error
 **********************************************************************************************************************************/
sfx_u8 MCU_API_free(sfx_u8 *ptr){
	free(ptr);
	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_get_voltage_temperature(sfx_u16 *voltage_idle, sfx_u16 *voltage_tx, sfx_s16 *temperature)
 * \brief Get voltage and temperature for Out of band frames
 * Value must respect the units bellow for <B>backend compatibility</B>
 *
 * \param[in] none
 * \param[out] sfx_u16 *voltage_idle             Device's voltage in Idle state (mV)
 * \param[out] sfx_u16 *voltage_tx               Device's voltage in Tx state (mV) - for the last transmission
 * \param[out] sfx_s16 *temperature              Device's temperature in 1/10 of degrees celcius
 *
 * \retval SFX_ERR_NONE:                         No error
 * \retval MCU_ERR_API_VOLT_TEMP:                Get voltage/temperature error
 **********************************************************************************************************************************/
sfx_u8 MCU_API_get_voltage_temperature(sfx_u16 *voltage_idle, sfx_u16 *voltage_tx, sfx_s16 *temperature){
	(*voltage_idle) = 0;
	(*voltage_tx) = 0;
	(*temperature) = 0;
	return 0;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_delay(sfx_delay_t delay_type)
 * \brief Inter stream delay, called between each RF_API_send
 * - SFX_DLY_INTER_FRAME_TX  : 0 to 2s in Uplink DC
 * - SFX_DLY_INTER_FRAME_TRX : 500 ms in Uplink/Downlink FH & Downlink DC 
 * - SFX_DLY_OOB_ACK :         1.4s to 4s for Downlink OOB
 * - SFX_CS_SLEEP :            delay between several trials of carrier sense (for the first frame only)
 *
 * \param[in] sfx_delay_t delay_type             Type of delay to call
 * \param[out] none
 *
 * \retval SFX_ERR_NONE:                         No error
 * \retval MCU_ERR_API_DLY:                      Delay error
 **********************************************************************************************************************************/
sfx_u8 MCU_API_delay(sfx_delay_t delay_type){
	switch(delay_type){
		case SFX_DLY_INTER_FRAME_TX:
			MCURFDrivers_Delay(50);					// (500-36-12) ETSI, (500-7-12) FCC
			break;
		case SFX_DLY_INTER_FRAME_TRX:
			MCURFDrivers_Delay(500);				// (500-36-12) ETSI, (500-7-12) FCC
			break;
		case SFX_DLY_OOB_ACK:
			MCURFDrivers_Delay(2000);				// (2000-36-12)
			break;
		case SFX_DLY_CS_SLEEP:						// (500-36-12) ETSI, (500-7-12) FCC
			MCURFDrivers_Delay(60);
			break;
		default:
			return MCU_ERR_API_DLY;
	}
	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_aes_128_cbc_encrypt(sfx_u8 *encrypted_data, sfx_u8 *data_to_encrypt, sfx_u8 aes_block_len, sfx_u8 key[16],
 * sfx_credentials_use_key_t use_key)
 * \brief Encrypt a complete buffer with Secret or Test key.<BR>
 * The secret key corresponds to the private key provided from the CRA.
 * <B>These keys must be stored in a secure place.</B> <BR>
 * Can be hardcoded or soft coded (iv vector contains '0')
 *
 * \param[out] sfx_u8 *encrypted_data            Result of AES Encryption
 * \param[in] sfx_u8 *data_to_encrypt            Input data to Encrypt
 * \param[in] sfx_u8 aes_block_len               Input data length (should be a multiple of an AES block size, ie.
 * 												 AES_BLOCK_SIZE bytes)
 * \param[in] sfx_u8 key[16]                     Input key
 * \param[in] sfx_credentials_use_key_t use_key  Key to use - private key or input key
 *
 * \retval SFX_ERR_NONE:                         No error
 * \retval MCU_ERR_API_AES:                      AES Encryption error
 **********************************************************************************************************************************/
sfx_u8 MCU_API_aes_128_cbc_encrypt(sfx_u8 *encrypted_data, sfx_u8 *data_to_encrypt, sfx_u8 aes_block_len, sfx_u8 key[16],
                                   sfx_credentials_use_key_t use_key){

	MCURFDrivers_Encrypt(encrypted_data, data_to_encrypt, aes_block_len, key, (int) use_key);
	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_get_nv_mem(sfx_u8 read_data[SFX_NVMEM_BLOCK_SIZE]) 
 * \brief This function copies the data read from non volatile memory
 * into the buffer pointed by read_data.<BR>
 * The size of the data to read is \link SFX_NVMEM_BLOCK_SIZE \endlink
 * bytes.
 * CAREFUL : this value can change according to the features included
 * in the library (covered zones, etc.)
 *
 * \param[in] none
 * \param[out] sfx_u8 read_data[SFX_NVMEM_BLOCK_SIZE] Pointer to the data bloc to write with the data stored in memory
 *
 * \retval SFX_ERR_NONE:                         No error
 * \retval MCU_ERR_API_GETNVMEM:                 Read nvmem error
 **********************************************************************************************************************************/
sfx_u8 MCU_API_get_nv_mem(sfx_u8 read_data[SFX_NVMEM_BLOCK_SIZE]){
	unsigned int data_size = SFX_NVMEM_BLOCK_SIZE;
	MCURFDrivers_FFSReadBlock(read_data, &data_size);
	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_set_nv_mem(sfx_u8 data_to_write[SFX_NVMEM_BLOCK_SIZE]) 
 * \brief This function writes data pointed by data_to_write to non
 * volatile memory.<BR> It is strongly recommended to use NV memory
 * like EEPROM since this function is called at each SIGFOX_API_send_xxx.
 * The size of the data to write is \link SFX_NVMEM_BLOCK_SIZE \endlink
 * bytes.
 * CAREFUL : this value can change according to the features included
 * in the library (covered zones, etc.)
 *
 * \param[in] sfx_u8 data_to_write[SFX_NVMEM_BLOCK_SIZE] Pointer to data bloc to be written in memory
 * \param[out] none
 *
 * \retval SFX_ERR_NONE:                         No error
 * \retval MCU_ERR_API_SETNVMEM:                 Write nvmem error
 **********************************************************************************************************************************/
sfx_u8 MCU_API_set_nv_mem(sfx_u8 data_to_write[SFX_NVMEM_BLOCK_SIZE]){
	unsigned int data_size = SFX_NVMEM_BLOCK_SIZE;
	MCURFDrivers_FFSWriteBlock(data_to_write, &data_size);
	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_timer_start_carrier_sense(sfx_u16 time_duration_in_ms)
 * \brief Start timer for :
 * - carrier sense maximum window (used in ARIB standard)
 *
 * \param[in] sfx_u16 time_duration_in_ms        Timer value in milliseconds
 * \param[out] none
 *
 * \retval SFX_ERR_NONE:                         No error
 * \retval MCU_ERR_API_TIMER_START_CS:           Start CS timer error
 **********************************************************************************************************************************/
sfx_u8 MCU_API_timer_start_carrier_sense(sfx_u16 time_duration_in_ms){
	timeout_mcu_flag = 0;

	if(time_duration_in_ms > 0)
		timeout_mcu = RTC_getUpTimeMs() + time_duration_in_ms;
	else
		return MCU_ERR_API_TIMER_START_CS;

	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_timer_start(sfx_u32 time_duration_in_s)
 * \brief Start timer for in second duration 
 *
 * \param[in] sfx_u32 time_duration_in_s         Timer value in seconds
 * \param[out] none
 *
 * \retval SFX_ERR_NONE:                         No error
 * \retval MCU_ERR_API_TIMER_START:              Start timer error
 **********************************************************************************************************************************/
sfx_u8 MCU_API_timer_start(sfx_u32 time_duration_in_s){
	timeout_mcu_flag = 0;

	if(time_duration_in_s > 0)
		timeout_mcu = RTC_getUpTimeMs() + (time_duration_in_s*1000);
	else
		return MCU_ERR_API_TIMER_START_CS;

	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_timer_stop(void)
 * \brief Stop the timer (started with MCU_API_timer_start)
 *
 * \param[in] none
 * \param[out] none
 *
 * \retval SFX_ERR_NONE:                         No error
 * \retval MCU_ERR_API_TIMER_STOP:               Stop timer error
 **********************************************************************************************************************************/
sfx_u8 MCU_API_timer_stop(void){
	timeout_mcu_flag = 1;
	timeout_mcu = 0;
	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_timer_stop_carrier_sense(void)
 * \brief Stop the timer (started with MCU_API_timer_start_carrier_sense)
 *
 * \param[in] none
 * \param[out] none
 *
 * \retval SFX_ERR_NONE:                         No error
 * \retval MCU_ERR_API_TIMER_STOP_CS:            Stop timer error
 **********************************************************************************************************************************/
sfx_u8 MCU_API_timer_stop_carrier_sense(void){
	timeout_mcu_flag = 1;
	timeout_mcu = 0;
	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_timer_wait(sfx_u16 time_duration_in_ms)
 * \brief Non-blocking function to wait for interrupt indicating timer
 * elapsed.<BR>
 *
 * \param[in] none
 * \param[out] none
 *
 * \retval SFX_ERR_NONE:                         No error
 **********************************************************************************************************************************/
sfx_u8 MCU_API_timer_wait(sfx_u16 time_duration_in_ms){
	MCURFDrivers_Delay(time_duration_in_ms);
	if(RTC_getUpTimeMs() > timeout_mcu)
		timeout_mcu_flag = 1;
	return timeout_mcu_flag;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_timer_wait_for_end(void)
 * \brief Blocking function to wait for interrupt indicating timer
 * elapsed.<BR> This function is only used for the 20 seconds wait
 * in downlink.
 *
 * \param[in] none
 * \param[out] none
 *
 * \retval SFX_ERR_NONE:                         No error
 * \retval MCU_ERR_API_TIMER_END:                Wait end of timer error
 **********************************************************************************************************************************/
sfx_u8 MCU_API_timer_wait_for_end(void){
	while(timeout_mcu > RTC_getUpTimeMs()){
		MCURFDrivers_Delay(1);
	}
	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_report_test_result(sfx_bool status, sfx_s16 rssi)
 * \brief To report the result of Rx test for each valid message
 * received/validated by library.<BR> Manufacturer api to show the result
 * of RX test mode : can be uplink radio frame or uart print or
 * gpio output.
 * RSSI parameter is only used to report the rssi of received frames (downlink test)
 *
 * \param[in] sfx_bool status                    Is SFX_TRUE when result ok else SFX_FALSE
 *                                               See SIGFOX_API_test_mode summary
 * \param[out] rssi                              RSSI of the received frame 
 *
 * \retval SFX_ERR_NONE:                         No error
 * \retval MCU_ERR_API_TEST_REPORT:              Report test result error
 **********************************************************************************************************************************/
sfx_u8 MCU_API_report_test_result(sfx_bool status, sfx_s16 rssi){
	char buffer[64];

	if(status)
		sprintf(buffer,"MCU_API:\t\t Received valid frame, RSSI: %d", rssi);
	else
		sprintf(buffer,"MCU_API:\t\t Received invalid frame");
	MCURFDrivers_Debug(buffer);

	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_get_version(sfx_u8 **version, sfx_u8 *size)
 * \brief Returns current MCU API version
 *
 * \param[out] sfx_u8 **version                  Pointer to Byte array (ASCII format) containing library version
 * \param[out] sfx_u8 *size                      Size of the byte array pointed by *version
 *
 * \retval SFX_ERR_NONE:                         No error
 * \retval MCU_ERR_API_GET_VERSION:              Get Version error 
 **********************************************************************************************************************************/
sfx_u8 MCU_API_get_version(sfx_u8 **version, sfx_u8 *size){
	*version = ST_MANUF_API_VER_STR;
	(*size) = strlen((char*) *version);
	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_get_device_id_and_payload_encryption_flag(sfx_u8 dev_id[ID_LENGTH], sfx_bool *payload_encryption_enabled)
 * \brief This function copies the device ID in dev_id, and
 * the payload encryption flag in payload_encryption_enabled.
 *
 * \param[in]  none
 * \param[out] sfx_u8 dev_id[ID_LENGTH]          Pointer on the device ID
 * \param[out] sfx_bool *payload_encryption_enabled  Payload is encrypted if SFX_TRUE, not encrypted else
 *
 * \retval SFX_ERR_NONE:                         No error
 * \retval MCU_ERR_API_GET_ID_PAYLOAD_ENCR_FLAG: Error when getting device ID or payload encryption flag
 **********************************************************************************************************************************/
sfx_u8 MCU_API_get_device_id_and_payload_encryption_flag(sfx_u8 dev_id[ID_LENGTH], sfx_bool *payload_encryption_enabled){
    char secrets_buff[28];
    esp_efuse_read_field_blob(ESP_EFUSE_SIGFOX_SECRETS,
            (void*)secrets_buff,
            SIGFOX_SECRETS_FIELD_LEN);

	// Getting Device Type ( bytes are inverted )
	for(unsigned int i = 0; i < ID_LENGTH; i++){
		dev_id[ID_LENGTH - 1 - i] = secrets_buff[i];
	}

    *payload_encryption_enabled = atoi(secrets_buff);

	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 MCU_API_get_initial_pac(sfx_u8 initial_pac[PAC_LENGTH])
 * \brief Get the value of the initial PAC stored in the device. This
 * value is used when the device is registered for the first time on
 * the backend.
 *
 * \param[in]  none
 * \param[out] sfx_u8 *initial_pac               Pointer to initial PAC
 *
 * \retval SFX_ERR_NONE:                         No error
 * \retval MCU_ERR_API_GET_PAC:                  Error when getting initial PAC
 **********************************************************************************************************************************/
sfx_u8 MCU_API_get_initial_pac(sfx_u8 initial_pac[PAC_LENGTH]){
	char info[128];
	unsigned int infoSize = 128;
	unsigned int i;

	MCURFDrivers_FFSReadInfo((unsigned char*) info, &infoSize);
	for(i = 0; i < PAC_LENGTH; i++){
		initial_pac[PAC_LENGTH-1-i] = info[ID_LENGTH + i];
	}

	return SFX_ERR_NONE;
}
