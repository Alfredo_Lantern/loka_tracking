//**********************************************************************************************************************************
// Filename: mcu_api.c
// Date: 18.12.2017
// Author: Hugo Silva & Luis Rosado
// Company: LOKA SYSTEMS
// Version: 1.0
// Description: Provides mcu functionality access to the SIGFOX library. Built from scratch taking into account the mcu_api.h
//				from SIGFOX library
//**********************************************************************************************************************************


//**********************************************************************************************************************************
//                                                      Include Section
//**********************************************************************************************************************************
#include "include/rf_api.h"
#include "include/mcu_api.h"
#include "include/MCU_RF_Wrapper.h"
#include "LOKA_RTC.h"
#include <stdint.h>
#include <stdio.h>

#include "driver/timer.h"
#include "string.h"

#define FOR_ALL
//#define FOR_ETSI
//#define FOR_FCC
//#define FOR_ARIB

#if defined(FOR_FCC) || defined(FOR_ALL)
#include "include/fifo_ramps_fcc.h"
#endif

#if defined(FOR_ETSI) || defined(FOR_ARIB) || defined(FOR_ALL)
#include "include/fifo_ramps_etsi.h"
#endif


//**********************************************************************************************************************************
//                                                    	 Define Section
//**********************************************************************************************************************************

#define PA_CPS_PIN 0
#define PA_CTX_PIN 1
#define S2LP_INTR_PIN 3

typedef struct{										// Holds the ramp up/down profiles as well as other configurations
  const uint8_t fifo_ramp_fast[82];
  const uint8_t fifo_const_fast[82];
  const uint8_t fifo_start_ramp_down_1[82];
  const uint8_t fifo_start_ramp_down_2[82];
  const uint8_t fifo_start_ramp_up_1[82];
  const uint8_t fifo_start_ramp_up_2[66];
  const uint8_t fdev_neg;
  const uint8_t fdev_pos;
  const uint8_t ramp_start_duration;
  const uint8_t min_power;
  const uint8_t max_power;
}bpsk_ramps_t;

typedef enum{										// State of the library
  ST_MANUF_STATE_IDLE  =  0,
  ST_MANUF_STATE_TX,
  ST_MANUF_STATE_RX,
  ST_MANUF_STATE_WAIT_TIMER,
  ST_MANUF_STATE_WAIT_CLEAR_CH
} st_manuf_state_t;

typedef enum{										// State within the current transmission
  ST_TX_STATE_NONE  =  0,
  ST_TX_STATE_RAMP_UP_1,
  ST_TX_STATE_RAMP_UP_2,
  ST_TX_STATE_DATA,
  ST_TX_STATE_RAMP_DOWN_1,
  ST_TX_STATE_RAMP_DOWN_2,
  ST_TX_STATE_RAMP_DOWN_3,
  ST_TX_STATE_STOP
} st_tx_state_t;

typedef enum{										// State of the S2LP registers, configured for transmission or reception
  TX_MODULATION = 0,
  RX_MODULATION
} st_sfx_mod_t;

#define CMD_TX							0x60		//S2-LP state command codes
#define CMD_RX							0x61
#define CMD_READY						0x62
#define CMD_STANDBY						0x63
#define CMD_SLEEP						0x64
#define CMD_LOCKRX						0x65
#define CMD_LOCKTX						0x66
#define CMD_SABORT						0x67
#define CMD_FLUSHRXFIFO					0x71
#define CMD_FLUSHTXFIFO					0x72
#define DIG_DOMAIN_XTAL_THRESH			30000000

																// SPI control functions
#define CMD_STROBE_TX()											priv_ST_MANUF_CmdStrobe(CMD_TX)
#define CMD_STROBE_RX()											priv_ST_MANUF_CmdStrobe(CMD_RX)
#define CMD_STROBE_SRES()										priv_ST_MANUF_CmdStrobe(CMD_SRES)
#define CMD_STROBE_SABORT()										priv_ST_MANUF_CmdStrobe(CMD_SABORT)
#define CMD_STROBE_READY()										priv_ST_MANUF_CmdStrobe(CMD_READY)
#define CMD_STROBE_SLEEP()										priv_ST_MANUF_CmdStrobe(CMD_SLEEP)
#define CMD_STROBE_LOCKRX()										priv_ST_MANUF_CmdStrobe(CMD_LOCKRX)
#define CMD_STROBE_LOCKTX()										priv_ST_MANUF_CmdStrobe(CMD_LOCKTX)
#define CMD_STROBE_FRX()										priv_ST_MANUF_CmdStrobe(CMD_FLUSHRXFIFO)
#define CMD_STROBE_FTX()										priv_ST_MANUF_CmdStrobe(CMD_FLUSHTXFIFO)
#define priv_ST_MANUF_ReadFifo(N_BYTES, BUFFER)					priv_ST_MANUF_ReadRegisters(0xFF, N_BYTES, BUFFER)
#define priv_ST_MANUF_SpiRaw_(N_BYTES,BUF_IN,BUF_OUT,BLOCK)     MCURFDrivers_SpiRaw(N_BYTES,BUF_IN,BUF_OUT)
#define priv_ST_MANUF_SpiRaw(N_BYTES,BUF_IN,BUF_OUT)            MCURFDrivers_SpiRaw(N_BYTES,BUF_IN,BUF_OUT)

#define ST_RF_ERR_API_ERROR				(sfx_u8)(0x01) 	// Error on ST_RF_API

typedef struct{										// Holds the complete information about the API status
	struct{											// GPIO function structure: collects all the info to set the device GPIO function
		uint8_t gpio_tx_rx_pin;
		uint8_t gpio_rx_pin;
		uint8_t gpio_tx_pin;
		uint8_t gpio_irq_pin;
	}gpio_function_struct;

	struct{											// TX state structure: collects all the info needed to track the status of the transmission
		st_tx_state_t tx_state;
		uint16_t data_to_send_size;
		uint16_t byte_index;
		uint16_t bit_index;
		uint8_t* data_to_send;
	}tx_packet_struct;

	uint8_t last_rssi_reg;
	volatile uint32_t xtal_lib;
	volatile uint32_t priv_xtal_freq;
	volatile int32_t rf_offset;
	volatile uint8_t smps_mode;
	volatile uint8_t tcxo_flag;
	volatile uint8_t pa_flag;
	volatile uint8_t tim_started;
	volatile uint8_t s2lp_irq_raised;
	volatile uint8_t api_timer_raised, api_timer_channel_clear_raised;
	sfx_modulation_type_t radio_conf;
	volatile st_manuf_state_t manuf_state;
	int16_t power_reduction;
	int8_t rssi_offset;

	#if defined(FOR_ARIB) || defined(FOR_ALL)		// The tx_is_ready flag is used for CS (ARIB only) and is used in the rf_init for SFX_RF_MODE_CS_RX
	uint8_t tx_is_ready;							// When the rf_init is called with SFX_RF_MODE_CS_RX, configure also the TX to be faster in case of TX
	uint8_t carrier_sense_tim_nested;
	uint32_t timer_when_stopped,static_time_duration_in_ms;
	#endif

	const uint8_t *zeroes;							// Zeroes to send null power
													// Ramps structures for BPSK modulation
	#if defined(FOR_ETSI) || defined(FOR_ARIB) || defined(FOR_ALL)
	const bpsk_ramps_t* etsi_bpsk_ramps;
	#endif
	#if defined(FOR_FCC) || defined(FOR_ALL)
	const bpsk_ramps_t* fcc_bpsk_ramps;
	#endif

	uint8_t aux_fifo_ramp_fast[82];					// Auxiliary buffer to manage the phase change and manage the power change

	const bpsk_ramps_t* bpsk_ramps;					// Pointers to the ramps to be used
} st_manuf_t;

typedef enum{
	SFX_RCZ1   = 0,
	SFX_RCZ2   = 1,
	SFX_RCZ3a  = 2,
	SFX_RCZ3c  = 3,
	SFX_RCZ4   = 4,
	SFX_RCZ5   = 5,
	SFX_RCZ101 = 6,
}rc_zone_t;

//**********************************************************************************************************************************
//                                                    	 Global Variables
//**********************************************************************************************************************************
#define ST_RF_API_VER_SIZE 4							// The version is (1.0.0) and last element is representing the flag ETSI(1),FCC(2),ARIB(3),ALL(0)
#if defined(FOR_ALL)
	static const uint8_t ST_RF_API_VER[ST_RF_API_VER_SIZE]  =  {1, 0, 0, 0};
	static const uint8_t ST_RF_API_VER_STR[] =  "V1.0.0.0";
#elif defined(FOR_ETSI)
	static const uint8_t ST_RF_API_VER[ST_RF_API_VER_SIZE]  =  {1, 0, 0, 1};
	static const uint8_t ST_RF_API_VER_STR[] =  "V1.0.0.1";
#elif defined(FOR_FCC)
	static const uint8_t ST_RF_API_VER[ST_RF_API_VER_SIZE]  =  {1, 0, 0, 2};
	static const uint8_t ST_RF_API_VER_STR[] =  "V1.0.0.2";
#elif defined(FOR_ARIB)
	static const uint8_t ST_RF_API_VER[ST_RF_API_VER_SIZE]  =  {1, 0, 0, 3};
	static const uint8_t ST_RF_API_VER_STR[] =  "V1.0.0.3";
#endif


														// This array is used to implement waiting times in the TX FIFO
static const uint8_t zeroes[] = {0,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,\
                                 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

#if defined(FOR_FCC) || defined(FOR_ALL)
static const bpsk_ramps_t s_fcc_bpsk_ramps = {			// Holds the ramps profiles for FCC based radio configurations
	.fifo_ramp_fast = FCC_FIFO_RAMP_FAST,
	.fifo_const_fast = FCC_FIFO_CONST_FAST,
	.fifo_start_ramp_down_1 = FCC_FIFO_START_RAMP_DOWN_1,
	.fifo_start_ramp_down_2 = FCC_FIFO_START_RAMP_DOWN_2,
	.fifo_start_ramp_up_1 = FCC_FIFO_START_RAMP_UP_1,
	.fifo_start_ramp_up_2 = FCC_FIFO_START_RAMP_UP_2,
	.fdev_neg = FCC_FDEV_NEG,
	.fdev_pos = FCC_FDEV_POS,
	.ramp_start_duration = FCC_START_RAMP_DURATION,
	.min_power = FCC_MIN_POWER,
	.max_power = FCC_MAX_POWER,
};
#endif

#if defined(FOR_ETSI) || defined(FOR_ARIB) || defined(FOR_ALL)
static const bpsk_ramps_t s_etsi_bpsk_ramps = {			// Holds the ramps profiles for ETSI based radio configurations
	.fifo_ramp_fast = ETSI_FIFO_RAMP_FAST,
	.fifo_const_fast = ETSI_FIFO_CONST_FAST,
	.fifo_start_ramp_down_1 = ETSI_FIFO_START_RAMP_DOWN_1,
	.fifo_start_ramp_down_2 = ETSI_FIFO_START_RAMP_DOWN_2,
	.fifo_start_ramp_up_1 = ETSI_FIFO_START_RAMP_UP_1,
	.fifo_start_ramp_up_2 = ETSI_FIFO_START_RAMP_UP_2,
	.fdev_neg = ETSI_FDEV_NEG,
	.fdev_pos = ETSI_FDEV_POS,
	.ramp_start_duration = ETSI_START_RAMP_DURATION,
	.min_power = ETSI_MIN_POWER,
	.max_power = ETSI_MAX_POWER,
};
#endif


static st_manuf_t st_manuf = {
#if defined(FOR_FCC) || defined(FOR_ALL)
	.gpio_function_struct.gpio_tx_rx_pin = PA_CTX_PIN,
	.gpio_function_struct.gpio_rx_pin = 0xFF,
	.gpio_function_struct.gpio_tx_pin = PA_CPS_PIN,
#else
	.gpio_function_struct.gpio_tx_rx_pin = 0xFF,
	.gpio_function_struct.gpio_rx_pin = 0xFF,
	.gpio_function_struct.gpio_tx_pin = 0xFF,
#endif
	.gpio_function_struct.gpio_irq_pin = S2LP_INTR_PIN,
	.last_rssi_reg = 0,
	.priv_xtal_freq = 26000000,													// Use this when load capacitors are 2 pF on further productions
//	.priv_xtal_freq = 25997936,													// Use this when load capacitors are 22 pF as on the first design
	.xtal_lib = 0,
	.rf_offset = 0,
	.smps_mode = 0,
	.tcxo_flag = 0,
	.pa_flag = 0,
	.tim_started = 0,
	.s2lp_irq_raised = 0,
	.api_timer_raised = 0,
	.api_timer_channel_clear_raised = 0,
	.manuf_state = ST_MANUF_STATE_IDLE,
	.rssi_offset = 0,																//16 for FCC
	.power_reduction = 12,
//	.power_reduction = 0,
#if defined(FOR_ARIB) || defined(FOR_ALL)
	.tx_is_ready = 0,
	.carrier_sense_tim_nested = 0,
#endif
#if defined(FOR_ETSI) || defined(FOR_ARIB) || defined(FOR_ALL)
	.etsi_bpsk_ramps = &s_etsi_bpsk_ramps,
#endif
#if defined(FOR_FCC) || defined(FOR_ALL)
  .fcc_bpsk_ramps = &s_fcc_bpsk_ramps,
#endif
#if defined(FOR_ETSI) || defined(FOR_ARIB) || defined(FOR_ALL)
  .bpsk_ramps = &s_etsi_bpsk_ramps,
#elif defined(FOR_FCC)
  .bpsk_ramps = &s_fcc_bpsk_ramps,
#endif
};

#define st_manuf_context (&st_manuf)

rc_zone_t rc_zone = SFX_RCZ1;


//**********************************************************************************************************************************
//                                                        Code Section
//**********************************************************************************************************************************


//**********************************************************************************************************************************
// Header: priv_ST_MANUF_SpiRaw_Ramp
// Function: Writes a ramp up/down into the S2LP TX FIFO buffer
//**********************************************************************************************************************************
static void priv_ST_MANUF_SpiRaw_Ramp(uint8_t n_bytes, uint8_t* buff_in, uint8_t* buff_out, uint8_t blocking){
	uint32_t i;
	uint8_t fifo_buff[82];															// 82 is the maximum rampo size

	if(st_manuf_context->power_reduction != 0 && buff_in != zeroes){
		fifo_buff[0] = buff_in[0];
		fifo_buff[1] = buff_in[1];
		for(i = 2; i < n_bytes; i++){
			if(((i%2) !=  0) && (buff_in[i] + st_manuf_context->power_reduction < st_manuf_context->bpsk_ramps->min_power))
				fifo_buff[i] = buff_in[i] + st_manuf_context->power_reduction;
			else
				fifo_buff[i] = buff_in[i];
		}
		priv_ST_MANUF_SpiRaw(n_bytes,fifo_buff,buff_out);
	}else{

		priv_ST_MANUF_SpiRaw(n_bytes,buff_in,buff_out);
	}
}


//**********************************************************************************************************************************
// Header: priv_ST_MANUF_CmdStrobe
// Function: Issue a strobe control command to the s2LP
//**********************************************************************************************************************************
void priv_ST_MANUF_CmdStrobe(uint8_t cmd){
	uint8_t tx_spi_buffer[2];
	tx_spi_buffer[0] = 0x80;
	tx_spi_buffer[1] = cmd;
	priv_ST_MANUF_SpiRaw(2, tx_spi_buffer, NULL);
}


//**********************************************************************************************************************************
// Header: priv_ST_MANUF_WriteRegisters
// Function: Writes a specific registers set on the S2LP
//**********************************************************************************************************************************
void priv_ST_MANUF_WriteRegisters(uint8_t address, uint8_t n_bytes, uint8_t* buffer){
	uint8_t tx_spi_buffer[256];
	uint32_t i;

	tx_spi_buffer[0] = 0x00;
	tx_spi_buffer[1] = address;
	for(i = 0; i < n_bytes; i++){
		tx_spi_buffer[i+2] = buffer[i];
	}
	priv_ST_MANUF_SpiRaw(n_bytes+2, tx_spi_buffer, NULL);
}


//**********************************************************************************************************************************
// Header: priv_ST_MANUF_ReadRegisters
// Function: Reads a specific registers set on the S2LP
//**********************************************************************************************************************************
void priv_ST_MANUF_ReadRegisters(uint8_t address, uint8_t n_bytes, uint8_t* buffer){
	uint8_t rx_spi_buffer[256];
	uint8_t tx_spi_buffer[256];
	uint32_t i;

	tx_spi_buffer[0] = 0x01;
	tx_spi_buffer[1] = address;

	for(i = 0; i < n_bytes; i++){
		tx_spi_buffer[i+2] = 0xFF;
	}
	priv_ST_MANUF_SpiRaw(n_bytes+2,tx_spi_buffer,rx_spi_buffer);
	for(i = 0; i < n_bytes; i++){
		buffer[i] = rx_spi_buffer[i+2];
	}
}


//**********************************************************************************************************************************
// Header: priv_ST_MANUF_ReadRegisters
// Function: Reads a specific registers set on the S2LP
//**********************************************************************************************************************************
void priv_ST_MANUF_DumpRegisters(){
	uint8_t values[256];
	uint32_t address;
	char buffer[32];

	for(address = 0; address < 256; address++){
		priv_ST_MANUF_ReadRegisters(address, 1, values+address);
	}
	MCURFDrivers_Debug("RF_API:\t\t S2LP registers dump");
	for(address = 0; address < 256; address++){
		sprintf(buffer,"%3d,%3d ", address, values[address]);
		MCURFDrivers_Debug(buffer);
	}
}


//**********************************************************************************************************************************
// Header: priv_ST_MANUF_ReadRegisters
// Function: Reads a specific registers set on the S2LP
//**********************************************************************************************************************************
uint8_t valuesHistory[256];
void priv_ST_MANUF_DumpRegistersHistory(){
	uint32_t address;

	for(address = 0; address < 256; address++){
		priv_ST_MANUF_ReadRegisters(address, 1, valuesHistory+address);
	}
}


//**********************************************************************************************************************************
// Header: priv_ST_MANUF_ReadRegisters
// Function: Reads a specific registers set on the S2LP
//**********************************************************************************************************************************
void priv_ST_MANUF_DumpRegistersHistoryPrint(){
	uint32_t address;
	char buffer[32];

	MCURFDrivers_Debug("RF_API:\t\t S2LP registers dump");
	for(address = 0; address < 256; address++){
		sprintf(buffer,"%3d,%3d ", address, valuesHistory[address]);
		MCURFDrivers_Debug(buffer);
	}
}


//**********************************************************************************************************************************
// Header: privSetXtalFrequency
// Function: Matches the firmware with the S2LP oscillator frequency
//**********************************************************************************************************************************
static void privSetXtalFrequency(uint32_t lXtalFrequency){
	st_manuf_context->priv_xtal_freq  =  lXtalFrequency;
}


//**********************************************************************************************************************************
// Header: privGetXtalFrequency
// Function: Retrieved the S2LP oscillator frequency
//**********************************************************************************************************************************
static uint32_t privGetXtalFrequency(void){
	return st_manuf_context->priv_xtal_freq;
}


//**********************************************************************************************************************************
// Header: priv_ST_MANUF_tx_rf_init
// Function: Initializes the S2LP in TX state
//**********************************************************************************************************************************
static void priv_ST_MANUF_tx_rf_init(void){
	uint8_t tmp;

	tmp = 0x04;												// TX in direct via FIFO mode
	priv_ST_MANUF_WriteRegisters(0x30, 1, &tmp);
	tmp = 0x07;
	priv_ST_MANUF_WriteRegisters(0x62, 1, &tmp);
	priv_ST_MANUF_ReadRegisters(0x63, 1, &tmp);				// Disable newmode? No clue what does it means
	tmp &= 0xFD;
	priv_ST_MANUF_WriteRegisters(0x63, 1, &tmp);
	tmp = 0xD7;
	priv_ST_MANUF_WriteRegisters(0x65, 1, &tmp);
	tmp = 0x9C;												// SMPS switch to 5.5MHz
	priv_ST_MANUF_WriteRegisters(0x76, 1, &tmp);
	tmp = 0x28;
	priv_ST_MANUF_WriteRegisters(0x77, 1, &tmp);
	tmp = 0xC8;												// SMPS normal
	priv_ST_MANUF_WriteRegisters(0x64, 1, &tmp);
	tmp = 48;												// FIFO almost empty threshold to 48 bytes
	priv_ST_MANUF_WriteRegisters(0x3F, 1, &tmp);
	#if defined(FOR_ARIB) || defined(FOR_ALL)
	st_manuf_context->tx_is_ready = 1;
	#endif
}


//**********************************************************************************************************************************
// Header: priv_ST_MANUF_tx_rf_dbpsk_init
// Function: Initializes the S2LP for DBPSK TX modulation
//**********************************************************************************************************************************
static void priv_ST_MANUF_tx_rf_dbpsk_init(sfx_modulation_type_t type){
	uint32_t f_dig = privGetXtalFrequency();
	uint16_t dr_m = 0;
	uint8_t mod_e = 0;
	uint8_t fdev_e = 0;
	uint8_t fdev_m = 0;
	uint8_t regs[3];
	uint64_t tgt1 = 0;
	uint64_t tgt2 = 0;
	uint8_t i;

	if(f_dig > DIG_DOMAIN_XTAL_THRESH){
		f_dig >>=  1;
	}

	#if defined(FOR_ETSI) || defined(FOR_ARIB) || defined(FOR_ALL)
	#if defined(FOR_ALL)
	if(type == SFX_DBPSK_100BPS){
	#endif

															// ETSI datarate is 100bps, chip datarate 500 (500*8/40 = 100)
		mod_e = 0x60 | 0x01;								// Modulation is POLAR | DR EXPONENT  =  1
		dr_m = (uint16_t)((uint64_t) 0x1f400000000 / f_dig - 65536);			// dr_num = (2**32)*500
		tgt1 = (uint64_t) f_dig*((uint64_t) dr_m + 65536);	// Understand if we are getting the nearest integer
		tgt2 = (uint64_t) f_dig*((uint64_t) dr_m + 1 + 65536);
		dr_m = ((uint64_t) 0x1f400000000 - tgt1 > tgt2-(uint64_t) 0x1f400000000) ? (dr_m + 1):(dr_m);

		if(privGetXtalFrequency() > DIG_DOMAIN_XTAL_THRESH){
			fdev_e = 0;
			fdev_m = (uint8_t)((uint64_t) 8388608000 / privGetXtalFrequency());	// fdev_num = ((2**22)*2000)
			tgt1 = (uint64_t) privGetXtalFrequency()*((uint64_t) fdev_m);		// Understand if we are getting the nearest integer
			tgt2 = (uint64_t) privGetXtalFrequency()*((uint64_t) fdev_m + 1);
			fdev_m = ((uint64_t) 8388608000 - tgt1 > tgt2 - (uint64_t) 8388608000) ? (fdev_m + 1) : (fdev_m);
		}else{
			fdev_e = 1;
			fdev_m = (uint8_t)((uint64_t) 8388608000 / privGetXtalFrequency() - 256);
			tgt1 = (uint64_t)privGetXtalFrequency()*((uint64_t) fdev_m + 256);		// Understand if we are getting the nearest integer
			tgt2 = (uint64_t)privGetXtalFrequency()*((uint64_t) fdev_m + 1 + 256);
			fdev_m = ((uint64_t) 8388608000 - tgt1 > tgt2 - (uint64_t) 8388608000) ? (fdev_m + 1) : (fdev_m);
		}

		st_manuf_context->pa_flag = 0;

	#if defined(FOR_ALL)
	}
	#endif
	#endif

	#if defined(FOR_FCC) || defined(FOR_ALL)
	#if defined(FOR_ALL)
	else if(type == SFX_DBPSK_600BPS){
	#endif
															// FCC datarate is 600bps - chip datarate 3000 (3000*8/40 = 600)
		mod_e = 0x60 | 0x03;								// Modulation is POLAR | DR EXPONENT = 3

		dr_m = (uint16_t)((uint64_t) 0x2EE00000000 / f_dig - 65536);				// dr_num = (2**30)*3000
		tgt1 = (uint64_t) f_dig*((uint64_t) dr_m + 65536);							// Understand if we are getting the nearest integer
		tgt2 = (uint64_t) f_dig*((uint64_t) dr_m + 1 + 65536);
		dr_m = ((uint64_t) 0x2EE00000000 - tgt1 > tgt2 - (uint64_t) 0x2EE00000000) ? (dr_m+1) : (dr_m);

		if(privGetXtalFrequency()>DIG_DOMAIN_XTAL_THRESH){
			fdev_e = 2;
			fdev_m = (uint8_t) ((uint64_t) 25165824000 / privGetXtalFrequency() - 256);
			tgt1 = (uint64_t) privGetXtalFrequency()*((uint64_t) fdev_m + 256);		// Understand if we are getting the nearest integer
			tgt2 = (uint64_t) privGetXtalFrequency()*((uint64_t) fdev_m + 1 + 256);
			fdev_m = ((uint64_t) 25165824000 - tgt1 > tgt2 - (uint64_t) 25165824000) ? (fdev_m+1) : (fdev_m);
		}else{
			fdev_e = 3;
			fdev_m = (uint8_t)((uint64_t) 12582912000 / privGetXtalFrequency() - 256);
			tgt1 = (uint64_t) privGetXtalFrequency()*((uint64_t) fdev_m + 256);		// Understand if we are getting the nearest integer
			tgt2 = (uint64_t) privGetXtalFrequency()*((uint64_t) fdev_m + 1 + 256);
			fdev_m = ((uint64_t) 12582912000 - tgt1 > tgt2 - (uint64_t) 12582912000) ? (fdev_m + 1) : (fdev_m);
		}

		st_manuf_context->pa_flag = 1;

	#if defined(FOR_ALL)
	}
	#endif
	#endif

	regs[0] = (dr_m >> 8) & 0xFF;							// Write datarate mantissa and exponent
	regs[1] = (dr_m) & 0xFF;
	regs[2] = mod_e;
	priv_ST_MANUF_WriteRegisters(0x0E, 3, regs);

	regs[0] = fdev_e | 0x80;								// Write FDEV mantissa and exponentwrite FDEV mantissa and exponent
	regs[1] = fdev_m;										// Here the exponent is in | with 0x80 to enable the digital smooth of the ramps
	priv_ST_MANUF_WriteRegisters(0x11, 2, regs);

	#if defined(FOR_ALL)
	if(st_manuf_context->pa_flag){
		st_manuf_context->bpsk_ramps = st_manuf_context->fcc_bpsk_ramps;
		st_manuf_context->smps_mode = 0;
		st_manuf_context->power_reduction = 12;
	}else{
		st_manuf_context->bpsk_ramps = st_manuf_context->etsi_bpsk_ramps;
		st_manuf_context->smps_mode = 0;
		st_manuf_context->power_reduction = 0;
	}
	#elif defined(FOR_ETSI) || defined(FOR_ARIB)
	st_manuf_context->bpsk_ramps = st_manuf_context->etsi_bpsk_ramps;
	st_manuf_context->smps_mode = 0;
	st_manuf_context->power_reduction = 0;
	#elif defined(FOR_FCC)
	st_manuf_context->bpsk_ramps = st_manuf_context->fcc_bpsk_ramps;
	st_manuf_context->smps_mode = 0;
	st_manuf_context->power_reduction = 12;
	#endif

	for(i = 0; i < 82; i++)
		st_manuf_context->aux_fifo_ramp_fast[i] = st_manuf_context->bpsk_ramps->fifo_ramp_fast[i];

	if(st_manuf_context->smps_mode > 0 && st_manuf_context->smps_mode < 8){
		regs[0] = (st_manuf_context->smps_mode << 4)  | 0x02;	// 16dBm setting, SMPS to 1.8V -> smps_mode = 7
		priv_ST_MANUF_WriteRegisters(0x79, 1, regs);
		if(st_manuf_context->smps_mode > 4){				// Enable degeneration mode, introduzes pre-distortion
			regs[0] = 0x88;
			priv_ST_MANUF_WriteRegisters(0x64, 1, regs);
		}
	}

	#if defined(FOR_ALL)
	if(st_manuf_context->pa_flag){
		regs[0] = 0x9A;										// CTX = 1, TX Transmit
		regs[1] = 0x9A;										// CPS = 1, PA on
	}else{
		regs[0] = 0x9A;										// CTX = 1, TX Transmit
		regs[1] = 0xA2;										// CPS = 0. PA off
	}
	#elif defined(FOR_FCC)
	regs[0] = 0x9A;											// CTX = 1, TX Transmit
	regs[1] = 0x9A;											// CPS = 1, PA on
	#endif
	priv_ST_MANUF_WriteRegisters(st_manuf_context->gpio_function_struct.gpio_tx_rx_pin, 1, &regs[0]);
	priv_ST_MANUF_WriteRegisters(st_manuf_context->gpio_function_struct.gpio_tx_pin, 1, &regs[1]);

}


//**********************************************************************************************************************************
// Header: priv_ST_MANUF_Transmission_Tick
// Function: Updates the ramp being generated, called in interrupt mode
//**********************************************************************************************************************************
static void priv_ST_MANUF_Transmission_Tick(void){

	switch(st_manuf_context->tx_packet_struct.tx_state){
																			// When the state machine is here means that we have
																			// loaded the very first part of the RAMP_UP
																			// and we need to load the second part (the values of
																			// this part is stored into the variable into the fifo_start_ramp_up_2)
		case ST_TX_STATE_RAMP_UP_2:{
			priv_ST_MANUF_SpiRaw_Ramp(66, (uint8_t*) st_manuf_context->bpsk_ramps->fifo_start_ramp_up_2, NULL, 1);
																			// Prepare the next state through the following operations
			st_manuf_context->tx_packet_struct.byte_index = 0;				// Need to take the first byte (index 0 of the data array)
			st_manuf_context->tx_packet_struct.bit_index = 7;				// Starting from the most significative bit (bit 7)
			st_manuf_context->tx_packet_struct.tx_state = ST_TX_STATE_DATA;	// Set state to TX_STATE DATA and start modulate the data bits next time
			}
			break;
		case ST_TX_STATE_DATA:{
																			// Extract the bit to modulate from the array pointed by data_to_send
			uint8_t bit = (st_manuf_context->tx_packet_struct.data_to_send[st_manuf_context->tx_packet_struct.byte_index]>>st_manuf_context->tx_packet_struct.bit_index)&0x01;
			if(bit == 0){													// Bit '0' should be represented by a phase inversion
																			// Give FDEV a peak in the FDEV_PEAK position, value should be the opposite of the last one
																			// Change the auxiliary buffer in RAM
				st_manuf_context->aux_fifo_ramp_fast[82-32] = (st_manuf_context->aux_fifo_ramp_fast[82-32] == st_manuf_context->bpsk_ramps->fdev_neg)?(st_manuf_context->bpsk_ramps->fdev_pos):(st_manuf_context->bpsk_ramps->fdev_neg);
				priv_ST_MANUF_SpiRaw_Ramp(82, st_manuf_context->aux_fifo_ramp_fast, NULL, 1);
			}else{
																			// Bit '1' give no change in phase or power
				priv_ST_MANUF_SpiRaw_Ramp(82, (uint8_t*) st_manuf_context->bpsk_ramps->fifo_const_fast, NULL, 1);
			}

			if(st_manuf_context->tx_packet_struct.bit_index == 0){			// Prepare the next data, check if we need to change byte
				st_manuf_context->tx_packet_struct.bit_index = 7;			// If so, restore the bit "pointer" to the maximum
				st_manuf_context->tx_packet_struct.byte_index++;			// And increase the byte pointer
																			// Check if data is over
				if(st_manuf_context->tx_packet_struct.byte_index == st_manuf_context->tx_packet_struct.data_to_send_size){
					st_manuf_context->tx_packet_struct.byte_index = 0;		// Move the state machine to the RAMP_DOWN_1 state
					st_manuf_context->tx_packet_struct.tx_state = ST_TX_STATE_RAMP_DOWN_1;
				}
			}else{
				st_manuf_context->tx_packet_struct.bit_index--;				// If we don't need to change byte just decrease the bit pointer
			}
			}
			break;
		case ST_TX_STATE_RAMP_DOWN_1:
																			// Store the 1st part of the TX RAMP DOWN pattern
			priv_ST_MANUF_SpiRaw_Ramp(82, (uint8_t*) st_manuf_context->bpsk_ramps->fifo_start_ramp_down_1, NULL, 1);
																			// Move the state to store the second part
			st_manuf_context->tx_packet_struct.tx_state = ST_TX_STATE_RAMP_DOWN_2;
			break;
		case ST_TX_STATE_RAMP_DOWN_2:
																			// Store the 2nd part of the TX RAMP DOWN pattern
			priv_ST_MANUF_SpiRaw_Ramp(82, (uint8_t*) st_manuf_context->bpsk_ramps->fifo_start_ramp_down_2, NULL, 1);
																			// Move the state to store the third part
			st_manuf_context->tx_packet_struct.tx_state = ST_TX_STATE_RAMP_DOWN_3;
			break;
		case ST_TX_STATE_RAMP_DOWN_3:
																			// Notice that the 3rd part of the ramp is just a
																			// sequence of zeroes used as a padding to wait the
																			// fifo_ramp_down_2 is over
			priv_ST_MANUF_SpiRaw_Ramp(66, (uint8_t*)zeroes, NULL, 1);

																			// Next state will be TX_STATE_STOP to shut off
			st_manuf_context->tx_packet_struct.tx_state = ST_TX_STATE_STOP;
			break;
		case ST_TX_STATE_STOP:
			CMD_STROBE_SABORT();											// Give the ABORT command to stop the transmitter
			CMD_STROBE_FTX();												// Give the FLUSH_TX command to flush the TX FIFO
																			// Disable the IRQ on the MCU side
			MCURFDrivers_GpioIRQ(st_manuf_context->gpio_function_struct.gpio_irq_pin, SFX_FALSE, SFX_TRUE);
			st_manuf_context->tx_packet_struct.tx_state = ST_TX_STATE_NONE;	// Reset the state machine state
																			// notify to the MANUF_API_rf_send that we transmission has finished
			st_manuf_context->s2lp_irq_raised = 1;
			break;
		case ST_TX_STATE_NONE:
		default:
			break;
	}
}


//**********************************************************************************************************************************
// Header: priv_ST_MANUF_rf_modulation_dbpsk
// Function: Sends a DBPSK frame
//**********************************************************************************************************************************
static void priv_ST_MANUF_rf_modulation_dbpsk(uint8_t* stream, uint8_t size){
	uint8_t tmp;

	st_manuf_context->tx_packet_struct.data_to_send = stream;				// Save the current pointer to data and size */
	st_manuf_context->tx_packet_struct.data_to_send_size = size;

	CMD_STROBE_FTX();														// TX FIFO flush

	priv_ST_MANUF_SpiRaw_Ramp(18, (uint8_t*) zeroes, NULL, 0);				// Fill the 1st part of the FIFO
	priv_ST_MANUF_SpiRaw_Ramp(82, (uint8_t*) st_manuf_context->bpsk_ramps->fifo_start_ramp_up_1, NULL, 0);

																			// All the FIFO management is done in the priv_ST_MANUF_Transmission_Tick,
    																		// invoked by the ST_MANUF_S2LP_Exti_CB() ISR
																			// Use the s2lp_irq_raised flag to detect the transmission end
	tmp = 0x32;																// GPIO interrupt on S2-LP side
	priv_ST_MANUF_WriteRegisters(st_manuf_context->gpio_function_struct.gpio_irq_pin, 1, &tmp);

	// S2-LP IRQ on MCU side
	MCURFDrivers_GpioIRQ(st_manuf_context->gpio_function_struct.gpio_irq_pin, SFX_TRUE, SFX_TRUE);

	st_manuf_context->manuf_state = ST_MANUF_STATE_TX;						// Change the manuf_state to TX
	st_manuf_context->tx_packet_struct.tx_state = ST_TX_STATE_RAMP_UP_2;	// Next TX state is the 2nd part of the ramp-up

	st_manuf_context->s2lp_irq_raised = 0;

	CMD_STROBE_TX();

//	MCU_API_timer_start(3);													// This was a fail-safe timeout timer in case the S2LP SPI stucks

	st_manuf_context->api_timer_raised = 0;
	while(!st_manuf_context->s2lp_irq_raised){
		st_manuf_context->api_timer_raised = MCU_API_timer_wait(10);
	}

	st_manuf_context->s2lp_irq_raised = 0;									// Transmission has finished
	st_manuf_context->manuf_state = ST_MANUF_STATE_IDLE;					// Resets the state to IDLE

}


//**********************************************************************************************************************************
// Header: priv_ST_MANUF_rx_rf_init
// Function: Initializes the S2LP for GFSK RX modulation
//**********************************************************************************************************************************
static void priv_ST_MANUF_rx_rf_init(void){
	uint64_t tgt1,tgt2;
	uint32_t f_dig = privGetXtalFrequency();
	uint16_t dr_m;
	uint8_t mod_e,fdev_e,fdev_m,tmp;
	uint8_t regs[3];
	uint8_t if_regs[2];
	uint8_t pckt_setting[] = {0,0,0,0,15};									// packet setting registers

	if(f_dig > DIG_DOMAIN_XTAL_THRESH){
		f_dig >>=  1;
	}

	mod_e = 0x20 | 0x01;													// Modulation is 2GFSK01 | DR EXPONENT = 1
	dr_m = (uint16_t)((uint64_t) 0x25800000000/f_dig - 65536);				// dr_num = (2**32)*600
	tgt1 = (uint64_t) f_dig*((uint64_t) dr_m + 65536);						// Understand if we are getting the nearest integer
	tgt2 = (uint64_t) f_dig*((uint64_t) dr_m + 1 + 65536);
	dr_m = ((uint64_t) 0x25800000000 - tgt1 > tgt2 - (uint64_t) 0x25800000000) ? (dr_m+1) : (dr_m);
	fdev_e = 0;
	fdev_m = (uint8_t)((uint64_t) 3355443200 / privGetXtalFrequency());		// fdev_num = ((2**22)*800)
	tgt1 = (uint64_t) privGetXtalFrequency()*((uint64_t) fdev_m);			// Understand if we are getting the nearest integer
	tgt2 = (uint64_t) privGetXtalFrequency()*((uint64_t) fdev_m + 1);
	fdev_m = ((uint64_t) 3355443200 - tgt1 > tgt2 - (uint64_t) 3355443200) ? (fdev_m + 1) : (fdev_m);
	if_regs[0] = (uint8_t)(((uint64_t) 7372800000/privGetXtalFrequency()) - 100);
	if_regs[1] = (uint8_t)(((uint64_t) 7372800000/f_dig)-100);
	priv_ST_MANUF_WriteRegisters(0x09, 2, if_regs);

	regs[0] = (dr_m >> 8) & 0xFF;											// Write datarate mantissa and exponent
	regs[1] = (dr_m) & 0xFF;
	regs[2] = mod_e;
	priv_ST_MANUF_WriteRegisters(0x0E, 3, regs);

	regs[0] = fdev_e;														// Write FDEV mantissa and exponent
	regs[1] = fdev_m;
	priv_ST_MANUF_WriteRegisters(0x11, 2, regs);

	tmp = 0x88;																// Channel filter
	priv_ST_MANUF_WriteRegisters(0x13, 1, &tmp);

	priv_ST_MANUF_WriteRegisters(0x2E, 5, pckt_setting);					// packet settings (CRC, FEC, WHIT, ...) + packet length to 15

	pckt_setting[0] = 0x40;													// SYNC length
	priv_ST_MANUF_WriteRegisters(0x2B, 1, pckt_setting);

	pckt_setting[0] = 0x27;													// SYNC with 16 bits 0xB227
	pckt_setting[1] = 0xB2;
	priv_ST_MANUF_WriteRegisters(0x35, 2, pckt_setting);

	pckt_setting[0] = 0x40;													// SYNC WORD length is 16bits
	pckt_setting[1] = 0x00;
	priv_ST_MANUF_WriteRegisters(0x39, 2, pckt_setting);

	tmp = 0x00;																// Equalizer control and cs blank
	priv_ST_MANUF_WriteRegisters(0x1f, 1, &tmp);

	tmp = 0x07;																// RSSI threshold
	priv_ST_MANUF_WriteRegisters(0x18, 1, &tmp);

	tmp = 0x70;																// Clock recovery fast
	priv_ST_MANUF_WriteRegisters(0x21, 1, &tmp);

  	tmp = 0x20;																// Clock recovery slow
  	priv_ST_MANUF_WriteRegisters(0x20, 1, &tmp);

  	tmp = 0x00;																// AFC
  	priv_ST_MANUF_WriteRegisters(0x14,1,&tmp);

  	tmp = 0x88;																// SMPS switch to 0x8800
  	priv_ST_MANUF_WriteRegisters(0x76, 1, &tmp);
  	tmp = 0x00;
  	priv_ST_MANUF_WriteRegisters(0x77, 1, &tmp);

	#if defined(FOR_ARIB) || defined(FOR_ALL)
  	st_manuf_context->tx_is_ready = 0;
	#endif

	#if defined(FOR_ALL) || defined(FOR_FCC)
  	tmp = 0xA2;																// CTX = 0, RX LNA Mode
	priv_ST_MANUF_WriteRegisters(st_manuf_context->gpio_function_struct.gpio_tx_rx_pin, 1, &tmp);
	#endif

}


//**********************************************************************************************************************************
// Header: RF_API_IRQ_CB
// Function: Callback for each of the S2LP triggered interrupt
//**********************************************************************************************************************************
void RF_API_IRQ_CB(void* arg){
																			// if not transmitting, notify the API through the
																			// s2lp_irq_raised flag
	if(st_manuf_context->manuf_state != ST_MANUF_STATE_TX){
		st_manuf_context->s2lp_irq_raised = 1;
	}else{
																			// if transmitting, tick the state machine
																			// to fill the TX FIFO according to the actual state
		priv_ST_MANUF_Transmission_Tick();
	}
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 RF_API_init(sfx_rf_mode_t rf_mode)
 * \brief Init and configure Radio link in RX/TX
 *
 * [RX Configuration]
 * To receive Sigfox Frame on your device, program the following:
 *  - Preamble  : 0xAAAAAAAAA
 *  - Sync Word : 0xB227
 *  - Packet of the Sigfox frame is 15 bytes length.
 *
 * \param[in] sfx_rf_mode_t rf_mode         Init Radio link in Tx or RX
 * \param[out] none
 *
 * \retval SFX_ERRNONE:              No error
 * \retval RF_ERR_API_INIT:          Init Radio link error
 **********************************************************************************************************************************/
sfx_u8 RF_API_init(sfx_rf_mode_t rf_mode){
	sfx_u8 tmp;
	int32_t xtal_off;

	#if defined(FOR_ARIB) || defined(FOR_ALL)
	if(rf_mode == SFX_RF_MODE_TX && st_manuf_context->tx_is_ready){
		st_manuf_context->tx_packet_struct.tx_state = ST_TX_STATE_NONE;
		return SFX_ERR_NONE;
	}
	#endif

	if(st_manuf_context->xtal_lib == 0)
		st_manuf_context->xtal_lib = privGetXtalFrequency();

	MCURFDrivers_Shutdown(1);
	MCURFDrivers_Delay(1);
	MCURFDrivers_Shutdown(0);
	MCURFDrivers_Delay(1);

	MCURFDrivers_SpiInit();
	MCURFDrivers_InitIRQ();
	MCURFDrivers_Delay(1);

	if(st_manuf_context->tcxo_flag){
		tmp = 0xB0;
		priv_ST_MANUF_WriteRegisters(0x6D, 1, &tmp);
	}

	MCURFDrivers_GpioIRQ(st_manuf_context->gpio_function_struct.gpio_irq_pin, SFX_FALSE, SFX_FALSE);

	xtal_off = (1000*2097152/36406559)*(st_manuf_context->rf_offset/1000);

	privSetXtalFrequency(st_manuf_context->xtal_lib-xtal_off);

	if(privGetXtalFrequency() < 30000000){
		tmp = 0x55;													// Digital divider, set to 1 if xtal < 30MHz so that the divider is disabled
		priv_ST_MANUF_WriteRegisters(0x6C, 1, &tmp);
	}

	#if defined(FOR_FCC) || defined(FOR_ALL)
	uint8_t gpio_conf[3] = {0x9A, 0xA2, 0xA2};
	if(st_manuf_context->gpio_function_struct.gpio_tx_rx_pin != 0xFF)
		priv_ST_MANUF_WriteRegisters(st_manuf_context->gpio_function_struct.gpio_tx_rx_pin, 1, &gpio_conf[0]);
	if(st_manuf_context->gpio_function_struct.gpio_rx_pin != 0xFF)
		priv_ST_MANUF_WriteRegisters(st_manuf_context->gpio_function_struct.gpio_rx_pin, 1, &gpio_conf[1]);
	if(st_manuf_context->gpio_function_struct.gpio_tx_pin != 0xFF)
		priv_ST_MANUF_WriteRegisters(st_manuf_context->gpio_function_struct.gpio_tx_pin, 1, &gpio_conf[2]);
	#endif

	switch(rf_mode){
	#if defined(FOR_ARIB) || defined(FOR_ALL)
		case SFX_RF_MODE_CS200K_RX:
			// configure the S2LP into sensing 200KHz bandwidth to be able to read out RSSI level
			//RSSI level will outputed during the wait_for_clear_channel api
		case SFX_RF_MODE_CS300K_RX:{
			// configure the S2LP into sensing 300KHz bandwidth to be able to read out RSSI level
			// This is possible to make this carrier sense in 2 * 200Kz if you respect the regulation time for listening
			// RSSI level will outputed during the wait_for_clear_channel api
    		uint8_t tmp,if_regs[2];
    		uint32_t f_dig = privGetXtalFrequency();

    		if(f_dig > DIG_DOMAIN_XTAL_THRESH)
    			f_dig >>=  1;
    		if_regs[0]  =  (uint8_t)(((uint64_t) 7372800000/privGetXtalFrequency()) - 100);
    		if_regs[1]  =  (uint8_t)(((uint64_t) 7372800000/f_dig) - 100);
    		priv_ST_MANUF_WriteRegisters(0x09, 2, if_regs);
    		tmp = 0x81; 											// 200kHz+ channel filter
    		if(rf_mode == SFX_RF_MODE_CS300K_RX)
    			tmp = 0x51;  										// 300kHz+ channel filter
    		priv_ST_MANUF_WriteRegisters(0x13, 1, &tmp);
    		tmp = 0x20;
    		priv_ST_MANUF_WriteRegisters(0x2E, 1, &tmp);
			}
			// Intentionally lacks break statement, we should do the case SFX_RF_MODE_TX as well
	#endif
		case SFX_RF_MODE_TX:
			priv_ST_MANUF_tx_rf_init();
			break;
		case SFX_RF_MODE_RX:
			priv_ST_MANUF_rx_rf_init();
			break;
		default:
			break;
	}
	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 RF_API_stop(void)
 * \brief Close Radio link
 *
 * \param[in] none
 * \param[out] none
 *
 * \retval SFX_ERR_NONE:              No error
 * \retval RF_ERR_API_STOP:           Close Radio link error
 **********************************************************************************************************************************/
sfx_u8 RF_API_stop(void){

	CMD_STROBE_SABORT();												// Give the abort command (just for safety)

	MCURFDrivers_SpiClose();

	MCURFDrivers_Shutdown(1);											// Shut down the radio transceiver

	st_manuf_context->s2lp_irq_raised = 0;
	#if defined(FOR_ARIB) || defined(FOR_ALL)
	st_manuf_context->tx_is_ready = 0;
	#endif

	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 RF_API_send(sfx_u8 *stream, sfx_modulation_type_t type, sfx_u8 size)
 * \brief BPSK Modulation of data stream
 * (from synchro bit field to CRC)
 *
 * NOTE : during this function, the voltage_tx needs to be retrieved and stored in 
 *        a variable to be returned into the MCU_API_get_voltage_and_temperature or
 *        MCU_API_get_voltage functions.
 *
 * \param[in] sfx_u8 *stream                Complete stream to modulate
 * \param[in]sfx_modulation_type_t          Type of the modulation ( enum with baudrate and modulation information)
 * \param[in] sfx_u8 size                   Length of stream
 * \param[out] none
 *
 * \retval SFX_ERR_NONE:                    No error
 * \retval RF_ERR_API_SEND:                 Send data stream error
 **********************************************************************************************************************************/
sfx_u8 RF_API_send(sfx_u8 *stream, sfx_modulation_type_t type, sfx_u8 size){

	if(type == SFX_NO_MODULATION || st_manuf_context->manuf_state!=ST_MANUF_STATE_IDLE)
		return RF_ERR_API_SEND;

	priv_ST_MANUF_tx_rf_dbpsk_init(type);								// Configure the transceiver to the modulation (100bps, 600bps)
	priv_ST_MANUF_rf_modulation_dbpsk(stream, size);					// Start the modulation of the data stream

	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 RF_API_start_continuous_transmission (sfx_modulation_type_t type)
 * \brief Generate a signal with modulation type. All the configuration ( Init of the RF and Frequency have already been executed
 *        when this function is called.
 *
 * \param[in] sfx_modulation_type_t         Type of the modulation ( enum with baudrate and modulation information is contained in sigfox_api.h)
 *
 * \retval SFX_ERR_NONE:                                 No error
 * \retval RF_ERR_API_START_CONTINUOUS_TRANSMISSION:     Continuous Transmission Start error 
 **********************************************************************************************************************************/
sfx_u8 RF_API_start_continuous_transmission(sfx_modulation_type_t type){
    sfx_u8 tmp;
    sfx_u8 tmp_stream[2];

	if(st_manuf_context->manuf_state != ST_MANUF_STATE_IDLE)
		return RF_ERR_API_SEND;

	switch (type){
		case SFX_NO_MODULATION:{
																		// configure the RF IC into pure carrier CW : no modulation
																		// the frequency is chosen in the RF_API_change_frequency
			MCURFDrivers_GpioIRQ(st_manuf_context->gpio_function_struct.gpio_irq_pin, SFX_FALSE, SFX_FALSE);

			tmp = 0x77;
			priv_ST_MANUF_WriteRegisters(0x10, 1, &tmp);

			tmp = 1 + st_manuf_context->power_reduction;
			priv_ST_MANUF_WriteRegisters(0x5A, 1, &tmp);

			if(st_manuf_context->smps_mode > 0 && st_manuf_context->smps_mode < 8){
				tmp = (st_manuf_context->smps_mode << 4)  | 0x02;	// 16dBm setting, SMPS to 1.8V -> smps_mode = 7
				priv_ST_MANUF_WriteRegisters(0x79, 1, &tmp);
				if(st_manuf_context->smps_mode > 4){				// Enable degeneration mode, introduzes pre-distortion
					tmp = 0x88;
					priv_ST_MANUF_WriteRegisters(0x64, 1, &tmp);
				}
			}

			tmp_stream[0] = 0x9A;										// PA = ON
			tmp_stream[1] = 0x9A;
			//tmp_stream[0] = 0xA2;										// PA = OFF
			//tmp_stream[1] = 0x9A;
			priv_ST_MANUF_WriteRegisters(PA_CPS_PIN, 2, tmp_stream);

			st_manuf_context->manuf_state = ST_MANUF_STATE_TX;
			priv_ST_MANUF_CmdStrobe(CMD_TX);
			break;
		case SFX_DBPSK_100BPS:
																		// infinite BPSK 100bps modulation
			break;
		case SFX_DBPSK_600BPS:
																		// infinite BPSK 600bps modulation
			break;
		}
		default:
			break;
	}
	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 RF_API_stop_continuous_transmission (void)
 * \brief Stop the current continuous transmisssion 
 *
 * \retval SFX_ERR_NONE:                                 No error
 * \retval RF_ERR_API_STOP_CONTINUOUS_TRANSMISSION:      Continuous Transmission Stop error 
 **********************************************************************************************************************************/
sfx_u8 RF_API_stop_continuous_transmission (void){

	priv_ST_MANUF_CmdStrobe(CMD_SABORT);
	st_manuf_context->manuf_state = ST_MANUF_STATE_IDLE;
	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 RF_API_change_frequency(sfx_u32 frequency)
 * \brief Change synthesizer carrier frequency
 *
 * \param[in] sfx_u32 frequency             Frequency in Hz to program in the radio chipset
 * \param[out] none
 *
 * \retval SFX_ERR_NONE:                    No error
 * \retval RF_ERR_API_CHANGE_FREQ:          Change frequency error
 **********************************************************************************************************************************/
sfx_u8 RF_API_change_frequency(sfx_u32 frequency){
	uint8_t tmp[4];
	uint32_t synth;
	uint64_t tgt1,tgt2;
	uint64_t vcofreq  =  frequency*4;
	uint8_t cp_isel,pfd_split;

	synth = ((uint64_t) 2097152*frequency/privGetXtalFrequency());
	tgt1 = (uint64_t) privGetXtalFrequency()*((uint64_t) synth);			// Understand if we are getting the nearest integer
	tgt2 = (uint64_t) privGetXtalFrequency()*((uint64_t) synth + 1);
	synth = ((uint64_t) 2097152*frequency-tgt1 > tgt2 -(uint64_t) 2097152*frequency) ? (synth + 1) : (synth);

	if(vcofreq >= (uint64_t) 3600000000){								// Set the correct charge pump word
		if(privGetXtalFrequency() > DIG_DOMAIN_XTAL_THRESH){
			cp_isel = 0x02;
			pfd_split = 0;
		}else{
			cp_isel = 0x01;
			pfd_split = 1;
		}
	}else{
		if(privGetXtalFrequency() > DIG_DOMAIN_XTAL_THRESH){
			cp_isel = 0x03;
			pfd_split = 0;
		}else{
			cp_isel = 0x02;
			pfd_split = 1;
		}
	}

	priv_ST_MANUF_ReadRegisters(0x65, 1, tmp);
	tmp[0] &=  (~0x04);
	tmp[0] |=  (pfd_split << 2);
	priv_ST_MANUF_WriteRegisters(0x65, 1, tmp);

	tmp[0]  =  (((uint8_t)(synth>>24)) & 0x0F) | (cp_isel<<5);
	tmp[1]  =  (uint8_t) (synth >> 16);
	tmp[2]  =  (uint8_t) (synth >> 8);
	tmp[3]  =  (uint8_t) synth;

	priv_ST_MANUF_WriteRegisters(0x05, 4, tmp);

	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 RF_API_wait_frame(sfx_u8 *frame, sfx_s16 *rssi, sfx_rx_state_enum_t * state)
 * \brief Get all GFSK frames received in Rx buffer, structure of
 * frame is : Synchro bit + Synchro frame + 15 Bytes.<BR> This function must
 * be blocking state since data is received or timer of 25 s has elapsed.
 *
 * - If received buffer, function returns SFX_ERR_MANUF_NONE then the
 *   library will try to decode frame. If the frame is not correct, the
 *   library will recall RF_API_wait_frame .
 *
 * - If 25 seconds timer has elapsed, function returns
 *   SFX_ERR_MANUF_WAIT_FRAME_TIMEOUT then library will stop receive
 *   frame phase.
 *
 * \param[in] none
 * \param[out] sfx_s8 *frame                  Receive buffer
 * \param[out] sfx_s16 *rssi                  Chipset RSSI
 * Warning: This is the 'raw' RSSI value. Do not add 100 as made
 * in Library versions 1.x.x
 * Resolution: 1 LSB  =  1 dBm
 *
 * \param[out] sfx_rx_state_enum_t state      Indicate the final state of the reception. Value can be TIMEOUT or PASSED
 *                                            if a frame has been received,  as per defined in sigfox_api.h file.
 *
 * \retval SFX_ERR_NONE:                      No error
 * \retval RF_ERR_API_WAIT_FRAME_TIMEOUT:     Wait frame error
 **********************************************************************************************************************************/
sfx_u8 RF_API_wait_frame(sfx_u8 *frame, sfx_s16 *rssi, sfx_rx_state_enum_t * state){
	sfx_u8 n_bytes, tmp[4] = {0,0,0,0};
	sfx_u8 sync_detected = 0;

	tmp[3] = 0x01;														// Enable the RX data ready interrupt
	tmp[2] = 0x20;														// Enable the valid sync interrupt
	priv_ST_MANUF_WriteRegisters(0x50, 4, tmp);

	tmp[0] = 0x02;														// GPIO configuration: IRQ on S2-LP pin
	priv_ST_MANUF_WriteRegisters(st_manuf_context->gpio_function_struct.gpio_irq_pin, 1, tmp);

																		// Enable the GPIO interrupt on the MCU side, falling edge
	MCURFDrivers_GpioIRQ(st_manuf_context->gpio_function_struct.gpio_irq_pin, SFX_TRUE, SFX_FALSE);

	CMD_STROBE_FRX();													// Flush RX fifo

	priv_ST_MANUF_ReadRegisters(0xFA, 4, tmp);							// Cleanup the IRQ status registers

	st_manuf_context->s2lp_irq_raised = 0;								// Cleanup the IRQ flag

	st_manuf_context->manuf_state = ST_MANUF_STATE_RX;					// Set the manuf state to RX

	CMD_STROBE_RX();													// RX strobe command to start listening

	st_manuf_context->api_timer_raised = 0;								// Clean the timer raised flag

	while((!st_manuf_context->api_timer_raised) || sync_detected){
		st_manuf_context->api_timer_raised = MCU_API_timer_wait(10);	// Hook the wait for interrupt function

		if(st_manuf_context->s2lp_irq_raised){
			st_manuf_context->s2lp_irq_raised = 0;
			priv_ST_MANUF_ReadRegisters(0xFA, 4, tmp);					// read the IRQ status register
			if(tmp[3] & 0x01){											// Check the RX_DATA_READY interrupt
																		// Some data is present in the RX fifo
				priv_ST_MANUF_ReadRegisters(0x90, 1, &n_bytes);			// Read the number of bytes available in the RX fifo
				n_bytes = n_bytes & 0x7F;
				priv_ST_MANUF_ReadFifo(n_bytes, frame);					// Read the data stored in the RX fifo

				priv_ST_MANUF_ReadRegisters(0xA2, 1, &st_manuf_context->last_rssi_reg);		// Read the RSSI value
				(*rssi) = ((sfx_s8) st_manuf_context->last_rssi_reg)-146+st_manuf_context->rssi_offset;
				CMD_STROBE_SABORT();									// Stop the reception
																		// Disable the interrupt on the MCU side
				MCURFDrivers_GpioIRQ(st_manuf_context->gpio_function_struct.gpio_irq_pin, SFX_FALSE, SFX_FALSE);
				st_manuf_context->manuf_state = ST_MANUF_STATE_IDLE;

																		// Successful reception
				(*state)  = DL_PASSED;									// Notifies upper layer to send the acknowledge frame

				return SFX_ERR_NONE;
			}
			if(tmp[2] & 0x20){											// Checks the VALID_SYNC interrupt, needed to manage the case when
				sync_detected = 1;										// the SYNC has been detected near the edge of the RX window
			}
		}
	}

	CMD_STROBE_SABORT();												// Stop the reception
																		// Disable the interrupt on the MCU side
	MCURFDrivers_GpioIRQ(st_manuf_context->gpio_function_struct.gpio_irq_pin, SFX_FALSE, SFX_FALSE);
	st_manuf_context->manuf_state = ST_MANUF_STATE_IDLE;				// Reset the ST_MANUF state to IDLE
	(*state) = DL_TIMEOUT;
	return SFX_ERR_INT_GET_RECEIVED_FRAMES_TIMEOUT;						// Reception failed due to RX timeout
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 RF_API_wait_for_clear_channel (sfx_u8 cs_min, sfx_u8 cs_threshold, sfx_rx_state_enum_t * state);
 * \brief This function is used in ARIB standard for the Listen Before Talk
 *        feature. It listens on a specific frequency band initialized through the RF_API_init(), during a sliding window set
 *        in the MCU_API_timer_start_carrier_sense().
 *        If the channel is clear during the minimum carrier sense
 *        value (cs_min), under the limit of the cs_threshold,
 *        the functions returns with SFX_ERR_NONE (transmission
 *        allowed). Otherwise it continues to listen to the channel till the expiration of the 
 *        carrier sense maximum window and then updates the state ( with timeout enum ).
 *      
 * \param[in] none
 * \param[out] sfx_u8 cs_min                  Minimum Carrier Sense time in ms.
 * \param[out] sfx_s8 cs_threshold            Power threshold limit to declare the channel clear.
 *                                            i.e : cs_threshold value -80dBm in Japan / -65dBm in Korea
 * \param[out] sfx_rx_state_enum_t state      Indicate the final state of the carrier sense. Value can be DL_TIMEOUT or PASSED
 *                                            as per defined in sigfox_api.h file.
 *
 * \retval SFX_ERR_NONE:                      No error
 **********************************************************************************************************************************/
sfx_u8 RF_API_wait_for_clear_channel(sfx_u8 cs_min, sfx_s8 cs_threshold, sfx_rx_state_enum_t * state){

	#if defined(FOR_ARIB) || defined(FOR_ALL)
	uint8_t tmp[4] = {0,0,0,0};
	uint32_t f_dig = privGetXtalFrequency();
	if(f_dig>DIG_DOMAIN_XTAL_THRESH) {
		f_dig >>=  1;
	}

	tmp[2] = 0x40;  													// RSSI_ABOVE_TH
	tmp[0] = 0x10; 														// RX timeout

	priv_ST_MANUF_WriteRegisters(0x50,4,tmp);

	priv_ST_MANUF_ReadRegisters(0xFA, 4, tmp);							// Cleanup the IRQ status registers

	tmp[0] = 0x02;
	priv_ST_MANUF_WriteRegisters(st_manuf_context->gpio_function_struct.gpio_irq_pin,1,tmp);

	MCURFDrivers_GpioIRQ(st_manuf_context->gpio_function_struct.gpio_irq_pin, SFX_TRUE, SFX_FALSE);

																		// RSSI threshold setting
	tmp[0] = (uint8_t)((sfx_s16)146+(sfx_s16)cs_threshold+st_manuf_context->rssi_offset);
	priv_ST_MANUF_WriteRegisters(0x18,1,tmp);

	tmp[0] = 0x80;														// Stop the timer at RSSI above threshold
	priv_ST_MANUF_WriteRegisters(0x39,1,tmp);

	tmp[1] = (uint8_t)((((uint64_t)cs_min*f_dig/1000)/1210+1)/255);		// Set the RX timeout
	if(tmp[1] > 1){
		tmp[1]--;
	}else{
		tmp[1] = 1;
	}

	tmp[0] = (uint8_t)((((uint64_t)cs_min*f_dig/1000)/1210+1)/(tmp[1]+1));
	priv_ST_MANUF_WriteRegisters(0x46,2,tmp);

	st_manuf_context->manuf_state = ST_MANUF_STATE_WAIT_CLEAR_CH;

	(*state)  =  DL_PASSED;

	while(1){
		CMD_STROBE_FRX();												// Flush the RX fifo
		priv_ST_MANUF_ReadRegisters(0xFA,4,tmp);						// Clean the IRQ STATUS registers
		st_manuf_context->s2lp_irq_raised = 0;

		CMD_STROBE_RX();												// Enter in RX through the RX command

		st_manuf_context->api_timer_channel_clear_raised = 0;
		while(!(st_manuf_context->s2lp_irq_raised || st_manuf_context->api_timer_channel_clear_raised)){
			st_manuf_context->api_timer_channel_clear_raised = MCU_API_timer_wait(10);
		}

		if(st_manuf_context->s2lp_irq_raised){
			st_manuf_context->s2lp_irq_raised = 0;

			priv_ST_MANUF_ReadRegisters(0xFA,4,tmp);					// Read the IRQ STATUS registers

			if(tmp[2] & 0x40){											// Check the IRQ_RSSI_ABOVE_TH flag
				CMD_STROBE_SABORT();
			}else if(tmp[0] & 0x10){									// Check the IRQ_RX_TIMEOUT flag
				(*state)  =  DL_PASSED;
				break;
			}
		}
		if(st_manuf_context->api_timer_channel_clear_raised){
			st_manuf_context->api_timer_channel_clear_raised = 0;
			(*state)  =  DL_TIMEOUT;
			break;
		}
	}

	if((*state)  ==  DL_PASSED){
		tmp[3] = 0x00;													// Disable all the interrupts
		tmp[2] = 0x00;
		tmp[1] = 0x00;
		tmp[0] = 0x00;
		priv_ST_MANUF_WriteRegisters(0x50,4,tmp);
	}else{
		MCURFDrivers_Shutdown(1);
		st_manuf_context->tx_is_ready = 0;
	}
	MCURFDrivers_GpioIRQ(st_manuf_context->gpio_function_struct.gpio_irq_pin, SFX_FALSE, SFX_FALSE);
	st_manuf_context->manuf_state = ST_MANUF_STATE_IDLE;
	#endif

	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn sfx_u8 RF_API_get_version(sfx_u8 **version, sfx_u8 *size)
 * \brief Returns current RF API version
 *
 * \param[out] sfx_u8 **version                 Pointer to Byte array (ASCII format) containing library version
 * \param[out] sfx_u8 *size                     Size of the byte array pointed by *version
 *
 * \retval SFX_ERR_NONE:                No error
 * \retval RF_ERR_API_GET_VERSION:      Get Version error 
 **********************************************************************************************************************************/
sfx_u8 RF_API_get_version(sfx_u8 **version, sfx_u8 *size){

	*version = ST_RF_API_VER_STR;
	(*size) = strlen((char*) *version);

	return SFX_ERR_NONE;
}


/*!*********************************************************************************************************************************
 * \fn void RF_API_RSSI_OOK_Level(sfx_u8 ch_filter, sfx_s16 *rssi, sfx_u32 frequency)
 * \brief RAW RSSI Level from a specific frequency
 *
 * \param[in] sfx_u8 ch_filter              Channel filter (this value has to be set with values from S2-LP datasheet Table 44)
 * \param[out] sfx_s16 size                 RSSI value. Range value from 0 to -145. 146 is read error
 * \param[in] sfx_u32 frequency				Frequency in Hz (Range between 860MHz and 1030MHz)
 *
 **********************************************************************************************************************************/
void RF_API_RSSI_OOK_Level(sfx_u8 ch_filter, sfx_s16 *rssi, sfx_u32 frequency){
	sfx_u8 tmp[5] = {0,0,0,0,0};

	RF_API_change_frequency(frequency);

	tmp[0] = 0x50;														// No Modulation
	priv_ST_MANUF_WriteRegisters(0x10,1,tmp);

	priv_ST_MANUF_WriteRegisters(0x13, 1, &ch_filter);					// Channel filter

	CMD_STROBE_RX();

	MCURFDrivers_Delay(1);

	priv_ST_MANUF_ReadRegisters(0xEF, 1, &st_manuf_context->last_rssi_reg);
	priv_ST_MANUF_ReadRegisters(0xEF, 1, &st_manuf_context->last_rssi_reg);

	(*rssi) = ((sfx_s8)st_manuf_context->last_rssi_reg)-146+st_manuf_context->rssi_offset;

	CMD_STROBE_SABORT();
}


//**********************************************************************************************************************************
// Header: RF_API_IQ_Sampling
// Function: Triggers the IQ bypass
//**********************************************************************************************************************************
void ST_RF_API_IQ_Sampling(){
	unsigned char tmp;

	CMD_STROBE_TX();

	tmp = 0x03;
	priv_ST_MANUF_WriteRegisters(0x00, 2, &tmp);

	tmp = 0x03;
	priv_ST_MANUF_WriteRegisters(0x01, 2, &tmp);

	tmp = 0xB7;
	priv_ST_MANUF_WriteRegisters(0x02, 2, &tmp);

	tmp = 0x03;
	priv_ST_MANUF_WriteRegisters(0x03, 2, &tmp);

	CMD_STROBE_RX();
}
