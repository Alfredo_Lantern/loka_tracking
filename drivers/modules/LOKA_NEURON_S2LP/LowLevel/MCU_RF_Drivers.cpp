//**********************************************************************************************************************************
// Filename: MCURFDrivers.cpp
// Date: 08.12.2017
// Author: Luis Rosado
// Company: LOKA SYSTEMS
// Version: 1.0
// Description: Implements low functions needed to operate the S2LP and the Sigfox library
//**********************************************************************************************************************************


//**********************************************************************************************************************************
//                                                      Include Section
//**********************************************************************************************************************************
#include "include/MCU_RF_Drivers.h"


//**********************************************************************************************************************************
//                                                     Global Variables
//**********************************************************************************************************************************
bool S2LP_SPI_initialized = false;

spi_nodma_device_handle_t s2lp_spi_handler = NULL;
spi_nodma_bus_config_t s2lp_buscfg;
spi_nodma_device_interface_config_t s2lp_devcfg;

nvs_handle s2lp_nvsHandle;
nvs_handle module_nvsHandle;


//**********************************************************************************************************************************
//                                                        Code Section
//**********************************************************************************************************************************
extern "C" void RF_API_IRQ_CB(void* arg);


//**********************************************************************************************************************************
// Header: initIRQ
// Function: Initializes the shared IRQ between the transceiver and the ESP32
//**********************************************************************************************************************************
void MCURFDrivers::initIRQ(){
}


//**********************************************************************************************************************************
// Header: gpioIRQ
// Function: Enables or Disables the external interrupt on the microcontroller side
//			 The interrupt must be set on the rising or falling edge of the input signal according to the trigger_flag
//			 Pin is the GPIO pin of the S2-LP (integer from 0 to 3)
//**********************************************************************************************************************************
void MCURFDrivers::gpioIRQ(unsigned char pin, unsigned char enable, unsigned char trigger){
	gpio_config_t io_conf;

	if(enable){
		io_conf.pin_bit_mask = ((uint64_t) 1) << (S2LP_GPIO3);				// Bit mask for pins to set
		if(trigger){
		    io_conf.intr_type = GPIO_INTR_POSEDGE;						// Interrupt on rising edge
		}else{
		    io_conf.intr_type = GPIO_INTR_NEGEDGE;						// Interrupt on falling edge
		}
		io_conf.mode = GPIO_MODE_INPUT;										// Pin Mode selection
		io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
		io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
		gpio_config(&io_conf);											// Install gpio ISR service

	    gpio_install_isr_service(0);									// Hook ISR handler for specific gpio pin
	    gpio_isr_handler_add(S2LP_GPIO3, RF_API_IRQ_CB, (void*) NULL);
	    gpio_intr_enable(S2LP_GPIO3);

	}else{
		gpio_intr_disable(S2LP_GPIO3);
	}
}


//**********************************************************************************************************************************
// Header: Shutdown
// Function: Sets the S2LP and the PA on or off
//**********************************************************************************************************************************
void MCURFDrivers::shutdownIC(unsigned char value){
	if(value){															// Turns off the ICs
		drivers::GPIO_STUB::digitalWrite(S2LP_SDN, 1, 1);
		drivers::GPIO_STUB::digitalWrite(FE_SDN, 0, 1);
	}else{
		drivers::GPIO_STUB::digitalWrite(S2LP_SDN, 0, 1);
		drivers::GPIO_STUB::digitalWrite(FE_SDN, 1, 1);
	}

}


//**********************************************************************************************************************************
// Header: Delay
// Function: Wait delay_ms milliseconds in a blocking way
//**********************************************************************************************************************************
void MCURFDrivers::delay(unsigned int delay_millis){
	vTaskDelay(delay_millis / portTICK_PERIOD_MS);	
	//delay_ms(delay_millis);
}


//**********************************************************************************************************************************
// Header: spiInit
// Function: Initializes the S2LP SPI device handler, uses the non DMA version of the SPI driver since the original latency was too
//			 high to ensure proper continuous feeding of the S2Lp buffer
//**********************************************************************************************************************************
void MCURFDrivers::spiInit(){
	esp_err_t ret;
	gpio_config_t io_conf;

	if(S2LP_SPI_initialized == false){

		drivers::GPIO_STUB::pinMode(SPI_CLK, OUTPUT, 0);
		drivers::GPIO_STUB::digitalWrite(SPI_CLK, 0, 0);
		drivers::GPIO_STUB::pinMode(SPI_MOSI, OUTPUT, 0);
		drivers::GPIO_STUB::digitalWrite(SPI_MOSI, 0, 0);
		drivers::GPIO_STUB::pinMode(SPI_MISO, INPUT, 0);
		drivers::GPIO_STUB::pinMode(S2LP_CS, OUTPUT, 0);
		drivers::GPIO_STUB::digitalWrite(S2LP_CS, 1, 0);
		drivers::GPIO_STUB::pinMode(S2LP_GPIO3, INPUT, 0);


		io_conf.intr_type = GPIO_INTR_DISABLE;
		io_conf.pin_bit_mask = (1ULL << S2LP_CS);
		io_conf.mode = GPIO_MODE_OUTPUT ;
		io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
		io_conf.pull_up_en = GPIO_PULLUP_DISABLE;

		ESP_ERROR_CHECK(gpio_config(&io_conf));

		s2lp_buscfg.miso_io_num = SPI_MISO;
		s2lp_buscfg.mosi_io_num = SPI_MOSI;
		s2lp_buscfg.sclk_io_num = SPI_CLK;
		s2lp_buscfg.quadwp_io_num = -1;
		s2lp_buscfg.quadhd_io_num = -1;

		s2lp_devcfg.command_bits = 0;
		s2lp_devcfg.address_bits = 0;
		s2lp_devcfg.dummy_bits = 0;
		s2lp_devcfg.duty_cycle_pos = 0;

		s2lp_devcfg.clock_speed_hz = 6500000;               			// Clock out at 6.5 MHz

		s2lp_devcfg.mode = 0;                                			// Sets SPI mode to 0 (This mode is standard mode 1)
		s2lp_devcfg.spics_io_num = -1;		               				// Redirects the chip select flag
		s2lp_devcfg.spics_ext_io_num = S2LP_CS;
		s2lp_devcfg.queue_size = 1;                         			// Queue 1 transactions at a time

		ret = spi_nodma_bus_add_device(HSPI_NODMA_HOST, &s2lp_buscfg, &s2lp_devcfg, &s2lp_spi_handler);
		assert(ret == ESP_OK);

		spi_nodma_set_speed(s2lp_spi_handler, (uint32_t) s2lp_devcfg.clock_speed_hz);

		ESP_ERROR_CHECK(gpio_pullup_dis(SPI_MISO));
		ESP_ERROR_CHECK(gpio_pulldown_dis(SPI_MISO));

		S2LP_SPI_initialized = true;
	}
}


//**********************************************************************************************************************************
// Header: spiClose
// Function: Releases the S2LP SPI device handler
//**********************************************************************************************************************************
void MCURFDrivers::spiClose(){
	esp_err_t ret;

	if(S2LP_SPI_initialized == true){
		ret = spi_nodma_bus_remove_device(s2lp_spi_handler);
		assert(ret == ESP_OK);
		S2LP_SPI_initialized = false;

		drivers::GPIO_STUB::pinMode(SPI_CLK, OUTPUT, 1);
		drivers::GPIO_STUB::digitalWrite(SPI_CLK, 0, 1);
		drivers::GPIO_STUB::pinMode(SPI_MOSI, OUTPUT, 1);
		drivers::GPIO_STUB::digitalWrite(SPI_MOSI, 0, 1);
		drivers::GPIO_STUB::pinMode(SPI_MISO, INPUT, 1);
		drivers::GPIO_STUB::pinMode(S2LP_CS, OUTPUT, 1);
		drivers::GPIO_STUB::digitalWrite(S2LP_CS, 1, 1);
		drivers::GPIO_STUB::pinMode(S2LP_GPIO3, INPUT, 1);

	}
}


//**********************************************************************************************************************************
// Header: SpiRaw
// Function: Performs a raw SPI operation with the passed input buffer and stores the returned SPI bytes in the output buffer
//**********************************************************************************************************************************
void MCURFDrivers::spiRaw(unsigned char n_bytes, unsigned char* tx_buffer, unsigned char* rx_buffer){
	esp_err_t ret;
	spi_nodma_transaction_t t;

	memset(&t, 0, sizeof(t));       										// Zero out the transaction
	t.tx_buffer = tx_buffer;
	t.rx_buffer = rx_buffer;
	t.length = ((unsigned int) n_bytes)*8;
	t.rxlength = t.length;
	t.flags = 0;

	ret = spi_nodma_transfer_data(s2lp_spi_handler, &t);
	assert(ret == ESP_OK);
}


//**********************************************************************************************************************************
// Header: timerStop
// Function: Encrypts the data passed in the input buffer, uses AES128 CBC
//**********************************************************************************************************************************
void MCURFDrivers::encrypt(unsigned char* buffer_out, unsigned char* buffer_in, unsigned char data_len, unsigned char * key, int use_key){
	unsigned char buffer_tmp[64];
	unsigned char tempIV[16];
	mbedtls_aes_context* ctx;
	//unsigned char sfx_priv_key[16] = {0x01,0x23,0x45,0x67,0x89,0xAB,0xCD,0xEF,0x01,0x23,0x45,0x67,0x89,0xAB,0xCD,0xEF};//ID:fedcba98/4275878552
	//unsigned char sfx_priv_key[16] = {0xA4, 0x21, 0xF4, 0x79, 0xEC, 0x8A, 0x57, 0x7A, 0x71, 0x2C, 0xA8, 0x68, 0x9F, 0x3D, 0x86, 0x6B};
	unsigned char private_key[16] = {};
	//char info[128];
	//unsigned int infoSize = 128;
	//MCURFDrivers::FFSReadInfo((unsigned char*) info, (unsigned long int*) &infoSize);

    char secrets_buff[SFX_SECRETS_BUFF_LEN];
    esp_efuse_read_field_blob(ESP_EFUSE_SIGFOX_SECRETS,
                              (void*)secrets_buff,
                              SFX_SECRETS_FIELD_LEN);

							  	char info[28];
	printf("\n Data: ");
	for (int i = 0; i < 28; i++) {
		printf("%hhx", secrets_buff[i]);
	}
	printf("\n");

	memcpy(private_key, secrets_buff + 12, KEY_LENGTH);

	memcpy(buffer_tmp, buffer_in, data_len);
	memset(tempIV, 0, 16);

	ctx = (mbedtls_aes_context*) malloc(sizeof(mbedtls_aes_context));
	mbedtls_aes_init(ctx);
	if(use_key == 0)
		mbedtls_aes_setkey_enc(ctx, (const unsigned char *) private_key, 128);						// AES 128	encoding
	else
		mbedtls_aes_setkey_enc(ctx, (const unsigned char *) key, 128);

	memset(buffer_out, 0, data_len);
	mbedtls_aes_crypt_cbc(ctx, MBEDTLS_AES_ENCRYPT , data_len, tempIV, (const unsigned char*) buffer_tmp, (unsigned char*) buffer_out);
	free(ctx);
}


//**********************************************************************************************************************************
// Header: FFSReadBlock
// Function: Reads data from FFS partition used by the SigFox library
//**********************************************************************************************************************************
void MCURFDrivers::FFSReadBlock(unsigned char* read_mem, unsigned long int* size){
	/*
	
	//memset(read_mem, 0, *size);
	drivers::NVS nvs_driver;
	nvs_driver.setFlash("page","key","abc");
    char test[*size];
    esp_err_t ret = nvs_driver.getFlash(SFX_STATE_PAGE_NAME,SFX_STATE_KEY_NAME,test);
		if (ret == ESP_OK) {
		memcpy (read_mem,test,*size);
	} else {
		memset(read_mem, 0, *size);
	}
	*/
	int bytes_num;
	drivers::SPIFFS spiffs_driver;
	spiffs_driver.mount(SFX_STATE_PARTITION_LABEL);
	bytes_num = spiffs_driver.readFile(SFX_STATE_PARTITION_LABEL, SFX_STATE_FILE_NAME, *size, 0, (char*) read_mem);
	spiffs_driver.unmount(SFX_STATE_PARTITION_LABEL);
	//if(ret == ESP_FAIL)
	if (bytes_num <= 0)
		memset(read_mem, 0, *size);
}


//**********************************************************************************************************************************
// Header: FFSWriteBlock
// Function: Writes data to FFS partition used by the SigFox library
//**********************************************************************************************************************************
void MCURFDrivers::FFSWriteBlock(unsigned char* write_mem, unsigned long int* size){
	// std::string _write_mem = std::string((char*)write_mem);
	// drivers::NVS nvs_driver;
	// esp_err_t ret = nvs_driver.setFlash(SFX_STATE_PAGE_NAME, SFX_STATE_KEY_NAME,_write_mem);
	drivers::SPIFFS spiffs_driver;
	spiffs_driver.mount(SFX_STATE_PARTITION_LABEL);
	spiffs_driver.writeFile(SFX_STATE_PARTITION_LABEL, SFX_STATE_FILE_NAME, (char*) write_mem, *size, false);
	spiffs_driver.unmount(SFX_STATE_PARTITION_LABEL);
	
}


//**********************************************************************************************************************************
// Header: FFSReadBlock
// Function: Reads data from FFS partition used by the SigFox library
//**********************************************************************************************************************************
void MCURFDrivers::FFSReadInfo(unsigned char* read_mem, unsigned long int* size){
	int bytes_num;
	
	drivers::SPIFFS spiffs_driver;
	spiffs_driver.mount(SFX_INFO_PARTITION_LABEL);
	bytes_num = spiffs_driver.readFile(SFX_INFO_PARTITION_LABEL, SFX_INFO_FILE_NAME, *size, 0, (char*) read_mem);
	spiffs_driver.unmount(SFX_INFO_PARTITION_LABEL);
	if(bytes_num <= 0)
		memset(read_mem, 0, *size);
}


//**********************************************************************************************************************************
// Header: waitForInterrupt
// Function: Waits for the timer or the GPIO interrupts, may be implemented on LPM
//**********************************************************************************************************************************
int MCURFDrivers::waitForInterrupt(void){
	vTaskDelay(10 / portTICK_PERIOD_MS);
	//delay_ms(10);
	return 0;
}


//**********************************************************************************************************************************
// Header: getTemperature
// Function: Returns the device temperature
//**********************************************************************************************************************************
int MCURFDrivers::getTemperature(void){
	return 0;
}


//**********************************************************************************************************************************
// Header: getBatteryVoltageLoad
// Function: Returns the device battery voltage in mV
//**********************************************************************************************************************************
int MCURFDrivers::getBatteryVoltageLoad(void){
	return 0;
}


//**********************************************************************************************************************************
// Header: getBatteryVoltageLoad
// Function: Returns the device battery voltage in mV
//**********************************************************************************************************************************
int MCURFDrivers::getBatteryVoltageOpen(void){
	return 0;
}

