/**
 * @file S2LP_Sigfox.h
 * @author Rafael Pires
 * @author Gabriel Sanchez
 * @author Andres Arias
 * @date 18/10/2017
 * @brief High level driver
 */

#ifndef S2LP_SIGFOX_H_
#define S2LP_SIGFOX_H_

#include <string>
#include <cstdio>
#include "esp_log.h"
#include "esp_efuse.h"
#include "esp_efuse_custom_table.h"
#include "LOKA_GPIO.h"
#include "LOKA_NVS.h"

#ifdef __cplusplus
extern "C" {
#endif


// md5_digest_table 3f8522918fa7bf1f9d82aad6fd8c2f1a
// This file was generated from the file esp_efuse_custom_table.csv. DO NOT CHANGE THIS FILE MANUALLY.
// If you want to change some fields, you need to change esp_efuse_custom_table.csv file
// then run `efuse_common_table` or `efuse_custom_table` command it will generate this file.
// To show efuse_table run the command 'show_efuse_table'.


extern const esp_efuse_desc_t* ESP_EFUSE_SIGFOX_SECRETS[];

#ifdef __cplusplus
}
#endif

extern "C" {
	#include "../SigfoxLib/include/sigfox_types.h"
	#include "../SigfoxLib/include/sigfox_api.h"
}

#define S2LP_TAG			        "S2LP"
#define SFX_CONFIGS_PAGE			"sigfox"
#define SIGFOX_SECRETS_FIELD_LEN    224
#define SIGFOX_SECRETS_BUFF_LEN     28
#define SIGFOX_ID_LEN               8
#define SIGFOX_PAC_LEN              16
#define SIGFOX_DOWNLINK_LEN         8
#define SIGFOX_CW_RCZ1_FREQ         868130000
#define SIGFOX_CW_RCZ2_FREQ         902199999
#define SIGFOX_CW_RCZ4_FREQ         920800000

#ifndef S2LP_SDN
    #define	S2LP_SDN	(gpio_num_t)-1
#endif
#ifndef S2LP_CS
    #define S2LP_CS		(gpio_num_t)-1
#endif
#ifndef S2LP_GPIO3
    #define S2LP_GPIO3	(gpio_num_t)-1
#endif
#ifndef FE_SDN
    #define FE_SDN		(gpio_num_t)-1
#endif

namespace drivers {

    class S2LP {

        public:
            S2LP(void);
            S2LP(const std::string &selected_zone);
            ~S2LP(void);

            /**
             * @brief Enables the Sigfox modem.
             * @retval ESP_OK Sigfox modem up and running.
             * @retval ESP_FAIL Could not start Sigfox modem.
             */
            esp_err_t turnOn(void);

            /**
             * @brief Sends the Sigfox modem to sleep.
             * @retval ESP_OK Sigfox modem turned off.
             * @retval ESP_FAIL Could not turn off Sigfox modem.
             */
            esp_err_t turnOff(void);

            /**
             * @brief Indicates whether the Sigfox modem is on or not.
             * @retval true Sigfox modem is currently up and running.
             * @retval false Sigfox modem is currently sleeping.
             */
            bool isOn(void);

            /**
             * @brief Retrieves the Sigfox ID for the current device.
             * @return Sigfox device ID in hex form.
             */
            std::string getID(void);

            /**
             * @brief Retrieves the Sigfox PAC for the current device.
             * @return Sigfox device PAC in hex form.
             */
            std::string getPAC(void);

            /**
             * @brief Send the provided string in hex ASCII as a Sigfox uplink message.
             * @param[in] data The string containing the data to send.
             * @retval ESP_ERR_INVALID_STATE Sigfox modem is currently sleeping.
             * Call S2LP::turnOn() before calling this function.
             * @retval ESP_OK Message successfully sent.
             * @retval ESP_FAIL Modem failed to send the message.
             */
            esp_err_t sendData(const std::string &data);

            /**
             * @brief Sends the string in hex ASCII as an uplink message, but
             * then requests a downlink message.
             * @param[in] uplink The data to send as a message.
             * @return String containing the data retrieved from the downlink
             * message. NOTE: Empty string means no available downlink.
             */
            std::string sendAndReadData(const std::string &uplink);

            /**
             * @brief Configures the Sigfox modem on Continuous Wave mode.
             * @note This is only needed for certification purposes.
             * @retval ESP_OK Modem entered CW mode.
             * @retval ESP_FAIL Modem failed to enter in CW mode.
             */
            esp_err_t startCW(void);

            /**
             * @brief Stops the Sigfox modem from being on Continuous Wave mode.
             * @note This is only needed for certification purposes.
             * @retval ESP_OK CW mode stopped.
             * @retval ESP_FAIL Modem failed to stop CW mode.
             */
            esp_err_t stopCW(void);

        private:
            bool powerOn;
            std::string zone;
            std::string getSecrets(void);
    };

} /* drivers */

#endif
