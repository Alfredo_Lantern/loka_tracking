/**
 * @file S2LP.cpp
 * @author Rafael Pires
 * @author Gabriel Sanchez
 * @author Andres Arias
 * @date 18/10/2017
 * @brief 
 */

#include "LOKA_NEURON_S2LP.h"
#include "sdkconfig.h"
#include "esp_efuse.h"
#include <assert.h>
#include "esp_efuse_custom_table.h"

// md5_digest_table 3f8522918fa7bf1f9d82aad6fd8c2f1a
// This file was generated from the file esp_efuse_custom_table.csv. DO NOT CHANGE THIS FILE MANUALLY.
// If you want to change some fields, you need to change esp_efuse_custom_table.csv file
// then run `efuse_common_table` or `efuse_custom_table` command it will generate this file.
// To show efuse_table run the command 'show_efuse_table'.

#define MAX_BLK_LEN CONFIG_EFUSE_MAX_BLK_LEN

// The last free bit in the block is counted over the entire file.
#define LAST_FREE_BIT_BLK3 256


static const esp_efuse_desc_t SIGFOX_SECRETS[] = {
    {EFUSE_BLK3, 32, 8}, 	 // Sigfox ID [0],
    {EFUSE_BLK3, 40, 8}, 	 // Sigfox ID [1],
    {EFUSE_BLK3, 48, 8}, 	 // Sigfox ID [2],
    {EFUSE_BLK3, 56, 8}, 	 // Sigfox ID [3],
    {EFUSE_BLK3, 64, 8}, 	 // Sigfox PAC [0],
    {EFUSE_BLK3, 72, 8}, 	 // Sigfox PAC [1],
    {EFUSE_BLK3, 80, 8}, 	 // Sigfox PAC [2],
    {EFUSE_BLK3, 88, 8}, 	 // Sigfox PAC [3],
    {EFUSE_BLK3, 96, 8}, 	 // Sigfox PAC [4],
    {EFUSE_BLK3, 104, 8}, 	 // Sigfox PAC [5],
    {EFUSE_BLK3, 112, 8}, 	 // Sigfox PAC [6],
    {EFUSE_BLK3, 120, 8}, 	 // Sigfox PAC [7],
    {EFUSE_BLK3, 128, 8}, 	 // Sigfox KEY [0],
    {EFUSE_BLK3, 136, 8}, 	 // Sigfox KEY [1],
    {EFUSE_BLK3, 144, 8}, 	 // Sigfox KEY [2],
    {EFUSE_BLK3, 152, 8}, 	 // Sigfox KEY [3],
    {EFUSE_BLK3, 160, 8}, 	 // Sigfox KEY [4],
    {EFUSE_BLK3, 168, 8}, 	 // Sigfox KEY [5],
    {EFUSE_BLK3, 176, 8}, 	 // Sigfox KEY [6],
    {EFUSE_BLK3, 184, 8}, 	 // Sigfox KEY [7],
    {EFUSE_BLK3, 192, 8}, 	 // Sigfox KEY [8],
    {EFUSE_BLK3, 200, 8}, 	 // Sigfox KEY [9],
    {EFUSE_BLK3, 208, 8}, 	 // Sigfox KEY [10],
    {EFUSE_BLK3, 216, 8}, 	 // Sigfox KEY [11],
    {EFUSE_BLK3, 224, 8}, 	 // Sigfox KEY [12],
    {EFUSE_BLK3, 232, 8}, 	 // Sigfox KEY [13],
    {EFUSE_BLK3, 240, 8}, 	 // Sigfox KEY [14],
    {EFUSE_BLK3, 248, 8}, 	 // Sigfox KEY [15],
};

const esp_efuse_desc_t* ESP_EFUSE_SIGFOX_SECRETS[] = {
    &SIGFOX_SECRETS[0],    		// Sigfox ID [0]
    &SIGFOX_SECRETS[1],    		// Sigfox ID [1]
    &SIGFOX_SECRETS[2],    		// Sigfox ID [2]
    &SIGFOX_SECRETS[3],    		// Sigfox ID [3]
    &SIGFOX_SECRETS[4],    		// Sigfox PAC [0]
    &SIGFOX_SECRETS[5],    		// Sigfox PAC [1]
    &SIGFOX_SECRETS[6],    		// Sigfox PAC [2]
    &SIGFOX_SECRETS[7],    		// Sigfox PAC [3]
    &SIGFOX_SECRETS[8],    		// Sigfox PAC [4]
    &SIGFOX_SECRETS[9],    		// Sigfox PAC [5]
    &SIGFOX_SECRETS[10],    		// Sigfox PAC [6]
    &SIGFOX_SECRETS[11],    		// Sigfox PAC [7]
    &SIGFOX_SECRETS[12],    		// Sigfox KEY [0]
    &SIGFOX_SECRETS[13],    		// Sigfox KEY [1]
    &SIGFOX_SECRETS[14],    		// Sigfox KEY [2]
    &SIGFOX_SECRETS[15],    		// Sigfox KEY [3]
    &SIGFOX_SECRETS[16],    		// Sigfox KEY [4]
    &SIGFOX_SECRETS[17],    		// Sigfox KEY [5]
    &SIGFOX_SECRETS[18],    		// Sigfox KEY [6]
    &SIGFOX_SECRETS[19],    		// Sigfox KEY [7]
    &SIGFOX_SECRETS[20],    		// Sigfox KEY [8]
    &SIGFOX_SECRETS[21],    		// Sigfox KEY [9]
    &SIGFOX_SECRETS[22],    		// Sigfox KEY [10]
    &SIGFOX_SECRETS[23],    		// Sigfox KEY [11]
    &SIGFOX_SECRETS[24],    		// Sigfox KEY [12]
    &SIGFOX_SECRETS[25],    		// Sigfox KEY [13]
    &SIGFOX_SECRETS[26],    		// Sigfox KEY [14]
    &SIGFOX_SECRETS[27],    		// Sigfox KEY [15]
    NULL
};



namespace drivers {

    S2LP::S2LP(void)
    {
        ESP_LOGI(S2LP_TAG, "Initializing Sigfox modem .");
        char zone_buff[FLASH_SIZE_MAX + 1];
        drivers::NVS nvs_driver;
        nvs_driver.getFlash(SFX_CONFIGS_PAGE, "rcz", zone_buff);
        std::string selected_zone(zone_buff);
        if (selected_zone.length() == 0) {
            ESP_LOGI(S2LP_TAG, "No zone configuration found. Defaulting to RCZ1");
            this->zone = "RCZ1";
        } else {
            ESP_LOGI(S2LP_TAG, "Configuration zone selected: %s", selected_zone.c_str());
            this->zone = selected_zone;
        }
    }

    S2LP::S2LP(const std::string &selected_zone)
    {
        if (selected_zone == "RCZ1" || selected_zone == "RCZ2" ||
            selected_zone == "RCZ3a" || selected_zone == "RCZ3c" ||
            selected_zone == "RCZ4" || selected_zone == "RCZ5" ||
            selected_zone == "RCZ6") {
            ESP_LOGI(S2LP_TAG, "Selected zone: %s", selected_zone.c_str());
            this->zone = selected_zone;
        } else {
            ESP_LOGE(S2LP_TAG, "Invalid zone provided, loading from flash memory...");
            char zone_buff[FLASH_SIZE_MAX + 1];
            drivers::NVS nvs_driver;
            nvs_driver.getFlash(SFX_CONFIGS_PAGE, "rcz", zone_buff);
            std::string selected_zone(zone_buff);
            if (selected_zone.length() == 0) {
                ESP_LOGI(S2LP_TAG, "No zone configuration found. Defaulting to RCZ1");
                this->zone = "RCZ1";
            } else {
                ESP_LOGI(S2LP_TAG, "Configuration zone selected: %s",
                         selected_zone.c_str());
                this->zone = selected_zone;
            }
        }
    }

    S2LP::~S2LP(void)
    {
        if (this->powerOn)
            this->turnOff();
        ESP_LOGI(S2LP_TAG, "Sigfox modem deinitialized");
    }

    esp_err_t S2LP::turnOn(void)
    {
        sfx_error_t error = SFX_ERR_NONE;
        sfx_rc_t rcZone;
        sfx_u32 configWords[3];

        if (this->zone == "RCZ1") {
            rcZone = RC1;
            error = SIGFOX_API_open(&rcZone);
            if (error == SFX_ERR_NONE) {
                configWords[0] = 0x00000000; configWords[1] = 0x00000000; configWords[2] = 0x00000000;
                error = SIGFOX_API_set_std_config(configWords, true);
            }
        } else if (this->zone == "RCZ2") {
            rcZone = RC2;
            error = SIGFOX_API_open(&rcZone);
            if (error == SFX_ERR_NONE) {
                configWords[0] = 0x000001FF; configWords[1] = 0x00000000; configWords[2] = 0x00000000;
                error = SIGFOX_API_set_std_config(configWords, true);
            }
        } else if (this->zone == "RCZ3a") {
            rcZone = RC3A;
            error = SIGFOX_API_open(&rcZone);
            if (error == SFX_ERR_NONE) {
                configWords[0] = 0x00000003; configWords[1] = 0x00001388; configWords[2] = 0x00000000;
                error = SIGFOX_API_set_std_config(configWords, true);
            }
        } else if (this->zone == "RCZ3c") {
            rcZone = RC3C;
            error = SIGFOX_API_open(&rcZone);
            if (error == SFX_ERR_NONE) {
                configWords[0] = 0x00000003; configWords[1] = 0x00001388; configWords[2] = 0x00000000;
                error = SIGFOX_API_set_std_config(configWords, true);
            }
        } else if (this->zone == "RCZ4") {
            rcZone = RC4;
            error = SIGFOX_API_open(&rcZone);
            if (error == SFX_ERR_NONE) {
                configWords[0] = 0x00000000; configWords[1] = 0xF0000000; configWords[2] = 0x0000001F;
                error = SIGFOX_API_set_std_config(configWords, true);
            }
        } else if (this->zone == "RCZ5") {
            rcZone = RC5;
            error = SIGFOX_API_open(&rcZone);
            if (error == SFX_ERR_NONE) {
                configWords[0] = 0x00000003; configWords[1] = 0x00001388; configWords[2] = 0x00000000;
                error = SIGFOX_API_set_std_config(configWords, true);
            }
        } else if (this->zone == "RCZ6") {
            rcZone = RC6;
            error = SIGFOX_API_open(&rcZone);
            if (error == SFX_ERR_NONE) {
                configWords[0] = 0x00000000; configWords[1] = 0x00000000; configWords[2] = 0x00000000;
                error = SIGFOX_API_set_std_config(configWords, true);
            }
        }
        if (error == SFX_ERR_NONE) {
            ESP_LOGI(S2LP_TAG, "Sigfox modem turned on, running on zone %s",
                               this->zone.c_str());
            this->powerOn = true;
            return ESP_OK;
        } else {
            ESP_LOGE(S2LP_TAG, "Could not turn on Sigfox modem.");
            this->powerOn = false;
            return ESP_FAIL;
        }
    }

    esp_err_t S2LP::turnOff(void)
    {
        sfx_error_t error;
        error = SFX_ERR_NONE;
        error = SIGFOX_API_close();
        if (error == SFX_ERR_NONE) {
            ESP_LOGI(S2LP_TAG, "Sigfox modem went to sleep.");
            this->powerOn = false;
            return ESP_OK;
        }
        ESP_LOGE(S2LP_TAG, "Could not turn off Sigfox modem.");
        return ESP_FAIL;
    }

    bool S2LP::isOn(void)
    {
        return this->powerOn;
    }

    std::string S2LP::getID(void)
    {
        std::string secrets_str = this->getSecrets();
        return secrets_str.substr(0, SIGFOX_ID_LEN);
    }

    std::string S2LP::getPAC(void)
    {
        std::string secrets_str = this->getSecrets();
        return secrets_str.substr(SIGFOX_ID_LEN, SIGFOX_PAC_LEN);
    }

    esp_err_t S2LP::sendData(const std::string &data)
    {
        if (!this->powerOn) {
            ESP_LOGE(S2LP_TAG, "Cannot send message while modem is sleeping.");
            return ESP_ERR_INVALID_STATE;
        }
        unsigned char* buff = (unsigned char*)data.c_str();
        sfx_error_t error;
        error = SIGFOX_API_send_frame((sfx_u8*) buff, (sfx_u8) data.length(), NULL, 2, false);
        if (error == SFX_ERR_NONE) {
            ESP_LOGI(S2LP_TAG, "Message %s successfully sent!", data.c_str());
            return ESP_OK;
        }
        ESP_LOGE(S2LP_TAG, "Could not send message. Error code: 0x%x", error);
        return ESP_FAIL;
    }

    //esp_err_t S2LP::sendAndReadData(unsigned char *buffer, int size, unsigned char *output)
    std::string S2LP::sendAndReadData(const std::string &uplink)
    {
        unsigned char* buff = (unsigned char*)uplink.c_str();
        unsigned char output_buff[SIGFOX_DOWNLINK_LEN];
        char hex_buff[1];
        std::string result = "";
        SIGFOX_API_send_frame((sfx_u8*)buff, (sfx_u8)uplink.length(),
                              (sfx_u8*)output_buff, 2, true);
        for (int i = 0; i < SIGFOX_DOWNLINK_LEN; ++i) {
            
            std::sprintf(hex_buff, "%d", (int)output_buff[i]);
            std::printf("%hhx\n", output_buff[i]);
            result += hex_buff;
        }
        return result;
    }

    esp_err_t S2LP::startCW(void)
    {
        sfx_error_t error = SFX_ERR_NONE;

        if (this->zone == "RCZ1")
            error = SIGFOX_API_start_continuous_transmission(SIGFOX_CW_RCZ1_FREQ,
                                                             SFX_NO_MODULATION);
        else if (this->zone == "RCZ2")
            error = SIGFOX_API_start_continuous_transmission(SIGFOX_CW_RCZ2_FREQ,
                                                             SFX_NO_MODULATION);
        else if (this->zone == "RCZ4")
            error = SIGFOX_API_start_continuous_transmission(SIGFOX_CW_RCZ4_FREQ,
                                                             SFX_NO_MODULATION);
        if (error == SFX_ERR_NONE) {
            ESP_LOGI(S2LP_TAG, "Starting CW test mode for zone %s", this->zone.c_str());
        }
            return ESP_OK;
        ESP_LOGE(S2LP_TAG, "Could not start CW test mode.");
        return ESP_FAIL;
    }

    esp_err_t S2LP::stopCW(void)
    {
        sfx_error_t error;
        error = SIGFOX_API_stop_continuous_transmission();
        if (error == SFX_ERR_NONE) {
            ESP_LOGI(S2LP_TAG, "CW test mode stopped");
            return ESP_OK;
        }
        ESP_LOGE(S2LP_TAG, "Could not stop CW test mode.");
        return ESP_FAIL;
    }

    std::string S2LP::getSecrets(void)
    {
        std::string result;
        char secrets_buff[SIGFOX_SECRETS_BUFF_LEN];
        char byte_hex[1];
        esp_efuse_read_field_blob(ESP_EFUSE_SIGFOX_SECRETS,
                                  (void*)secrets_buff,
                                  SIGFOX_SECRETS_FIELD_LEN);
        for (int i = 0; i < SIGFOX_SECRETS_BUFF_LEN; ++i) {
            std::sprintf(byte_hex, "%02X", (int)secrets_buff[i]);
            result += byte_hex;
        }
        return result;
    }
} /* drivers */ 
