/**
 * @file LOKA_Interrumptions.h
 * @author Gabriel Sanchez 
 * @author Andres Arias
 * @author Rafael Pires
 * @date 21/10/2019
 * @brief Driver for ESP32 GPIO interruptions
 */

#ifndef LOKA_INTERRUPTIONS_H_
#define LOKA_INTERRUPTIONS_H_

// #include "../../LOKA_STUB_GPIO/include/LOKA_STUB_GPIO.h"

#include "LOKA_GPIO.h"

#include "driver/gpio.h"
#include "esp_intr_alloc.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "stdio.h"
#include "esp_sleep.h"

#define HAL_OK 0
#define HAL_ERROR (~0)

namespace drivers {
	namespace interruptions {
		typedef void (*ISR_FUNC_PTR) (void);

		typedef enum {
			DISSABLE,
			RISING,
			FALLING
		} intType;

		/**
		 * @brief Interrupt handler for GPIO generated, send message to gpio event queue
		 */
		void IRAM_ATTR gpioInterruptionHandler(void* arg);

		/**
		 * @brief Handling of interruption vector
		 */
		void gpioInterruptionsTask(void* arg);

		/**
		 * @brief Init interruption handlers and create and init interruption task
		 */
		void initIsrHandler();

		/**
		 * @brief Redirects to the right interrupt routine, this is normally executed in the loop setup
		 * @param io Custom enum that could be 	INPUT, OUTPUT, INPUT_PULLDOWN, INPUT_PULLUP
		 * @param func pointer to handler function
		 * @retval ESP_OK Init Successfully
		 * @retval ESP_FAIL Init Failure
		 */
		esp_err_t intConnect(pin_mode_t io, ISR_FUNC_PTR func);

		/**
		 * @brief Enables a given GPIO to wake up the processor
		 * @param io GPIO pin
		 * @retval ESP_OK Enable pin successfully
		 * @retval ESP_FAIL Enable pin Failure
		 */
		esp_err_t intEnable (pin_mode_t io);

		/**
		 * @brief Disables the wake-up from a given GPIO
		 * @param io GPIO pin macro
		 * @retval ESP_OK Disable pin successfully
		 * @retval ESP_FAIL Disable pin Failure
		 */
		esp_err_t intDisable(pin_mode_t io);

		/**
		 * @brief Configures the wake-up/interrupt edge polarity
		 * @param io GPIO pin macro
		 * @param edge GPIO Edge macro
		 * @retval ESP_OK Set edge on  pin successfully
		 * @retval ESP_FAIL Set edge on pin Failure
		 */
		esp_err_t intSetEdge(pin_mode_t io, gpio_int_type_t edge);

		/**
		 * @brief Configures the wake-up/interrupt edge polarity
		 * @param io GPIO pin macro
		 * @param edge intType custom edge 
		 * @retval ESP_OK Set edge on  pin successfully
		 * @retval ESP_FAIL Set edge on pin Failure
		 */
		esp_err_t intSetEdge(pin_mode_t io, intType edge );
	}
}

#endif
