/**
 * @file LOKA_Interrumptions.cpp
 * @author Gabriel Sanchez 
 * @author Rafael Pires
 * @author Andres Arias
 * @date 21/10/2019
 * @brief Driver for ESP32 GPIO interruptions
 */

#include "LOKA_Interruptions.h"


namespace drivers {
	namespace interruptions {

		xQueueHandle gpioEvtQueue;
		RTC_DATA_ATTR uint64_t wakeupBitMask;
		RTC_DATA_ATTR uint64_t interruptionBitMask;
		void (*isrFunction[39]) (void);

		void IRAM_ATTR gpioInterruptionHandler(void* arg){
			uint32_t gpioNum = (uint32_t) arg;

			xQueueSendFromISR(gpioEvtQueue, &gpioNum, NULL);					// Throw info for the pin raising the interrupt
		}

		void gpioInterruptionsTask(void* arg){
			uint32_t gpioNum;
			for(;;){
				if(xQueueReceive(gpioEvtQueue, &gpioNum, portMAX_DELAY)){
					isrFunction[gpioNum]();
				}
			}
		}

		void initIsrHandler(){
			gpioEvtQueue=NULL;
			wakeupBitMask=0;													// By default no interrupt is active
			gpioEvtQueue = xQueueCreate(15, sizeof(uint32_t));					// Create a queue to deal with successive pin interrupts
			gpio_install_isr_service(0);										// Mask low priority interrupts
																				// Activate the interrupt vector handling
			xTaskCreate(gpioInterruptionsTask, "gpioInterruptionsTask", 2048, NULL, 20, NULL);
		}

		esp_err_t intConnect(gpio_num_t io, ISR_FUNC_PTR func){
			isrFunction[io] = func;

			interruptionBitMask = esp_sleep_get_ext1_wakeup_status();
			if((interruptionBitMask & BIT(io))){ 								// Verifies which pin waked up the processor
				isrFunction[io]();
			}
			esp_err_t ret = gpio_isr_handler_add(io, gpioInterruptionHandler, (void*) io);
			return ret;
		}

		esp_err_t intEnable(gpio_num_t io){
			ESP_ERROR_CHECK(gpio_intr_enable(io));
																// Filters to wake-up capable pins only
			if(io == 0 || io == 2 || io == 4 || (io >= 12 && io <= 15) || (io >= 25 && io <= 27) || (io >= 32 && io <= 39) ){
				wakeupBitMask |= (1LL << io);
				ESP_ERROR_CHECK(esp_sleep_enable_ext1_wakeup(wakeupBitMask, ESP_EXT1_WAKEUP_ANY_HIGH));
			}
			return ESP_OK;
		}

		esp_err_t intDisable(gpio_num_t io){
			ESP_ERROR_CHECK( gpio_intr_disable(io));

			if(io == 0 || io == 2 || io == 4 || (io >= 12 && io <= 15) || (io >= 25 && io <= 27) || (io >= 32 && io <= 39) ){
				wakeupBitMask &= ~(1LL << io);
				ESP_ERROR_CHECK( esp_sleep_enable_ext1_wakeup(wakeupBitMask, ESP_EXT1_WAKEUP_ANY_HIGH));
			}
			return ESP_OK;
		}

		esp_err_t intSetEdge(gpio_num_t io, gpio_int_type_t edge){
			ESP_ERROR_CHECK(gpio_set_intr_type(io, edge));
			return ESP_OK;

		}

		esp_err_t intSetEdge(gpio_num_t io, intType edge){
			return intSetEdge(io, static_cast<gpio_int_type_t>(edge) );
		}
	}
}