//**********************************************************************************************************************************
// Filename: DS18B20_Temperature.cpp
// Date: 18.10.2017
// Author: Hugo Silva, Luis Rosado
// Company: LOKA SYSTEMS
// Version: 1.0
// Description: Driver for the DS18B20 temperature sensor in stub context
//**********************************************************************************************************************************
#ifndef DS18B20_TEMPERATURE_H_
#define DS18B20_TEMPERATURE_H_


//**********************************************************************************************************************************
//                                                      Includes Section
//**********************************************************************************************************************************

#include "soc/rtc.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "rom/gpio.h"
#include "rom/ets_sys.h"
#include "soc/timer_group_reg.h"

#include "esp_log.h"

//**********************************************************************************************************************************
//                                                      Define Section
//**********************************************************************************************************************************
#define EXTRA_TIME_CONVERTION 0


//**********************************************************************************************************************************
//                                                     Templates Section
//**********************************************************************************************************************************
class DS18B20{
public:
	static int init(int oneWireGpio);
	static int getTemperature();
	static void changeResolution(unsigned char resolution);
private:
	static gpio_num_t gpio;
	static char resolution;
	static char initialized;
	static unsigned char reset(void);
	static void send(char bit);
	static unsigned char read(void);
	static void sendByte(char data);
	static unsigned char readByte(void);
};


#endif
