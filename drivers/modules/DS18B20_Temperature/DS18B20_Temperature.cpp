//**********************************************************************************************************************************
// Filename: DS18B20_Temperature.cpp
// Date: 18.10.2017
// Author: Hugo Silva, Luis Rosado
// Company: LOKA SYSTEMS
// Version: 1.0
// Description: Driver for the DS18B20 temperature sensor in stub context
//**********************************************************************************************************************************
#include "DS18B20_Temperature.h"


//**********************************************************************************************************************************
//                                                     Global Variables
//**********************************************************************************************************************************
RTC_DATA_ATTR gpio_num_t DS18B20::gpio;
RTC_DATA_ATTR char DS18B20::resolution = 0;
RTC_DATA_ATTR char DS18B20::initialized = 0;

//**********************************************************************************************************************************
// Header: DS18B20::reset
// Function: Resets the temperature sensor
//**********************************************************************************************************************************
unsigned char RTC_IRAM_ATTR DS18B20::reset(void){

	REG_WRITE(TIMG_WDTFEED_REG(0), 1);

	GPIO_OUTPUT_SET(DS18B20::gpio, 0);
	ets_delay_us(500);
	GPIO_OUTPUT_SET(DS18B20::gpio, 1);
	GPIO_DIS_OUTPUT(DS18B20::gpio);
	ets_delay_us(100);
	GPIO_INPUT_GET(DS18B20::gpio);
	ets_delay_us(500);
	if(GPIO_INPUT_GET(DS18B20::gpio) == 1)
		return(1);
	else
		return(0);
}


//**********************************************************************************************************************************
// Header: DS18B20::send
// Function: Sends a bit over the one-wire bus
//**********************************************************************************************************************************
void RTC_IRAM_ATTR DS18B20::send(char bit){
	GPIO_OUTPUT_SET(DS18B20::gpio, 0);
	ets_delay_us(5);
	if(bit == 1)
		GPIO_OUTPUT_SET(DS18B20::gpio, 1);
	ets_delay_us(80);
	GPIO_OUTPUT_SET(DS18B20::gpio, 1);
}


//**********************************************************************************************************************************
// Header: DS18B20::read
// Function: Reads one bit over the one-wire bus
//**********************************************************************************************************************************
unsigned char RTC_IRAM_ATTR DS18B20::read(void){
	GPIO_OUTPUT_SET(DS18B20::gpio, 0);
	ets_delay_us(2);
	GPIO_OUTPUT_SET(DS18B20::gpio, 1);
	ets_delay_us(15);
	GPIO_DIS_OUTPUT(DS18B20::gpio);
	if(GPIO_INPUT_GET(DS18B20::gpio) == 1)
		return(1);
	else
		return(0);
}


//**********************************************************************************************************************************
// Header: DS18B20::send_byte
// Function: Sends one byte over the one-wire bus
//**********************************************************************************************************************************
void RTC_IRAM_ATTR DS18B20::sendByte(char data){
	unsigned char i;
	unsigned char x;

	REG_WRITE(TIMG_WDTFEED_REG(0), 1);

	for(i = 0; i < 8; i++){
		x = data >> i;
		x &= 0x01;
		DS18B20::send(x);
	}
	ets_delay_us(100);
}


//**********************************************************************************************************************************
// Header: DS18B20::send_byte
// Function: Reads one byte over the one-wire bus
//**********************************************************************************************************************************
unsigned char RTC_IRAM_ATTR DS18B20::readByte(void){
	unsigned char i;
	unsigned char data = 0;

	REG_WRITE(TIMG_WDTFEED_REG(0), 1);

	for (i = 0; i < 8; i++){
		if(DS18B20::read())
			data |= 0x01 << i;
		ets_delay_us(15);
	}
	return(data);
}


//**********************************************************************************************************************************
// Header:		DS18B20::init
// Function:	Initialize ds18b20 sensor
//				parameter: 	pin number where is the sensor bus
//				return:		3 - 12bit resolution - 750ms conversion time 0.0625 °C resolution
//							2 - 11bit resolution - 375ms conversion time 0.125 °C resolution
//							1 - 10bit resolution - 188ms conversion time 0.25 °C resolution
//							0 - 9bit resolution	 - 94ms conversion time; 0.5 °C resolution
//**********************************************************************************************************************************
int RTC_IRAM_ATTR DS18B20::init(int oneWireGpio){
	char tmp;

	DS18B20::gpio = (gpio_num_t) oneWireGpio;

	gpio_pad_select_gpio(DS18B20::gpio);
	gpio_pad_pullup(DS18B20::gpio);

	DS18B20::reset();
	DS18B20::sendByte(0xCC);
	DS18B20::sendByte(0xBE);
	DS18B20::readByte();
	DS18B20::readByte();
	DS18B20::readByte();
	DS18B20::readByte();
	tmp = DS18B20::readByte();
	DS18B20::reset();
	tmp = tmp & 0x7F;
	DS18B20::resolution = tmp >> 5;

	DS18B20::initialized = 1;

	return DS18B20::resolution;
}



//**********************************************************************************************************************************
// Header:		DS18B20::getTemperature
// Function:	Returns temperature sensor reading. By default, temperature sensor is configured with 12bit resolution output, witch
//				means, 750ms conversion time.
//**********************************************************************************************************************************
int RTC_IRAM_ATTR DS18B20::getTemperature() {
	char check, msb, lsb;
	int i, iMax;

	gpio_pad_select_gpio(DS18B20::gpio);

	if(DS18B20::initialized == 1){
		check = reset();
		if(check == 1){
			sendByte(0xCC);
			sendByte(0x44);
			switch(DS18B20::resolution){
				case 3:
					iMax = 80;
					break;
				case 2:
					iMax = 40;
					break;
				case 1:
					iMax = 20;
					break;
				case 0:
					iMax = 10;
					break;
				default:
					iMax = 80;
					break;
			}

			for(i = 0; i < iMax + EXTRA_TIME_CONVERTION; i++){
				ets_delay_us(10000);
				REG_WRITE(TIMG_WDTFEED_REG(0), 1);
			}

			check = reset();
			sendByte(0xCC);
			sendByte(0xBE);
			lsb = readByte();
			msb = readByte();
			check = reset();
			return ((msb << 8) + lsb) >> 4;
		}else{
			return -127;
		}
	}else{
		return -127;
	}
}


//**********************************************************************************************************************************
// Header:		DS18B20::changeResolution
// Function:	Function that change bit resolution output
//				parameter:	3 - 12bit resolution - 750ms conversion time 0.0625C resolution
//							2 - 11bit resolution - 375ms conversion time 0.125C resolution
//							1 - 10bit resolution - 188ms conversion time 0.25C resolution
//							0 - 9bit resolution	 - 94ms conversion time; 0.5C resolution
//**********************************************************************************************************************************
void RTC_IRAM_ATTR DS18B20::changeResolution(unsigned char resolution){
	unsigned char check;
	char tmp;

	if(initialized == 1){
		check = reset();
		if(check == 1){
			DS18B20::sendByte(0xCC);
			DS18B20::sendByte(0x4E);		// write on scratchPad
			DS18B20::sendByte(0x00);		// User byte 0 – Unused
			DS18B20::sendByte(0x00);		// User byte 1 – Unused
			tmp = 0x0F | (resolution << 5);
			DS18B20::sendByte(tmp);		// User byte 2 – Unused
			DS18B20::reset();
			DS18B20::sendByte(0xCC);
			DS18B20::sendByte(0x48);
			DS18B20::resolution = resolution;
	      }
	  }
}

