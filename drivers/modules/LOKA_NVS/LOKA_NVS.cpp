/**
 * @file LOKA_NVS.cpp
 * @author Gabriel Sanchez 
 * @author Andres Arias
 * @author Luis Rosado
 * @date 17/10/2019
 * @brief Driver for the ESP32 internal Non Volatine Storage.
 */


#include "LOKA_NVS.h"

namespace drivers {
	
	NVS::NVS(){
        ESP_LOGI(NVS_DRIVER_TAG, "Initializing NVS Driver.");
		nvs_flash_init();
    }
	NVS::~NVS(){
		ESP_LOGI(NVS_DRIVER_TAG, "Task done");
		nvs_flash_deinit();
	}

	esp_err_t NVS::setFlash(const std::string &page, const std::string &key, const std::string &value){

		const char * _page 	= page.c_str();										// Parse inputs variables
		const char * _key  	= key.c_str();
		const char * _value = value.c_str();

		ESP_LOGI(NVS_DRIVER_TAG, "Setting key: %s with value: %s in %s", _key, _value, _page);

		char list[FLASH_PAGE_MAX*FLASH_SIZE_MAX + FLASH_PAGE_MAX + 1] = {};		// Maximum size for the concatenated configurations names
		char* ptr;
		bool found = false;

		getFlash(_page, "list", list);											// Retrieves the list of the existing configurations

		ptr = strtok(list,"/");													// Loop all the configurations
		
		while (ptr != NULL) {
			if(strcmp(ptr, _key) == 0){											// Look for the desired configuration
				found = true;
				break;
			}
			ptr = strtok(NULL, "/");
		}

		if (found) {															// If the new configuration already exists
																				// Delete the previous definition
			ESP_LOGI(NVS_DRIVER_TAG, "Word already defined will be overwritten");
			nvs_open(_page, NVS_READWRITE, &nvsHandle);
			nvs_erase_key(nvsHandle, _key);
			nvs_commit(nvsHandle);
			nvs_close(nvsHandle);
		} else {
			ESP_LOGI(NVS_DRIVER_TAG, "Word no defined will be created");																// If the new configuration doens't exists
			getFlash(_page, "list", list);										// Retrieves the list of the existing configurations
			sprintf(list, "%s/%s", list, _key);									// Add it to the configurations list
																				// Save the updated configurations list
			nvs_open(_page, NVS_READWRITE, &nvsHandle);
			nvs_set_str (nvsHandle, "list", list);
			nvs_commit(nvsHandle);
			nvs_close(nvsHandle);
		}
																				// Finally store the new configuration value
		nvs_open(_page, NVS_READWRITE, &nvsHandle);
		nvs_set_str (nvsHandle, _key, _value);
		esp_err_t result = nvs_commit(nvsHandle);
		nvs_close(nvsHandle);

		if (result == ESP_OK) {
			ESP_LOGI(NVS_DRIVER_TAG, "Word store succesfully");
		}
		else {
			ESP_LOGE(NVS_DRIVER_TAG, "Cannot write word on flash");
		}
		return result;



	}

	esp_err_t NVS::getFlash(const std::string &page, const std::string &key, char* value){
		
		size_t requiredSize;
		const char * _page 	= page.c_str();										// Parse inputs variables
		const char * _key  	= key.c_str();
		ESP_LOGI(NVS_DRIVER_TAG, "Getting key: %s in %s page", _key, _page);

		memset(value, 0, FLASH_SIZE_MAX + 1);
		nvs_open(_page, NVS_READONLY, &nvsHandle);							// Check if the configuration already exists
		nvs_get_str(nvsHandle, _key, NULL, &requiredSize);

		if (requiredSize == 0){												// Configuration not found
			ESP_LOGI(NVS_DRIVER_TAG,"Flash word not found: %s", _key);
			nvs_close(nvsHandle);
			return ESP_FAIL;
		} else {
			ESP_LOGI(NVS_DRIVER_TAG,"Flash word found: %s", _key);
			esp_err_t result = nvs_get_str(nvsHandle, _key, value, &requiredSize);				// Read the configuration
			nvs_close(nvsHandle);
			return result;
		}
		
	}

	esp_err_t NVS::getFlash(const std::string &page, const std::string &key, int index, char* value) {
		char aux[FLASH_SIZE_MAX + 1] = {};
		char* ptr;
		int i = 0;
		const char * _page 	= page.c_str();										// Parse inputs variables
		const char * _key  	= key.c_str();

		ESP_LOGI(NVS_DRIVER_TAG, "Getting key: %s in %s page with index: %d", _key, _page, index);

		memset(value, 0, FLASH_SIZE_MAX + 1);
		esp_err_t result = getFlash(_page, _key, aux);
		if (result == ESP_OK) {
			ESP_LOGI(NVS_DRIVER_TAG, "Getting values separated by commas");
			if (strlen(aux) > 0) {												// Look for specific sub-configuration
				ptr = strtok(aux, ",");
				for (i = 0; i < index && ptr != NULL; i++) {
					ptr = strtok(NULL, ",");
				}
				if (ptr != NULL) {
					strcpy(value, ptr);											// Update the return value
				}
			}
		} else  {
			ESP_LOGE(NVS_DRIVER_TAG, "Error while trying to get flash data");
		}
		return result;
	}
	
	esp_err_t NVS::eraseFlash(const std::string &page){
		const char * _page 	= page.c_str();	
		ESP_LOGI(NVS_DRIVER_TAG, "Erasing flash %s page", _page);
		nvs_open(_page, NVS_READWRITE, &nvsHandle);
		nvs_erase_all(nvsHandle);
		esp_err_t result = nvs_commit(nvsHandle);
		nvs_close(nvsHandle);
		if (result == ESP_OK){
			ESP_LOGI(NVS_DRIVER_TAG, "Erase %s page succesfully", _page);
		}
		else {
			ESP_LOGE(NVS_DRIVER_TAG, "Something went wrong erasing %s page", _page);
		}
		return result;
	}

	esp_err_t NVS::clearFlash(const std::string &page, const std::string &key){
		char list[FLASH_SIZE_MAX*FLASH_PAGE_MAX + 1] = {};
		char listAux[FLASH_SIZE_MAX*FLASH_PAGE_MAX + 1] = {};
		char* ptr = NULL;
        const char * _page 	= page.c_str();										// Parse inputs variables
		const char * _key  	= key.c_str();

		ESP_LOGI(NVS_DRIVER_TAG, "Erasing flash key: %s in %s page", _key, _page);
		nvs_open(_page, NVS_READWRITE, &nvsHandle);
		nvs_erase_key(nvsHandle, _key);
		nvs_commit(nvsHandle);
		nvs_close(nvsHandle);

		getFlash(_page, "list", list);										// Retrieves the list of the existing configurations

		ptr = strtok(list, "/");											// Loop all the configurations
		while(ptr != NULL){
			if(strcmp(ptr, _key) != 0){										// Build a new list without the deprecated configuration
				sprintf(listAux,"%s/%s", listAux, ptr);
			}
			ptr = strtok(NULL, "/");
		}
																			// Save the updated configurations list
		nvs_open(_page, NVS_READWRITE, &nvsHandle);
		nvs_set_str (nvsHandle, "list", listAux);
		esp_err_t result = nvs_commit(nvsHandle);
		nvs_close(nvsHandle);
		if (result == ESP_OK){
			ESP_LOGI(NVS_DRIVER_TAG, "Erasing flash key: %s in %s page successfully", _key, _page);
		}
		else {
			ESP_LOGE(NVS_DRIVER_TAG, "Something went wrong erasing key: %s in %s page", _key, _page);
		}
		return result;
	}

}


