/**
 * @file LOKA_NVS.h
 * @author Gabriel Sanchez
 * @author Andres Arias
 * @author Joao Pombas
 * @date 17/10/2019
 * @brief Driver for the ESP32 internal Non Volatine Storage.
 */

#ifndef LOKA_NVS_H_
#define LOKA_NVS_H_


#include <string.h>
#include <string>
//#include <stdio.h>
//#include <stdlib.h>
#include "nvs_flash.h"
#include "nvs.h"
#include "esp_log.h"

/**
 * @brief Tag used by the logging library.
 */
#define NVS_DRIVER_TAG 				"nvs"

/**
 * @brief Flash maximun amount of pages
 */
#define FLASH_PAGE_MAX				8

/**
 * @brief Flash maximum page size
 */
#define FLASH_SIZE_MAX 				128

/**
 * @brief Identifier for messages page 
 */
#define FLASH_MESSAGES_PAGE			"message"

/**
 * @brief Identifier for configuration page
 */
#define FLASH_CONFIGURATIONS_PAGE	"configuration"

/**
 * @brief Identifier for hardware page 
 */
#define FLASH_HARDWARE_PAGE			"hardware"

/**
 * @brief Identifier for messages page 
 */
#define FLASH_CALIBRATIONS_PAGE		"calibration"

/**
 * @brief Identifier for flags page 
 */
#define FLASH_FLAGS_PAGE			"flag"


namespace drivers {
	/**
     * @class NVS
     * @brief Low-level driver for the ESP32 internal non volatil memory
     */
	class NVS{
		public:
			/**
             * @brief Class constructor. Initializes non volatile memory.
             */
			NVS();

			/**
             * @brief Write a key-value field in specific page in non volatile memory 
			 * used for store a single value.
             * @param[in] page Name of the page where locate new key-value
			 * @param[in] name Name of the key for finding values
			 * @param[in] value that will stored in the key 
			 * @retval ESP_OK if write a word succesfully
			 * @retval ESP_FAIL if something went wrong while writting
             */
			esp_err_t setFlash(const std::string &page, const std::string &key, const std::string &value);
			
			/**
             * @brief Read a key-value field from specific page in non volatile memory
             * @param[in] page Name of the page where key-value is located
			 * @param[in] name Name of the key for finding values
			 * @param[out] value Retrieved value from the key in the page 
			 * @retval ESP_OK if get a word succesfully
			 * @retval ESP_FAIL if something went wrong while reading
             */
			esp_err_t getFlash(const std::string &page, const std::string &key, char* value);

			/**
             * @brief Read a key-values field from specific page in non volatile memory 
			 * used for store a multiples values stored separated by commas.
             * @param[in] page Name of the page where key-value is located
			 * @param[in] name Name of the key for finding values
			 * @param[in] index Name of the key for finding values
			 * @param[out] value Retrieved value from the key in the page 
             */
			esp_err_t getFlash(const std::string &page, const std::string &key, int index, char* value);

			/**
             * @brief Delete content of a specific page in non volatile storage
             * @param[in] page Name of the page for deleting content
             */
			esp_err_t eraseFlash(const std::string &page);

			/**
             * @brief Delete a key-value inside specific page
             * @param[in] page Name of the page for deleting key-value
			 * @param[in] name Key value for deleting
             */
			esp_err_t clearFlash(const std::string &page, const std::string &key);

			/**
             * @brief Class destructor deinit non volatile storage
             */
			~NVS();
			
		private:
			nvs_handle nvsHandle;
	};
}



#endif
