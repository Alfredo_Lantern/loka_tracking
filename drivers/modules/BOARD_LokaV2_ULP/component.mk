
# ULP_APP_NAME must be unique (if multiple components use ULP)
#ULP_APP_NAME ?= ulp_$(COMPONENT_NAME)
ULP_APP_NAME ?= ulp_service

# Specify all assembly source files here. Files should be placed into a separate directory (in this case, ulp/),
# which should not be added to COMPONENT_SRCDIRS.
ULP_S_SOURCES =  $(COMPONENT_PATH)/ulp/ulp_service.S

# List all the component object files which include automatically generated ULP export file, $(ULP_APP_NAME).h:
ULP_EXP_DEP_OBJECTS := BOARD_LokaV2_ULP.o

# Include build rules for ULP program 
include $(IDF_PATH)/components/ulp/component_ulp_common.mk
