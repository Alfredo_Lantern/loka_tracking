//**********************************************************************************************************************************
// Filename: BOARD_LokaV2.cpp
// Date: 18.10.2017
// Author: Rafael Pires
// Company: LOKA SYSTEMS
// Version: 1.0
// Description: Code to operate the LOKA V2 board firmware
//**********************************************************************************************************************************


//**********************************************************************************************************************************
//                                                      Includes Section
//**********************************************************************************************************************************
#include "include/BOARD_LokaV2_ULP.h"
#include "ulp_service.h"


//**********************************************************************************************************************************
//                                                      External Functions
//**********************************************************************************************************************************
extern "C"{
	uint32_t esp_clk_slowclk_cal_get();
}

extern const uint8_t ulp_main_bin_start[] asm("_binary_ulp_service_bin_start");
extern const uint8_t ulp_main_bin_end[]   asm("_binary_ulp_service_bin_end");

static const char RTC_RODATA_ATTR wakeString[] = "STUB:\t\t\t\t\t time: %llu\r\n";
extern bool debug;

uint64_t timerIncrement;

//**********************************************************************************************************************************
//                                                        Code Section
//**********************************************************************************************************************************


//**********************************************************************************************************************************
// Header: BoardULP::init
// Function: Inits the LOKA board
//**********************************************************************************************************************************
void BoardULP::init(){
	unsigned int ulp_period;

	ulp_load_binary(0, ulp_main_bin_start, (ulp_main_bin_end - ulp_main_bin_start)/sizeof(uint32_t));
	ulp_period = rtc_time_us_to_slowclk((uint64_t) 100000, (uint32_t) esp_clk_slowclk_cal_get());
	REG_SET_FIELD(SENS_ULP_CP_SLEEP_CYC0_REG, SENS_SLEEP_CYCLES_S0, ulp_period);

	ulp_wakeup_enable = ULP_TIMER_WAKEUP | ULP_BUTTON_WAKEUP;
	ulp_battery_locked = 0;
	ulp_run((&ulp_entry - RTC_SLOW_MEM)/sizeof(uint32_t));
}

//**********************************************************************************************************************************
// Header: getCurrentTime
// Function: Gets the current RTC time
//**********************************************************************************************************************************
 uint64_t RTC_IRAM_ATTR BoardULP::getCurrentTime(){
	uint64_t timerValue;

	SET_PERI_REG_MASK(RTC_CNTL_TIME_UPDATE_REG, RTC_CNTL_TIME_UPDATE);
	while (GET_PERI_REG_MASK(RTC_CNTL_TIME_UPDATE_REG, RTC_CNTL_TIME_VALID) == 0){
		ets_delay_us(1); 												// Might take 1 RTC slowclk period, avoids flooding the bus
	}
	SET_PERI_REG_MASK(RTC_CNTL_INT_CLR_REG, RTC_CNTL_TIME_VALID_INT_CLR);
	timerValue = READ_PERI_REG(RTC_CNTL_TIME0_REG);
	timerValue |= ((uint64_t) READ_PERI_REG(RTC_CNTL_TIME1_REG)) << 32;
	return timerValue;
}


//**********************************************************************************************************************************
// Header: setWakeupTimer
// Function: Sets the RTC timer next wake-up
//**********************************************************************************************************************************
void RTC_IRAM_ATTR BoardULP::resetWakeUp(){
	uint64_t timerValue;

	timerValue = getCurrentTime();

	//DEBUG_RTC(wakeString, timerValue);

	timerValue += timerIncrement;

	//DEBUG_RTC(wakeString, timerValue);

	ulp_target_time3  = (timerValue >> 48) & 0xFFFF;
	ulp_target_time2  = (timerValue >> 32) & 0xFFFF;
	ulp_target_time1  = (timerValue >> 16) & 0xFFFF;
	ulp_target_time0  = timerValue & 0xFFFF;
}


//**********************************************************************************************************************************
// Header: BoardULP::init
// Function: Inits the LOKA board
//**********************************************************************************************************************************
uint64_t BoardULP::getWakeUpTime(unsigned int seconds){
	uint64_t timerValue = 0;
	timerValue = getCurrentTime();
	timerValue += rtc_time_us_to_slowclk((uint64_t) seconds*1000000, (uint32_t) esp_clk_slowclk_cal_get());
	return timerValue;
}


//**********************************************************************************************************************************
// Header: BoardULP::init
// Function: Inits the LOKA board
//**********************************************************************************************************************************
void BoardULP::setWakeUpTime(unsigned int seconds){
	uint64_t timerValue = 0;

	if(seconds == 0){
		disableWakeUp(ULP_TIMER_WAKEUP);
		timerIncrement = 0;
		ulp_target_time3 = 0;
		ulp_target_time2 = 0;
		ulp_target_time1 = 0;
		ulp_target_time0 = 0;
	}else{
		enableWakeUp(ULP_TIMER_WAKEUP);
		timerIncrement = rtc_time_us_to_slowclk((uint64_t) seconds*1000000, (uint32_t) esp_clk_slowclk_cal_get());
		timerValue = getCurrentTime();
		//DEBUG((char*)"BOARD_LokaV2_ULP:\t\t timer: %llu", timerValue);
		timerValue += timerIncrement;
		//DEBUG((char*)"BOARD_LokaV2_ULP:\t\t timer: %llu", timerValue);
		ulp_target_time3  = (timerValue >> 48) & 0xFFFF;
		ulp_target_time2  = (timerValue >> 32) & 0xFFFF;
		ulp_target_time1  = (timerValue >> 16) & 0xFFFF;
		ulp_target_time0  = timerValue & 0xFFFF;
	}
}


//**********************************************************************************************************************************
// Header: BoardULP::setBatteryState
// Function: Sets the battery locked message
//**********************************************************************************************************************************
void BoardULP::lockBattery(){
	unsigned int ulp_period;

	ulp_battery_locked = 1;
	ulp_period = rtc_time_us_to_slowclk((uint64_t) 30000000, (uint32_t) esp_clk_slowclk_cal_get());
	REG_SET_FIELD(SENS_ULP_CP_SLEEP_CYC0_REG, SENS_SLEEP_CYCLES_S0, ulp_period);
}


//**********************************************************************************************************************************
// Header: BoardULP::enableWakeUp
// Function: Enables a certain ULP wakeup mechanism
//**********************************************************************************************************************************
void BoardULP::enableWakeUp(unsigned int mask){

	ulp_wakeup_enable = ulp_wakeup_enable | mask;
}


//**********************************************************************************************************************************
// Header: BoardULP::disableWakeUp
// Function: Disables a certain ULP wakeup mechanism
//**********************************************************************************************************************************
void BoardULP::disableWakeUp(unsigned int mask){

	ulp_wakeup_enable = ulp_wakeup_enable & ~mask;
}


//**********************************************************************************************************************************
// Header: BoardULP::enableWakeUp
// Function: Enables a certain ULP wakeup mechanism
//**********************************************************************************************************************************
void BoardULP::setWakeUp(unsigned int mask){

	ulp_wakeup_enable = mask;
}


//**********************************************************************************************************************************
// Header: BoardULP::printDebug
// Function: Prints a debug variable
//**********************************************************************************************************************************
void BoardULP::printDebug(){

	//DEBUG((char*)"BOARD_LokaV2_ULP:\t\t ulp_debug0: %d", ulp_debug0 & 0xFFFF);
	//DEBUG((char*)"BOARD_LokaV2_ULP:\t\t ulp_debug1: %d", ulp_debug1 & 0xFFFF);
	//DEBUG((char*)"BOARD_LokaV2_ULP:\t\t ulp_debug2: %d", ulp_debug2 & 0xFFFF);
	//DEBUG((char*)"BOARD_LokaV2_ULP:\t\t ulp_debug3: %d", ulp_debug3 & 0xFFFF);
}
