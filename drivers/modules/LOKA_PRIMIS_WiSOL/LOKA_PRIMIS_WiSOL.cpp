/**
 * @file WiSOL.cpp
 * @author Andres Arias
 * @date 24/9/2019
 * @brief Driver for the WiSOL Sigfox modem.
 */

#include "LOKA_PRIMIS_WiSOL.h"

namespace drivers {


    WiSOL::WiSOL(void): uart_ctrl(UART_NUM_2, WISOL_TX, WISOL_RX),
                        is_on(false),
                        is_fcc(false)
    {
        DigitalPin::init(WISOL_WAKEUP, DigitalPin::Mode::OUTPUT);
        DigitalPin::init(WISOL_RESET, DigitalPin::Mode::OUTPUT);
        DigitalPin::write(WISOL_WAKEUP, 0);
        vTaskDelay(100 / portTICK_RATE_MS);
        DigitalPin::write(WISOL_RESET, 0);
        vTaskDelay(100 / portTICK_RATE_MS);
        DigitalPin::write(WISOL_WAKEUP, 1);
        vTaskDelay(100 / portTICK_RATE_MS);
        DigitalPin::write(WISOL_RESET, 1);
        vTaskDelay(100 / portTICK_RATE_MS);
        ESP_LOGI(WISOL_TAG, "Module initialized");
    }

    WiSOL::~WiSOL(void)
    {
        if (this->is_on)
            this->turnOff();
        ESP_LOGI(WISOL_TAG, "WiSOL module deinitialized");
    }

    esp_err_t WiSOL::turnOn(void)
    {
        std::string res;
        DigitalPin::write(WISOL_WAKEUP, 0);
        DigitalPin::init(WISOL_WAKEUP, DigitalPin::Mode::OUTPUT);
        vTaskDelay(20 / portTICK_RATE_MS);
        DigitalPin::init(WISOL_WAKEUP, DigitalPin::Mode::INPUT);
        vTaskDelay(20 / portTICK_RATE_MS);
        // All AT commanded devices should respond "OK" when asked "AT":
        this->uart_ctrl.write("AT\r\n");
        res = this->readClean(WISOL_TIMEOUT);
        if (res == "OK") {
            this->uart_ctrl.write("AT$I=7\r\n"); // Check if FCC or ETSI compliant.
            res = this->readClean(WISOL_TIMEOUT);
            if (res == "FCC") {
                ESP_LOGI(WISOL_TAG, "Sigfox module turned on. FCC compliant device.");
                this->is_on = true;
                this->is_fcc = true;
                return ESP_OK;
            } else if (res == "ETSI") {
                ESP_LOGI(WISOL_TAG, "Sigfox module turned on. ETSI compliant device.");
                this->is_on = true;
                this->is_fcc = false;
                return ESP_OK;
            } else
                return ESP_FAIL;
        } else {
            ESP_LOGE(WISOL_TAG, "Sigfox module failed to boot up.");
            return ESP_FAIL;
        }
    }

    esp_err_t WiSOL::turnOff(void)
    {
        if (!this->is_on)
            return ESP_OK;
        std::string res;
        // Disengages the control pins:
        DigitalPin::init(WISOL_WAKEUP, DigitalPin::Mode::INPUT);
        DigitalPin::init(WISOL_RESET, DigitalPin::Mode::INPUT);
        this->uart_ctrl.write("AT$P=2\r\n"); // Go to sleep.
        res = this->readClean(WISOL_TIMEOUT);
        vTaskDelay(20 / portTICK_RATE_MS);
        this->uart_ctrl.write("AT\r\n");
        /* A sleeping device should not answer any request, if it does,
         * insist in sending it to sleep. */
        while (res == "OK") {
            ESP_LOGI(WISOL_TAG, "Trying to go to sleep.");
            this->uart_ctrl.write("AT$P=2\r\n");
            vTaskDelay(20 / portTICK_RATE_MS);
            this->uart_ctrl.write("AT\r\n");
            res = this->readClean(WISOL_TIMEOUT);
        }
        ESP_LOGI(WISOL_TAG, "Module entered deep sleep mode.");
        this->is_on = false;
        return ESP_OK;
    }

    bool WiSOL::isOn(void)
    {
        return this->is_on;
    }

    std::string WiSOL::getID(void)
    {
        this->uart_ctrl.write("AT$I=10\r\n");
        std::string res = this->readClean(WISOL_TIMEOUT);
        std::string res_clean = "";
        for (auto const &character: res) {
            if (character != '0')
                res_clean += character;
        }
        return res;
    }

    std::string WiSOL::getPAC(void)
    {
        this->uart_ctrl.write("AT$I=11\r\n");
        std::string res = this->readClean(WISOL_TIMEOUT);
        return res;
    }

    esp_err_t WiSOL::sendData(const std::string &data)
    {
        char at_buffer[WISOL_BUFF_SIZE];
        std::string res;
        this->buildATCommand(at_buffer, WISOL_BUFF_SIZE, data);
        std::sprintf(at_buffer, "%s\r\n", at_buffer); // Indicates no downlink
        if (is_fcc) { // FCC-compliant devices must be reset when the module indicates so.
            int fcc_x, fcc_y;
            this->uart_ctrl.write("AT$GI?\r\n");
            res = this->readClean(WISOL_TIMEOUT);
            std::sscanf(res.c_str(), "%d,%d", &fcc_x, &fcc_y);
            if (fcc_y == 0 || fcc_y < 3) { // Indicates that a reset is needed.
                this->uart_ctrl.write("AT$RC\r\n"); // Perform reset.
                if (this->readClean(WISOL_TIMEOUT) == "OK")
                    ESP_LOGI(WISOL_TAG, "Device reset.");
            }
        }
        ESP_LOGI(WISOL_TAG, "Sending data frame: %s", data.c_str());
        this->uart_ctrl.write(at_buffer);
        if (this->readClean(WISOL_TX_TIMEOUT) == "OK") {
            ESP_LOGI(WISOL_TAG, "Message successfully sent!");
            return ESP_OK;
        } else {
            ESP_LOGE(WISOL_TAG, "Could not send message.");
            return ESP_FAIL;
        }
    }

    esp_err_t WiSOL::startCWMode(SigfoxZone zone)
    {
        if (zone == SigfoxZone::RCZ1) {
            ESP_LOGI(WISOL_TAG, "Entering Continuous Wave mode for RCZ1.");
            this->uart_ctrl.write("AT$CW=868130000,1,15");
        } else if (zone == SigfoxZone::RCZ2) {
            ESP_LOGI(WISOL_TAG, "Entering Continuous Wave mode for RCZ2.");
            this->uart_ctrl.write("AT$CW=902200000,1,24");
        } else {
            ESP_LOGI(WISOL_TAG, "Entering Continuous Wave mode");
            this->uart_ctrl.write("AT$CW=920800000,1,24");
        }
        std::string res = this->readClean(WISOL_TIMEOUT);
        if (res == "OK") {
            ESP_LOGI(WISOL_TAG, "Successfully entered CW Mode!");
            return ESP_OK;
        } else {
            ESP_LOGE(WISOL_TAG, "Could not start CW mode.");
            return ESP_FAIL;
        }
    }

    esp_err_t WiSOL::stopCWMode(void)
    {
        this->uart_ctrl.write("AT$CW=0,0");
        std::string res = this->readClean(WISOL_TIMEOUT);
        if (res == "OK") {
            ESP_LOGI(WISOL_TAG, "CW mode stopped.");
            return ESP_OK;
        } else {
            ESP_LOGE(WISOL_TAG, "Could not stop CW mode.");
            return ESP_FAIL;
        }
    }

    std::string WiSOL::sendAndReadData(const std::string &input)
    {
        char at_buffer[WISOL_BUFF_SIZE];
        this->buildATCommand(at_buffer, WISOL_BUFF_SIZE, input);
        std::sprintf(at_buffer, "%s,1\r\n", at_buffer); // Indicates downlink request
        if (is_fcc) {
            ESP_LOGI(WISOL_TAG, "Resetting device.");
            this->uart_ctrl.write("AT$RC\r\n");
        }
        auto res = this->readClean(false);
        ESP_LOGI(WISOL_TAG, "Sending data frame: %s", input.c_str());
        this->uart_ctrl.write(at_buffer);
        res = this->readClean(WISOL_RX_TIMEOUT);
        return this->parseDownlink(res);
    }

    std::string WiSOL::readClean(unsigned int timeout)
    {
        std::string res = this->uart_ctrl.read(timeout);
        if (res.size() == 0 )
            return res;
        res.erase(res.size() - 2); // Removes trailing \r\n
        return res;
    }

    void WiSOL::buildATCommand(char *buffer,
                               unsigned int size,
                               const std::string &data) const
    {
        std::memset(buffer, '\0', size); // So the resulting string is null-terminated
        std::sprintf(buffer, "AT$SF=");
        for (auto const &character: data) {
            std::sprintf(buffer, "%s%02x", buffer, character);
        }
    }

    std::string WiSOL::parseDownlink(const std::string &raw_data)
    {
        std::string result = "";
        std::size_t beginning = raw_data.find("=");
        for (auto i = beginning + 1; i < raw_data.size(); ++i) {
            if (raw_data.at(i) == ' ')
                continue;
            result += raw_data.at(i);
        }
        return result;
    }
} /* drivers */ 
