/**
 * @file LOKA_WIFI.hpp
 * @author Andres Arias
 * @author Alfredo Piedra
 * @date 17 Sep 2019
 * @brief WiFi driver for the ESP32.
 * @todo Status management on connect()
 */

#ifndef ESPWIFIDRIVER_H
#define ESPWIFIDRIVER_H

#include <vector>
#include <string>
#include <cstring>

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "tcpip_adapter.h"
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_log.h"

#define MAX_SCAN_SIZE 				16
#define ESPWIFI_TAG "ESPWifi"
#define PARTIAL_SCAN_MIN_TIME		100		/** Minimum listening time spent per channel, 
                                                enforced when no AP was found */
#define PARTIAL_SCAN_MAX_TIME		150		/** Maximum listening time spent per channel, 
                                                enforced when at least one AP was found */
#define WIFI_SCAN_STOP 				BIT2

typedef struct accessPoint {

    std::string ssid;
    unsigned char bssid[6];                 // Most of the time is also the MAC address
    signed char rssi;
    unsigned char channel;
    unsigned char category;

} accessPoint;

namespace drivers
{    
    /**
     * @class ESPWiFiDriver
     * @brief WiFi manager for ESP32, allows to easily connect to WiFi APs.
     */
    class ESPWiFiDriver
    {
        public:

            bool is_init; /** Indicates if the network stack is up. */

            bool is_connected; /** Indicates if there's a currently active connection. */

            /**
             * @brief Class constructor. Initializes a network stack.
             *
             * Initializes the TCP/IP stack.
             * Also initializes the WiFi Event loop for handling async events.
             */
            ESPWiFiDriver(void);

            /**
             * @brief Class destructor.
             */
            ~ESPWiFiDriver(void);

            /**
             * @brief Return the WiFi Event handle. Useful for syncronizing
             * tasks based on async WiFi events.
             * @note Use xEventGroupWaitBits to put a task to wait for successful
             * connection.
             * @return The WiFi EventGroupHandle.
             */
            EventGroupHandle_t *getEventGroup(void);

            /**
             * @brief Establishes a connection to an AP with the given credentials.
             * @param ssid the SSID of the desired AP.
             * @param password The AP password.
             * @retval ESP_OK Is a connection is established successfully.
             * @retval Other values. Refer to the ESP IDF Programming guide.
             */
            esp_err_t connectAP(const std::string &ssid, 
                                const std::string &password);

            /**
             * @brief Disconnects to the currently connected AP.
             * @return ESP_OK Is a connection is established successfully.
             */
            esp_err_t disconnect(void);

            /**
             * @brief Scan for access points on a specific channel
             * @param specific_channel the number of the channel
             * @param apsList access point list passed by reference 
             * @return ESP_OK if the scan was successfully executed.
             */
            esp_err_t scanOnChannel(unsigned char specific_channel, 
                                    std::vector<accessPoint *> &apsList);

            /**
             * @brief Scan for a access points in a set of channels specified by a mask.
             * @param channel_mask the channel mask (set bits enable the scan on a specific channel).
             * @note For example: use mask 0x842 to scan the channels 1,6,11
             * @param apsLists list of lists of access points  
             * @return ESP_OK if the scan was successfully executed.
             */
            unsigned char scanWithMask(uint16_t channel_mask,
                                       std::vector<std::vector<accessPoint *>> &apsList);

        private:
            
            EventGroupHandle_t event_group; /** Used to syncronize tasks based on WiFi events. */

            /**
             * @brief Handles async events related to wifi connections.
             * @param ctx Pointer to a class that initialized the event loop.
             * @param event The generated event.
             */
            static esp_err_t eventHandler(void *ctx, system_event_t *event);

            /**
             * @brief Initializes the WiFi controller.
             * @return ESP_OK if init of the wifi controller was successful.
             */
            esp_err_t turnWiFiOn(void);

            /**
             * @brief Releases the WiFi controller.
             * @return ESP_OK if deinit of the wifi controller was successful.
             */
            esp_err_t turnWiFiOff(void);
    };

} /* drivers */

#endif /* ESPWIFIDRIVER_H */
