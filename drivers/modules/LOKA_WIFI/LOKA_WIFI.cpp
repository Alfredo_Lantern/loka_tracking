/**
 * @file LOKA_WIFI.cpp
 * @author Andres Arias
 * @author Alfredo Piedra
 * @date 17 Sep 2019
 * @brief WiFi driver for the ESP32.
 */

#include "LOKA_WIFI.h"

void format_nvs(void)
{
    const esp_partition_t* nvs_partition = 
        esp_partition_find_first(ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_NVS, NULL);      
    if(!nvs_partition)
        ESP_LOGE(ESPWIFI_TAG, "FATAL ERROR: No NVS partition found");
    esp_err_t err = (esp_partition_erase_range(nvs_partition, 0, nvs_partition->size));
    if(err != ESP_OK)
        ESP_LOGE(ESPWIFI_TAG, "FATAL ERROR: Unable to erase the partition");
}

namespace drivers
{
    ESPWiFiDriver::ESPWiFiDriver()
    {
        this->is_init = false;
        esp_err_t res = nvs_flash_init();
        if (res == ESP_ERR_NVS_NO_FREE_PAGES) 
        {
            format_nvs();
            ESP_ERROR_CHECK(nvs_flash_init());
        }
        tcpip_adapter_init();
        this->event_group = xEventGroupCreate();
        ESP_ERROR_CHECK(esp_event_loop_init(this->eventHandler, this));
        /*wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
        ESP_ERROR_CHECK(esp_wifi_init(&cfg));
        ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));*/
    }

    ESPWiFiDriver::~ESPWiFiDriver()
    {
        /*ESP_ERROR_CHECK(esp_wifi_stop());
        ESP_ERROR_CHECK(esp_wifi_deinit());*/
    }

    EventGroupHandle_t *ESPWiFiDriver::getEventGroup(void)
    {
        return &(this->event_group);
    }

    esp_err_t ESPWiFiDriver::turnWiFiOn(void)
    {
        wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
        ESP_ERROR_CHECK(esp_wifi_init(&cfg));
        ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));

        return ESP_OK;
    }

    esp_err_t ESPWiFiDriver::turnWiFiOff(void)
    {
        ESP_ERROR_CHECK(esp_wifi_stop());
        ESP_ERROR_CHECK(esp_wifi_deinit());

        return ESP_OK;
    }

    esp_err_t ESPWiFiDriver::eventHandler(void *ctx, 
                                          system_event_t *event)
    {
        /* The ctx pointer in the event handle will point to the ESPWiFiDriver
         * instance that initializes the event loop. */
        ESPWiFiDriver *wifi_ctrl = (ESPWiFiDriver *)ctx;

        switch (event->event_id) {

            case SYSTEM_EVENT_STA_START:
                ESP_LOGI(ESPWIFI_TAG, "WiFi Station initialized");
                wifi_ctrl->is_init = true;
                break;

            case SYSTEM_EVENT_STA_GOT_IP:
                ESP_LOGI(ESPWIFI_TAG, "WiFi Station got IP");
                xEventGroupSetBits(*(wifi_ctrl->getEventGroup()), BIT0);
                wifi_ctrl->is_connected = true;
                break;

            case SYSTEM_EVENT_STA_DISCONNECTED:
                ESP_LOGE(ESPWIFI_TAG, "WiFi Station disconnected");
                xEventGroupClearBits(*(wifi_ctrl->getEventGroup()), BIT0);
                wifi_ctrl->is_connected = false;
                break;

            case SYSTEM_EVENT_SCAN_DONE:
                ESP_LOGI(ESPWIFI_TAG, "Wifi Station finish scan");
                ESP_ERROR_CHECK(esp_wifi_scan_stop());
                xEventGroupSetBits(*(wifi_ctrl->getEventGroup()), WIFI_SCAN_STOP);
                break;

            default:
                break;
        }

        return ESP_OK; 
    }

    esp_err_t ESPWiFiDriver::connectAP(const std::string &ssid,
                                       const std::string &password)
    {
        wifi_config_t sta_config = {};
        std::strcpy((char*)sta_config.sta.ssid, ssid.c_str());
        std::strcpy((char*)sta_config.sta.password, password.c_str());
        sta_config.sta.bssid_set = false;

        ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &sta_config));
        ESP_ERROR_CHECK(esp_wifi_start());
        ESP_ERROR_CHECK(esp_wifi_connect());

        return ESP_OK;
    }

    esp_err_t ESPWiFiDriver::disconnect(void)
    {
        if (this->is_connected)
        {
            ESP_ERROR_CHECK(esp_wifi_disconnect());
            this->is_connected = false;
        }
        
        return ESP_OK;
    }

    esp_err_t ESPWiFiDriver::scanOnChannel(unsigned char specific_channel,
                                           std::vector<accessPoint *> &apsList)
    {
        wifi_scan_config_t scanConfig;
        uint16_t apNum = MAX_SCAN_SIZE;
        wifi_ap_record_t apRecords[MAX_SCAN_SIZE];

        scanConfig.ssid = 0;
        scanConfig.bssid = 0;
        scanConfig.channel = specific_channel;
        scanConfig.show_hidden = true;
        scanConfig.scan_type = WIFI_SCAN_TYPE_ACTIVE;
        scanConfig.scan_time.active.min = PARTIAL_SCAN_MIN_TIME;
        scanConfig.scan_time.active.max = PARTIAL_SCAN_MAX_TIME;

        turnWiFiOn();

        ESP_ERROR_CHECK(esp_wifi_start());
        ESP_ERROR_CHECK(esp_wifi_scan_start(&scanConfig, false));// Scan is non-blocking mode
															     // Wait for the scan to finish
        xEventGroupWaitBits(this->event_group, WIFI_SCAN_STOP, true, true, portMAX_DELAY);
	    
        // Retrieve the APs list
	    ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&apNum, apRecords));

        turnWiFiOff();

        for (int i = 0; i < apNum; ++i)
        {    
            apsList.push_back(new accessPoint());

            apsList[i]->category = 0;
            apsList[i]->rssi = -1*apRecords[i].rssi;
            apsList[i]->channel = apRecords[i].primary;
            apsList[i]->ssid = std::string((char *) apRecords[i].ssid);
            
            apsList[i]->bssid[0] = apRecords[i].bssid[0];
            apsList[i]->bssid[1] = apRecords[i].bssid[1];
            apsList[i]->bssid[2] = apRecords[i].bssid[2];
            apsList[i]->bssid[3] = apRecords[i].bssid[3];
            apsList[i]->bssid[4] = apRecords[i].bssid[4];
            apsList[i]->bssid[5] = apRecords[i].bssid[5];
        }
        
        return ESP_OK;
    }

    unsigned char ESPWiFiDriver::scanWithMask(uint16_t channelMask,
                                              std::vector<std::vector<accessPoint *>> &apsLists)
    {
        unsigned char ap_index = 0;
        
        for(unsigned char channel = 1; channel <= 11; ++channel)
        {
            channelMask = channelMask >> 1;

            if((channelMask & 0x01) == 0x01) 
            {
                //ESP_LOGI(ESPWiFi_TAG, ".... Scanning for channel %i", (int) channel);
                apsLists.push_back(std::vector<accessPoint *>());
                scanOnChannel(channel, apsLists[ap_index++]);
            }
        }

        return (unsigned char) apsLists.size();
    }

} /* drivers */ 

