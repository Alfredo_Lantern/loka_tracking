/**
 * @file LOKA_STUB_GPIO.h
 * @author Gabriel Sanchez
 * @author Andres Arias
 * @author Joao Pombas
 * @date 17/10/2019
 * @brief Driver for the ESP32 Stub GPIOs.
 */

#include "LOKA_GPIO.h"


RTC_DATA_ATTR int8_t rtc_gpios_ptr[40] = {	0,	// 0
											-1,	// 1
											1,	// 2
											-1,	// 3
											2,	// 4
											-1,	// 5
											-1,	// 6
											-1,	// 7
											-1,	// 8
											-1,	// 9
											-1,	// 10
											-1,	// 11
											3,	// 12
											4,	// 13
											5,	// 14
											6,	// 15
											-1,	// 16
											-1,	// 17
											-1,	// 18
											-1,	// 19
											-1,	// 20
											-1,	// 21
											-1,	// 22
											-1,	// 23
											-1,	// 24
											7,	// 25
											8,	// 26
											9,	// 27
											-1,	// 28
											-1,	// 29
											-1,	// 30
											-1,	// 31
											10,	// 32
											11,	// 33
											12,	// 34
											13,	// 35
											14,	// 36
											15,	// 37
											16,	// 38
											17	// 39
										};

// Why do we have pointers up to 16 and 18 structures?


RTC_DATA_ATTR rtc_io_t rtc_gpios[18] = {
    {0x98, 19, 17, 13, 27, 28, 16, 15, 31, 9,  0x3, 29, 11},			// Field values for GPIO0 retrieved from rtc_peripeh.c
    {0x9c, 19, 17, 13, 27, 28, 16, 15, 31, 10, 0x3, 29, 12},
    {0x94, 19, 17, 13, 27, 28, 16, 15, 31, 8,  0x3, 29, 10},
    {0xa8, 19, 17, 13, 27, 28, 16, 15, 31, 13, 0x3, 29, 15},
    {0xa4, 19, 17, 13, 27, 28, 16, 15, 31, 12, 0x3, 29, 14},
    {0xac, 19, 17, 13, 27, 28, 16, 15, 31, 14, 0x3, 29, 16},
    {0xa0, 19, 17, 13, 27, 28, 16, 15, 31, 11, 0x3, 29, 13},
    {0x84, 17, 15, 11, 27, 28, 14, 13, 29, 2,  0x3, 30, 6},
    {0x88, 17, 15, 11, 27, 28, 14, 13, 29, 3,  0x3, 30, 7},
    {0xb0, 19, 17, 13, 27, 28, 16, 15, 31, 15, 0x3, 29, 17},
	{0x8c, 17, 9,  5,  22, 23, 8,  7,  24, 16, 0x3, 25, 9},
    {0x8c, 18, 15, 11, 27, 28, 14, 13, 29, 17, 0x3, 30, 8},
    {0x80, 29, 26, 23, 0,  0,  25, 24, 31, 0,  0,   0,  4},
    {0x80, 28, 21, 18, 0,  0,  20, 19, 30, 1,  0,   0,  5},
    {0x7c, 27, 22, 19, 0,  0,  21, 20, 31, 4,  0,   0,  0},
    {0x7c, 26, 17, 14, 0,  0,  16, 15, 30, 5,  0,   0,  1},
    {0x7c, 25, 12, 9,  0,  0,  11, 10, 29, 6,  0,   0,  2},
	{0x7c, 24, 7,  4,  0,  0,  6,  5,  28, 7,  0,   0,  3},
};

RTC_DATA_ATTR int8_t rtc_state[40] = {0};

namespace drivers {

	esp_err_t GPIO_STUB::pinMode(gpio_num_t gpio, pin_mode_t mode, uint8_t rtc)
    {
		gpio_config_t io_conf;

		if (rtc && RTC_PINS_CONDITION) {										// Check if the GPIO is on the RTC domain or not
			rtc_state[gpio] = 1;
			rtc_gpio_init(gpio);											// Sets the pin configurations
			rtc_gpio_mode_t gpioMode = (mode == INPUT || mode == INPUT_PULLUP || mode == INPUT_PULLDOWN) ? \
					RTC_GPIO_MODE_INPUT_ONLY : RTC_GPIO_MODE_OUTPUT_ONLY;
			rtc_gpio_set_direction(gpio, gpioMode);
			if (mode == INPUT_PULLUP) {										// Pull up and pull down configuration
				rtc_gpio_pullup_en(gpio);
				rtc_gpio_pulldown_dis(gpio);
			} else if (mode == INPUT_PULLUP) {
				rtc_gpio_pulldown_en(gpio);
				rtc_gpio_pullup_dis(gpio);
			} else {
				rtc_gpio_pullup_dis(gpio);
				rtc_gpio_pulldown_dis(gpio);
			}
		} else {																// For pins on the CPU bus
			rtc_state[gpio] = 0;
			rtc_gpio_deinit(gpio);
			rtc_gpio_hold_dis(gpio);
			io_conf.intr_type = GPIO_INTR_DISABLE;							// Disable any associated interrupt
			io_conf.pin_bit_mask = ((uint64_t) 1) << (gpio);				// Bit mask for pins to set
																			// Pin Mode selection
			io_conf.mode = (mode == INPUT || mode == INPUT_PULLUP || mode == INPUT_PULLDOWN) ? GPIO_MODE_INPUT : GPIO_MODE_OUTPUT ;
																			// Pull-down mode selection
			io_conf.pull_down_en = (mode == INPUT_PULLDOWN) ? GPIO_PULLDOWN_ENABLE : GPIO_PULLDOWN_DISABLE;
																			// Pull-up mode selection
			io_conf.pull_up_en = (mode == INPUT_PULLUP) ? GPIO_PULLUP_ENABLE : GPIO_PULLUP_DISABLE;
			gpio_config(&io_conf);											// Push the configuration
		}
		return ESP_OK;
	}

	esp_err_t RTC_IRAM_ATTR GPIO_STUB::digitalWrite(gpio_num_t gpio, uint8_t value, uint8_t hold)
    {
		int8_t ptr;

		if (rtc_state[gpio]) {												// Check if the GPIO is on the RTC domain or not
			ptr = rtc_gpios_ptr[gpio];
			if(hold)
				CLEAR_PERI_REG_MASK(DR_REG_RTCIO_BASE + rtc_gpios[ptr].reg, 1UL << rtc_gpios[ptr].hold);
			if(value)
				WRITE_PERI_REG(RTC_GPIO_OUT_W1TS_REG, (1UL << (rtc_gpios[ptr].rtc_num + RTC_GPIO_OUT_DATA_W1TS_S)));
			else
				WRITE_PERI_REG(RTC_GPIO_OUT_W1TC_REG, (1UL << (rtc_gpios[ptr].rtc_num + RTC_GPIO_OUT_DATA_W1TC_S)));
			if(hold)
				SET_PERI_REG_MASK(DR_REG_RTCIO_BASE + rtc_gpios[ptr].reg, 1UL << rtc_gpios[ptr].hold);
		} else {
			GPIO_OUTPUT_SET(gpio, value);
		}
		return ESP_OK;
	}

	unsigned RTC_IRAM_ATTR char GPIO_STUB::digitalRead(gpio_num_t gpio)
    {
		int8_t ptr;
		uint32_t level;

		if (rtc_state[gpio]) {												// Check if the GPIO is on the RTC domain or not
			ptr = rtc_gpios_ptr[gpio];
			level = READ_PERI_REG(RTC_GPIO_IN_REG);
			return ((level >> (RTC_GPIO_IN_NEXT_S + rtc_gpios[ptr].rtc_num)) & 0x01);
		} else {
			return GPIO_INPUT_GET(gpio);
		}
	}


}
