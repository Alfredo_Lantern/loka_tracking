/**
 * @file LOKA_STUB_GPIO.h
 * @author Gabriel Sanchez
 * @author Andres Arias
 * @author Joao Pombas
 * @date 17/10/2019
 * @brief Driver for the ESP32 Stub GPIOs.
 */

#ifndef LOKA_GPIO_H_
#define LOKA_GPIO_H_

#include "driver/rtc_io.h"
#include "driver/gpio.h"

#include "soc/rtc_io_reg.h"
#include "soc/soc.h"
#include "esp_log.h"

#include "BoardIO.h"


/**
 * @brief Tag used by the logging library.
 */
#define GPIOSTUB_DRIVER_TAG		"gpio_stub_driver"

/**
 * @brief Check rtc valid pines
 */
#define RTC_PINS_CONDITION		(gpio == GPIO_NUM_0 || gpio == GPIO_NUM_2  || gpio == GPIO_NUM_4  || gpio == GPIO_NUM_12 || \
								gpio == GPIO_NUM_13 || gpio == GPIO_NUM_14 || gpio == GPIO_NUM_15 || gpio == GPIO_NUM_25 || \
								gpio == GPIO_NUM_26 || gpio == GPIO_NUM_27 || gpio == GPIO_NUM_32 || gpio == GPIO_NUM_33 || \
								gpio == GPIO_NUM_34 || gpio == GPIO_NUM_35 || gpio == GPIO_NUM_36 || gpio == GPIO_NUM_37 || \
								gpio == GPIO_NUM_38 || gpio == GPIO_NUM_39)

typedef struct {
	uint8_t reg;      /*!< Register of RTC pad, or 0 if not an RTC GPIO */
    uint8_t mux;       /*!< Bit mask for selecting digital pad or RTC pad */
    uint8_t func;      /*!< Shift of pad function (FUN_SEL) field */
    uint8_t ie;        /*!< Mask of input enable */
    uint8_t pullup;    /*!< Mask of pullup enable */
    uint8_t pulldown;  /*!< Mask of pulldown enable */
    uint8_t slpsel;    /*!< If slpsel bit is set, slpie will be used as pad input enabled signal in sleep mode */
    uint8_t slpie;     /*!< Mask of input enable in sleep mode */
    uint8_t hold;      /*!< Mask of hold enable */
    uint8_t hold_force;/*!< Mask of hold_force bit for RTC IO in RTC_CNTL_HOLD_FORCE_REG */
    uint8_t drv_v;     /*!< Mask of drive capability */
    uint8_t drv_s;     /*!< Offset of drive capability */
    uint8_t rtc_num;   /*!< RTC IO number, or -1 if not an RTC GPIO */
} rtc_io_t;

namespace drivers {
	/**
     * @class GPIO_STUB
     * @brief Low-level driver for the ESP32 Stub GPIO
     */
	namespace GPIO_STUB {

			/**
             * @brief Configure a specific pin as output/input, pullup/pulldown and rtc enable
             * @param[in] gpio GPIO number
			 * @param[in] mode 	INPUT, OUTPUT, INPUT_PULLDOWN, INPUT_PULLUP
			 * @param[in] value that will stored in the key 
			 * @retval ESP_OK Define configuration succesfully
			 * @retval ESP_FAIL if something went wrong while creating configuration
             */
			esp_err_t pinMode(gpio_num_t gpio, pin_mode_t mode, uint8_t rtc = 0);

			/**
             * @brief Writes a digital value in the desired GPIO pin
             * @param[in] gpio GPIO number
			 * @param[in] value Value to write on pin
			 * @param[in] hold Hold configuration
			 * @retval ESP_OK Write succesfully
			 * @retval ESP_FAIL if something went wrong while writing on pin
             */
			esp_err_t digitalWrite(gpio_num_t gpio, uint8_t value, uint8_t hold = 0);

			/**
             * @brief Reads the digital value from a GPIO pin
             * @param[in] gpio GPIO number
			 * @return Value read from gpio
             */
			unsigned char digitalRead(gpio_num_t gpio);
	};
}


#endif
