/**
 * @file LOKA_LIS3DX.cpp
 * @author Rafael Pires
 * @author Andres Arias
 * @date 5/11/2019
 * @brief Driver for the LIS3DE and LIS3DH accelerometers
 */

#include "LOKA_LIS3DX.h"

namespace drivers {

    LIS3DX::LIS3DX(uint8_t mode): spi_controller(ACC_CS, SPI_CLK, SPI_MISO, SPI_MOSI),
                                  nvs_controller()
    {
        
        gpio_pad_select_gpio(BLINK_GPIO);
        /* Set the GPIO as a push/pull output */
        gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);

        ESP_LOGI(LIS3DX_TAG, "Initializing accelerometer");
        if (this->spi_controller.read8(LIS3DX_WHO_AM_I) != 0x33) // TODO Unable to find the LIS3DX acceleromete
            ESP_LOGI(LIS3DX_TAG, "Chip detected: %x", this->spi_controller.read8(LIS3DX_WHO_AM_I)); //CTRL1 = 4bits data rate, 1bit low power, 1 bit X enable, 1bit Y enable, 1bit Z enable */
        this->setDataRate(LIS3DX_DATARATE_10_HZ);
        this->setAxis(1, 1, 1);
        this->setLPenBit(1);

        /* CTRL2 = 2bits high pass filter, 2bit high pass filter cutoff,
         * 1bit bypass filter, 1bit high pass for click, 1bit high pass for
         * Int 2, 1bit High Pass for Int1.
         * CTRL5 = 1bit boot, 1bit fifo_en, 2bits none, 1bit lir_ig1,
         * 1bit d4d_ig1, 1bit lig_ig2, 1bit d4d_ig2
         */ 
        this->setFilter(mode);

        /* CTRL4 = 1 bit BDU, 1bit none, 2bits range, 1 bit none, 2bits
         * test mode, 2bits this->spi_controllerconfigs (00-normal mode)
         */
        this->spi_controller.write8(LIS3DX_CTRL_REG4, 0x80); // BDU enabled
	    
        this->setRange(LIS3DX_RANGE_2_G);

        /* CTRL3 = 1bit click on INT1, 1bit event1 on INT1, 1bit event2
         * on INT1, 1bit dataready1 on INT1, 1bit dataready2 on INT1 | 1bit
         * watermark on INT1 | 1bit buffer overflow on INT1 | 1bit none
         */ 
        // Interruptions disabled by default:
        this->spi_controller.write8(LIS3DX_CTRL_REG3, 0x00);

        // CTRL6 = Same as CTRL3 but for INT2
        this->spi_controller.write8(LIS3DX_CTRL_REG6, 0x00);

        // TEMPCFG = [1bit ADC enable | 1 bit Temp EN | 6 bits none]
        // Configure ADC to temperature readout:
        this->spi_controller.write8(LIS3DX_TEMP_CFG_REG, 0xC0);

        /* FM [1:0] FIFO mode selection: 00-bypass, 01-fifo, 10-Stream,
         * 11-Stream to fifo
         * TR Trigger selection. Default value: 0: trigger event
         * linked to trigger signal on INT1 1: trigger event
         * linked to trigger signal on INT2
         * FTH [4:0] Default value: 0
         */

         // Disables watermark
        this->spi_controller.write8(LIS3DX_FIFO_CTRL_REG, 0x80); // Según el firmware
        // this->spi_controller.write8(LIS3DX_FIFO_CTRL_REG, 0x00);
        //this->spi_controller.write8(LIS3DX_FIFO_CTRL_REG, 0x40);

        this->getCalibration(); // Retrieves calibration from non-volatile memory
    }

    LIS3DX::~LIS3DX(void)
    {
        ESP_LOGI(LIS3DX_TAG, "Deinitializing accelerometer");
        this->spi_controller.~SPI();
    }

    void LIS3DX::saveCalibration(int calibrationFactor)
    {

        ESP_LOGI(LIS3DX_TAG, "Saving temperature calibration value: %d", calibrationFactor);
        char value[20];

        sprintf(value, "%d", calibrationFactor);
        this->nvs_controller.setFlash(FLASH_CALIBRATIONS_PAGE,
                                      LIS3DE_CALIB_TEMP_LABEL,
                                      value);
        temperatureOffset = calibrationFactor;
    }

    void LIS3DX::getCalibration(void)
    {
        char value[FLASH_SIZE_MAX];

        this->nvs_controller.getFlash(FLASH_CALIBRATIONS_PAGE,
                                      LIS3DE_CALIB_TEMP_LABEL,
                                      value);
        temperatureOffset = atoi(value);
        ESP_LOGI(LIS3DX_TAG, "Loaded temperature calibration: %d", temperatureOffset);
    }

    bool LIS3DX::checkConfiguration(void)
    {
        // Checks if the temperature configuration is still hold:
        char read_val = this->spi_controller.read8(LIS3DX_TEMP_CFG_REG);
        if (read_val == 0xC0) {
            ESP_LOGI(LIS3DX_TAG, "Temperature configuration holded");
            return true;
        } else {
            ESP_LOGI(LIS3DX_TAG, "Temperature configuration not holded");
            return false;
        }
    }

    void LIS3DX::clearConfigs(void)
    {
        ESP_LOGI(LIS3DX_TAG, "Clearning accelerometer configuration");
        // Resets all the registers to the default value:
        this->spi_controller.write8(LIS3DX_CTRL_REG5, 0b10000000);
    }

    uint8_t LIS3DX::readRegister(uint8_t reg)
    {
        return this->spi_controller.read8(reg);
    }

    uint16_t LIS3DX::readRegister16(uint8_t reg)
    {
        return this->spi_controller.read16(reg);
    }

    void LIS3DX::writeRegister(uint8_t reg, uint8_t data)
    {
        this->spi_controller.write8(reg, data);
    }

    void LIS3DX::setAxis(uint8_t xx, uint8_t yy, uint8_t zz)
    {
        ESP_LOGI(LIS3DX_TAG, "Setting axis values - x: %d, y: %d, z: %d", xx, yy, zz);
        char read_value;

        read_value = this->spi_controller.read8(LIS3DX_CTRL_REG1);
        read_value &= ~ 0x07;
        read_value |=  (xx << 2 | yy << 1 | zz); // Enables the desired axis
        this->spi_controller.write8(LIS3DX_CTRL_REG1, read_value);
    }

    void LIS3DX::setFilter(bool enable)
    {
        if (enable) {
            ESP_LOGI(LIS3DX_TAG, "Enabling filtering, disabling FIFO");
            // Enables filtering, sets normal mode:
            this->spi_controller.write8(LIS3DX_CTRL_REG2, 0xAF);
            this->spi_controller.write8(LIS3DX_CTRL_REG5, 0x08); // FIFO disabled
        } else {
            ESP_LOGI(LIS3DX_TAG, "Disabling filtering, enabling FIFO");
            this->spi_controller.write8(LIS3DX_CTRL_REG2, 0x00); // No filtering
            this->spi_controller.write8(LIS3DX_CTRL_REG5, 0x48); // FIFO enabled
        }
    }


    void LIS3DX::setWakeUp(bool itr, uint8_t thresh)
    {
        char response;

        this->setFilter(1);
        if(itr){
            ESP_LOGI(LIS3DX_TAG, "Enabling interrupt on pin 1");
            response = this->spi_controller.read8(LIS3DX_CTRL_REG3);
            // Enables interrupt 1 on pin 1:
            this->spi_controller.write8(LIS3DX_CTRL_REG3, response | 0x40);
        }else{
            ESP_LOGI(LIS3DX_TAG, "Disabling interrupt on pin 1");
            response = this->spi_controller.read8(LIS3DX_CTRL_REG3);
            // Disables interrupt 1 on pin 1:
            this->spi_controller.write8(LIS3DX_CTRL_REG3, response & (~0x40));
        }
        // Configures the event threshold:
        this->spi_controller.write8(LIS3DX_INT1_THS, thresh);
        // Configures the event duration:
        this->spi_controller.write8(LIS3DX_INT1_DURATION, 0x01);
        this->spi_controller.write8(LIS3DX_INT1_CFG, 0x2A);
    }

    char LIS3DX::getWakeUp(void)
    {
        ESP_LOGI(LIS3DX_TAG, "Checking for interrupts");
        char response = this->spi_controller.read8(LIS3DX_INT1_SRC);
        ESP_LOGI(LIS3DX_TAG, "Response wakeup: %d", (response & 0x40));
        return (response & 0x40);
    }

    void LIS3DX::setClick(uint8_t mode,
                          uint8_t clickthresh,
                          uint8_t duration,
                          uint8_t timelatency,
                          uint8_t timewindow) 
    {
        char response;

        this->setFilter(1);

        response = this->spi_controller.read8(LIS3DX_CTRL_REG3);

        if (mode == 0) { // Disable the interrupt
            ESP_LOGI(LIS3DX_TAG, "Disabling click interrupt");
            // Turn off I1_CLICK:
            this->spi_controller.write8(LIS3DX_CTRL_REG3, response & ~(0x80));
            this->spi_controller.write8(LIS3DX_CLICK_CFG, 0);
            return;
        }

        // Turn on int1 click:
        this->spi_controller.write8(LIS3DX_CTRL_REG3, response | 0x80);

        if (mode == 1)
            // Turn on all axes single click:
            this->spi_controller.write8(LIS3DX_CLICK_CFG, 0x15);
        if (mode == 2)
            // Turn on all axes double click
            this->spi_controller.write8(LIS3DX_CLICK_CFG, 0x2A);
        if (mode == 3)
            // Turn on all axes single and double click
            this->spi_controller.write8(LIS3DX_CLICK_CFG, 0x3F);

        this->spi_controller.write8(LIS3DX_CLICK_THS, clickthresh); // Arbitrary
        this->spi_controller.write8(LIS3DX_TIME_LIMIT, duration);
        this->spi_controller.write8(LIS3DX_TIME_LATENCY, timelatency);
        this->spi_controller.write8(LIS3DX_TIME_WINDOW, timewindow);
    }

    uint8_t LIS3DX::getClick(void)
    {
        ESP_LOGI(LIS3DX_TAG, "Cheking for click interrupt");
        char response =  this->spi_controller.read8(LIS3DX_CLICK_SRC);
        return (response & 0x20);
    }

    void LIS3DX::setWatermark(uint8_t level, bool itr)
    {

        ESP_LOGI(LIS3DX_TAG, "Configuring FIFO mark interrupt");
        char response;

        if (itr) {
            ESP_LOGI(LIS3DX_TAG, "Enabling FIFO mark interrupt");
            response = this->spi_controller.read8(LIS3DX_CTRL_REG3);
            // Enables the watermark interrupt:
            this->spi_controller.write8(LIS3DX_CTRL_REG3, response | 0x04);
        } else {
            ESP_LOGI(LIS3DX_TAG, "Disabling FIFO mark interrupt");
            response = this->spi_controller.read8(LIS3DX_CTRL_REG3);
            // Disables the watermark interrupt:
            this->spi_controller.write8(LIS3DX_CTRL_REG3, response & ~0x04);
        }
        if (level <= 31) {
            response = this->spi_controller.read8(LIS3DX_FIFO_CTRL_REG);
            response= (response & 0xE0) | level;
            this->spi_controller.write8(LIS3DX_FIFO_CTRL_REG, response );
        }
    }

    int LIS3DX::getWatermark(void)
    {
        ESP_LOGI(LIS3DX_TAG, "Checking if FIFO watermark interrupt has been triggered");
        char response = 0x1F & this->spi_controller.read8(LIS3DX_FIFO_CTRL_REG);
        return response;
    }

    int LIS3DX::checkWatermarkLevel(void)
    {
        ESP_LOGI(LIS3DX_TAG, "Checking current watermark level");
        char response = 0x80 & this->spi_controller.read8(LIS3DX_FIFO_SRC_REG);
        return response;
    }

    int LIS3DX::getNumberofAvailableSamples(void)
    {
        char response = this->spi_controller.read8(LIS3DX_FIFO_SRC_REG);

        if ((response & 0x20) != 0x00)
            return 0;
        else
            return response & 0x1F;
    }

    int LIS3DX::getTemperatureRaw(void)
    {
        char response = 0x0;
        char ctrl4;

        ctrl4 = this->spi_controller.read8(LIS3DX_CTRL_REG4);
        // BDU enabled:
        this->spi_controller.write8(LIS3DX_CTRL_REG4, (ctrl4 | 0x80) );
        vTaskDelay(110 / portTICK_PERIOD_MS); // Needed to update the available flag

        // Checks if a sample is available:
        if ((this->spi_controller.read8(LIS3DX_STATUS_REG_AUX) & 0x04) != 0x04) {
            // Wait 30 Hz sampling + overhead | Previous value 10Hz + overhead = 110000
            vTaskDelay(310 / portTICK_PERIOD_MS);
        }

        // Checks if a sample is available:
        if ((this->spi_controller.read8(LIS3DX_STATUS_REG_AUX) & 0x04) == 0x04) {
            this->spi_controller.read8(LIS3DX_OUT_ADC3_L);
            response = (char) this->spi_controller.read8(LIS3DX_OUT_ADC3_H);
            // BDU disabled:
            this->spi_controller.write8(LIS3DX_CTRL_REG4, (ctrl4 & 0x7F));
        } else {
            // BDU disabled:
            this->spi_controller.write8(LIS3DX_CTRL_REG4, (ctrl4 & 0x7F));
            return -100;
        }

        return (int8_t) response;
    }

    double LIS3DX::getInputVoltage(void)
    {
        int16_t z, r;

        char CTRL4 = this->spi_controller.read8(LIS3DX_CTRL_REG4);
        // BDU enabled:
        this->spi_controller.write8(LIS3DX_CTRL_REG4, (CTRL4 | 0x80));

        // Checks if a sample is available:
        if ((this->spi_controller.read8(LIS3DX_STATUS_REG_AUX) & 0x01) != 0x01)
            vTaskDelay(200 / portTICK_PERIOD_MS); // Wait 10 Hz sampling + overhead
        // Checks if a sample is available:
        if ((this->spi_controller.read8(LIS3DX_STATUS_REG_AUX) & 0x01) == 0x01) {
            z = this->spi_controller.read16(LIS3DX_OUT_ADC1_L);
            r = ((z & 0xFFC0) >> 6);
            if(r & 0x200)
                r |= 0xFC00; // Performs signal extension
            // BDU disabled:
            this->spi_controller.write8(LIS3DX_CTRL_REG4, (CTRL4 & 0x7F));
            // Those values were calibrated manually:
            return (1.313 - ((double)(r) * 0.000851));

            /* The input range is 1200 mv ±400 mV and the data output is
             * expressed in left-aligned 2's complement.
             * The ADC resolution is 10 bits if the LPen (bit 3) in
             * CTRL_REG1 (20h) is cleared (high resolution normal mode),
             * otherwise, in low-power mode, the ADC resolution is 8-bit.
             * Channel 3 of the ADC can be connected to the
             * temperature sensor by setting the TEMP_EN bit (bit 6)
             * to 1 in TEMP_CFG_REG (1Fh). Refer to Table 5: Temperature
             * sensor characteristics for the conversion factor.
             */

        } else {
            // BDU disabled:
            this->spi_controller.write8(LIS3DX_CTRL_REG4, (CTRL4 & 0x7F));
            return 0.0;
        }
    }

    int RTC_IRAM_ATTR LIS3DX::readTemperature(void)
    {
        uint8_t response, ctrl4;
        int temperatureValue = 0;

        //spi.init(ACC_CS, SPI_CLK, SPI_MISO, SPI_MOSI);
        ctrl4 = this->spi_controller.read8(LIS3DX_CTRL_REG4);
        // BDU enabled:
        this->spi_controller.write8(LIS3DX_CTRL_REG4, (ctrl4 | 0x80));

        // Checks if a sample is available
        if ((this->spi_controller.read8(LIS3DX_STATUS_REG_AUX) & 0x04) != 0x04) {
             // Wait 30 Hz sampling + overhead | Previous value 10Hz + overhead = 110000
            ets_delay_us(310000);
        } // Insufficient in successive readings
        if ((this->spi_controller.read8(LIS3DX_STATUS_REG_AUX) & 0x04) == 0x04) {
            this->spi_controller.read8(LIS3DX_OUT_ADC3_L);
            response = (char) this->spi_controller.read8(LIS3DX_OUT_ADC3_H);
            // BDU disabled:
            this->spi_controller.write8(LIS3DX_CTRL_REG4, (ctrl4 & 0x7F));
            temperatureValue = ((int8_t) response) + temperatureOffset;
        } else
            temperatureValue = -100;

        return temperatureValue;
    }

    bool RTC_IRAM_ATTR LIS3DX::checkMovement(void)
    {
        bool movementValue = 0; // Check for movement

        movementValue = (0x40 & this->spi_controller.read8(LIS3DX_INT1_SRC)) == 0x40;

        return movementValue;
    }

    void LIS3DX::setRange(LIS3DX_range_t range)
    {
        uint8_t response = this->spi_controller.read8(LIS3DX_CTRL_REG4);

        response &= ~(0x30);
        response |= range << 4;
        this->spi_controller.write8(LIS3DX_CTRL_REG4, response);
    }

    int LIS3DX::getRange(void)
    {
        return (int) ((this->spi_controller.read8(LIS3DX_CTRL_REG4) >> 4) & 0x03);
    }

    void LIS3DX::setDataRate(LIS3DX_dataRate_t dataRate)
    {
        uint8_t ctl1 = this->spi_controller.read8(LIS3DX_CTRL_REG1);
        ctl1 &= ~(0xF0);
        ctl1 |= (dataRate << 4);
        this->spi_controller.write8(LIS3DX_CTRL_REG1, ctl1);
    }

    int LIS3DX::getDataRate(void)
    {
        int rates[] = {0, 1, 10, 25, 50, 100, 200, 400};
        LIS3DX_dataRate_t data_rate = (LIS3DX_dataRate_t)((this->spi_controller.read8(LIS3DX_CTRL_REG1) >> 4)& 0x0F);
        return rates[data_rate];
    }

    Accel LIS3DX::readAxis(void)
    {
        // Y and X to use the reference of the device
        uint8_t range = LIS3DX::getRange();
        uint16_t divider = 1;
        Accel acc;

        if (range == LIS3DX_RANGE_16_G) divider = 7;
        if (range == LIS3DX_RANGE_8_G) divider = 15;
        if (range == LIS3DX_RANGE_4_G) divider = 31;
        if (range == LIS3DX_RANGE_2_G) divider = 63;

        this->x_axis = this->spi_controller.read8(LIS3DX_REG_OUT_Y);
        this->y_axis = this->spi_controller.read8(LIS3DX_REG_OUT_X);
        this->z_axis = this->spi_controller.read8(LIS3DX_REG_OUT_Z);
        acc.x = (float) this->x_axis/ divider;	
        acc.y = (float) -(this->y_axis)/ divider;
        acc.z = (float) -(this->z_axis)/ divider;


        #ifndef CONFIG_ENABLE_LIS3DE

        this->xl_axis = this->spi_controller.read8(LIS3DX_REG_OUT_YL);
        this->yl_axis = this->spi_controller.read8(LIS3DX_REG_OUT_XL);
        this->zl_axis = this->spi_controller.read8(LIS3DX_REG_OUT_ZL);

        acc.xl = (float) this->xl_axis/ divider;	
        acc.yl = (float) -(this->yl_axis)/ divider;
        acc.zl = (float) -(this->zl_axis)/ divider;
        #endif

        return acc;
    }

    void LIS3DX::setLPenBit(uint8_t LPenBit){
        uint8_t ctl1 = this->spi_controller.read8(LIS3DX_CTRL_REG1);
        ctl1 &= (0xF7);
        ctl1 |= (LPenBit << 3);
        this->spi_controller.write8(LIS3DX_CTRL_REG1, ctl1);
    }


} /* namespace drivers */ 