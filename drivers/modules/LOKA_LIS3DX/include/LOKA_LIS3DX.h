/**
 * @file LOKA_LIS3DX.h
 * @author Rafael Pires
 * @author Andres Arias
 * @author Jairo Ortega
 * @date 12/12/2019
 * @brief Driver for the LIS3DE and LIS3DH accelerometers
 */

#ifndef LOKA_LIS3DX_H
#define LOKA_LIS3DX_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_err.h"
#include "BoardIO.h"
#include "LOKA_SPI.h"
#include "LOKA_NVS.h"

#include "stdio.h"
#include "stdint.h"

#include "sdkconfig.h"

#include "LOKA_LIS3DX_Constans.h"

#define LIS3DX_TAG "LIS3DX"
#define LIS3DE_CALIB_TEMP_LABEL "lis3dx"

extern int temperatureOffset;
namespace drivers
{
    typedef enum
    {
        LIS3DX_RANGE_16_G = 0b11, // +/- 16g
        LIS3DX_RANGE_8_G = 0b10,  // +/- 8g
        LIS3DX_RANGE_4_G = 0b01,  // +/- 4g
        LIS3DX_RANGE_2_G = 0b00   // +/- 2g (default value)
    } LIS3DX_range_t;

    typedef enum
    {
        LIS3DX_AXIS_X = 0x0,
        LIS3DX_AXIS_Y = 0x1,
        LIS3DX_AXIS_Z = 0x2
    } LIS3DX_axis_t;

    typedef enum // Used on register 0x2A (LIS3DE_REG_CTRL_REG1) to set bandwidth
    {
        LIS3DX_DATARATE_400_HZ = 0b0111, // 400Hz
        LIS3DX_DATARATE_200_HZ = 0b0110, // 200Hz
        LIS3DX_DATARATE_100_HZ = 0b0101, // 100Hz
        LIS3DX_DATARATE_50_HZ = 0b0100,  // 50Hz
        LIS3DX_DATARATE_25_HZ = 0b0011,  // 25Hz
        LIS3DX_DATARATE_10_HZ = 0b0010,  // 10 Hz
        LIS3DX_DATARATE_1_HZ = 0b0001,   // 1 Hz
        LIS3DX_DATARATE_POWERDOWN = 0,
        LIS3DX_DATARATE_LOWPOWER_1K6HZ = 0b1000,
        LIS3DX_DATARATE_LOWPOWER_5KHZ = 0b1001,

    } LIS3DX_dataRate_t;

    #ifdef CONFIG_ENABLE_LIS3DE
        typedef struct
        {
            float x;
            float y;
            float z;
        } Accel;
    #else
        typedef struct
        {
            float x;
            float xl;
            float y;
            float yl;
            float z;
            float zl;
        } Accel;
    #endif

    class LIS3DX{

    public:
        /**
                 * @brief Class constructor. Initializes the on-board accelerometer with
                 * filter mode on or off
                 * @param[in] mode The selected filtering mode for the accelerometer.
                 */
        LIS3DX(uint8_t mode);

        /**
                 * @brief Class destructor.
                 */
        ~LIS3DX(void);

        /**
         * @brief Verifies if the accelerometer holds the previously
         * set configuration.
         * @retval true The devices holds the previous configuration.
         * @retval false The device is not holding the previous configuration.
         */
        bool checkConfiguration(void);

        /**
         * @brief Clears any previous configuration on the LIS3DX
         */
        void clearConfigs(void);

        /**
         * @brief Checks if the accelerometer registered any significant
         * movement.
         * @retval true The device did move.
         * @retval false The device did not move.
         */
        bool checkMovement(void);

        /**
         * @brief Gets the temperature from the accelerometer's internal
         * temperature sensor.
         * @return The latest temperature reading.
         */
        int readTemperature(void);

        /**
         * 
         * @brief Gets the temperature from the accelerometer's internal
         * temperature sensor without calibration offset.
         * @return The latest temperature reading without calibration.
         */
        int getTemperatureRaw(void);

        /**
         * @brief Reads the input voltage on the ADC1 input of
         * the accelerometer.
         * @return The voltage read from the built-in ADC1 input.
         */
        double getInputVoltage(void);

        /**
         * @brief Enables or disables axis readout on the LIS3DX.
         * @param[in] xx Enabling or disabling the X axis.
         * @param[in] yy Enabling or disabling the Y axis.
         * @param[in] zz Enabling or disabling the Z axis.
         */
        void setAxis(uint8_t xx, uint8_t yy, uint8_t zz);

        /**
         * @brief Enables or disables filtering and FIFO.
         * @param[in] enable If true, enables filtering and disables
         * FIFO, if false, disables filtering and enables FIFO.
         */
        void setFilter(bool enable);

        /**
         * @brief Sets the acceleration readout range of the accelerometer.
         * @param[in] range Selected readout range.
         */
        void setRange(LIS3DX_range_t range);

        /**
         * @brief Return the acceleration readout range of the accelerometer.
         * @return Currently configured readout range.
         */
        int getRange(void);

        /**
         * @brief Sets the sampling rate for the accelerometer.
         * param[in] dataRate The selected sampling rate.
         */
        void setDataRate(LIS3DX_dataRate_t dataRate);

        /**
         * @brief Gets the currently configured sampling rate
         * for the accelerometer.
         * @return The currently configured sampling rate.
         */
        int getDataRate(void);

        // ------------ MODE 1 ------------

        /**
         * @brief Configures the LIS3DX interrupt.
         * @param[in] itr Whether or not to emit an interrupt signal.
         * @param[in] thresh The limit for emitting an interrupt.
         */
        void setWakeUp(bool itr, uint8_t thresh);

        /**
         * @brief Checks if an interrupt has been generated or not on the
         * accelerometer.
         * @retval 1 The accelerometer generated an interrupt.
         * @retval 0 The accelerometer did not generate an interrupt.
         */
        char getWakeUp(void);

        /**
         * @brief Configures or disables the click interrupt on the
         * accelerometer.
         * @param[in] mode The selected click detection mode. Possible values 
         * are:
         * * 1: All axes single click.
         * * 2: All axes double click.
         * * 3: All axes single and double click.
         * @param[in] clickthresh The movement threshold to count as "click".
         * @param[in] duration Time above the threshold to count as "click".
         * @param[in] timelatency Time the interrupt will be kept high when a
         * click happens.
         * @param[in] timewindow Time between clicks to count as double click.
         */
        void setClick(uint8_t mode,
                    uint8_t clickthresh,
                    uint8_t duration = 60,
                    uint8_t timelatency = 120,
                    uint8_t timewindow = 60);

        /**
         * @brief Checks if a click interrupt has been generated or not
         * on the accelerometer.
         * @return Whether the click interrupt is HIGH or not.
         */
        uint8_t getClick(void);

        // ------------ MODE 2 ------------

        /**
         * @brief Sets the FIFO watermark interrupt.
         * @note Sets an interrupt the amount of elements inside the
         * accelerometer's FIFO memory gets higher than the selected
         * threshold.
         * @param[in] level The maximum amount of elements before triggering
         * an interrupt.
         * @param[in] itr Enables (true) the hardware interrupt or not (false).
         */
        void setWatermark(uint8_t level, bool itr);

        /**
                 * @brief Retrieves the current accelerometer FIFO
                 * watermark level
                 * @return The current amount of elements inside the FIFO.
                 */
        int getWatermark(void);

        /**
                 * @brief Checks if the accelerometer FIFO watermark level
                 * has been reached.
                 * @return 1 if the watermark level has been reached, 0 if not.
                 */
        int checkWatermarkLevel(void);

        /**
                 * @brief Verify how many spaces are there available
                 * on the accelerometer FIFO.
                 * @return The number of available samples on FIFO memory.
                 */
        int getNumberofAvailableSamples(void);

        /**
                 * @brief Reads the acceleration values for the every
                 * axis on the LIS3DX
                 * @return Accel structure with the acceleration values
                 * for x, y and z axes.
                 */
        Accel readAxis(void);

        /**
         * @brief Stores a new temperature offset.
         * @param[in] calibrationFactor The offset that will be added
         * to every temperature reading.
         */
        void saveCalibration(int calibrationFactor);
        
        /**
         * @brief Set the LPen bit on the LIS3DX_CTRL_REG1 register 
         * @param LPenBit value of bit: 1 or 0
         */
        void setLPenBit(uint8_t LPenBit);        

    private:
        drivers::SPI spi_controller; /** SPI bus driver */

        drivers::NVS nvs_controller; /** Non-volatile memory driver */
        
        int8_t x_axis;
        int8_t y_axis;
        int8_t z_axis;

        #ifndef CONFIG_ENABLE_LIS3DE
        int8_t xl_axis;
        int8_t yl_axis;
        int8_t zl_axis;
        #endif

        /**
         * @brief Loads temperature offset from the NVS.
         */
        void getCalibration(void);
        /**
         * @brief Reads 8 bits from the accelerometer registers via SPI.
         * @param[in] reg Register address.
         * @return The values read from the register.
         */
        uint8_t readRegister(uint8_t reg);

        /**
         * @brief Reads 16 bits from the accelerometer registers via SPI.
         * @param[in] reg Register address.
         * @return The values read from the register.
         */
        uint16_t readRegister16(uint8_t reg);

        /**
         * @brief Writes 8 bits to the accelerometer registers via SPI.
         * @param[in] reg Register address.
         * @paramp[in] The values to write on the selected register.
         */
        void writeRegister(uint8_t reg, uint8_t data);
    };

} // namespace drivers

#endif

