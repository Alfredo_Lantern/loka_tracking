/**
 * @file ESPULP.h
 * @author Andres Arias
 * @date 27/9/2019
 * @brief Driver for interacting with the ultra-low power coprocessor 
 * found on the ESP32.
 */

#include "LOKA_ULP.h"

#include "ulp_service.h" // Must be included here to avoid "no such file" errors
                         // with modules importing the ESPULP component.

extern "C"{
	uint32_t esp_clk_slowclk_cal_get();
}

extern const uint8_t ulp_main_bin_start[] asm("_binary_ulp_service_bin_start");
extern const uint8_t ulp_main_bin_end[]   asm("_binary_ulp_service_bin_end");

static RTC_DATA_ATTR uint64_t g_timer_increment;

namespace drivers {

    ESPULP::ESPULP(uint32_t useconds)
    {
        unsigned int ulp_period;

        ulp_load_binary(0, ulp_main_bin_start, (ulp_main_bin_end - ulp_main_bin_start)/sizeof(uint32_t));
        ulp_period = rtc_time_us_to_slowclk((uint64_t) 100000, (uint32_t) esp_clk_slowclk_cal_get());
        REG_SET_FIELD(SENS_ULP_CP_SLEEP_CYC0_REG, SENS_SLEEP_CYCLES_S0, ulp_period);

        ulp_wakeup_enable = ULP_TIMER_WAKEUP | ULP_BUTTON_WAKEUP;
        ulp_battery_locked = 0;
        ulp_run((&ulp_entry - RTC_SLOW_MEM)/sizeof(uint32_t));
    }

    ESPULP::~ESPULP(void)
    {
    }

    void ESPULP::run(void)
    {
        ESP_LOGI(ESPULP_TAG, "Booting ULP");
        ulp_run((&ulp_entry - RTC_SLOW_MEM) / sizeof(uint32_t));
    }

    void ESPULP::setWakeUpTime(uint32_t seconds)
    {
        if (seconds == 0) {
            this->disableTimerWakeUp();
            g_timer_increment = 0;
            ulp_target_time3 = 0;
            ulp_target_time2 = 0;
            ulp_target_time1 = 0;
            ulp_target_time0 = 0;
        } else {
            this->enableTimerWakeUp();
            g_timer_increment = rtc_time_us_to_slowclk((uint64_t) seconds * 1000000,
                                (uint32_t) esp_clk_slowclk_cal_get());
            uint64_t timer_value = rtc_time_get() + g_timer_increment;
            ulp_target_time3 = (timer_value >> 48) & 0xFFFF;
            ulp_target_time2 = (timer_value >> 32) & 0xFFFF;
            ulp_target_time1 = (timer_value >> 16) & 0xFFFF;
            ulp_target_time0 = timer_value & 0xFFFF;
        }
    }

    void ESPULP::enableWakeUp(uint8_t wakeup_source)
    {
        ulp_wakeup_enable = wakeup_source;
    }

    void ESPULP::lockBattery(void)
    {
        ulp_battery_locked = 1;
        unsigned int ulp_period = rtc_time_us_to_slowclk(
                                  (uint64_t) ULP_BATTERY_PERIOD * 1000000,
                                  (uint32_t) esp_clk_slowclk_cal_get());
        REG_SET_FIELD(SENS_ULP_CP_SLEEP_CYC0_REG, SENS_SLEEP_CYCLES_S0, ulp_period);
    }

    void ESPULP::disableTimerWakeUp(void)
    {
        ulp_wakeup_enable = ulp_wakeup_enable & ~ULP_TIMER_WAKEUP;
    }

    void ESPULP::enableTimerWakeUp(void)
    {
        ulp_wakeup_enable = ulp_wakeup_enable | ULP_TIMER_WAKEUP;
    }

    void ESPULP::resetWakeUp(void){
	uint64_t timerValue;

	timerValue = RTC_getTimer();

	// ets_printf(wakeString, timerValue);

	timerValue += g_timer_increment;

	// ets_printf(wakeString, timerValue);

	ulp_target_time3  = (timerValue >> 48) & 0xFFFF;
	ulp_target_time2  = (timerValue >> 32) & 0xFFFF;
	ulp_target_time1  = (timerValue >> 16) & 0xFFFF;
	ulp_target_time0  = timerValue & 0xFFFF;
}

} /* drivers */
