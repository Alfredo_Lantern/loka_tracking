/**
 * @file ESPULP.h
 * @author Andres Arias
 * @date 27/9/2019
 * @brief Driver for interacting with the ultra-low power coprocessor 
 * found on the ESP32.
 */

#ifndef ESPULP_H
#define ESPULP_H

#include <cstdint>
#include "esp32/ulp.h"
#include "soc/rtc.h"
#include "soc/sens_reg.h"
#include "esp_attr.h"
#include "esp_sleep.h"
#include "esp_log.h"

#include "LOKA_RTC.h"
/**
 * @brief Tag used by the logging library.
 */
#define ESPULP_TAG "LOKAULP"

/**
 * @brief How often the ULP boots and runs the loaded program.
 */
#define ULP_WAKEUP_PERIOD           100000

/**
 * @brief Bitmask indicating timer wakeup trigger.
 */
#define ULP_TIMER_WAKEUP			0x01

/**
 * @brief Bitmask indicating button wakeup trigger.
 */
#define ULP_BUTTON_WAKEUP			0x02

/**
 * @brief Bitmask indicating accelerometer wakeup trigger.
 */
#define ULP_ACCELEROMETER_WAKEUP	0x04

/**
 * @brief Bitmask indicating GPIO wakeup trigger.
 */
#define ULP_INTERRUPT_WAKEUP		0x08

#define ULP_BATTERY_PERIOD          30

namespace drivers {

    /**
     * @class ESPULP
     * @brief Module for interacting with the ultra-low power coprocessor.
     */
    class ESPULP {

        public:

            //ESPULP();
            /**
             * @brief Class constructor. Loads the ULP program 
             * to the RTC memory.
             * @param[in] seconds How often should the ULP boot and
             * run the loaded program. Default: every 0.1 s.
             */
            ESPULP(uint32_t useconds = ULP_WAKEUP_PERIOD);

            /**
             * @brief Class destructor.
             */
            ~ESPULP(void);

            /**
             * @brief Boots the coprocessor and makes it run periodically 
             * according to the defined wakeup period.
             */
            void run(void);

            /**
             * @brief Commands the ULP to wake the main processor after
             * an amount of time has passed.
             * @param[in] seconds The amount of time to sleep in seconds.
             */
            void setWakeUpTime(uint32_t seconds);

            /**
             * @brief Indicates the ULP to send a wakeup signal when the
             * selected event is detected.
             * @param[in] wakeup_source ULP_BUTTON_WAKEUP for selecting
             * a button wakeup, ULP_ACCELEROMETER_WAKEUP for selecting
             * a movement detection wakeup or ULP_INTERRUPT_WAKEUP for
             * a GPIO wakup.
             */
            void enableWakeUp(uint8_t wakeup_source);

            /**
             * @brief Sets the battery lock message.
             */
            void lockBattery(void);


	        void resetWakeUp(void);

        private:
            uint32_t wakeup_timer; /** Period for booting the ULP */

            /**
             * @brief Enables the ULP wakeup trigger after a certain amounto
             * of time has passed.
             */
            void enableTimerWakeUp(void);

            /**
             * @brief Disables the timer wakeup.
             */
            void disableTimerWakeUp(void);
    };


} /* drivers */

#endif
