#==================================
# Date: 27/9/2019
# Author: Andres Arias
# Company: Lantern Technologies
# Description: Component makefile
#==================================

COMPONENT_ADD_INCLUDEDIRS = include
ULP_APP_NAME ?= ulp_service

ULP_S_SOURCES =  $(COMPONENT_PATH)/ulp/ulp_service.S

ULP_EXP_DEP_OBJECTS := ulp_service.o

# Include build rules for ULP program 
include $(IDF_PATH)/components/ulp/component_ulp_common.mk
