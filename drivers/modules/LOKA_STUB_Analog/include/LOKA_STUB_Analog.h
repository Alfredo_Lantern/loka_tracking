/**
 * @file LOKA_STUB_Analog.h
 * @author Gabriel Sanchez
 * @author Andres Arias
 * @author Joao Pombas
 * @date 17/10/2019
 * @brief Driver for the ESP32 Analog Stub.
 */

#ifndef LOKA_ANALOG_STUB_H_
#define LOKA_ANALOG_STUB_H_

#include "soc/rtc.h"
#include "driver/touch_pad.h"
#include "driver/adc.h"
#include "driver/dac.h"
#include "driver/touch_pad.h"
#include "soc/sens_reg.h"
#include "soc/sens_struct.h"
#include "soc/timer_group_reg.h"
#include "esp_log.h"

/**
 * @brief Tag used by the logging library.
 */
#define ANALOGSTUB_DRIVER_TAG   "analog_stub_driver"

/**
 * @brief Defines ADC_WIDTH for readings
 */
#define DEFAULT_ADC_WIDTH 		ADC_WIDTH_12Bit

namespace drivers {
	/**
     * @class AnalogSTUB
     * @brief Low-level driver for the ESP32 Analog Stub
     */
	class AnalogSTUB{
		public:
			/**
             * @brief Class constructor.
             */
			AnalogSTUB();
			/**
             * @brief Class destructor. 
             */
			~AnalogSTUB();

			/**
             * @brief Define specific pin as analog input for adc reading on rtc memory
             * @param[in] adc_unit Define which of two internal ADC will be used
			 * @param[in] channel Describe which channel will be used in selected ADC Unit
			 * @param[in] atten Defines what kind of attenuation will be applied to 
			 * @retval ESP_OK Init Analog Pin Successfully.
			 * @retval ESP_FAIL if something went wrong analog init
             */
			esp_err_t initAnalogPin(adc_unit_t adc_unit, adc_channel_t channel, adc_atten_t atten);

			
			/**
             * @brief Sets the adc sample width on rtc memory
             * @param[in] adc_unit Define which of two internal ADC will be used
			 * @param[in] width_bit Width of the sample
			 * @retval ESP_OK Add configuration Successfully.
			 * @retval ESP_FAIL if something went wrong while adding configuration
             */
			esp_err_t adcConfigWidth(adc_unit_t adc_unit, adc_bits_width_t width_bit);

			/**
             * @brief Read analog value from adc1
             * @param[in] channel Define which of internal ADC1 will be used
			 * @param[in] samples Amount of samples of reading
			 * @return value Average of samples readed by ADC depends of ADC width
             */
			unsigned int adc1ReadValue(adc1_channel_t channel, uint32_t samples);

			/**
             * @brief Read analog value from adc1
             * @param[in] channel Define which of internal ADC2 will be used
			 * @param[in] samples Amount of samples of reading
			 * @return value Average of samples readed by ADC depends of ADC width
             */
			unsigned int adc2ReadValue(adc2_channel_t channel, uint32_t samples);

			/**
             * @brief Read the hall sensor without LNA
			 * @return value Read from hall sensor
             */
			int hallSensorRead();

	};
}



#endif
