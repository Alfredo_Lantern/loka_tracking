
/**
 * @file LOKA_STUB_Analog.cpp
 * @author Gabriel Sanchez
 * @author Andres Arias
 * @author Joao Pombas
 * @date 17/10/2019
 * @brief Driver for the ESP32 Analog Stub.
 */

#include "LOKA_STUB_Analog.h"

namespace drivers {

	AnalogSTUB::AnalogSTUB(){
		ESP_LOGI(ANALOGSTUB_DRIVER_TAG, "Init driver");

	}

	AnalogSTUB::~AnalogSTUB(){
		ESP_LOGI(ANALOGSTUB_DRIVER_TAG, "Task Done");
	}

	esp_err_t RTC_IRAM_ATTR AnalogSTUB::initAnalogPin(adc_unit_t adc_unit, adc_channel_t channel, adc_atten_t atten){
		uint32_t i, index;

		if (adc_unit == ADC_UNIT_1) {
			SET_PERI_REG_BITS(SENS_SAR_ATTEN1_REG, SENS_SAR1_ATTEN_VAL_MASK, atten, (channel * 2));
		}
		if (adc_unit == ADC_UNIT_2) {
			SET_PERI_REG_BITS(SENS_SAR_ATTEN2_REG, SENS_SAR2_ATTEN_VAL_MASK, atten, (channel * 2));
		}
		return ESP_OK;
	}

	esp_err_t RTC_IRAM_ATTR AnalogSTUB::adcConfigWidth(adc_unit_t adc_unit, adc_bits_width_t width_bit){

		if (adc_unit == ADC_UNIT_1) {
			SENS.sar_start_force.sar1_bit_width = width_bit;					//adc1_config_width
			SENS.sar_read_ctrl.sar1_sample_bit = width_bit;
			SENS.sar_read_ctrl.sar1_data_inv = true;							// Enable ADC data invert

		} else if(adc_unit == ADC_UNIT_2) {										// adc2_config_width
			SENS.sar_start_force.sar2_bit_width = width_bit;					// sar_start_force shared with ADC1
			SENS.sar_read_ctrl2.sar2_data_inv = 1;								// Invert the adc value,the Output value is invert
			SENS.sar_read_ctrl2.sar2_sample_bit = width_bit;					// Set The adc sample width, invert adc value, must digital sar2_bit_width[1:0]=3
			SENS.sar_read_ctrl2.sar2_pwdet_force = 0;							// Take the control from WIFI
		}
		return ESP_OK;
	}

	unsigned int RTC_IRAM_ATTR AnalogSTUB::adc1ReadValue(adc1_channel_t channel, uint32_t samples){
		uint32_t nextSample, accumulator;

		SENS.sar_read_ctrl.sar1_dig_force = 0;									// adc1_adc_mode_acquire: Switch SARADC into RTC channel

		if (SENS.sar_meas_wait2.force_xpd_sar & SENS_FORCE_XPD_SAR_SW_M) {		// adc_power_on
			SENS.sar_meas_wait2.force_xpd_sar = SENS_FORCE_XPD_SAR_PU;
		} else {
			SENS.sar_meas_wait2.force_xpd_sar = SENS_FORCE_XPD_SAR_FSM;
		}

		SENS.sar_meas_start1.meas1_start_force = 1;								// Adc Controler is Rtc module,not ulp coprocessor
		SENS.sar_meas_wait2.force_xpd_amp = 0x2;								// Disable Amp Bit1=0:Fsm  Bit1=1(Bit0=0:PownDown Bit10=1:Powerup)
		SENS.sar_meas_start1.sar1_en_pad_force = 1;								// Open the ADC1 Data port Not ulp coprocessor

		SENS.sar_meas_start1.sar1_en_pad = (1 << channel);						// Select the channel to read

		samples = samples < 1 ? 1 : samples;									// Divide by 0 protection
		accumulator = 0;
		for(nextSample = 0; nextSample < samples; nextSample++){
			SENS.sar_meas_ctrl.amp_rst_fb_fsm = 0;
			SENS.sar_meas_ctrl.amp_short_ref_fsm = 0;
			SENS.sar_meas_ctrl.amp_short_ref_gnd_fsm = 0;
			SENS.sar_meas_wait1.sar_amp_wait1 = 1;
			SENS.sar_meas_wait1.sar_amp_wait2 = 1;
			SENS.sar_meas_wait2.sar_amp_wait3 = 1;
			while (SENS.sar_slave_addr1.meas_status != 0){
				REG_WRITE(TIMG_WDTFEED_REG(0), 1);
				ets_delay_us(1);
			}
			SENS.sar_meas_start1.meas1_start_sar = 0;
			SENS.sar_meas_start1.meas1_start_sar = 1;
			while (SENS.sar_meas_start1.meas1_done_sar == 0){
				REG_WRITE(TIMG_WDTFEED_REG(0), 1);
				ets_delay_us(1);
			}
			accumulator += SENS.sar_meas_start1.meas1_data_sar;					// Read reg value
		}

		return accumulator/samples;
	}

	unsigned int RTC_IRAM_ATTR AnalogSTUB::adc2ReadValue(adc2_channel_t channel, uint32_t samples){
		uint32_t nextSample, accumulator;

		SENS.sar_read_ctrl2.sar2_dig_force = 0;									// adc1_adc_mode_acquire Switch SARADC into RTC channel

		if (SENS.sar_meas_wait2.force_xpd_sar & SENS_FORCE_XPD_SAR_SW_M) {		// adc_power_on
			SENS.sar_meas_wait2.force_xpd_sar = SENS_FORCE_XPD_SAR_PU;
		} else {
			SENS.sar_meas_wait2.force_xpd_sar = SENS_FORCE_XPD_SAR_FSM;
		}

		samples = samples < 1 ? 1 : samples;									// Divide by 0 protection
		accumulator = 0;
		for(nextSample = 0; nextSample < samples; nextSample++){				// Adc Controler is Rtc module,not ulp coprocessor
			SENS.sar_meas_start2.meas2_start_force = 1; 						// Force pad mux and force start
			SENS.sar_meas_start2.sar2_en_pad_force = 1; 						// Open the ADC2 data port

			SENS.sar_meas_start2.sar2_en_pad = 1 << channel; 					// Selects the channel to read, pad enable
			SENS.sar_meas_start2.meas2_start_sar = 0; 							// Start force 0
			SENS.sar_meas_start2.meas2_start_sar = 1; 							// Start force 1
			while (SENS.sar_meas_start2.meas2_done_sar == 0){
				REG_WRITE(TIMG_WDTFEED_REG(0), 1);
				ets_delay_us(1);
			}
			accumulator += SENS.sar_meas_start2.meas2_data_sar;
		}
		return accumulator/samples;
	}


	//**********************************************************************************************************************************
	// Header: AnalogSTUB::hallSensorRead
	// Function: Read the hall sensor without LNA
	//**********************************************************************************************************************************
	int RTC_IRAM_ATTR AnalogSTUB::hallSensorRead(){
		int nextSample, accumulator, Sens_Vp0, Sens_Vn0, Sens_Vp1, Sens_Vn1;

		initAnalogPin(ADC_UNIT_1, ADC_CHANNEL_0, ADC_ATTEN_0db);				// Initializes the reading pins (according hall sensor channels)
		initAnalogPin(ADC_UNIT_1, ADC_CHANNEL_3, ADC_ATTEN_0db);

		SET_PERI_REG_MASK(SENS_SAR_TOUCH_CTRL1_REG, SENS_XPD_HALL_FORCE_M);     // hall sens force enable
		SET_PERI_REG_MASK(RTC_IO_HALL_SENS_REG, RTC_IO_XPD_HALL);               // xpd hall
		SET_PERI_REG_MASK(SENS_SAR_TOUCH_CTRL1_REG, SENS_HALL_PHASE_FORCE_M);   // phase force

		accumulator = 0;														// Read REG values
		for(nextSample = 0; nextSample < 256; nextSample++){
			CLEAR_PERI_REG_MASK(RTC_IO_HALL_SENS_REG, RTC_IO_HALL_PHASE);       // hall phase
			Sens_Vp0 = adc1ReadValue(ADC1_CHANNEL_0, 1);
			Sens_Vn0 = adc1ReadValue(ADC1_CHANNEL_3, 1);
			SET_PERI_REG_MASK(RTC_IO_HALL_SENS_REG, RTC_IO_HALL_PHASE);
			Sens_Vp1 = adc1ReadValue(ADC1_CHANNEL_0, 1);
			Sens_Vn1 = adc1ReadValue(ADC1_CHANNEL_3, 1);

			accumulator = accumulator + (Sens_Vp1 - Sens_Vp0) - (Sens_Vn1 - Sens_Vn0);
			REG_WRITE(TIMG_WDTFEED_REG(0), 1);									// Feed the watch dog REG
		}

		SET_PERI_REG_BITS(SENS_SAR_MEAS_WAIT2_REG, SENS_FORCE_XPD_SAR, 0, SENS_FORCE_XPD_SAR_S);		// Resets ADC configs
		CLEAR_PERI_REG_MASK(SENS_SAR_TOUCH_CTRL1_REG, SENS_XPD_HALL_FORCE);
		CLEAR_PERI_REG_MASK(SENS_SAR_TOUCH_CTRL1_REG, SENS_HALL_PHASE_FORCE);

		accumulator = accumulator/2048;
		return accumulator;
	}


}
