/**
 * @file NeuronPower.cpp
 * @author Gabriel Sanchez
 * @author Andres Arias 
 * @author Rafael Pires
 * @date 21/10/2019
 * @brief Driver for deep sleep mode on Neuron.
 */

#include "include/LOKA_NEURON_Power.h"


namespace drivers {

    NeuronPower::NeuronPower(){
        ESP_LOGI(NEURON_POWER_TAG,"Initializing Neuron board.");
        drivers::GPIO_STUB::pinMode(DCDC_ON, OUTPUT, 1);
        drivers::GPIO_STUB::digitalWrite(DCDC_ON, 1, 1);

        drivers::GPIO_STUB::pinMode(SPI_CLK, OUTPUT, 1);
        drivers::GPIO_STUB::digitalWrite(SPI_CLK, 0, 1);
        drivers::GPIO_STUB::pinMode(SPI_MOSI, OUTPUT, 1);
        drivers::GPIO_STUB::digitalWrite(SPI_MOSI, 0, 1);
        drivers::GPIO_STUB::pinMode(SPI_MISO, INPUT, 1);

        drivers::GPIO_STUB::pinMode(S2LP_SDN, OUTPUT, 1);
        drivers::GPIO_STUB::digitalWrite(S2LP_SDN, 1, 1);
        drivers::GPIO_STUB::pinMode(S2LP_CS, OUTPUT, 1);
        drivers::GPIO_STUB::digitalWrite(S2LP_CS, 1, 1);
        drivers::GPIO_STUB::pinMode(S2LP_GPIO3, INPUT, 1);
        drivers::GPIO_STUB::pinMode(FE_SDN, OUTPUT, 1);
        drivers::GPIO_STUB::digitalWrite(FE_SDN, 0, 1);

        drivers::GPIO_STUB::pinMode(ACC_CS, OUTPUT, 1);
        drivers::GPIO_STUB::digitalWrite(ACC_CS, 1, 1);
        drivers::GPIO_STUB::pinMode(ACC_INT, INPUT, 1);

        // drivers::GPIO_STUB::pinMode(GPIO5,  INPUT, 1);											// Unused pins
        // drivers::GPIO_STUB::pinMode(GPIO18, INPUT, 1);
        // drivers::GPIO_STUB::pinMode(GPIO_NUM_19, INPUT, 1);
        // drivers::GPIO_STUB::pinMode(GPIO21, INPUT, 1);
        // drivers::GPIO_STUB::pinMode(GPIO22, INPUT, 1);
        // drivers::GPIO_STUB::pinMode(GPIO23, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_25, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_26, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_32, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_33, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_36, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_37, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_38, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_39, INPUT, 1);
        
    }
    
    NeuronPower::~NeuronPower(){
        ESP_LOGI(NEURON_POWER_TAG,"Deinitializing Neuron board");
        prepareSleep();
    }
    
    void NeuronPower::sleep(unsigned long long seconds, void (*wakeStub)()){

        ESP_LOGI(NEURON_POWER_TAG,"Entering in deepsleep mode");

        if(wakeStub == NULL){
            esp_set_deep_sleep_wake_stub(&esp_default_wake_deep_sleep);					// Sets the sleep stub pointer
        }else{
            esp_set_deep_sleep_wake_stub((void (*)()) wakeStub);						// Sets the sleep stub pointer
        }

        adc_power_off();																// Sorts a bug on the latest IDF
                                                                                        // The WiFi driver doesn't shutdown the ADC

        if(seconds == 0){
            esp_sleep_disable_wakeup_source(ESP_SLEEP_WAKEUP_TIMER);
            esp_deep_sleep_start();
        }
        esp_sleep_enable_timer_wakeup(seconds*1000000);
        esp_deep_sleep_start();
    }

    void RTC_IRAM_ATTR wakeStubDefault(void){
	    esp_default_wake_deep_sleep();
	    return;
    }


    void RTC_IRAM_ATTR NeuronPower::sleepStub(unsigned long long seconds, void (*wakeStub)()){

        ESP_LOGI(NEURON_POWER_TAG,"Entering in deepsleep mode from stub"); 
        
        unsigned long long timerTarget;
        unsigned int wakeMask;

        if(seconds == 0){
            wakeMask = REG_GET_FIELD(RTC_CNTL_WAKEUP_STATE_REG, RTC_CNTL_WAKEUP_ENA);
            wakeMask &= ~(1 << 3);
            REG_SET_FIELD(RTC_CNTL_WAKEUP_STATE_REG, RTC_CNTL_WAKEUP_ENA, wakeMask);
        }else{
            wakeMask = REG_GET_FIELD(RTC_CNTL_WAKEUP_STATE_REG, RTC_CNTL_WAKEUP_ENA);
            wakeMask |= 1 << 3;
            REG_SET_FIELD(RTC_CNTL_WAKEUP_STATE_REG, RTC_CNTL_WAKEUP_ENA, wakeMask);
            timerTarget = RTC_getTimerTarget(seconds);
            WRITE_PERI_REG(RTC_CNTL_SLP_TIMER0_REG, timerTarget & UINT32_MAX);
            WRITE_PERI_REG(RTC_CNTL_SLP_TIMER1_REG, timerTarget >> 32);
        }

        if(wakeStub == NULL){
            REG_WRITE(RTC_ENTRY_ADDR_REG, (uint32_t) &esp_default_wake_deep_sleep);
        }else{
            REG_WRITE(RTC_ENTRY_ADDR_REG, (uint32_t) wakeStub);
        }

        while(REG_GET_FIELD(UART_STATUS_REG(0), UART_ST_UTX_OUT)){
            REG_WRITE(TIMG_WDTFEED_REG(0), 1);
        }
        CLEAR_PERI_REG_MASK(RTC_CNTL_STATE0_REG, RTC_CNTL_SLEEP_EN);
        SET_PERI_REG_MASK(RTC_CNTL_STATE0_REG, RTC_CNTL_SLEEP_EN);
        while(true){
            REG_WRITE(TIMG_WDTFEED_REG(0), 1);
        }
    }

    void RTC_IRAM_ATTR NeuronPower::wakeStub(void){
        drivers::GPIO_STUB::digitalWrite(DCDC_ON, 1, 1);
        esp_default_wake_deep_sleep();
    }

    void NeuronPower::prepareSleep(){

        drivers::GPIO_STUB::pinMode(SPI_CLK, OUTPUT, 1);
        drivers::GPIO_STUB::digitalWrite(SPI_CLK, 0, 1);											// PD
        drivers::GPIO_STUB::pinMode(SPI_MOSI, OUTPUT, 1);
        drivers::GPIO_STUB::digitalWrite(SPI_MOSI, 0, 1);										// PD
        drivers::GPIO_STUB::pinMode(SPI_MISO, INPUT, 1);

        
        drivers::GPIO_STUB::pinMode(S2LP_SDN, OUTPUT, 1);
        drivers::GPIO_STUB::digitalWrite(S2LP_SDN, 1, 1);										// PU
        drivers::GPIO_STUB::pinMode(S2LP_CS, OUTPUT, 1);
        drivers::GPIO_STUB::digitalWrite(S2LP_CS, 1, 1);											// PU
        drivers::GPIO_STUB::pinMode(S2LP_GPIO3, INPUT, 1);
        drivers::GPIO_STUB::pinMode(FE_SDN, OUTPUT, 1);
        drivers::GPIO_STUB::digitalWrite(FE_SDN, 0, 1);											// PD

        
        drivers::GPIO_STUB::pinMode(ACC_CS, OUTPUT, 1);
        drivers::GPIO_STUB::digitalWrite(ACC_CS, 1, 1);											// PU
        drivers::GPIO_STUB::pinMode(ACC_INT, INPUT, 1);

        //drivers::GPIO_STUB::pinMode(GPIO5,  INPUT, 1);											// Unused pins
        //drivers::GPIO_STUB::pinMode(GPIO18, INPUT, 1);
        //drivers::GPIO_STUB::pinMode(GPIO19, INPUT, 1);
        //drivers::GPIO_STUB::pinMode(GPIO21, INPUT, 1);
        //drivers::GPIO_STUB::pinMode(GPIO22, INPUT, 1);
        //drivers::GPIO_STUB::pinMode(GPIO23, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_25, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_26, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_32, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_33, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_36, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_37, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_38, INPUT, 1);
        drivers::GPIO_STUB::pinMode(GPIO_NUM_39, INPUT, 1);
        

        drivers::GPIO_STUB::pinMode(DCDC_ON, OUTPUT, 1);
        drivers::GPIO_STUB::digitalWrite(DCDC_ON, 0, 1);
    }
}

