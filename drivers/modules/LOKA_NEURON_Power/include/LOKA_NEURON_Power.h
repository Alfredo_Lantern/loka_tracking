/**
 * @file neuron_power.h
 * @author Gabriel Sanchez 
 * @author Andres Arias
 * @author Rafael Pires
 * @date 21/10/2019
 * @brief Driver for Serial Peripheral Interface Flash File System.
 */


#ifndef BOARD_NEURON_H_
#define BOARD_NEURON_H_


#include "BoardIO.h"

#include "LOKA_GPIO.h"
#include "LOKA_RTC.h"
#include "LOKA_STUB_Analog.h"
#include "driver/adc.h"
#include "esp_log.h"


#include "esp32/ulp.h"
#include "soc/sens_reg.h"
#include "string.h"
#include "esp_task_wdt.h"

#include "soc/rtc.h"
#include "soc/rtc_cntl_reg.h"
#include "soc/timer_group_reg.h"

#include "soc/apb_ctrl_reg.h"

extern "C" {
	#include "rom/uart.h"
	#include "driver/gpio.h"
	#include "driver/rtc_io.h"
	#include "nvs_flash.h"
	#include "nvs.h"
	#include "stdlib.h"
	#include "esp_system.h"
	#include "rom/rtc.h"
	#include "esp_sleep.h"
}


#include "BOARD_LokaV2_ULP.h"
/**
 * @brief Tag used by the logging library.
 */
#define NEURON_POWER_TAG        "NeuronPower"

namespace drivers {
    /**
     * @class NeuronPower
     * @brief Low-level driver for sleep mode in Neuron platfom.
     */
    class NeuronPower {

        public:
        	/**
             * @brief Class constructor.
             */
            NeuronPower();

            /**
             * @brief Class destructor.
             */
            ~NeuronPower();

            /**
             * @brief Enter in deep sleep mode ( USE ONLY FOR MORE THEN 3 SECONDS)
             * @param[in] seconds amount of seconds that device will be in deep sleep mode
             * @param[in] wakestub pointer
			 * @retval ESP_OK Enter in deepsleep successfully.
			 * @retval ESP_FAIL Enter in deepsleep failure.
             */
            void sleep(unsigned long long seconds, void (*wakeStub)() = NULL);
            
            /**
             * @brief Enter in deep sleep mode for stub ( USE ONLY FOR MORE THEN 3 SECONDS)
             * @param[in] seconds amount of seconds that device will be in deep sleep mode
             * @param[in] wakestub pointer
			 * @retval ESP_OK Enter in deepsleep successfully.
			 * @retval ESP_FAIL Enter in deepsleep failure.
             */
            void sleepStub(unsigned long long seconds, void (*wakeStub)() = NULL);

            /**
             * @brief Place wake up stub function in RTC memory
             */
            void wakeStub();

            /**
             * @brief Disable necessary gpios for low consumption
             */
            void prepareSleep();

            

            

    };
}





#endif
