/**
 * @file LOKA_NVS.cpp
 * @author Gabriel Sanchez
 * @author Andres Arias
 * @author Alfredo Piedra
 * @author Joao Pombas
 * @date 21/10/2019
 * @brief Driver for Serial Peripheral Interface Flash File System.
 */

#include "LOKA_SPIFFS.h"

namespace drivers
{
	SPIFFS::SPIFFS()
	{
		ESP_LOGI(SPIFFS_DRIVER_TAG, "Initializing SPIFSS Driver.");
		mounted = false;
	}

	SPIFFS::~SPIFFS()
	{
		ESP_LOGI(SPIFFS_DRIVER_TAG, "Task done");

	}
	
	esp_err_t SPIFFS::mount(const std::string &partitionName) 
	{
		const char * _partitionName = partitionName.c_str();

		if(!mounted) {
			esp_vfs_spiffs_conf_t conf;

			conf.base_path = BASE_PATH;
			conf.partition_label = _partitionName;
			conf.max_files = MAX_FILES;
			conf.format_if_mount_failed = false;
			mounted = false;
																			// Note: esp_vfs_spiffs_register is an all-in-one convenience function.
			esp_err_t ret = esp_vfs_spiffs_register(&conf);					// Use settings defined above to initialize and mount SPIFFS filesystem.
			ESP_LOGI(SPIFFS_DRIVER_TAG,"Start Mounting %s", _partitionName);
			if (ret != ESP_OK) {
				if (ret == ESP_FAIL) {
					ESP_LOGE(SPIFFS_DRIVER_TAG,"Failed to mount or format filesystem");
				} else if (ret == ESP_ERR_NOT_FOUND) {
					ESP_LOGE(SPIFFS_DRIVER_TAG,"Failed to find SPIFFS partition");
				} else {
					ESP_LOGE(SPIFFS_DRIVER_TAG,"Failed to initialize SPIFFS (%d)", ret);
				}
				return ESP_FAIL;
			}
			ESP_LOGI(SPIFFS_DRIVER_TAG,"%s partition mounted Succesfully", _partitionName);
			mounted = true;
			return ret;
		}
		else {
			return ESP_FAIL;
		}
	}

	esp_err_t SPIFFS::unmount(const std::string &partitionName) 
	{
		const char * _partitionName = partitionName.c_str();
		ESP_LOGI(SPIFFS_DRIVER_TAG,"Unmounting %s partition", _partitionName);
		esp_err_t ret = esp_vfs_spiffs_unregister(_partitionName);			// Unmount partition and disable SPIFFS
		mounted = false;
		if ( ret == ESP_OK ){
			ESP_LOGI(SPIFFS_DRIVER_TAG,"Partition %s unmounted succesfully", _partitionName);
		}
		else {
			ESP_LOGE(SPIFFS_DRIVER_TAG,"Error while unmounting %s partition", _partitionName);
		}
		return ret;
	}

	esp_err_t SPIFFS::printSizeInfo(const std::string &partitionName) 
	{
		const char * _partitionName = partitionName.c_str();

		mount(_partitionName);													// try to mount partition before operation
		if(!mounted) {
			return ESP_FAIL;																// Failed to mount partition
		}

		size_t total = 0, used = 0;
		esp_err_t ret = esp_spiffs_info(_partitionName, &total, &used);

		if (ret != ESP_OK) {
			ESP_LOGE(SPIFFS_DRIVER_TAG,"Failed to get SPIFFS partition information");
		} else {
			ESP_LOGI(SPIFFS_DRIVER_TAG,"Partition size: total: %d, used: %d", total, used);
		}
		return ret;
	}

	std::fstream SPIFFS::getFile(const std::string &partitionName, 
								  const std::string &fileName, 
								  std::ios_base::openmode openMode) 
	{

		const char * _partitionName = partitionName.c_str();
		const char * _fileName = fileName.c_str();

		ESP_LOGI(SPIFFS_DRIVER_TAG,"Getting %s file from %s partition",_fileName, _partitionName);
		
		mount(_partitionName);													// try to mount partition before operation
		
		if(!mounted) {
			return (std::fstream) NULL;														// Failed to mount partition
		}

		char fName[100];
		parseFileName(fileName, fName);
		
		ESP_LOGI(SPIFFS_DRIVER_TAG,"File location: %s",fName);

		std::fstream fpn;

		fpn.open(fName, openMode);

		if (!fpn){

			ESP_LOGE(SPIFFS_DRIVER_TAG,"File %s Not Exists",fName);
		
		} else {

			ESP_LOGI(SPIFFS_DRIVER_TAG," Get file successfuly");
		}

		return fpn;

	}

	esp_err_t SPIFFS::cleanFile(const std::string &partitionName, const std::string &fileName) 
	{
		const char * _partitionName = partitionName.c_str();
		const char * _fileName = fileName.c_str();

		mount(_partitionName);																// try to mount partition before operation
		if(!mounted) {
			ESP_LOGE(SPIFFS_DRIVER_TAG,"Failed to open file: %s for cleaning", _fileName);
			return ESP_FAIL;																// Failed to mount partition
		}

		char fName[100];
		parseFileName(fileName, fName);
		unlink(fName);							// remove file
		FILE* file = fopen(fName, "w");			// write binary mode

		if (file == NULL) {
			ESP_LOGE(SPIFFS_DRIVER_TAG,"Failed to open file: %s for writting", _fileName);
			return ESP_FAIL;
		}
		else {
			ESP_LOGI(SPIFFS_DRIVER_TAG,"Clean %s file content successfully", _fileName);
			fclose(file);
			return ESP_OK;
		}		
	}

	int SPIFFS::readFile(const std::string &partitionName, 
						 const std::string &fileName, 
						 int size, 
						 int offset, 
						 char* content) 
	{
		const char * _partitionName = partitionName.c_str();
		const char * _fileName = fileName.c_str();

		ESP_LOGI(SPIFFS_DRIVER_TAG,"Reading file: %s", _fileName);
		int filelen;
		char fName[100];

		mount(_partitionName);									// try to mount partition before operation
		if(!mounted) {
			return (long) NULL;									// Failed to mount partition
		}

		parseFileName(fileName, fName);
		FILE* file = fopen(fName, "rb");  						// Open the file in binary mode

		if (file == NULL) {
			ESP_LOGE(SPIFFS_DRIVER_TAG,"Failed to open file: %s for reading", _fileName);
			return (long)NULL;
		}

		fseek(file, 0, SEEK_END);          						// Jump to the end of the file
		filelen = ftell(file);             						// Get the current byte offset in the file
		rewind(file);
		fseek(file, offset, SEEK_SET);							// Jump back to the beginning of the file and SEEK the Offset

		fread(content, size, 1, file);	 						// Read in the file at offset
		fclose(file);
		char fullContent[filelen];
		strcpy(fullContent, content);
		ESP_LOGI(SPIFFS_DRIVER_TAG,"Content file size: %d", filelen);
		for (int i = 0; i < filelen; i++) {
			ESP_LOGI(SPIFFS_DRIVER_TAG,"Content at %d: %hhx", i, (int)fullContent[i]);
		}
		

		return filelen;
	}

	esp_err_t SPIFFS::writeFile(const std::string &partitionName, 
								const std::string &fileName, 
								const std::string &content, 
								int size, 
								bool append) 
	{
		const char * _partitionName = partitionName.c_str();
		const char * _fileName = fileName.c_str();
		const char * _content = content.c_str();


		ESP_LOGI(SPIFFS_DRIVER_TAG,"Writting %2hhx word in %s file of %s partition", (int)_content, _partitionName, _fileName);
		char fullContent[size];
		strcpy(fullContent, _content);
		for (int i = 0; i < size; i++) {
			ESP_LOGI(SPIFFS_DRIVER_TAG,"Content at %d: %x", i, fullContent[i]);
		}

		mount(_partitionName);													// try to mount partition before operation
		if (!mounted) {
			ESP_LOGE(SPIFFS_DRIVER_TAG,"Failed to open file: %s for reading", _fileName);
			return ESP_FAIL;																// Failed to mount partition
		}

		char fName[100];
		parseFileName(fileName, fName);
		FILE* file = append ? fopen(fName, "a+b") : fopen(fName, "wb");			// write binary mode

		if (file == NULL) {
			ESP_LOGE(SPIFFS_DRIVER_TAG,"Failed to open file: %s for writing", _fileName);
			return ESP_FAIL;
		}
		else {
			ESP_LOGI(SPIFFS_DRIVER_TAG,"Wrote %s file in %s ", _fileName, _partitionName);
			fwrite(_content, 1, size, file);
			fclose(file);
			return ESP_OK;

		}

	}

	esp_err_t SPIFFS::writeHistory(const std::string &partitionName, 
								   const std::string &fileName, 
								   unsigned long timestamp, 
								   int numberOfElements, 
								   void* firstElement, 
								   int elementSize, 
								   bool append)
	{
		const char * _partitionName = partitionName.c_str();
		const char * _fileName = fileName.c_str();
		
		int headerOffset = 4 + 1; 												// Timestamp + Aps number (4 bytes + 1 byte)
		int dataSize = headerOffset + (numberOfElements * elementSize);
		unsigned char size = (unsigned char)numberOfElements;
		char* dataToWrite;

		dataToWrite = (char*)malloc(dataSize);

		memcpy(dataToWrite, &timestamp, 4);
		memcpy(dataToWrite+4, &size, 1);

		for(int i = 0; i < numberOfElements; i++) {
			memcpy(dataToWrite+headerOffset+(i*elementSize), firstElement+(i*elementSize), elementSize);
		}

		esp_err_t ret = SPIFFS::writeFile(partitionName, fileName, dataToWrite, dataSize, append);
		free(dataToWrite);
		if ( ret == ESP_OK) {
			ESP_LOGI(SPIFFS_DRIVER_TAG,"Writting history in %s file of %s partition", _fileName, _partitionName);
		}
		else {
			ESP_LOGE(SPIFFS_DRIVER_TAG,"Error while writting history in %s file of %s partition", _fileName, _partitionName);
		}
		return ret;
	}

	esp_err_t SPIFFS::removeFile(const std::string &partitionName, 
								 const std::string &fileName) 
	{

		const char * _partitionName = partitionName.c_str();
		const char * _fileName		= fileName.c_str();

		mount(partitionName);													// try to mount partition before operation
		if(!mounted) {
			return ESP_FAIL;																// Failed to mount partition
		}

		char fName[100];
		parseFileName(fileName, fName);
		unlink(fName);
		int result = remove(fName);															// Removes the file (if exists)
		if ( result == 0 ) {
			ESP_LOGI(SPIFFS_DRIVER_TAG,"Removed %s file in %s partition successfully", _fileName, _partitionName);
			return ESP_OK;
		}
		else {
			ESP_LOGE(SPIFFS_DRIVER_TAG,"Error while removing %s file in %s partition", _fileName, _partitionName);
			return ESP_FAIL;
		}
	}

	void SPIFFS::parseFileName(const std::string &fileName, 
							   char * parsedName) 
	{
		
		const char * _fileName = fileName.c_str();
		strcpy(parsedName, BASE_PATH);
		strcat(parsedName, "/");
		strcat(parsedName, _fileName);
	
	}
}