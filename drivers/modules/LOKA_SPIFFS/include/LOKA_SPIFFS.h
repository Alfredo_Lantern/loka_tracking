/**
 * @file LOKA_NVS.h
 * @author Gabriel Sanchez 
 * @author Andres Arias
 * @author Alfredo Piedra
 * @author Joao Pombas
 * @date 21/10/2019
 * @brief Driver for Serial Peripheral Interface Flash File System.
 */

#ifndef LOKA_SPIFFS_H_
#define LOKA_SPIFFS_H_

#include <cstring>
#include <string>
#include <sys/unistd.h>
#include "esp_spiffs.h"
#include "esp_log.h"
#include <stdio.h>
#include <fstream>

/**
 * @brief Tag used by the logging library.
 */
#define SPIFFS_DRIVER_TAG			"spiffs"

/**
 * @brief Base partition path for SPIFFS.
 */
#define BASE_PATH					"/spiffs"

/**
 * @brief Max amout of files inside SPIFFS.
 */
#define MAX_FILES					5

/**
 * @brief Storage partition Label.
 */
#define STORAGE_PARTITION_LABEL		"storage"

namespace drivers
{
	/**
     * @class SPIFFS
     * @brief Low-level driver for Serial Peripheral Interface Flash File System.
     */
	class SPIFFS {
		public:

			/**
             * @brief Class constructor.
             */
			SPIFFS();

			/**
             * @brief Mount a specific partition in SPIFFS.
             * @param[in] partitionName Name of the partition that user want to mount. ( Need to be defined on partitions.csv )
			 * @retval ESP_OK Mounting partition successfully.
			 * @retval ESP_FAIL Mounting partition failure.
             */
			esp_err_t mount(const std::string &partitionName);

			/**
             * @brief Unmount a specific partition in SPIFFS.
             * @param[in] partitionName Name of the partition that user want to unmount. ( Need to be defined on partitions.csv )
			 * @retval ESP_OK Unmounting partition successfully.
			 * @retval ESP_FAIL Unmounting partition failure.
             */
			esp_err_t unmount(const std::string &partitionName);

			/**
             * @brief Print total and used size of specific partition.
             * @param[in] partitionName Name of the partition that user want get size info. ( Need to be defined on partitions.csv )
             * @retval ESP_OK Show partition information successfully.
			 * @retval ESP_FAIL Mounting partition failure.
			 */
			esp_err_t printSizeInfo(const std::string &partitionName);

			/**
             * @brief Get a file pointer from specfic partition.
             * @param[in] partitionName Name of the partition
			 * @param[in] fileName Name of the file to get 
			 * @param[in] openMode File open mode ( r , w )
			 * @return Pointer to file content
             */
			std::fstream getFile(const std::string &partitionName, const std::string &fileName, std::ios_base::openmode openMode);

			/**
             * @brief Delete the content of a file in specific.
             * @param[in] partitionName Name of the partition
			 * @param[in] fileName Name of the file to clean
			 * @retval ESP_OK Cleaning file successfully.
			 * @retval ESP_FAIL Mounting partition failure.
             */
			esp_err_t cleanFile(const std::string &partitionName, const std::string &fileName);

			/**
             * @brief Read the content of a file with specific size and file offset.
             * @param[in] partitionName Name of the partition
			 * @param[in] fileName Name of the file to clean
			 * @param[in] size Size of the content in bytes
			 * @param[in] offset Offset for reading
			 * @return lenght in bytes of read file
             */
			int readFile(const std::string &partitionName, const std::string &fileName, int size, int offset,  char* content );

			/**
             * @brief Write or append some content in a file.
             * @param[in] partitionName Name of the partition
			 * @param[in] fileName Name of the file to write or append to
			 * @param[in] content Content to write
			 * @param[in] size Content size in bytes
			 * @param[in] append if true append constent else replace the content
			 * @retval ESP_OK Write file in partition successfully.
			 * @retval ESP_FAIL Write file failure.
             */
			esp_err_t writeFile(const std::string &partitionName, const std::string &fileName, const std::string &content, int size, bool append);
			
			/**
             * @brief Keep track of history events using a file.
             * @param[in] partitionName Name of the partition
			 * @param[in] fileName Name of the file to delete
			 * @param[in] timestamp Date when data was saved
			 * @param[in] numberOfElements Amount of elements to write
			 * @param[in] firstElement Pointer to first element to save
			 * @param[in] append if true append constent else replace the content
			 * @retval ESP_OK Write history successfully.
			 * @retval ESP_FAIL Write history failure.
			 * 
             */
			esp_err_t writeHistory(const std::string &partitionName, const std::string &fileName, unsigned long timestamp, int numberOfElements, void* firstElement, int elementSize, bool append);
			
			/**
             * @brief Delete file from partition.
             * @param[in] partitionName Name of the partition
			 * @param[in] fileName Name of the file to delete
			 * @retval ESP_OK Remove file partition successfully.
			 * @retval ESP_FAIL Can not remove partition failure.
             */
			esp_err_t removeFile(const std::string &partitionName, const std::string &fileName);

			/**
             * @brief Class destructor.
             */
			~SPIFFS();



		private:

			bool mounted = false; // Allows to know when de object already mount a partition
			/**
             * @brief Use base path for get specific name of the file.
             * @param[in] fileName Name of the partition
			 * @param[out] parsedName complete file name including path
             */
			void parseFileName(const std::string &fileName, char* parsedName);

	};
}



#endif
