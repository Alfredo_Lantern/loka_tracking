//**********************************************************************************************************************************
// Filename: ParseNumerics.cpp
// Date: 18.10.2017
// Author: Rafael Pires
// Company: LOKA SYSTEMS
// Version: 1.0
// Description: Parses different numeric formats into/from a byte stream
//**********************************************************************************************************************************


//**********************************************************************************************************************************
//                                                      Includes Section
//**********************************************************************************************************************************
#include "ParseNumerics.h"


//**********************************************************************************************************************************
//                                                        Code Section
//**********************************************************************************************************************************

namespace utils {
	namespace parseNumerics {
		//**********************************************************************************************************************************
		// Header: readInt
		// Function: Reads an integer from a byte stream
		//**********************************************************************************************************************************
		int readInt(unsigned char* packet, int position){
			printf("TEST1323");
			int result;
			unsigned char aux[4];
			int i;

			for(i=0; i < 4; i++){
				aux[3-i] = packet[position+i];
			}
			memcpy(&result, &aux, 4);

			return result;
		}


		//**********************************************************************************************************************************
		// Header: insertInt
		// Function: Inserts an integer into a byte stream
		//**********************************************************************************************************************************
		void insertInt(unsigned char* packet, int value, int position){
			int i;
			unsigned char aux[4];

			memcpy(&aux, &value, 4);
			for (i=0; i < 4; i++){
				packet[i+position] = aux[3-i];
			}
		}


		//**********************************************************************************************************************************
		// Header: readDouble
		// Function: Reads a double from a byte stream
		//**********************************************************************************************************************************
		double readDouble(char* packet, int position){
			double result;
			unsigned char aux[8];
			int i;

			for(i=0; i < 8; i++){
				aux[7-i] = packet[position+i];
			}
			memcpy(&result, &aux, 8);

			return result;
		}


		//**********************************************************************************************************************************
		// Header: insertDouble
		// Function: Inserts a double into a byte stream
		//**********************************************************************************************************************************
		void insertDouble(unsigned char* packet, double value, int position){
			int i;
			unsigned char aux[8];

			memcpy(&aux, &value, 8);
			for(i=0; i < 8; i++){
				packet[i+position] = aux[7-i];
			}
		}


		//**********************************************************************************************************************************
		// Header: readFloat
		// Function: Reads a float from a byte stream
		//**********************************************************************************************************************************
		float readFloat(unsigned char* packet, int position){
			double result;
			unsigned char aux[4];
			int i;

			for (i=0; i < 4; i++){
				aux[3-i] = packet[position+i];
			}
			memcpy(&result, &aux, 4);

			return result;
		}


		//**********************************************************************************************************************************
		// Header: insertFloat
		// Function: Inserts a float into a byte stream
		//**********************************************************************************************************************************
		void insertFloat(unsigned char* packet, float value, int position){
			int i;
			unsigned char aux[4];

			memcpy(&aux, &value, 4);
			for(i=0; i < 4; i++){
				packet[i+position] = aux[3-i];
			}
		}


		//**********************************************************************************************************************************
		// Header: readValueFromMessage
		// Function: Reads a value from a byte stream. msg: (message without header byte) startPosition: start bit position(most significant pos) length: in bits
		//**********************************************************************************************************************************
		unsigned int readValueFromMessage(unsigned char* msg, int startPosition, int length){
			uint64_t buf = 0, mask = 1;
			int i, byte, bit, bytesToRead;
			unsigned char* aux;

			byte = startPosition/8;
			bit = startPosition%8;
			bytesToRead = (length/8)+2;											// to catch info divided in 2 more bytes
			mask = ((mask << length) - 1);

			aux = (unsigned char*) &buf;
			for(i = 0; i < bytesToRead; i++)
				aux[bytesToRead - i - 1] = msg[byte + i];

			return (buf >> (8 * bytesToRead - bit - length) & mask);
		}


		//**********************************************************************************************************************************
		// Header: writeValueInMessage
		// Function: Writes a value into a byte stream. msg: (message without header byte) startPosition: start bit position(most significant pos) length: in bits
		//**********************************************************************************************************************************
		void writeValueIntoMessage(unsigned char* msg, unsigned int value, int startPosition, int length){
			uint64_t buf = 0;
			int i, byte, bit;
			unsigned char* aux;

			byte = startPosition/8;
			bit = startPosition%8;

			buf = value;
			buf = buf << (64 - length - bit);
			aux = (unsigned char*) &buf;

			for(i = 0; i < 8; i++)
				msg[byte + i] |= aux[8-1-i];
		}

		//**********************************************************************************************************************************
		// Header: writeFloatIntoMessage
		// Function: Writes a float into a byte stream. msg: (message without header byte) startPosition: start bit position(most significant pos) length: in bits [MIN 32 bits]
		//**********************************************************************************************************************************
		void writeFloatIntoMessage(unsigned char* msg, float value, int startPosition, int length){
			unsigned long long buf = 0;
			int i, byte, bit;
			unsigned char* aux;

			byte = startPosition/8;
			bit = startPosition%8;

			memcpy(&buf, &value, 4);
			aux = (unsigned char*) &buf;

			buf = buf << (64 - length - bit);
			aux = (unsigned char*) &buf;

			for(i = 0; i < 8; i++)
				msg[byte + i] |= aux[8-1-i];
		}

		//**********************************************************************************************************************************
		// Header: parseStringToHexArray
		// Function: Reads 8 values from a byte stream.
		//**********************************************************************************************************************************
		void parseStringToHexArray(unsigned char* message, unsigned char* result){
			sscanf((char*) message, "%2x%2x%2x%2x%2x%2x%2x%2x", (unsigned int*)&result[0], (unsigned int*)&result[1], (unsigned int*)&result[2],
				(unsigned int*)&result[3], (unsigned int*)&result[4], (unsigned int*)&result[5], (unsigned int*)&result[6], (unsigned int*)&result[7]);
		}

	}
}