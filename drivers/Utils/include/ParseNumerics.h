//**********************************************************************************************************************************
// Filename: ProgramTracker.h
// Date: 18.10.2017
// Author: Rafael Pires
// Company: LOKA SYSTEMS
// Version: 1.0
// Description: Parses different numeric formats into/from a byte stream
//**********************************************************************************************************************************
#ifndef UTILS_PARSE_NUMERICS_H_
#define UTILS_PARSE_NUMERICS_H_


//**********************************************************************************************************************************
//                                                      Includes Section
//**********************************************************************************************************************************
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>

//**********************************************************************************************************************************
//                                                     Templates Section
//**********************************************************************************************************************************
namespace utils {
    namespace parseNumerics {
        void insertInt(unsigned char* packet, int value, int position);
        int readInt(unsigned char* packet, int position);
        void insertFloat(unsigned char* packet, float value, int position);
        float readFloat(unsigned char* packet, int position);
        double readDouble(unsigned char* packet, int position);
        void insertDouble(unsigned char* packet, double value, int position);
        unsigned int readValueFromMessage(unsigned char* msg, int startPosition, int length);
        void writeValueIntoMessage(unsigned char* msg, unsigned int value, int startPosition, int length);
        void writeFloatIntoMessage(unsigned char* msg, float value, int startPosition, int length);
        void parseStringToHexArray(unsigned char* message, unsigned char* result);
    }
}



#endif
