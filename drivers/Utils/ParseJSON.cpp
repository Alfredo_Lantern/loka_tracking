//**********************************************************************************************************************************
// Filename: ParseJSON.cpp
// Date: 14.03.2018
// Author: Joao Pombas
// Company: LOKA SYSTEMS
// Version: 1.0
// Description: Parses different JSON objects and data types
//**********************************************************************************************************************************


//**********************************************************************************************************************************
//                                                      Includes Section
//**********************************************************************************************************************************
#include "ParseJSON.h"


//**********************************************************************************************************************************
//                                                        Code Section
//**********************************************************************************************************************************


//**********************************************************************************************************************************
// Header: parseEntityValues
// Function: BUilds a "stringify" JSON object from a byte stream
//**********************************************************************************************************************************
void parseEntityValues(char* input, cJSON* root, int fieldsNumber, char* fieldNames[], int fieldSizes[], char fieldTypes[]){

	unsigned int i, dataIterator = 0;
	char mac[18];
	char* auxPtr;

	for(i = 0; i < fieldsNumber; i++) {

		switch(fieldTypes[i]) {

		case INT:

			if(fieldSizes[i] == 1)
				cJSON_AddItemToObject(root, fieldNames[i], cJSON_CreateNumber(*((signed char*)(input+dataIterator)))); 	// cast to signed char pointer
			else
				cJSON_AddItemToObject(root, fieldNames[i], cJSON_CreateNumber(*((int*)(input+dataIterator)))); 			// cast to int pointer

			dataIterator += fieldSizes[i];
			break;

		case FLOAT:
			cJSON_AddItemToObject(root, fieldNames[i], cJSON_CreateNumber(*((float*)(input+dataIterator)))); 		// cast to float pointer
			dataIterator += fieldSizes[i];
			break;

		case STRING:
			cJSON_AddItemToObject(root, fieldNames[i], cJSON_CreateString(input+dataIterator));
			dataIterator += fieldSizes[i];
			break;

		case BSSID:
			auxPtr = input+dataIterator;
			sprintf(mac, "%02X:%02X:%02X:%02X:%02X:%02X", *auxPtr, *(auxPtr+1), *(auxPtr+2), *(auxPtr+3), *(auxPtr+4), *(auxPtr+5));
			cJSON_AddItemToObject(root, fieldNames[i], cJSON_CreateString(mac));
			dataIterator += fieldSizes[i];
			break;

		default:
			;
		}
	}
//
//	*result = cJSON_PrintUnformatted(root);
//	cJSON_Delete(root);
}



