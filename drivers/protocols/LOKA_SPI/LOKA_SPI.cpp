/**
 * @file LOKA_SPI.cpp
 * @author Joao Pombas
 * @author Andres Arias
 * @date 4/11/2019
 * @brief Low level driver for the SPI bus with STUB support
 */

#include "LOKA_SPI.h"

namespace drivers {

    RTC_IRAM_ATTR SPI::SPI(gpio_num_t cs,
                           gpio_num_t clk,
                           gpio_num_t miso,
                           gpio_num_t mosi)
    {
        ESP_LOGI(SPI_TAG, "Initializing SPI bus.");
        this->cs_pin = cs;
        this->clk_pin = clk;
        this->miso_pin = miso;
        this->mosi_pin = mosi;
    }

    RTC_IRAM_ATTR SPI::~SPI()
    {
        ESP_LOGI(SPI_TAG, "Deinitializing SPI bus.");
        drivers::GPIO_STUB::digitalWrite(this->clk_pin, 0, 1); // Hold the GPIO value
        drivers::GPIO_STUB::digitalWrite(this->mosi_pin, 0, 1);
        drivers::GPIO_STUB::digitalWrite(this->cs_pin, 0, 1);
        drivers::GPIO_STUB::digitalWrite(this->cs_pin, 1, 1);
    }

    void RTC_IRAM_ATTR SPI::request(uint8_t address, bool readWrite)
    {
        uint8_t i, bitMask;

        address &= 0x3F;
        // Include the read/write bit on address:
        address |= (readWrite) ? 0x00 : 0xC0; 

        // Sets initial bus state:
        drivers::GPIO_STUB::digitalWrite(this->clk_pin, 1, 1);
        drivers::GPIO_STUB::digitalWrite(this->cs_pin, 1, 1);
        drivers::GPIO_STUB::digitalWrite(this->mosi_pin, 0, 1);
        drivers::GPIO_STUB::digitalWrite(this->cs_pin, 0, 1);

        for (i = 0; i < 8; i++) { // Shift address out
            drivers::GPIO_STUB::digitalWrite(this->clk_pin, 0, 1);
            bitMask = (address & 0x80) == 0x80;	// MSbit first
            drivers::GPIO_STUB::digitalWrite(this->mosi_pin, bitMask, 1);
            drivers::GPIO_STUB::digitalWrite(this->clk_pin, 1, 1);
            address = address << 1; // Shift data
        }
    }


    uint8_t RTC_IRAM_ATTR SPI::read8(uint8_t addr)
    {
        ESP_LOGI(SPI_TAG, "Reading 8 bits from SPI bus");
        uint8_t result = 0;
        uint8_t i;

        SPI::request(addr, SPI_READ);
        // Notice that the cs_pin was left asserted:
        drivers::GPIO_STUB::digitalWrite(this->mosi_pin, 0, 1);	

        for (i = 0; i < 8; i++) { // Shift data in:
            drivers::GPIO_STUB::digitalWrite(this->clk_pin, 0, 1);
            // MSbit first:
            result |= (drivers::GPIO_STUB::digitalRead(this->miso_pin)) << (7 - i);
            drivers::GPIO_STUB::digitalWrite(this->clk_pin, 1, 1);
        }
        // Finally de-assert the chip select:
        drivers::GPIO_STUB::digitalWrite(this->cs_pin, 1, 1);
        return result;
    }


    uint16_t RTC_IRAM_ATTR SPI::read16(uint8_t addr)
    {
        ESP_LOGI(SPI_TAG, "Reading 16 bits from SPI bus");
        uint16_t result = 0;
        uint8_t i;

        SPI::request(addr, SPI_READ);
        // Notice that the cs_pin was left asserted:
        drivers::GPIO_STUB::digitalWrite(this->mosi_pin, 0, 1);

        for (i = 0; i < 8; i++) { // Shift LSB data in
            drivers::GPIO_STUB::digitalWrite(this->clk_pin, 0, 1);
            // MSbit first:
            result |= (drivers::GPIO_STUB::digitalRead(this->miso_pin)) << (7 - i);
            drivers::GPIO_STUB::digitalWrite(this->clk_pin, 1, 1);
        }

        for (i = 0; i < 8; i++) { // Shift MSB data in
            drivers::GPIO_STUB::digitalWrite(this->clk_pin, 0, 1);
            // MSbit first:
            result |= (drivers::GPIO_STUB::digitalRead(this->miso_pin) << (15 - i));	
            drivers::GPIO_STUB::digitalWrite(this->clk_pin, 1, 1);
        }

        // Finally de-assert the chip select:
        drivers::GPIO_STUB::digitalWrite(this->cs_pin, 1, 1);
        return result;
    }

    void RTC_IRAM_ATTR SPI::write8(uint8_t addr, uint8_t data)
    {
        ESP_LOGI(SPI_TAG, "Writing 8 bits from SPI bus");
        uint8_t bitMask;

        SPI::request(addr, SPI_WRITE);

        // Notice that the cs_pin was left asserted:
        for (uint8_t i=0; i<8; i++) {
            drivers::GPIO_STUB::digitalWrite(this->clk_pin, 0, 1);
            bitMask = ((data & 0x80) == 0x80); // MSBit first
            // Shift mosi_pin accordingly:
            drivers::GPIO_STUB::digitalWrite(this->mosi_pin, bitMask, 1);
            drivers::GPIO_STUB::digitalWrite(this->clk_pin, 1, 1);
            data = data << 1; // Move to the next bit
        }
        // Finally de-assert the chip select:
        drivers::GPIO_STUB::digitalWrite(this->cs_pin, 1, 1);
    }

} /* namespace drivers */
