/**
 * @file LOKA_SPI.h
 * @author Joao Pombas
 * @author Andres Arias
 * @date 4/11/2019
 * @brief Low level driver for the SPI bus with STUB support
 */

#ifndef LOKA_SPI_H
#define LOKA_SPI_H

#include "LOKA_GPIO.h"


/**
 * @brief Indicates if reading from the bus.
 */
#define SPI_READ  				false

/**
 * @brief Indicates if writing on the bus.
 */
#define SPI_WRITE 				true

#define SPI_TAG     "SPI"

namespace drivers {

    /**
     * @class SPI
     * @brief SPI driver that can be used on wakeup stubs.
     */
    class SPI {

        public:

            /**
             * @brief Class constructor. Initializes the SPI bus on the given
             * pins.
             * @param[in] cs Slave select pin.
             * @param[in] clk Clock signal pin.
             * @param[in] miso Master In, Slave Out pin.
             * @param[in] mosi Master Out, Slave In pin.
             */
            SPI(gpio_num_t cs, gpio_num_t clk, gpio_num_t miso, gpio_num_t mosi);

            /**
             * @brief Class destructor. Deinitializes the SPI bus.
             */
            ~SPI(void);

            /**
             * @brief Reads one byte from the selected slave.
             * @param[in] addr Address of the requested slave.
             * @return The read byte.
             */
            uint8_t read8(uint8_t addr);

            /**
             * @brief Reads 16 bits from the selected slave.
             * @param[in] addr Address of the requested slave.
             * @return The read 16 bits.
             */
            uint16_t read16(uint8_t addr);

            /**
             * @brief Write a byte on the selected slave.
             * @param[in] addr Address of the requested slave.
             * @param[in] data Byte to write.
             */
            void write8(uint8_t addr, uint8_t data);

        private:
            gpio_num_t clk_pin; /** GPIO handling the clock signal. */
            gpio_num_t cs_pin; /** Slave select pin. */
            gpio_num_t miso_pin; /** Master In, Slave Out pin */
            gpio_num_t mosi_pin; /** Master Out, Slave In pin */

            /**
             *
             * @brief Addressing function on stub mode
             * @param[in] address The selected slave address.
             * @param[in] readWrite false to read, true to write.
             */
            void request(uint8_t address, bool readWrite);
    };

} /* namespace drivers */

#endif
