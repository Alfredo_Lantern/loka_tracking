/**
 * @file UART.cpp
 * @author Andres Arias
 * @date 13/3/2019
 * @brief Simple UART driver.
 */

#include "LOKA_UART.h"

namespace drivers
{
    static QueueHandle_t uart_event_queue;

    static void uartEventHandler(void * event_param)
    {
        uart_port_t uart_num = (uart_port_t)((int)event_param);
        uart_event_t event;
        while (true) {
            if (xQueueReceive(uart_event_queue, (void*)&event, portMAX_DELAY)) {
                if (event.type == UART_FIFO_OVF || event.type == UART_BUFFER_FULL) {
                    ESP_LOGI(UART_TAG, "UART emitted event code: %d", event.type);
                    uart_flush(uart_num);
                }
            }
        }
        vTaskDelete(NULL);
    }

    UART::UART(uart_port_t port_num,
            int tx_pin,
            int rx_pin,
            int rts_pin,
            int cts_pin,
            int baud_rate,
            int buffer_size,
            uart_word_length_t word_length,
            uart_parity_t parity,
            uart_stop_bits_t stop_bits,
            uart_hw_flowcontrol_t flow_control,
            uint8_t rx_flow_ctrl_thresh):
        port_num(port_num),
        uart_task_handler(NULL),
        buffer_size(buffer_size)
    {
        esp_err_t res;

        uart_config_t uart_config;
        uart_config.baud_rate = baud_rate;
        uart_config.data_bits = word_length;
        uart_config.parity = parity;
        uart_config.stop_bits = stop_bits;
        uart_config.flow_ctrl = flow_control;
		uart_config.rx_flow_ctrl_thresh = rx_flow_ctrl_thresh;

        res = uart_param_config(port_num, &uart_config);
        if (res != ESP_OK)
            this->status = res;
        res = uart_set_pin(port_num, tx_pin, rx_pin, rts_pin, cts_pin);
        if (res != ESP_OK)
            this->status = res;
        res = uart_driver_install(port_num,             // UART port number
                                  buffer_size,          // RX buffer size
                                  buffer_size,          // TX buffer size
                                  10,                   // Event Queue size.
                                  &uart_event_queue,    // Event Queue to use.
                                  0);                   // Intr allocation flags.

        uart_disable_pattern_det_intr(port_num);
        xTaskCreate(uartEventHandler,
                    "wisolUARTTask",
                    1024,
                    (void*) port_num,
                    1,
                    &this->uart_task_handler);
        ESP_LOGI(UART_TAG, "Module initialized on TX: %d RX: %d", tx_pin, rx_pin);
        this->status = res;
    }

    UART::~UART()
    {
        this->flush();
        uart_driver_delete(this->port_num);
        vTaskDelete(this->uart_task_handler);
    }

    esp_err_t UART::get_status()
    {
        return this->status;
    }

    int UART::write(const std::string &data, bool with_break, int break_len)
    {
        if (with_break)
            return uart_write_bytes_with_break(this->port_num,
                    data.c_str(),
                    data.size(),
                    break_len);
        else 
            return uart_write_bytes(this->port_num, data.c_str(), data.size());
    }

    std::string UART::read(unsigned int timeout)
    {
        if (timeout > 0) {
            unsigned int uptime = 0;
            unsigned int available_bytes;
            uart_get_buffered_data_len(this->port_num, &available_bytes);
            while (available_bytes <= 0 && uptime < timeout) {
                vTaskDelay(10 / portTICK_PERIOD_MS);
                uptime += 10;
                uart_get_buffered_data_len(this->port_num, &available_bytes);
            }
            if (available_bytes <= 0)
                return "";
        }
        uint8_t data[this->buffer_size];
        std::memset(data, '\0', this->buffer_size);
        uart_read_bytes(this->port_num, data, this->buffer_size, UART_TIMEOUT / portTICK_PERIOD_MS);
        std::string data_string(reinterpret_cast<const char*>(data));
        return data_string;
    }

    void UART::flush()
    {
        uart_flush(this->port_num);
    }
} /* drivers */ 
