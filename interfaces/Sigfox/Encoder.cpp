/**
 * @file Encoder.cpp
 * @author Andres Arias
 * @date 31/10/2019
 * @brief Encodes Sigfox messages according to the LOKA
 * uplink format.
 */

#include "Encoder.h"

namespace interfaces {
    
    static void writeValueIntoMessage(unsigned char* msg,
                                      unsigned int value,
                                      int startPosition,
                                      int length)
    {
        uint64_t buf = 0;
        int byte, bit;
        unsigned char* aux;

        byte = startPosition / 8;
        bit = startPosition % 8;

        buf = value;
        buf = buf << (64 - length - bit);
        aux = (unsigned char*) &buf;

        for(int i = 0; i < 8; i++)
            msg[byte + i] |= aux[7 - i];
    }

    static void writeFloatIntoMessage(unsigned char* msg,
                                      float value,
                                      int startPosition,
                                      int length)
    {
        unsigned long long buf = 0;
        int byte, bit;
        unsigned char* aux;

        byte = startPosition / 8;
        bit = startPosition % 8;

        std::memcpy(&buf, &value, 4);
        aux = (unsigned char*) &buf;

        buf = buf << (64 - length - bit);
        aux = (unsigned char*) &buf;

        for(int i = 0; i < 8; i++)
            msg[byte + i] |= aux[7 - i];
    }

    int SigfoxEncoder::encodeAnalog(unsigned char *buffer,
                                   unsigned int portNum,
                                   float value)
    {
        std::memset(buffer, '\0', SENSOR_ANALOG_MESSAGE_SIZE);
        // BIT: 0 	LEN: 3  (Header)
        writeValueIntoMessage(buffer, SENSOR_MESSAGE_HEADER, 0, 3);
        // BIT: 3 	LEN: 2  (Sub Header)
        writeValueIntoMessage(buffer, ANALOG_MESSAGE_HEADER, 3, 2);
        // BIT: 5 	LEN: 8  (Port)
        writeValueIntoMessage(buffer, portNum, 5, 8);
        // BIT: 13 	LEN: 32 (Value)
        writeFloatIntoMessage(buffer, value, 13, 32);
        return SENSOR_ANALOG_MESSAGE_SIZE;
    }

    int SigfoxEncoder::encodeAnalog(unsigned char* buffer,
                                   int portNum,
                                   float value,
                                   int portNum2,
                                   float value2)
    {
        std::memset(buffer, '\0', SENSOR_DOUBLE_ANALOG_MESSAGE_SIZE);
        // BIT: 0 	LEN: 3  (Header)
        writeValueIntoMessage(buffer, SENSOR_MESSAGE_HEADER, 0, 3);
        // BIT: 3 	LEN: 2  (Sub Header)
        writeValueIntoMessage(buffer, DOUBLE_ANALOG_MESSAGE_HEADER, 3, 2);
        // BIT: 5 	LEN: 8  (Port)
        writeValueIntoMessage(buffer, portNum, 5, 8);
        // BIT: 13 	LEN: 32 (Value)
        writeFloatIntoMessage(buffer, value, 13, 32);
        // BIT: 45 	LEN: 8  (Port 2)
        writeValueIntoMessage(buffer, portNum2, 45, 8);
        // BIT: 53 	LEN: 32 (Value 2)
        writeFloatIntoMessage(buffer, value2, 53, 32);
        return SENSOR_DOUBLE_ANALOG_MESSAGE_SIZE;
    }
} /* interfaces */ 
