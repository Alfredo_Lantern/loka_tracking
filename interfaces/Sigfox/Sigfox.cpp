/**
 * @file Sigfox.cpp
 * @author Andres Arias
 * @date 7/10/2019
 * @brief 
 */

#include "Sigfox.h"

#ifdef CONFIG_ENABLE_NEURON
    static drivers::S2LP *sigfox_controller = nullptr;
#else
    static drivers::WiSOL *sigfox_controller = nullptr;
#endif

namespace interfaces {

    esp_err_t Sigfox::turnOn(void)
    {
#ifdef CONFIG_ENABLE_NEURON
        sigfox_controller = new drivers::S2LP();
#else
        sigfox_controller = new drivers::WiSOL();
#endif
        return sigfox_controller->turnOn();
    }

    esp_err_t Sigfox::turnOn(const std::string &zone)
    {
#ifdef CONFIG_ENABLE_NEURON
        sigfox_controller = new drivers::S2LP(zone);
#else
        sigfox_controller = new drivers::WiSOL();
#endif
        return sigfox_controller->turnOn();
    }

    esp_err_t Sigfox::turnOff(void)
    {
        esp_err_t res = sigfox_controller->turnOff();
        delete sigfox_controller;
        return res;
    }

    std::string Sigfox::getID(void)
    {
        return sigfox_controller->getID();
    }

    esp_err_t Sigfox::sendRaw(const std::string &message)
    {
        if (message.length() > 12)
            return ESP_ERR_INVALID_SIZE;
        return sigfox_controller->sendData(message);
    }
    
    esp_err_t Sigfox::sendAnalog(unsigned int port, double value)
    {
        unsigned char msg[12] = {0};
        SigfoxEncoder::encodeAnalog(msg, port, value);
        std::string msg_str((char*)msg);
        return sigfox_controller->sendData(msg_str);
    }

} /* namespace interfaces */ 
