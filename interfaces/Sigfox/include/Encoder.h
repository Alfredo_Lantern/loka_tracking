/**
 * @file Encoder.h
 * @author Andres Arias
 * @date 31/10/2019
 * @brief Encodes Sigfox messages according to the LOKA
 * uplink format.
 */

#ifndef ENCODER_H
#define ENCODER_H 

#include <cstdint>
#include <cstring>

#define GLOBAL_CONFIG_MSG_HEADER_START		0x70
#define GLOBAL_CONFIG_MSG_HEADER_END	    0x7E
#define DEFAULT_PROGRAMS_CONFIG_MSG_HEADER	0x7F
#define CONTROL_MSG_HEADER					0x80
#define TIME_MSG_HEADER						0x0E	// Former 0x0E on old protocol

#define ADC_STEP 							0.0234375

#define REGISTER_MSG_HEADER					0x01	// First 3 BITS of the MSG
#define DOWNLINK_ACK_MSG_HEADER				0x0D

#define SENSOR_MESSAGE_HEADER				0x02	// First 3 BITS	Header
#define ANALOG_MESSAGE_HEADER			    0x00	// next 2bits 	Sub Header
#define DOUBLE_ANALOG_MESSAGE_HEADER	    0x01	// next 2bits 	Sub Header
#define GPIO_MESSAGE_HEADER				    0x02	// next 2bits 	Sub Header

#define GEO_GPS_MESSAGE_HEADER				0x04	// First 3 BITS	Header
#define GEO_BT_MESSAGE_HEADER				0x05	// First 3 BITS	Header

#define DATA_MESSAGE_HEADER					0x06	// First 3 BITS	Header
#define LOG_GPIO_MESSAGE_HEADER			    0x00	// next 2bits 	Sub Header
#define LOG_CHAR_MESSAGE_HEADER			    0x01	// next 2bits 	Sub Header
#define RAW_MESSAGE_HEADER				    0x02	// next 2bits 	Sub Header
#define RULE_STAT_MESSAGE_HEADER		    0x03	// next 2bits 	Sub Header

#define ACK_REGISTER_MSG_SIZE 				1

#define SENSOR_ANALOG_MESSAGE_SIZE			6
#define SENSOR_DOUBLE_ANALOG_MESSAGE_SIZE	11
#define SENSOR_GPIO_MESSAGE_SIZE			2
#define GEO_GPS_MESSAGE_SIZE				11

#define LOG_GPIO_MESSAGE_SIZE				10
#define LOG_CHAR_MESSAGE_SIZE				2
#define RULE_STAT_MESSAGE_SIZE				12

#define WIFI_MESSAGE_HEADER			        0x03	// First 3 BITS	Header
#define SINGLE_AP_MESSAGE_HEADER	        0x00	// next 2bits 	Sub Header
#define DOUBLE_AP_MESSAGE_HEADER	        0x01	// next 2bits 	Sub Header
#define TRIPLE_AP_MESSAGE_HEADER	        0x02	// next 2bits 	Sub Header

namespace interfaces {

    namespace SigfoxEncoder {

        int encodeAnalog(unsigned char *buffer, unsigned int portNum, float value);

        int encodeAnalog(unsigned char* buffer,
                        int portNum,
                        float value,
                        int portNum2,
                        float value2);

    } /* Encoder */ 

} /* interfaces */ 

#endif /* ENCODER_H */
