/**
 * @file Sigfox.h
 * @author Andres Arias
 * @date 7/10/2019
 * @brief Sigfox protocol module. Sends Sigfox messages
 * according to the Loka standard.
 */

#ifndef SIGFOX_H
#define SIGFOX_H

#include "sdkconfig.h"

#ifdef CONFIG_ENABLE_NEURON
#include "LOKA_NEURON_S2LP.h"
#else
#include "LOKA_PRIMIS_WiSOL.h"
#endif

#include <string>
#include "esp_err.h"
#include "Encoder.h"

#define SIGFOX_TAG "Sigfox"

namespace interfaces {

    namespace Sigfox {

        esp_err_t turnOn(void);

        esp_err_t turnOn(const std::string &zone);

        esp_err_t turnOff(void);

        std::string getID(void);
        
        esp_err_t sendRaw(const std::string &message);

        esp_err_t sendAnalog(unsigned int port, double value);

    } /* namespace Sigfox */ 

} /* namespace interfaces */ 

#endif
