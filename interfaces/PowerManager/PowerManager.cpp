/**
 * @file PowerManager.cpp
 * @author Andres Arias
 * @date 30/9/2019
 * @brief 
 */

#include "PowerManager.h"

namespace interfaces {

    static void RTC_IRAM_ATTR wakeStubDefault(void)
    {
        esp_default_wake_deep_sleep();
        return;
    }

    PowerManager::PowerManager(void)
    {
        #ifndef CONFIG_ENABLE_NEURON
            Board::init();
        #else
            this->ptrBoard = new drivers::NeuronPower();
        #endif
    }

    PowerManager::~PowerManager(void)
    {
    }

#ifndef CONFIG_ENABLE_NEURON
    void PowerManager::sleepPrepare(void)
    {
        using namespace drivers;

        DigitalPin::write(POWER_SPI_PIN, 1);
        DigitalPin::init(POWER_SPI_PIN, DigitalPin::Mode::OUTPUT);
        // WiSOL modem must be turned on and off to avoid in-rush current draw
        
        WiSOL sigfox_driver;
        sigfox_driver.turnOn();
        vTaskDelay(20 / portTICK_PERIOD_MS);
        sigfox_driver.turnOff();
    }

    void PowerManager::selectWakeUp(uint8_t source)
    {
        BoardULP::setWakeUp(source);
    }

    void PowerManager::sleep(unsigned long seconds, void (*wakeup_stub)())
    {
        BoardULP::init();
        Board::gotoLowPowerMode(seconds,wakeup_stub); 
    }
    void PowerManager::sleepStub()
    {
        stubSleep();
    }


#else
    void PowerManager::sleep(unsigned long seconds, void (*wakeup_stub)())
    {
        this->ptrBoard->prepareSleep();
        // this->ptrULP = new drivers::ESPULP();
        vTaskDelay( 1000 / portTICK_PERIOD_MS);
        this->ptrBoard->sleep(seconds, wakeup_stub);
    }

    void PowerManager::sleepStub(unsigned long seconds, void (*wakeup_stub)())
    {
        this->ptrBoard->sleepStub(seconds, wakeup_stub);
    }

#endif
    void PowerManager::enableButtonWakeup(void)
    {
        ESP_LOGI(POWERMANAGER_TAG, "Button wakeup enabled");
        esp_sleep_enable_ext1_wakeup(BUTTON_GPIO_MASK, ESP_EXT1_WAKEUP_ANY_HIGH);
    }

} /* interfaces */ 
