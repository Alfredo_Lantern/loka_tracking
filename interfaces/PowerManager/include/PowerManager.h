/**
 * @file PowerManager.h
 * @author Andres Arias
 * @date 30/9/2019
 * @brief Power Manangement routines.
 */

#ifndef POWERMANAGER_H
#define POWERMANAGER_H

#include "sdkconfig.h"
#include "BoardIO.h"

#include "LOKA_DigitalPin.h"
// #include "LOKA_ULP.h"
#ifdef CONFIG_ENABLE_NEURON
#include "LOKA_NEURON_Power.h"
#else
#include "BOARD_LokaV2.h"
#include "LOKA_PRIMIS_WiSOL.h"

#define POWER_DCDC_ON       DCDC_ON
#define POWER_DCDC_LVL      DCDC_LVL
/**
 * @brief GPIO where the sleep trigger of the accelerometer is
 * soldered.
 */
#define POWER_SPI_PIN       26
#endif

/**
 * @brief Tag used by the logging library.
 */
#define POWERMANAGER_TAG    "PowerManager"
#define BUTTON_GPIO_MASK    0x800000000


namespace interfaces {

    /**
     * @class PowerManager
     * @brief Controls the different power modes on the Loka device.
     */
    class PowerManager {

        public:

            PowerManager(void);

            ~PowerManager(void);

            // drivers::ESPULP * ptrULP;

            /**
             * @brief Sends the device into deep sleep mode for a given amount
             * of time.
             * @param[in] seconds Amount of time to go to sleep.
             * @param[in] wakeup_stub Function to run as soon as the Loka wakes up.
             */
            void sleep(unsigned long seconds, void (*wakeup_stub)() = NULL);
            void enableButtonWakeup(void);

#ifndef CONFIG_ENABLE_NEURON
            void selectWakeUp(uint8_t source);

            void sleepStub();

        private:
            

            void sleepPrepare(void);
#else
            drivers::NeuronPower * ptrBoard;
            void sleepStub(unsigned long seconds, void (*wakeup_stub)());
#endif
        
    }; /* PowerManager */ 
} /* namespace interfaces */ 

#endif
