/**
 * @file Wifi.cpp
 * @author Alfredo Piedra
 * @date 03 Jan 2020
 * @brief Wifi interface for handling the ESP Wifi driver
 */


#include "Wifi.h"

namespace interfaces
{
    Wifi::Wifi():ssid("")
    {
        this->wifiDriver = new drivers::ESPWiFiDriver();
        ESP_LOGD(WIFI_TAG, "Wifi initialized.");
    }

    Wifi::~Wifi()
    {
        ESP_LOGD(WIFI_TAG, "Wifi deinitialized.");
    }

    std::string Wifi::getSSID(void) const
    {
        return this->ssid;
    }

    bool Wifi::isInitialized() const
    {
        return this->wifiDriver->is_init;
    }

    bool Wifi::isConnected() const
    {
        return this->wifiDriver->is_connected;
    }

    esp_err_t Wifi::connect(const std::string &ssid,
                            const std::string &password)
    {
        this->ssid = ssid;
        return wifiDriver->connectAP(ssid, password);
    }

    EventGroupHandle_t *Wifi::getEventGroup(void)
    {
        return wifiDriver->getEventGroup();
    }

    std::vector<std::vector<accessPoint *>> Wifi::scan()
    {
        unsigned char i, minSize = 0;
        std::vector<std::vector<accessPoint *>> apsLists;

        // Channel mask for scanning channels 1,6,11
        uint16_t channelMask = 0x842;

        // Scan for aps in the channels 1,6,11
        this->wifiDriver->scanWithMask(channelMask, apsLists);
        
        // Total aps detected on the channels 1,6,11
        for (i = 0; i < apsLists.size(); ++i)
        {
            minSize += apsLists[i].size();
        }

        // If the scan does not provide the minimum of aps
        // scan again but in the channels 2,3,4,5,7,8,9,10
        if (minSize < 2)
        {
            // Channel mask for scanning channels 2,3,4,5,7,8,9,10
            channelMask = 0x7BC;

            // Scan for aps in the channels 2,3,4,5,7,8,9,10
            this->wifiDriver->scanWithMask(channelMask, apsLists);
        }

        return apsLists;
    }
} // interfaces  
