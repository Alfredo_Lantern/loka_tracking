
/**
 * @file Wifi.hpp
 * @author Andres Arias
 * @author Alfredo Piedra
 * @date 03 Jan 2020
 * @brief Wifi interface for handling the ESP Wifi driver
 */

#ifndef WIFI_H
#define WIFI_H

#include "esp_log.h"
#include "LOKA_WIFI.h"

#define WIFI_TAG "Wifi"

/**
 * @namespace interfaces
 * @brief High level hardware-independent functions for interfacing with the 
 * hardware beneath.
 */ 
namespace interfaces
{
    /**
     * @class Wifi
     * @brief General WiFi API for WiFi management
     */
    class Wifi
    {
        public:
        
            /**
             * @brief class constructor
             */
            Wifi();

            /**
             * @brief class destructor
             */
            ~Wifi();

            /**
             * @brief Return the SSID of the last AP connected to.
             * @retval Empty string if never established a connection.
             */
            std::string getSSID(void) const;

            /**
             * @brief Indicates whether the network stack is initialized or not.
             * @retval true Network stack ready to receive connection request.
             * @retval false Network stack down.
             */
            bool isInitialized() const;

            /**
             * @brief Indicates if the module is currently connected to an AP.
             * @retval true Connection is currently up and running.
             * @retval false Network currently disconnected.
             */
            bool isConnected() const;

            /**
             * @brief Establishes a connection to an AP with the given credentials.
             * @param ssid the SSID of the desired AP.
             * @param password The AP password.
             * @retval ESP_OK Is a connection is established successfully.
             * @retval Other Refer to the ESP IDF Programming guide.
             */
            esp_err_t connect(const std::string &ssid,
                              const std::string &password);

            /**
             * @brief Return the WiFi Event handle. Useful for syncronizing
             * tasks based on async WiFi events.
             * @note Use xEventGroupWaitBits to put a task to wait for successful
             * connection.
             * @return The WiFi EventGroupHandle.
             */
            EventGroupHandle_t *getEventGroup(void);

            /**
             * @brief A prioritized scan is performed on non-overlapping channels 
             *        and if the minimum number of access points is not found, 
             *        the scan is performed on the remaining channels.
             * @retval list of lists of access point, one list for each scanned channel
             */
            std::vector<std::vector<accessPoint *>> scan();

        private:

            std::string ssid; // Last connected AP.
            drivers::ESPWiFiDriver *wifiDriver;

    };
    
} /* interfaces */

#endif /* WIFI_H */

