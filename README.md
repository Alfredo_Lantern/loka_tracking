Loka OS 3 (Refactor)
====================

[Loka](https://loka.systems/) is a low-cost, low-power smart tracker based on the global low-power Sigfox
Network. Loka provides different operation modes and a low-power global positioning system.

Firmware developed using Espressif's ESP IDF 3.3, based on [Loka OS v2](https://gitlab.com/loka-device/loka_os).

## Requirements

* [Espressif's ESP32 IoT Development Framework](https://docs.espressif.com/projects/esp-idf/en/stable/get-started/index.html).

## Compilation instructions

Just follow the usual ESP32 IDF's compilation routine:
````
make menuconfig
make
make flash
make monitor
````
