#include "Tracker.h"

extern "C"{
	void readMovement();
}

namespace programs{

    Tracker::Tracker(){
        ESP_LOGI(TRACKER_TAG, "Initializing program tracker.");
        this->sleepTime = 120;
        this->resendInterval = 152;
        this->movementThreshold = 45;
        this->gpsEnabled = 0;
        this->gpsBalloon = 0;
        this->ds18b20 = 0;
        // GeoWifiManager::init(scanInterval, resendInterval, gpsEnabled, gpsBalloon);
        this->started = 0;
        this->ptrPowMan = new interfaces::PowerManager();

    }

    Tracker::Tracker(unsigned long pSleepTime, unsigned long pResendInterval, unsigned char pMovementThreshold, 
        unsigned char pGpsEnabled, unsigned char pGpsBalloon, unsigned char pDs18b20){
            
        ESP_LOGI(TRACKER_TAG, "Initializing program tracker.");
        this->sleepTime = pSleepTime;
        this->resendInterval = pResendInterval;
        this->movementThreshold = pMovementThreshold;
        this->gpsEnabled = pGpsEnabled;
        this->gpsBalloon = pGpsBalloon;
        this->ds18b20 = pDs18b20;
        // GeoWifiManager::init(scanInterval, resendInterval, gpsEnabled, gpsBalloon);
        this->started = 0;
        this->ptrPowMan = new interfaces::PowerManager();
    }

    Tracker::~Tracker(){
        ESP_LOGI(TRACKER_TAG, "Deinitializing program tracker.");
    }

    void RTC_IRAM_ATTR TrackerWakeupStub(void){
        bool pressed, keepAlive;
        uint64_t currentTime;

        REG_WRITE(TIMG_WDTFEED_REG(0), 1);
        ets_update_cpu_frequency_rom(ets_get_detected_xtal_freq() / 1000000);			// update frequency register for timing

        pressed = GPIO_INPUT_GET(BUTTON);

        readMovement();

        keepAlive = false;
        if(keepAliveEnable){
            currentTime = RTC_getTimer();
            if(currentTime >= keepAliveTime)
                keepAlive = true;
        }
         
        if(movementValue || pressed || keepAlive){
            esp_default_wake_deep_sleep();
            return;
        }
        //It needs to go to sleep again
        vTaskDelay( 10 / portTICK_PERIOD_MS);


        drivers::ESPULP * ptrULP = new drivers::ESPULP();
        ptrULP->resetWakeUp();
        REG_WRITE(RTC_ENTRY_ADDR_REG, (uint32_t) &TrackerWakeupStub);	// Resets the stub pointer in case going back to sleep
        interfaces::PowerManager * ptrPM = new interfaces::PowerManager();
        #ifndef CONFIG_ENABLE_NEURON
            ptrPM->sleepStub();
        #else
            ptrPM->sleepStub(10, &TrackerWakeupStub);
        #endif
        // stubSleep();
    }

    void Tracker::sendFromCurrentBSSIDList()
    {
        //ESP_LOGI(TRACKER_TAG, "Inside sendFromCurrent");
        services::WifiScanning wifiScanningService;

        // Get the next two Sigfox payloads
        std::vector<std::string> nextPayloads = wifiScanningService.readPayloadFromResultFile();

        if (nextPayloads.size() == 2)
        {
            // Message sending procedure
            ESP_LOGI(ESPWIFI_TAG, "Turning Sigfox Radio On");
            interfaces::Sigfox::turnOn();
            ESP_LOGI(ESPWIFI_TAG, "Sending Sigfox Message");
            interfaces::Sigfox::sendRaw(nextPayloads[0]);
            interfaces::Sigfox::sendRaw(nextPayloads[1]);
            ESP_LOGI(ESPWIFI_TAG, "Turning Sigfox Radio Off");
            interfaces::Sigfox::turnOff();
        }
    }

    void Tracker::executeNewScanProcedure()
    {
       // ESP_LOGI(TRACKER_TAG, "Inside execute New Scan");
        unsigned char i, j;
        interfaces::Wifi wiFiInterface;
        std::vector<std::string> topList;
        services::WifiScanning wifiScanningService;
        std::vector<std::vector<accessPoint *>> scanAPsList = wiFiInterface.scan();

        scanAPsList = wifiScanningService.categorizeAPLists(scanAPsList);

        //ESP_LOGE(ESPWIFI_TAG,"SCAN INTERVENTION");
        
        // Sort by RSSI the APs of each category
        for (i = 0; i < scanAPsList.size(); ++i)
        {
            wifiScanningService.sortAPList(scanAPsList[i]);
            
            /*for(j = 0; j < scanAPsList[i].size(); ++j)
            {	
                ESP_LOGE(ESPWIFI_TAG,"%s : %x %x %x %x %x %x : %i : %i",
                        scanAPsList[i][j]->ssid.c_str(),
                        scanAPsList[i][j]->bssid[0],
                        scanAPsList[i][j]->bssid[1],
                        scanAPsList[i][j]->bssid[2],
                        scanAPsList[i][j]->bssid[3],
                        scanAPsList[i][j]->bssid[4],
                        scanAPsList[i][j]->bssid[5],
                        (int) scanAPsList[i][j]->channel,
                        (int) scanAPsList[i][j]->rssi);
            }*/
        }

        // Add the top elements to a list that will 
        // be used for comparison between scans
        for (i = 0; i < scanAPsList.size() && topList.size() < TOP_LIMIT; ++i)
        {
            for (j = 0; j < scanAPsList[i].size() && topList.size() < TOP_LIMIT; ++j)
            {
                topList.push_back(std::string((char *) scanAPsList[i][j]->bssid, 6));
            }
        }

        // In case the current scan is not 
        // the same as the previous one
        if (!wifiScanningService.compareScans(topList))
        {
            wifiScanningService.saveNewScan(topList,scanAPsList);
        }
        else
        {
            sendFromCurrentBSSIDList();
        }
        
        return;
    }

    void Tracker::run(){
        int temperature;

        this->ptrAcc = new drivers::LIS3DX(1);
        this->ptrAcc->setWakeUp(0, this->movementThreshold);

    	// GeoWifiManager::setMovement(true);									// Movement was surely detected

        if(this->ds18b20 == 1){
            DS18B20::init(GPIO_NUM_13);
		    temperature = DS18B20::getTemperature();
	    }
        else{
            temperature = ptrAcc->readTemperature();						// Updates hardware status on GeoWifiManager
	    }
        // GeoWifiManager::setTemperature(temperature);
	    // GeoWifiManager::run();

        ESP_LOGI(TRACKER_TAG, "Temperature: %d", temperature);

        services::WifiScanning wifiScanningService;

        if (!movementValue && wifiScanningService.resultFileExist())
        {
            sendFromCurrentBSSIDList();
        }
        else
        {
            executeNewScanProcedure();
        }

        // Reset all the accelerometer settings meanwhile changed on the battery manager
        this->ptrAcc->~LIS3DX();
        this->ptrAcc = new drivers::LIS3DX(1);
        this->ptrAcc->setWakeUp(0, this->movementThreshold);

    	ESP_LOGI(TRACKER_TAG, "Sleep time: %lu", sleepTime);

        // Sets the wake-up periodicity and sleeps
        this->ptrPowMan->sleep(sleepTime, &TrackerWakeupStub);
    }
    
    
} 
