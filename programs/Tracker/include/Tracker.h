#ifndef PROGRAM_TRACKER_H_
#define PROGRAM_TRACKER_H_

// #include "GeoWifiManager.h"
// #include "ProgramSigfoxProtocol.h"
// #include "SIGFOX_Protocol.h"

// #include "BOARD_LokaV2.h"
#include "BOARD_LokaV2_ULP.h"
#include "PowerManager.h"
#include "LOKA_Interruptions.h"
#include "LOKA_LIS3DX.h"
#include "DS18B20_Temperature.h"
#include "BoardIO.h"
#include "LOKA_RTC.h"
#include "LOKA_ULP.h"

#include "Wifi.h"
#include "Sigfox.h" 		    // included for turnOn, turnOff and sendData functions
#include "WifiScanning.h"

#include "string.h"
#include "rom/gpio.h"
#include "rom/ets_sys.h"
#include "soc/rtc_cntl_reg.h"

#define TEMPERATURE_MAX_INTERVAL		86400
#define VCC_MAX_INTERVAL 				86400
#define TEMPERATURE_VAR_INTERVAL		5
#define VCC_ANALOG_PORT					103
#define MOVE_ANALOG_PORT				101
#define TRACKER_TAG     "TRACKER"

    extern bool movementValue;
    extern bool keepAliveEnable;
    extern uint64_t keepAliveTime;
    extern void checkButtonPressed(void);
    
namespace programs{

    class Tracker{
        private:
            unsigned long sleepTime;
            unsigned long resendInterval;
            unsigned char movementThreshold;
            unsigned char gpsEnabled;
            unsigned char gpsBalloon;
            unsigned char ds18b20;
            unsigned char started;
            unsigned int remainingWakeups;
            drivers::LIS3DX * ptrAcc;
            interfaces::PowerManager * ptrPowMan;
            void executeNewScanProcedure();
            void sendFromCurrentBSSIDList();

        public:
            Tracker();
            Tracker(unsigned long pSleepTime, unsigned long pResendInterval, unsigned char pMovementThreshold, 
            unsigned char pGpsEnabled, unsigned char pGpsBalloon, unsigned char pDs18b20);
            ~Tracker();
            void run();


    }; 

}

#endif