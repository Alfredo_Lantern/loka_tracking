#====================================
# File: Makefile
# Author: Andres Arias
# Email: andres.arias@lantern.tech
# Compnay: Lantern Technologies
# Date: 16/9/2019
# Description: Main project makefile
#====================================

PROJECT_NAME := loka-os

EXTRA_COMPONENT_DIRS += $(PROJECT_PATH)/drivers/protocols
EXTRA_COMPONENT_DIRS += $(PROJECT_PATH)/drivers/modules
EXTRA_COMPONENT_DIRS += $(PROJECT_PATH)/drivers/utils
EXTRA_COMPONENT_DIRS += $(PROJECT_PATH)/interfaces
EXTRA_COMPONENT_DIRS += $(PROJECT_PATH)/services
EXTRA_COMPONENT_DIRS += $(PROJECT_PATH)/programs

include $(IDF_PATH)/make/project.mk

