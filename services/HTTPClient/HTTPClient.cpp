/**
 * @file HTTPClient.cpp
 * @author Andres Arias
 * @date 18/9/2019
 * @brief Simple HTTP/HTTPS client to perform different operations.
 */

#include "HTTPClient.h"

namespace services
{
    static EventGroupHandle_t g_http_event_group;
    static char* g_http_data;
    static int g_http_data_len;

    esp_err_t httpEventHandler(esp_http_client_event_t *evt)
    {
        switch(evt->event_id) {
            case HTTP_EVENT_ERROR:
                ESP_LOGI(HTTPCLIENT_TAG, "HTTP_EVENT_ERROR");
                break;
            case HTTP_EVENT_ON_CONNECTED:
                ESP_LOGI(HTTPCLIENT_TAG, "HTTP_EVENT_ON_CONNECTED");
                break;
            case HTTP_EVENT_HEADER_SENT:
                ESP_LOGI(HTTPCLIENT_TAG, "HTTP_EVENT_HEADER_SENT");
                break;
            case HTTP_EVENT_ON_HEADER:
                ESP_LOGI(HTTPCLIENT_TAG, "HTTP_EVENT_ON_HEADER");
                break;
            case HTTP_EVENT_ON_DATA:
                ESP_LOGI(HTTPCLIENT_TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
                g_http_data = (char*)evt->data;
                g_http_data_len = evt->data_len;
                break;
            case HTTP_EVENT_ON_FINISH:
                ESP_LOGI(HTTPCLIENT_TAG, "HTTP_EVENT_ON_FINISH");
                break;
            case HTTP_EVENT_DISCONNECTED:
                ESP_LOGI(HTTPCLIENT_TAG, "HTTP_EVENT_DISCONNECTED");
                xEventGroupSetBits(g_http_event_group, BIT0);
                break;
        }
        return ESP_OK;
    }

    HTTPClient::HTTPClient(bool use_https,
                           char *server_cert):
        using_https(use_https),
        https_cert(server_cert)
    {
        ESP_LOGI(HTTPCLIENT_TAG, "Client initializing.");
        g_http_event_group = xEventGroupCreate();
        g_http_data_len = 0;
    }

    HTTPClient::~HTTPClient()
    {
        ESP_LOGD(HTTPCLIENT_TAG, "Client deinitializing.");
    }

    bool HTTPClient::isUsingHTTPS(void) const
    {
        return this->using_https;
    }

    char *HTTPClient::getServerCert(void) const
    {
        return this->https_cert;
    }

    void HTTPClient::enableHTTPS(char *server_cert)
    {
        this->https_cert = server_cert;
        this->using_https = true;
    }

    void HTTPClient::disableHTTPS(void)
    {
        this->https_cert = nullptr;
        this->using_https = false;
    }

    std::string HTTPClient::getResponse(void) const
    {
        ESP_LOGI(HTTPCLIENT_TAG, "Retrieving response.");
        if (g_http_data_len == -1) {
            ESP_LOGE(HTTPCLIENT_TAG, "Error: Could not retrieve response, latest request failed.");
            return "";
        }
        xEventGroupWaitBits(g_http_event_group, BIT0, false, true, portMAX_DELAY);
        ESP_LOGI(HTTPCLIENT_TAG, "Response size: %d", g_http_data_len);
        char *terminated_data = (char*)malloc(g_http_data_len * sizeof(char));
        sprintf(terminated_data, "%.*s", g_http_data_len, g_http_data);
        std::string result(terminated_data);
        free(terminated_data);
        return result;
    }

    esp_err_t HTTPClient::get(const std::string &get_url) const
    {
        esp_http_client_config_t config = {};
        config.url = get_url.c_str();
        config.event_handler = httpEventHandler;
        if (this->using_https)
            config.cert_pem = this->https_cert;
        esp_http_client_handle_t client = esp_http_client_init(&config);
        esp_err_t res = esp_http_client_perform(client);
        xEventGroupClearBits(g_http_event_group, BIT0);
        if (res == ESP_OK) {
            ESP_LOGI(HTTPCLIENT_TAG, "Status = %d",
                    esp_http_client_get_status_code(client));
        } else {
            ESP_LOGE(HTTPCLIENT_TAG, "ERROR (%s): Could not perform HTTP GET request",
                                     esp_err_to_name(res));
            g_http_data_len = -1;
        }

        esp_http_client_cleanup(client);
        return res;
    }

    esp_err_t HTTPClient::post(const std::string &post_url, const std::string &body) const
    {
        ESP_LOGI(HTTPCLIENT_TAG, "URL: %s", post_url.c_str());
        esp_http_client_config_t config = {};
        config.url = post_url.c_str();
        config.event_handler = httpEventHandler;
        if (this->using_https)
            config.cert_pem = this->https_cert;
        config.buffer_size = HTTP_RX_BUFFER_SIZE;
        esp_http_client_handle_t client = esp_http_client_init(&config);
        esp_http_client_set_method(client, HTTP_METHOD_POST);
        esp_http_client_set_header(client, "Content-Type", "application/json");
        esp_http_client_set_post_field(client, body.c_str(), body.size());
        esp_err_t res = esp_http_client_perform(client);
        xEventGroupClearBits(g_http_event_group, BIT0);
        if (res == ESP_OK) {
            ESP_LOGI(HTTPCLIENT_TAG, "Status = %d",
                    esp_http_client_get_status_code(client));
        } else {
            ESP_LOGE(HTTPCLIENT_TAG, "ERROR (%s): Could not perform HTTP POST request",
                                     esp_err_to_name(res));
            g_http_data_len = -1;
        }

        esp_http_client_cleanup(client);
        return res;
    }

} /* services */ 
