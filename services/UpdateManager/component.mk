#==================================
# Date: 19/9/2019
# Author: Andres Arias
# Company: Lantern Technologies
# Description: Component makefile
#==================================

COMPONENT_ADD_INCLUDEDIRS = include
#COMPONENT_EMBED_TXTFILES :=  ${PROJECT_PATH}/certs/ca_cert.pem
COMPONENT_EMBED_TXTFILES :=  ${PROJECT_PATH}/certs/aws.pem
