/**
 * @file UpdateManager.cpp
 * @author Andres Arias
 * @date 19/9/2019
 * @brief Service to manage OTA updates.
 */

#include "UpdateManager.h"

namespace services
{
    extern const uint8_t server_cert_pem_start[] asm("_binary_aws_pem_start");
    extern const uint8_t server_cert_pem_end[] asm("_binary_aws_pem_end");

    UpdateManager::UpdateManager():
        checked_update(false),
        latest_major(FW_VERSION_MAJOR),
        latest_minor(FW_VERSION_MINOR),
        update_token(""),
        update_url("")
    {
        ESP_LOGD(UPDATES_TAG, "Initializing UpdateManager service.");
    }

    UpdateManager::~UpdateManager(void)
    {
        ESP_LOGD(UPDATES_TAG, "Update service stopped.");
    }

    std::pair<uint8_t, uint8_t> UpdateManager::getLatestVersion(void) const
    {
        return std::pair<uint8_t, uint8_t>(this->latest_major, this->latest_minor);
    }

    bool UpdateManager::requestUpdate(void)
    {
        drivers::WiSOL sigfox_driver;
        std::stringstream fw_stream;
        fw_stream << FW_VERSION_MAJOR << FW_VERSION_MINOR;
        sigfox_driver.turnOn();
        this->update_token = sigfox_driver.sendAndReadData(fw_stream.str());
        if (this->update_token == TIMEOUT_MESSAGE)
            return false;
        return true;
    }

    std::string UpdateManager::getUpdateURL(void) const
    {
        return this->update_url;
    }

    esp_err_t UpdateManager::getUpdateInfo(std::string token)
    {
        ESP_LOGI(UPDATES_TAG, "Checking if there's available updates...");
        HTTPClient http_client;
        std::string req_body = this->jsonifyToken(token);
        esp_err_t res = http_client.post("https://xwe5ucdoe8.execute-api.us-west-2.amazonaws.com/dev/token", req_body.c_str());
        std::string client_res = http_client.getResponse();
        ESP_LOGI(UPDATES_TAG, "Server Response : %s",client_res.c_str());
        this->parseJSON(client_res);
        this->checked_update = true;
        return res;
    }

    esp_err_t UpdateManager::performUpdate(void)
    {
        //if (this->update_token.length() == 0) {
            //ESP_LOGE(UPDATES_TAG, "No token acquired. Please run requestUpdate first.");
            //return ESP_FAIL;
        //}
        //this->getUpdateInfo();
        ESP_LOGI(UPDATES_TAG, "UPDATE FOUND: Loka OS v%s", this->latest_version.c_str());
        HTTPClient http_client;
        ESP_LOGI(UPDATES_TAG, "Performing Over-The-Air Update");
        esp_http_client_config_t config = {};
        config.buffer_size = UPDATE_TX_BUFFER_SIZE;
        //config.url = this->update_url.c_str();
        config.url = "https://loka-ota-test.s3-us-west-2.amazonaws.com/loka_os.bin";
        config.cert_pem = (char*)server_cert_pem_start;
        config.event_handler = httpEventHandler;

        ESP_LOGE(UPDATES_TAG, "before ota trigger");

        esp_err_t res = esp_https_ota(&config);

        ESP_LOGE(UPDATES_TAG, "after ota trigger");


        if (res == ESP_OK) {
            ESP_LOGI(UPDATES_TAG, "Updated successfully to Loka OS v%s! Rebooting now...",
                                  this->latest_version.c_str());
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            esp_restart();
        } else {
            ESP_LOGE(UPDATES_TAG, "ERROR (%s) Could not perform update.",
                                  esp_err_to_name(res));
            return res;
        }
        while (1) {
            vTaskDelay(1000 / portTICK_PERIOD_MS);
        }
    }

    void UpdateManager::parseJSON(const std::string &json_str)
    {
        // TODO Don't parse error response! It will cause a segfault!
        cJSON *json_root = cJSON_Parse(json_str.c_str());
        cJSON *json_data = cJSON_GetObjectItem(json_root, "data");
        this->latest_version = std::string(cJSON_GetObjectItem(json_data,"version")->valuestring);
        char *url_str = cJSON_GetObjectItem(json_data, "url")->valuestring;
        this->update_url = std::string(url_str);
        cJSON_Delete(json_root);
    }

    std::string UpdateManager::jsonifyToken(std::string token)
    {
        drivers::WiSOL sigfox_driver;
        sigfox_driver.turnOn();
        std::string device_id = sigfox_driver.getID();
        sigfox_driver.turnOff();
        vTaskDelay(1000/portTICK_PERIOD_MS);
        cJSON *root = cJSON_CreateObject();
        cJSON_AddStringToObject(root, "device_id",device_id.c_str());
        cJSON_AddStringToObject(root, "token", token.c_str());
        std::string result(cJSON_Print(root));
        cJSON_Delete(root);
        ESP_LOGI(UPDATES_TAG, "Device ID: %s ", device_id.c_str() );
        
        return result;

    }

} /* services */ 
