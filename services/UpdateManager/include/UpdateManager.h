/**
 * @file UpdateManager.h
 * @author Andres Arias
 * @date 19/9/2019
 * @brief Service to manage OTA updates.
 */

#ifndef UPDATES_H
#define UPDATES_H

#include <cmath>    // For float comparison
#include <limits>   // For float comparison
#include <utility>  // For std::pair
#include <string>
#include <sstream>
#include "esp_https_ota.h"
#include "esp_log.h"
#include "cJSON.h"
#include "HTTPClient.h"
#include "LOKA_PRIMIS_WiSOL.h"
#include "FirmwareInfo.h"

#define UPDATES_TAG             "UpdateManager"
#define TIMEOUT_MESSAGE         "ERR_SFX_ERR_SEND_FRAME_WAIT_TIMEOUT"
#define MANAGER_URL             CONFIG_OTA_MANAGER_UPDATE_URL
#define UPDATE_TX_BUFFER_SIZE   2048

namespace services
{
    /**
     * @class UpdateManager
     * @brief Checks for updates and performs OTA updates if the device's
     * firmware is out-of-date.
     */
    class UpdateManager
    {
        public:

            /**
             * @brief Class constructor. Initializes the update manager.
             */
            UpdateManager();

            /**
             * @brief Class destructor. Stops the update manager.
             */
            ~UpdateManager(void);

            /**
             * @brief Returns the latest version found. 
             * @note Its value will be updated after checking out for an update.
             * @retval FIRMWARE_VERSION The current firmware version if no checks
             * have been performed.
             */
            std::pair<uint8_t, uint8_t> getLatestVersion(void) const;

            bool requestUpdate(void);

            /**
             * @brief Return the URL where the up-to-date firmware's binaries
             * can be downloaded for the current device.
             * @note Its value will be updated after checking out for an update.
             * @retval "" Empty string if no checks have been performed.
             */
            std::string getUpdateURL(void) const;

            /**
             * @brief Connects to the update manager endpoint to check for the latest
             * version of the firmware available.
             * @retval ESP_OK The query was performed successfully.
             * @retval Other Refer to the ESP IDF Programming guide.
             */
            esp_err_t getUpdateInfo(std::string token);

            /**
             * @brief Checks if there's an update available and, if a new version is
             * found, downloads the new firmware, installs it and then reboots the 
             * device into the new firmware.
             * @retval ESP_OK The update query was performed successfully, however there
             * are no available updates.
             * @retval Other Refer to the ESP IDF Programming guide.
             */
            esp_err_t performUpdate(void);

        private:
            bool checked_update; /** An update request have been perfored. */

            std::string latest_version; /** The version number of the up-to-date firmware. */
            
            uint8_t latest_major;

            uint8_t latest_minor;

            std::string update_token;

            std::string update_url; /** The URL where the latest update can be downloaded. */

            /**
             * @brief Parses the response returned by the update manager endpoint.
             * @param json_str the string containing the server's response.
             */
            void parseJSON(const std::string &json_str);

            std::string jsonifyToken(std::string token);
            
    }; 
} /* services */ 

#endif
