/**
 * @file WifiScanning.hpp
 * @author Alfredo Piedra
 * @date 03 Jan 2020
 * @brief Category-based geolocation algorithm.
 */

#ifndef WIFISCANNING_H
#define WIFISCANNING_H

#include <map>
#include <cmath>
#include <algorithm>
#include "Wifi.h"
#include "Sigfox.h"
#include "LOKA_SPIFFS.h"

#define RESULT_FILE "scan.bin"              // Name of the file where the prioritized 
                                            // and sorted scan results are stored

#define PRIORITY_FILE "oui.bin"        // Name of the file that contains the known OUI
                                                    // and the corresponding category
                                                
#define EXACT_MATCH_FILE "exact.txt"        // Name of the file containing the specific SSID conventions 
                                                    // to be searched in the scans

#define SUBSTRING_FILE "substring.txt"      // Name of the file that contains the SSID that

#define SPIFFS_PARTITION "storage"                  // Name of the partition that contains the SPIFFS

#define TOP_LIMIT (unsigned char) 4                 // Max number of elements in the top of the 
                                                    // resulting BSSID list that will be used for comparison

#define SCAN_ID_MAX (unsigned char) 7               // The scan id range is [0, SCAN_ID_MAX] 
                                                    // when this value is reached the scan id resets to zero.

namespace services
{
    /**
     * @class WifiScanning
     * @brief Provides functionality for managing and processing 
     *        lists of access points and format-specific SPIFFS files
     */
    class WifiScanning
    {
        public:

            /**
             * @brief Class constructor.
             */
            WifiScanning();

            /**
             * @brief Class destructor.
             */
            ~WifiScanning();

            /**
             * @brief Verify if the result file exists in SPIFFS.
             * @retval true if result file exists in the SPIFFS.
             * @retval false if the result file does not exists in SPIFFS.
             */
            bool resultFileExist();
            
            /**
             * @brief Read the next two payloads from the result file according to the counter value
             * @retval list with the the next two payloads
             * @retval "" if all the payloads in the file have already been sent
             */
            std::vector<std::string> readPayloadFromResultFile();

            /**
             * @brief Sort the access points list by their RSSI values in descending order
             * @param apList unordered list of access points pointers
             * @note The list is passed by reference
             */
            void sortAPList(std::vector<accessPoint *> &apList);

            /**
             * @brief Compares the top of the list obtained as a result of the new scan 
             *        against the top of the list stored in the result file
             * @param newTopList list with the BSSID of the top elements of the new list, 
             *                   its size must be equal to TOP_LIMIT
             * @retval true in case the top elements of the lists are the same
             * @retval false in case the top elements of the lists are different
             * @note the top size is defined by the constant TOP_LIMIT
             * @note the comparison focuses on the elements of the top being equal regardless of the order
             */
            bool compareScans(std::vector<std::string> newTopList);
            
            /**
             * @brief Overwrites the result file with the new scan list and send the first payload by sigfox
             * @param topList list with the BSSID of the top elements, its size must be equal to TOP_LIMIT
             * @param scanAPLists a list of lists of access point, its size must be 4, one for each category 
             * @note to obtain the scanAPLists right format execute first the function categorizeAPLists 
             */
            void saveNewScan(std::vector<std::string> topList, 
                             std::vector<std::vector<accessPoint *>> scanAPsLists);

            /**
             * @brief Categorize the access points of each scanned channel
             * @param apsLists list of lists of access points, it must contain a list for each scanned channel
             * @note to obtain the apsLists right format execute the scan function of the ESP WiFi driver 
             */
            std::vector<std::vector<accessPoint *>> categorizeAPLists(std::vector<std::vector<accessPoint *>> apsLists);

        private:

            /**
             * @brief Read the current value of the scan id from the result file
             * @retval the actual value of the scan id 
             */
            unsigned char readScanId();

            /**
             * @brief Normalize the RSSI to fit in 5 bits
             * @param rssi the non-normalized RSSI value
             * @retval the normalized RSSI value
             */
            int normalizeRSSI(int rssi);

            /**
             * @brief Reads the current top elements of the list from the result file
             * @retval the BSSID list of the top elements
             * @note the top list size is equal to the value of TOP_LIMIT
             */
            std::vector<std::string> readActualTop();

            /**
             * @brief Reads the SSID conventions from the corresponding file
             * @retval a list of SSID common names
             */
            std::map<std::string, unsigned char> readSSIDFile(std::string filename);

            /**
             * @brief Generates a payload with the wifi simple format
             * @param ap the access point information that will be encoded
             * @param scanId the actual value of the scan id
             * @retval the encoded payload string
             */
            std::string wifiUpperPayload(accessPoint *firstAP,
                                         accessPoint *secondAP,
                                         unsigned char scanId);


            std::string wifiLowerPayload(accessPoint *secondAP,
                                         accessPoint *thirdAP,
                                         unsigned char scanId);
            /**
             * @brief Reads the priority list of OUIs from the corresponding file.
             * @retval a dictionary with OUIs and the corresponding category
             */
            std::map<std::string, unsigned char> readPriorityList();

            /**
             * @brief Overwrites the result file in the SPIFFS
             * @param topList the new top elements list that will be stored
             * @param payloadsList the payloads list that will be stored
             * @param lineCounter the line counter that indicates where is the next payload that will be sent
             */
            void writeResultFile(std::vector<std::string> topList,
                                 std::vector<std::string> payloadsList,
                                 unsigned char lineCounter,
                                 unsigned char scanId);

            /**
             * @brief Search for virtual BSSID in the access point list
             * @param apList the access point list
             * @retval dictionary with flags that indicates if a specific BSSID is virtual
             */
            std::map<unsigned char, bool> computeVirtualBSSID(std::vector<accessPoint *> apList);

            /**
             * @brief Searches for repeated SSIDs through the lists of all scanned channels
             * @param apLists the access point lists of all scanned channels
             * @retval dictionary with the SSID and a flag that indicates if is repeated or not
             */
            std::map<std::string, bool> computeRepeatedSSID(std::vector<std::vector<accessPoint *>> apLists);

    };

} /* services */

#endif /* WIFISCANNING_H */

