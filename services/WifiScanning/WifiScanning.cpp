/**
 * @file WifiScanning.cpp
 * @author Alfredo Piedra
 * @date 03 Jan 2020
 * @brief Category-based geolocation algorithm.
    */

 #include "WifiScanning.h"

namespace services
{
    WifiScanning::WifiScanning()
    {}

    WifiScanning::~WifiScanning()
    {}

    bool WifiScanning::resultFileExist() 
    {
        drivers::SPIFFS spiffs;
        std::fstream file = spiffs.getFile(SPIFFS_PARTITION,RESULT_FILE, std::fstream::in);


        if (file){
            
            file.close();
            spiffs.unmount(SPIFFS_PARTITION);
            return true;
        }

        spiffs.unmount(SPIFFS_PARTITION);
        return false;   
    }                                        

    std::vector<std::string> WifiScanning::readPayloadFromResultFile() 
    {
        char payload[12];
        unsigned char header[4];
        std::vector<std::string> payloadsList;
        unsigned char listSize, listCounter, topSize, scanId;

        drivers::SPIFFS spiffs;
        std::fstream file = spiffs.getFile(SPIFFS_PARTITION,RESULT_FILE, 
                                             std::fstream::in | 
                                             std::fstream::out |
                                             std::fstream::binary);

        if (file)
        {    
            // Get the header with the top size and the paylod list size and position counter
            file.read((char *) header, 4);

            topSize = header[0];
            listSize = header[1];
            listCounter = header[2];
            scanId = header[3];

            if (listSize > 0 && listCounter <= listSize)
            {
                unsigned long correctPos = 4 
                                           + (unsigned long) topSize * 6 
                                           + ((unsigned long) listCounter  - 1) * 12;

                // Position the cursor to desired line
                file.seekg(correctPos);

                // Read the two payloads
                file.read(payload, 12);
                payloadsList.push_back(std::string(payload,12));
                
                file.read(payload, 12);
                payloadsList.push_back(std::string(payload,12));

                // Reset the position of the cursor to the beginning of the file
                file.seekp(0);

                // Write the updated header
                header[0] = topSize;
                header[1] = listSize;
                header[2] = listCounter + 2;
                header[3] = scanId;

                file.write((char *) header, 4);        
            }
            
            file.close();
        }

        spiffs.unmount(SPIFFS_PARTITION);
        return payloadsList;
    }                                                     

    void WifiScanning::sortAPList(std::vector<accessPoint *> &apList)
    {
        int i, j;
        accessPoint *key;

        // Applies insertion sorting to the AP
        // list based on the value of RSSI
        for (i = 1; i < apList.size(); ++i)
        {
            key = apList[i];
            j = i - 1;

            while (j >= 0 && apList[j]->rssi < key->rssi)
            {
                apList[j + 1] = apList[j];
                --j;
            }
            apList[j + 1] = key;
        }
    }

    bool WifiScanning::compareScans(std::vector<std::string> newTopList)
    {
        bool isEqual = false;
        unsigned char i = 0, j;
        
        // Get the actual top list from the result file
        std::vector<std::string> actualTopList = readActualTop();

        if (newTopList.size() > 0 && actualTopList.size() > 0)
        {
            do
            {
                // Check if the mac address is in the current top list
                for (j = 0; j < actualTopList.size(); ++j)
                {
                    isEqual = isEqual || (newTopList[i] == actualTopList[j]);
                }

                ++i;

            } while (i < newTopList.size() && isEqual);
        }

        return isEqual;
    }

    void WifiScanning::saveNewScan(std::vector<std::string> topList, 
                                   std::vector<std::vector<accessPoint *>> scanAPsList)
    {
        ESP_LOGI(ESPWIFI_TAG, "^^^^^^^SCAN NOT EQUAL^^^^^^^^");

        unsigned char i,j;
        unsigned char lineCounter = 0;
        unsigned char scanId = readScanId();
        std::vector<std::string> payloadsList;
        std::vector<accessPoint *> allAPsList;


        // Update the scan id
        if (scanId < SCAN_ID_MAX)
        {
            ++scanId;
        }
        else
        {
            scanId = 0;
        }       

        // Put all the AP in one list
        for (i = 0; i < scanAPsList.size(); ++i)
        {
            for (j = 0; j < scanAPsList[i].size(); ++j)
            {
                allAPsList.push_back(scanAPsList[i][j]);
            }
        }

        // Create the payloads with the AP list
        if (allAPsList.size() > 1)
        {
            accessPoint *firstAP = allAPsList[0];
            accessPoint *lastAP = nullptr;

            // If the size is even, the last element must be removed
            if (allAPsList.size() % 2 == 0)
            {
                // Remove the last AP to make the size odd and 
                // use the same loop to process the list
                lastAP = allAPsList[allAPsList.size() - 1];
                allAPsList.pop_back();
            }

            // Create the payloads from the AP list with odd size
            for (i = 1; i < allAPsList.size(); i += 2)
            {
                payloadsList.push_back(wifiUpperPayload(allAPsList[i],firstAP,scanId));
                payloadsList.push_back(wifiLowerPayload(firstAP,allAPsList[i+1],scanId));
                delete allAPsList[i];
                delete allAPsList[i+1];
            }

            // In this case the original AP list size is even 
            // and it is necessary to send the last AP
            if (lastAP != nullptr)
            {
                payloadsList.push_back(wifiUpperPayload(firstAP,nullptr,scanId));
                payloadsList.push_back(wifiLowerPayload(nullptr,lastAP,scanId));
                delete firstAP;
                delete lastAP;
            }
            
        }
        else if (allAPsList.size() == 1)
        {
            // Just one element in the list
            // Send only the lower payload format
            payloadsList.push_back(wifiLowerPayload(nullptr,allAPsList[0],scanId));
            delete allAPsList[0];
        }
        
        // Send the first and if possible the 
        // second MAC address, each with the 
        // corresponding RSSI and scan counter
        if (payloadsList.size() > 0)
        {
            lineCounter = 2;
            // Message sending procedure
            interfaces::Sigfox::turnOn();
            interfaces::Sigfox::sendRaw(payloadsList[0]);
            if (payloadsList.size() >= 2)
            {
                interfaces::Sigfox::sendRaw(payloadsList[1]);
                ++lineCounter;
            }            
            interfaces::Sigfox::turnOff();

            // Write the scan info in the result file
            writeResultFile(topList, payloadsList, lineCounter, scanId);
        } 
    }

    std::vector<std::vector<accessPoint *>> WifiScanning::categorizeAPLists(std::vector<std::vector<accessPoint *>> apsLists)
    {
        unsigned char i,j;
        bool isRepeatedSSIDListInit = false;

        // Dictionary that keeps the position
        // of each private BSSIDs in the scan
        // list and if it is virtual or not
        std::map<unsigned char, bool> virtualList;

        // Dictionary that associate the SSID of
        // the current APs list with a boolean
        // flag that indicates if each SSID is
        // repeated or not.
        std::map<std::string, bool> repeatedSSID;

        // Dictionaries with SSID conventions used to identify
        // which is the best category for an access point based
        // on the SSID that contains
        std::map<std::string, unsigned char> ssidSubstringList = readSSIDFile(SUBSTRING_FILE);
        std::map<std::string, unsigned char> ssidExactMatchList =  readSSIDFile(EXACT_MATCH_FILE);

        // Dictionary that associate the OUI of
        // known vendors and the category that
        // will be associated to the BSSID that
        // has a match in the list
        std::map<std::string, unsigned char> priorityList = readPriorityList();

        // Create a list for the categories from 1 to 7
        std::vector<std::vector<accessPoint *>> categoriesList = {std::vector<accessPoint *>(),
                                                                  std::vector<accessPoint *>(),
                                                                  std::vector<accessPoint *>(),
                                                                  std::vector<accessPoint *>(),
                                                                  std::vector<accessPoint *>(),
                                                                  std::vector<accessPoint *>(),
                                                                  std::vector<accessPoint *>()};

        // Iterate over the different channels scanned
        for (i = 0; i < apsLists.size(); ++i)
        {
            bool isVirtualListInit = false;

            // Iterate over the APs encountered in the current channel
            for (j = 0; j < apsLists[i].size(); ++j)
            {

                // Check if the SSID has a exact match from the LUT
                if (ssidExactMatchList.find(apsLists[i][j]->ssid) != ssidExactMatchList.end())
                {
                    // Allocate the corresponding category
                    apsLists[i][j]->category = ssidExactMatchList[apsLists[i][j]->ssid];

                    // Put the AP in the corresponding list
                    categoriesList[ssidExactMatchList[apsLists[i][j]->ssid] - 1].push_back(apsLists[i][j]);
                    
                    continue; 
                }

                std::string lower_ssid = apsLists[i][j]->ssid;
                std::size_t substringMatch = std::string::npos;
                
                // Lower case the SSID
                transform(lower_ssid.begin(), 
                          lower_ssid.end(), 
                          lower_ssid.begin(), 
                          ::tolower);

                // Check if the SSID contains a specific substring
                for (auto const& it : ssidSubstringList)
                {
                    // Look for a match of the substring in the AP SSID.
                    substringMatch = lower_ssid.find(it.first);

                    // In case there is a match the AP is category 7
                    // and should not be included in the final list
                    if (substringMatch != std::string::npos)
                    {
                        // Allocate the appropiate category
                        apsLists[i][j]->category = it.second;

                        // Put the AP in the corresponding list
                        categoriesList[it.second - 1].push_back(apsLists[i][j]);

                        break;
                    }
                }

                // Category is allocated, so continue with the next AP info
                if (substringMatch != std::string::npos)
                {
                    continue;
                }

                // Apply mask to the most significant 
                // byte of the BSSID
                unsigned char msb = apsLists[i][j]->bssid[0] & 0x3;

                // Check if the BSSID is private
                if (msb == 0x2)
                {
                    // Lazy initialization of the virtual BSSID match list
                    // Implemented this way to save processing and memory
                    // in case this is not needed at all for the APs in
                    // the current channel
                    if (!isVirtualListInit)
                    {
                        virtualList = computeVirtualBSSID(apsLists[i]);
                        isVirtualListInit = true;
                    }

                    // Check for match in virtual BSSID
                    if (virtualList[j])
                    {
                        // The AP category is 4
                        apsLists[i][j]->category = 4;

                        // Put the AP in the corresponding list
                        categoriesList[3].push_back(apsLists[i][j]);

                        continue;
                    }

                    // Check if the AP SSID is repeated through all channels

                    // Lazy initialization of the repeated SSID dictionary
                    // Implemented this way to save processing and memory
                    // in case this is not needed at all for the APs in
                    // the current scan list
                    if (!isRepeatedSSIDListInit)
                    {
                        // Fill the repeated SSID dictionary
                        repeatedSSID = computeRepeatedSSID(apsLists);
                        isRepeatedSSIDListInit = true;
                    }

                    // Check in the dictionary if the ssid is repeated or not
                    if (repeatedSSID[apsLists[i][j]->ssid])
                    {
                        // The AP is category 2 
                        apsLists[i][j]->category = 2;

                        // Put the AP in the corresponding list
                        categoriesList[1].push_back(apsLists[i][j]);
                        
                    } 
                    else
                    {
                        // The AP is category 7
                        apsLists[i][j]->category = 7;

                        // Put the AP in the corresponding list
                        categoriesList[6].push_back(apsLists[i][j]);

                    } 

                }
                else // BBSID is public
                {
                    // Create the key to search in
                    // the priority list dictionary
                    std::string pKey((char *) apsLists[i][j]->bssid, 3);
                    unsigned char apCategory;

                    // Check if the BBSID match with the priority list
                    if (priorityList.find(pKey) != priorityList.end())
                    {
                        // Allocate the corresponding category
                        apCategory = priorityList[pKey];
                    }
                    else
                    {
                        // The OUI is not in the list,
                        // therefore the AP category is 4
                        apCategory = 4;
                    }

                    // Check if the AP SSID is repeated through all channels

                    // Lazy initialization of the repeated SSID dictionary
                    // Implemented this way to save processing and memory
                    // in case this is not needed at all for the APs in
                    // the current scan list
                    if (!isRepeatedSSIDListInit)
                    {
                        // Fill the repeated SSID dictionary
                        repeatedSSID = computeRepeatedSSID(apsLists);
                        isRepeatedSSIDListInit = true;
                    }

                    // Check in the dictionary if the ssid is repeated or not
                    if (repeatedSSID[apsLists[i][j]->ssid] && apCategory >= 3)
                    {
                        // The AP category is upgraded to 2 
                        apCategory = 2;
                    }

                    // Allocate the corresponding category
                    apsLists[i][j]->category = apCategory;

                    // Put the AP in the corresponding list
                    categoriesList[apCategory - 1].push_back(apsLists[i][j]);
                }
            }
        }


        return categoriesList;
    }

    unsigned char WifiScanning::readScanId()
    {
        unsigned char header[4] = {0,0,0,SCAN_ID_MAX};

        drivers::SPIFFS spiffs;
        std::fstream file = spiffs.getFile(SPIFFS_PARTITION,RESULT_FILE, std::fstream::in | std::fstream::binary);


        if (file)
        {
            file.read((char *) header, 4);
            file.close();
        }
        else
        {

           ESP_LOGI(ESPWIFI_TAG, "Error: result file does not exists");
        }

        spiffs.unmount(SPIFFS_PARTITION);

        return header[3];
    }

    int WifiScanning::normalizeRSSI(int rssi)
    {
        int rssiAux;

        if(rssi >= -20)
        {
            return 0;
        }
        if(rssi <= -113)
        {
            return 31;
        }

        rssiAux = (-rssi) - 20;
        if(remainder(rssiAux, 3) >= 0.5)
        {
            rssiAux = (int) ceil(rssiAux/3);
        }
        else
        {
            rssiAux = (int) floor(rssiAux/3);
        }

        return rssiAux;
    }

    std::vector<std::string> WifiScanning::readActualTop()
    {
        char topElement[6];
        unsigned char topSize = 0;
        std::vector<std::string> topList;

        drivers::SPIFFS spiffs;
        std::fstream file = spiffs.getFile(SPIFFS_PARTITION,RESULT_FILE, std::fstream::in | std::fstream::binary);


        if (file)
        {
            // Get the top size value
            file.read((char *) &topSize,1);

            for (unsigned char i = 0; i < topSize; ++i)
            {
                file.read(topElement, 6);
                topList.push_back(std::string(topElement,6));
            }

            file.close();
        }

        spiffs.unmount(SPIFFS_PARTITION);

        return topList;
    }

    std::map<std::string, unsigned char> WifiScanning::readSSIDFile(std::string filename)
    {
        std::string line;
        std::map<std::string, unsigned char> ssidList;

        drivers::SPIFFS spiffs;
        std::fstream file = spiffs.getFile(SPIFFS_PARTITION,filename, std::fstream::in);


        if (file)
        {
            while (!file.eof())
            {
                std::getline(file, line);

                if (line.size() > 3)
                {
                    ssidList[line.substr(1)] = line[0];
                }
                else if (line.size() == 1)
                {
                    ssidList[""] = line[0];
                }
            }
            file.close();
        }

        spiffs.unmount(SPIFFS_PARTITION);
        
        return ssidList;
    }

    std::string WifiScanning::wifiUpperPayload(accessPoint *firstAP,
                                               accessPoint *secondAP,
                                               unsigned char scanId)
    {
        std::string payload;
        std::string secondAPMAC = "";
        unsigned char secondNormalRSSI = 0;
    
        // WiFi header
        payload.push_back(0x53);
        
        // In case the AP is specified
        if (secondAP != nullptr)
        {
            secondNormalRSSI = (unsigned char) normalizeRSSI(-secondAP->rssi);
            secondAPMAC = std::string((char *) secondAP->bssid, 3); 
        }
        else // Only one mac address will be included in the payload
        {
            // Fill the bits with zero
            for (int i = 0; i < 3; ++i)
            {
                secondAPMAC.push_back(0);
            }
        }
        
        // First MAC address
        payload += std::string((char *) firstAP->bssid,6);
        
        // First half of second MAC address
        payload += secondAPMAC;

        unsigned char firstNormalRSSI = (unsigned char) normalizeRSSI(-firstAP->rssi);

        unsigned char temp = firstNormalRSSI << 3;
        temp = temp | (secondNormalRSSI >> 2);

        // First MAC RSSI and 3 bits of second MAC RSSI
        payload.push_back(temp);

        temp = secondNormalRSSI << 6;
        temp = temp | (scanId << 3);
        temp = temp | firstAP->category;
        
        // 2 bits of second MAC RSSI, scan counter and first MAC category                                                    
        payload.push_back(temp);

        return payload;
    }

    std::string WifiScanning::wifiLowerPayload(accessPoint *secondAP,
                                               accessPoint *thirdAP,
                                               unsigned char scanId)
    {
        std::string payload;
        std::string secondAPMAC = "";
        unsigned char secondAPCategory = 0;
        unsigned char multiAPBit = 0;
    
        // WiFi header
        payload.push_back(0x54);
        
        // In case the second AP is specified
        if (secondAP != nullptr)
        {
            secondAPCategory = secondAP->category;
            secondAPMAC = std::string((char *) (secondAP->bssid + 3), 3);
            
            // Set the multi AP bit to 1
            multiAPBit = 2;                                        
        }
        else // Only one mac address will be included in the payload
        {
            // Fill the bits with zero
            for (int i = 0; i < 3; ++i)
            {
                secondAPMAC.push_back(0);
            }
        }

        // Third MAC address
        payload += std::string((char *) thirdAP->bssid,6);
        
        // Second half of second MAC address
        payload += secondAPMAC;

        unsigned char thirdNormalRSSI = (unsigned char) normalizeRSSI(-thirdAP->rssi);
        unsigned char temp = (thirdNormalRSSI << 3) | thirdAP->category;
        
        // Third MAC RSSI and category
        payload.push_back(temp);         

        temp = scanId << 5;
        temp = temp | (secondAPCategory << 2);
        temp = temp | multiAPBit;

        // scan counter, second MAC category and multi AP bit
        payload.push_back(temp);                                                  

        return payload;
    }

    std::map<std::string, unsigned char> WifiScanning::readPriorityList()
    {
        char buffer[4];
        std::map<std::string, unsigned char> priorityList;

        drivers::SPIFFS spiffs;
        std::fstream file = spiffs.getFile(SPIFFS_PARTITION,PRIORITY_FILE, std::fstream::in | std::fstream::binary);


        if (file)
        {
            while(file.read(buffer,4))
            {
                priorityList[std::string(buffer,3)] =  (unsigned char) buffer[3];
            }

            file.close();
        }
        else
        {

           ESP_LOGI(ESPWIFI_TAG, "Error: priority list does not exists");
        }

        spiffs.unmount(SPIFFS_PARTITION);

        return priorityList;
    }

    void WifiScanning::writeResultFile(std::vector<std::string> topList,
                                             std::vector<std::string> payloadsList,
                                             unsigned char lineCounter,
                                             unsigned char scanId)
    {
        unsigned char i;

        drivers::SPIFFS spiffs;
        std::fstream file = spiffs.getFile(SPIFFS_PARTITION,RESULT_FILE, std::fstream::out | std::fstream::binary);


        if (file)
        {    
            // Write the payloads list size and line counter
            unsigned char info[4];
            info[0] = (unsigned char) topList.size();
            info[1] = (unsigned char) payloadsList.size();
            info[2] = lineCounter;
            info[3] = scanId;

            file.write((char *) info, 4);

            // Write the top list
            for (i = 0; i < topList.size(); ++i)
            {
                file.write(topList[i].c_str(), topList[i].size());
            }
            
            // Write the encoded payloads
            for(i = 0; i < payloadsList.size(); ++i)
            {
                file.write(payloadsList[i].c_str(), payloadsList[i].size());
            }
            file.close();
        }
        spiffs.unmount(SPIFFS_PARTITION);
    }

    std::map<unsigned char, bool> WifiScanning::computeVirtualBSSID(std::vector<accessPoint *> apList)
    {
        std::map<unsigned char, bool> virtualList;
        bool flagList[apList.size()] = {false};

        bool firstAppend;
        bool virtualCond;

        for (unsigned char k = 0; k < apList.size(); ++k)
        {
            if (!flagList[k])
            {
                firstAppend = false;

                for (unsigned char h = k + 1; h < apList.size(); ++h)
                {
                    virtualCond = (apList[k]->bssid[1] == apList[h]->bssid[1]) &&
                                  (apList[k]->bssid[2] == apList[h]->bssid[2]) &&
                                  (apList[k]->bssid[3] == apList[h]->bssid[3]) &&
                                  (apList[k]->bssid[4] == apList[h]->bssid[4]) &&
                                  (apList[k]->bssid[5] == apList[h]->bssid[5]);


                    if (virtualCond)
                    {
                        if (!firstAppend)
                        {
                            virtualList[k] = true;
                            firstAppend = true;
                        }

                        virtualList[h] = true;
                        flagList[h] = true;
                    }
                }

                if (!firstAppend)
                {
                    virtualList[k] = false;
                }
            }
        }

        return virtualList;
    }

    std::map<std::string, bool> WifiScanning::computeRepeatedSSID(std::vector<std::vector<accessPoint *>> apLists)
    {
        unsigned char i, j;

        std::vector<std::string> allSSIDS;
        std::map<std::string, bool> repeatedSSID;

        for (i = 0; i < apLists.size(); ++i)
        {

            for (j = 0; j < apLists[i].size(); ++j)
            {
                allSSIDS.push_back(apLists[i][j]->ssid);
            }
        }

        bool flagList[allSSIDS.size()] = {false};

        for (i = 0; i < allSSIDS.size(); ++i)
        {
            if (!flagList[i])
            {
                repeatedSSID[allSSIDS[i]] = false;

                for (j = i + 1; j < allSSIDS.size(); ++j)
                {
                    if (allSSIDS[i] == allSSIDS[j])
                    {
                        flagList[j] = true;
                        repeatedSSID[allSSIDS[i]] = true;
                    }
                }
            }
        }

        return repeatedSSID;
    }

}




